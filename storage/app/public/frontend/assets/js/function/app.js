/* ================================================
---------------------- Main.js ----------------- */
(function ($) {
    'use strict';
    var LckApp = {
        initialised: false,
        processing: false,
        mobile: false,
        init: function () {

            if (!this.initialised) {
                this.initialised = true;
            } else {
                return;
            }

            // Call LckApp Functions
            this.checkMobile();
            this.stickyHeader();
            this.headerSearchToggle();
            this.mMenuIcons();
            this.mMenuToggle();
            this.mobileMenu();
            this.scrollToTop();
            this.quantityInputs();
            this.countTo();
            this.tooltip();
            this.popover();
            this.changePassToggle();
            this.changeBillToggle();
            this.catAccordion();
            this.ajaxLoadProduct();
            this.toggleFilter();
            this.toggleSidebar();
            this.productTabSroll();
            this.scrollToElement();
            this.loginPopup();
            this.loginSubmit();
            this.registerPopup();
            this.registerSubmit();
            this.windowClick();
            this.myChangePassToggle();
            this.select2();
            this.addToBasket();
            this.cartCheckAll();
            this.cartChangeQuantity();
            this.changeAddress();
            this.addAddress();

            /* Menu via superfish plugin */
            if ($.fn.superfish) {
                this.menuInit();
            }

            /* Call function if Owl Carousel plugin is included */
            if ($.fn.owlCarousel) {
                this.owlCarousels();
            }

            /* Call function if noUiSlider plugin is included - for category pages */
            if (typeof noUiSlider === 'object') {
                this.filterSlider();
            }

            /* Call if not mobile and plugin is included */
            if ($.fn.themeSticky) {
                this.stickySidebar();
            }

            /* Call function if Light Gallery plugin is included */
            if ($.fn.magnificPopup) {
                this.lightBox();
            }

        },
        checkMobile: function () {
            /* Mobile Detect*/
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                this.mobile = true;
            } else {
                this.mobile = false;
            }
        },
        menuInit: function () {
            // Main Menu init with superfish plugin
            $('.menu').superfish({
                popUpSelector: 'ul, .megamenu',
                hoverClass: 'show',
                delay: 0,
                speed: 80,
                speedOut: 80,
                autoArrows: true
            });
        },
        stickyHeader: function () {
            // Sticky header - calls if sticky-header class is added to the header
            if ($('.sticky-header').length) {
                var sticky = new Waypoint.Sticky({
                    element: $('.sticky-header')[0],
                    stuckClass: 'fixed',
                    offset: -10
                });

                if (!$('.header-bottom').find('.logo, .cart-dropdown').length) {
                    var targetArea = $('.header-bottom').find('.container');

                    // Clone and put in the header bottom for sticky header
                    $('.header').find('.logo, .cart-dropdown')
                        .clone(true)
                        .prependTo(targetArea);
                }
            }

            //Set sticky headers in main part
            $('main').find('.sticky-header').each(function () {
                var sticky = new Waypoint.Sticky({
                    element: $(this),
                    stuckClass: 'fixed-nav',
                });
            });
        },
        headerSearchToggle: function () {
            // Search Dropdown Toggle
            $('.search-toggle').on('click', function (e) {
                $('.header-search-wrapper').toggleClass('show');
                e.preventDefault();
            });

            $('body').on('click', function (e) {
                if ($('.header-search-wrapper').hasClass('show')) {
                    $('.header-search-wrapper').removeClass('show');
                    $('body').removeClass('is-search-active');
                }
            });

            $('.header-search').on('click', function (e) {
                e.stopPropagation();
            });
        },
        mMenuToggle: function () {
            // Mobile Menu Show/Hide
            $('.mobile-menu-toggler').on('click', function (e) {
                $('body').toggleClass('mmenu-active');
                $(this).toggleClass('active');
                e.preventDefault();
            });

            $('.mobile-menu-overlay, .mobile-menu-close').on('click', function (e) {
                $('body').removeClass('mmenu-active');
                $('.menu-toggler').removeClass('active');
                e.preventDefault();
            });
        },
        mMenuIcons: function () {
            // Add Mobile menu icon arrows or plus/minus to items with children
            $('.mobile-menu').find('li').each(function () {
                var $this = $(this);

                if ($this.find('ul').length) {
                    $('<span/>', {
                        'class': 'mmenu-btn'
                    }).appendTo($this.children('a'));
                }
            });
        },
        mobileMenu: function () {
            // Mobile Menu Toggle
            $('.mmenu-btn').on('click', function (e) {
                var $parent = $(this).closest('li'),
                    $targetUl = $parent.find('ul').eq(0);

                if (!$parent.hasClass('open')) {
                    $targetUl.slideDown(300, function () {
                        $parent.addClass('open');
                    });
                } else {
                    $targetUl.slideUp(300, function () {
                        $parent.removeClass('open');
                    });
                }

                e.stopPropagation();
                e.preventDefault();
            });
        },
        owlCarousels: function () {
            var sliderDefaultOptions = {
                loop: true,
                margin: 0,
                responsiveClass: true,
                nav: false,
                navText: ['<i class="icon-left-open-big">', '<i class="icon-right-open-big">'],
                dots: true,
                autoplay: true,
                autoplayTimeout: 15000,
                items: 1,
            };

            /* Hom Slider */
            var homeSlider = $('.home-slider');

            homeSlider.owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                lazyLoad: true,
                autoplayTimeout: 20000,
                animateOut: 'fadeOut'
            }));
            homeSlider.on('loaded.owl.lazy', function (event) {
                $(event.element).closest('.home-slider').addClass('loaded')
            });

            /* Featured Products */
            $('.featured-products').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                loop: false,
                margin: 30,
                autoplay: false,
                responsive: {
                    0: {
                        items: 2
                    },
                    480: {
                        items: 2
                    },
                    768: {
                        items: 3
                    }
                }
            }));

            /* Featured Products */
            $('.home-featured-products').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                loop: false,
                margin: 30,
                autoplay: false,
                responsive: {
                    0: {
                        items: 3
                    },
                    700: {
                        items: 4,
                        margin: 15
                    },
                    1200: {
                        items: 4,
                        margin: 30
                    }
                }
            }));

            /* Featured Products */
            $('.detail-featured-products').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                loop: false,
                margin: 30,
                autoplay: false,
                responsive: {
                    0: {
                        items: 3
                    },
                    700: {
                        items: 4,
                        margin: 15
                    },
                    1200: {
                        items: 6,
                        margin: 15
                    }
                }
            }));

            /* Home Widget - Banners Slider */
            $('.widget-banners-slider').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                lazyLoad: true,
                navText: ['<i class="icon-angle-left">', '<i class="icon-angle-right">'],
                autoHeight: true
            }));

            /* Home Widget - Testimonials */
            $('.widget-testimonials-slider').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                lazyLoad: true,
                navText: ['<i class="icon-angle-left">', '<i class="icon-angle-right">'],
                autoHeight: true
            }));

            /* Home Widget - Blog Post */
            $('.widget-posts-slider').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                lazyLoad: true,
                navText: ['<i class="icon-angle-left">', '<i class="icon-angle-right">'],
                autoHeight: true
            }));

            /* Widget Featurd Products*/
            $('.widget-featured-products').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                lazyLoad: true,
                nav: true,
                navText: ['<i class="icon-angle-left">', '<i class="icon-angle-right">'],
                dots: false,
                autoHeight: true
            }));

            /* About Testimonials carousel */
            $('.testimonials-carousel').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                lazyLoad: true,
                navText: ['<i class="icon-angle-left">', '<i class="icon-angle-right">'],
                autoHeight: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    992: {
                        items: 2
                    }
                }
            }));

            // Entry Slider - Blog page
            $('.entry-slider').each(function () {
                $(this).owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                    margin: 2,
                    lazyLoad: true,
                    navText: ['<i class="icon-left-open-big">', '<i class="icon-right-open-big">'],
                }));
            });

            // Related posts
            $('.related-posts-carousel').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                loop: false,
                margin: 30,
                navText: ['<i class="icon-left-open-big">', '<i class="icon-right-open-big">'],
                autoplay: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    480: {
                        items: 2
                    },
                    1200: {
                        items: 3
                    }
                }
            }));

            //Category boxed slider
            $('.boxed-slider').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                lazyLoad: true,
                autoplayTimeout: 20000,
                animateOut: 'fadeOut'
            }));
            $('.boxed-slider').on('loaded.owl.lazy', function (event) {
                $(event.element).closest('.boxed-slider').addClass('loaded')
            });

            /* Product single carousel - extenden product */
            $('.product-single-default .product-single-carousel').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                nav: true,
                navText: ['<i class="icon-angle-left">', '<i class="icon-angle-right">'],
                dotsContainer: '#carousel-custom-dots',
                autoplay: false,
                onInitialized: function () {
                    var $source = this.$element;

                    if ($.fn.elevateZoom) {
                        $source.find('img').each(function () {
                            var $this = $(this),
                                zoomConfig = {
                                    responsive: true,
                                    zoomWindowFadeIn: 350,
                                    zoomWindowFadeOut: 200,
                                    borderSize: 0,
                                    zoomContainer: $this.parent(),
                                    zoomType: 'inner',
                                    cursor: 'grab'
                                };
                            $this.elevateZoom(zoomConfig);
                        });
                    }
                },
            }));

            $('.product-single-extended .product-single-carousel').owlCarousel($.extend(true, {}, sliderDefaultOptions, {
                dots: false,
                autoplay: false,
                responsive: {
                    0: {
                        items: 1
                    },
                    480: {
                        items: 2
                    },
                    1200: {
                        items: 3
                    }
                }
            }));

            $('#carousel-custom-dots .owl-dot').click(function () {
                $('.product-single-carousel').trigger('to.owl.carousel', [$(this).index(), 300]);
            });
        },
        filterSlider: function () {
            // Slider For category pages / filter price
            var priceSlider = document.getElementById('price-slider'),
                currencyVar = 'VNĐ';

            // Check if #price-slider elem is exists if not return
            // to prevent error logs
            if (priceSlider == null) return;

            noUiSlider.create(priceSlider, {
                start: [200, 700],
                connect: true,
                step: 100,
                margin: 100,
                range: {
                    'min': 0,
                    'max': 1000
                }
            });

            // Update Price Range
            priceSlider.noUiSlider.on('update', function (values, handle) {
                var values = values.map(function (value) {
                    return currencyVar + value;
                })
                $('#filter-price-range').text(values.join(' - '));
            });
        },
        stickySidebar: function () {
            $(".sidebar-wrapper, .sticky-slider").themeSticky({
                autoInit: true,
                minWidth: 991,
                containerSelector: '.row, .container',
                autoFit: true,
                paddingOffsetBottom: 10,
                paddingOffsetTop: 60
            });
        },
        countTo: function () {
            // CountTo plugin used count animations for homepages
            if ($.fn.countTo) {
                if ($.fn.waypoint) {
                    $('.count').waypoint(function () {
                        $(this.element).countTo();
                    }, {
                        offset: '90%',
                        triggerOnce: true
                    });
                } else {
                    $('.count').countTo();
                }
            } else {
                // fallback if count plugin doesn't included
                // Get the data-to value and add it to element
                $('.count').each(function () {
                    var $this = $(this),
                        countValue = $this.data('to');
                    $this.text(countValue);
                });
            }
        },
        tooltip: function () {
            // Bootstrap Tooltip
            if ($.fn.tooltip) {
                $('[data-toggle="tooltip"]').tooltip({
                    trigger: 'hover focus' // click can be added too
                });
            }
        },
        popover: function () {
            // Bootstrap Popover
            if ($.fn.popover) {
                $('[data-toggle="popover"]').popover({
                    trigger: 'focus'
                });
            }
        },
        changePassToggle: function () {
            // Toggle new/change password section via checkbox
            $('#change-pass-checkbox').on('change', function () {
                $('#account-chage-pass').toggleClass('show');
            });
        },
        changeBillToggle: function () {
            // Checkbox review - billing address checkbox
            $('#change-bill-address').on('change', function () {
                $('#checkout-shipping-address').toggleClass('show');
                $('#new-checkout-address').toggleClass('show');
            });
        },
        catAccordion: function () {
            // Toggle "open" Class for parent elem - Home cat widget
            $('.catAccordion').on('shown.bs.collapse', function (item) {
                var parent = $(item.target).closest('li');

                if (!parent.hasClass('open')) {
                    parent.addClass('open');
                }
            }).on('hidden.bs.collapse', function (item) {
                var parent = $(item.target).closest('li');

                if (parent.hasClass('open')) {
                    parent.removeClass('open');
                }
            });
        },
        scrollBtnAppear: function () {
            if ($(window).scrollTop() >= 400) {
                $('#scroll-top').addClass('fixed');
            } else {
                $('#scroll-top').removeClass('fixed');
            }
        },
        scrollToTop: function () {
            $('#scroll-top').on('click', function (e) {
                $('html, body').animate({
                    'scrollTop': 0
                }, 1200);
                e.preventDefault();
            });
        },
        lightBox: function () {
            // Newsletter popup
            /*if ( document.getElementById('newsletter-popup-form') ) {
                $.magnificPopup.open({
                    items: {
                        src: '#newsletter-popup-form'
                    },
                    type: 'inline'
                }, 0);
            }*/

            // Gallery Lightbox
            var links = [];
            var $productSliderImages = $('.product-single-carousel .owl-item:not(.cloned) img').length === 0 ? $('.product-single-gallery img') : $('.product-single-carousel .owl-item:not(.cloned) img');
            $productSliderImages.each(function () {
                links.push({'src': $(this).attr('data-zoom-image')});
            });

            $(".prod-full-screen").click(function (e) {
                var currentIndex;
                if (e.currentTarget.closest(".product-slider-container")) {
                    currentIndex = ($('.product-single-carousel').data('owl.carousel').current() + $productSliderImages.length - Math.ceil($productSliderImages.length / 2)) % $productSliderImages.length;
                }
                else {
                    currentIndex = $(e.currentTarget).closest(".product-item").index();
                }

                $.magnificPopup.open({
                    items: links,
                    navigateByImgClick: true,
                    type: 'image',
                    gallery: {
                        enabled: true
                    },
                }, currentIndex);
            });

            //QuickView Popup
            $('a.btn-quickview').on('click', function (e) {
                e.preventDefault();
                LckApp.ajaxLoading();
                var ajaxUrl = $(this).attr('href');
                setTimeout(function () {
                    $.magnificPopup.open({
                        type: 'ajax',
                        mainClass: "mfp-ajax-product",
                        tLoading: '',
                        preloader: false,
                        removalDelay: 350,
                        items: {
                            src: ajaxUrl
                        },
                        callbacks: {
                            ajaxContentAdded: function () {
                                LckApp.owlCarousels();
                                LckApp.quantityInputs();
                                if (typeof addthis !== 'undefined') {
                                    addthis.layers.refresh();
                                }
                                else {
                                    $.getScript("http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b927288a03dbde6");
                                }
                            },
                            beforeClose: function () {
                                $('.ajaxOverlay').remove();
                            }
                        },
                        ajax: {
                            tError: '',
                        }
                    });
                }, 1500);
            });
        },
        productTabSroll: function () {
            // Scroll to product details tab and show review tab - product pages
            $('.rating-link').on('click', function (e) {
                if ($('.product-single-tabs').length) {
                    $('#product-tab-reviews').tab('show');
                } else if ($('.product-single-collapse').length) {
                    $('#product-reviews-content').collapse('show');
                } else {
                    return;
                }

                if ($('#product-reviews-content').length) {
                    setTimeout(function () {
                        var scrollTabPos = $('#product-reviews-content').offset().top - 60;

                        $('html, body').stop().animate({
                            'scrollTop': scrollTabPos
                        }, 800);
                    }, 250);
                }
                e.preventDefault();
            });
        },
        quantityInputs: function () {
            // Quantity input - cart - product pages
            if ($.fn.TouchSpin) {
                // Vertical Quantity
                $('.vertical-quantity').TouchSpin({
                    verticalbuttons: true,
                    verticalup: '',
                    verticaldown: '',
                    verticalupclass: 'icon-up-dir',
                    verticaldownclass: 'icon-down-dir',
                    buttondown_class: 'btn btn-outline',
                    buttonup_class: 'btn btn-outline',
                    initval: 1,
                    min: 1
                });

                // Horizontal Quantity
                $('.horizontal-quantity').TouchSpin({
                    verticalbuttons: false,
                    buttonup_txt: '',
                    buttondown_txt: '',
                    buttondown_class: 'btn btn-outline btn-down-icon',
                    buttonup_class: 'btn btn-outline btn-up-icon',
                    initval: 1,
                    min: 1
                });
            }
        },
        ajaxLoading: function () {
            $('body').append("<div class='ajaxOverlay'><i class='porto-loading-icon'></i></div>");
        },
        ajaxLoadProduct: function () {
            var loadCount = 0;
            $loadButton.click(function (e) {
                e.preventDefault();
                $(this).text('Loading ...');
                $.ajax({
                    url: "ajax/category-ajax-products.html",
                    success: function (result) {
                        var $newItems = $(result);
                        setTimeout(function () {
                            $newItems.appendTo('.product-ajax-grid');
                            $loadButton.text('Load More');
                            loadCount++;
                            if (loadCount >= 2) {
                                $loadButton.hide();
                            }
                        }, 350);
                    },
                    failure: function () {
                        $loadButton.text("Sorry something went wrong.");
                    }
                });
            });
        },
        toggleFilter: function () {
            // toggle sidebar filter
            $('.filter-toggle a').click(function (e) {
                e.preventDefault();
                $('.filter-toggle').toggleClass('opened');
                $('main').toggleClass('sidebar-opened');
            });

            // hide sidebar filter and sidebar overlay
            $('.sidebar-overlay').click(function (e) {
                $('.filter-toggle').removeClass('opened');
                $('main').removeClass('sidebar-opened');
            });

            // show/hide sort menu
            $('.sort-menu-trigger').click(function (e) {
                e.preventDefault();
                $('.select-custom').removeClass('opened');
                $(e.target).closest('.select-custom').toggleClass('opened');
            });
        },
        toggleSidebar: function () {
            $('.sidebar-toggle').click(function () {
                $('main').toggleClass('sidebar-opened');
            });
        },
        scrollToElement: function () {
            $('.scrolling-box a[href^="#"]').on('click', function (event) {
                var target = $(this.getAttribute('href'));

                if (target.length) {
                    event.preventDefault();
                    $('html, body').stop().animate({
                        scrollTop: target.offset().top - 90
                    }, 700);
                }
            });
        },
        loginPopup: function () {
            $(document).on('click', '.login-link', function (e) {
                e.preventDefault();
                $('.ajaxOverlay').remove();
                LckApp.ajaxLoading();
                var ajaxUrl = BASE_URL + "/ajax/login-popup.html";
                $.magnificPopup.open({
                    type: 'ajax',
                    mainClass: "login-popup",
                    tLoading: '',
                    preloader: false,
                    removalDelay: 100,
                    items: {
                        src: ajaxUrl
                    },
                    callbacks: {
                        beforeClose: function () {
                            $('.ajaxOverlay').remove();
                        }
                    },
                    ajax: {
                        tError: '',
                    }
                });
            });
        },
        loginSubmit: function () {
            $(document).on('submit', '#form_login', function (e) {
                e.preventDefault();
                if (this.processing)
                    return;

                this.processing = true;

                var form = $(this);
                var url = form.attr('action');
                $(document).find('#form_login .error').addClass('hidden');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(),
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        this.processing = false;
                        $(document).find('#form_login .error').addClass('hidden');
                        if (data.status == false) {
                            $.each(data.message, function (index, value) {
                                $(document).find('#form_login #login_error_' + index).html(value).removeClass('hidden');
                            });
                        } else {
                            window.location.reload();
                        }
                    }
                });
            });
        },
        registerPopup: function () {
            $(document).on('click', '.register-link', function (e) {
                e.preventDefault();
                $('.ajaxOverlay').remove();
                LckApp.ajaxLoading();
                var ajaxUrl = BASE_URL + "/ajax/register-popup.html";
                $.magnificPopup.open({
                    type: 'ajax',
                    mainClass: "login-popup",
                    tLoading: '',
                    preloader: false,
                    removalDelay: 100,
                    items: {
                        src: ajaxUrl
                    },
                    callbacks: {
                        beforeClose: function () {
                            $('.ajaxOverlay').remove();
                        }
                    },
                    ajax: {
                        tError: '',
                    }
                });
            });
        },
        registerSubmit: function () {
            $(document).on('submit', '#form_register', function (e) {
                e.preventDefault();

                if (this.processing)
                    return;

                this.processing = true;
                var form = $(this);
                var url = form.attr('action');

                $(document).find('#form_register .error').addClass('hidden');
                $(document).find('#form_register #register_btn').text('Đang xử lý...');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(),
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        this.processing = false;
                        $(document).find('#form_register #register_btn').text('Đăng ký');
                        $(document).find('#form_register .error').addClass('hidden');
                        if (data.status == true) {
                            if (data.code == 200) {
                                if (data.message != '') {
                                    $(document).find('#form_register #login_message').html(data.message).removeClass('hidden');
                                    $(document).find('#form_register')[0].reset();
                                } else {
                                    window.location.reload();
                                }
                            } else {
                                $.each(data.message, function (index, value) {
                                    $(document).find('#form_register #login_error_' + index).html(value).removeClass('hidden');
                                });
                            }

                        } else {
                            alert('Có lỗi xảy ra, vui lòng thử lại!');
                            window.location.reload();
                        }
                    }
                });
            });
        },
        windowClick: function () {
            $(document).click(function (e) {
                // if click is happend outside of filter menu, hide it.
                if (!$(e.target).closest('.toolbox-item.select-custom').length) {
                    $('.select-custom').removeClass('opened');
                }
            });
        },
        myChangePassToggle: function () {
            // Toggle new/change password section via checkbox
            $('#my-change-pass-checkbox').on('change', function () {
                $('.change_pass').toggleClass('hidden');
            });
        },
        selectLocation: function (form_id) {
            $(form_id + '.select_province').change(function () {
                $(form_id + '.select_district').html('<option value="">Chọn</option>');
                $(form_id + '.select_ward').html('<option value="">Chọn</option>');
                var province_id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: LOCATION_DISTRICT_URL,
                    data: {province_id: province_id},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data.data, function (index, element) {
                            $(form_id + '.select_district').append('<option value=' + element.id + '>' + element.name + '</option>');
                        });
                    }
                });
            });

            $(form_id + '.select_district').change(function () {
                $(form_id + '.select_ward').html('<option value="">Chọn</option>');
                var district_id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: LOCATION_WARD_URL,
                    data: {district_id: district_id},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data.data, function (index, element) {
                            $(form_id + '.select_ward').append('<option value=' + element.id + '>' + element.name + '</option>');
                        });
                    }
                });
            });
        },
        select2: function () {
            $('.select2').select2();
        },
        numberFormat: function (nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        },
        addToBasket: function () {
            $('.add-cart').click(function () {
                var product_id = $(this).data('id');
                var quantity = $('.quantity').val();
                var _token = $('meta[name="csrf-token"]').attr('content');
                jQuery.ajax({
                    type: "POST",
                    url: BASE_URL + '/them-vao-gio-hang.html',
                    data: {
                        product_id: product_id,
                        quantity: quantity,
                        _token: _token
                    },
                    dataType: 'json',
                    success: function (result) {
                        if (result.status) {
                            jQuery('div.ajax-modal_content').html(result.data.html);
                            jQuery('#myModal').modal('toggle');
                            LckApp.getCartData();
                        } else {
                            if (result.code == -1) {
                                $('.login-link').trigger('click');
                            }
                        }
                    }, error: function (xhr, status, error) {
                        alert('Có lỗi xảy ra, vui lòng thử lại!');
                    },
                    cache: false
                });
            })
        },
        updateBasket: function (basket_item_id, quantity) {
            var _token = $('meta[name="csrf-token"]').attr('content');
            jQuery.ajax({
                type: "POST",
                url: BASE_URL + '/cap-nhat-gio-hang.html',
                data: {
                    basket_item_id: basket_item_id,
                    quantity: quantity,
                    _token: _token
                },
                dataType: 'json',
                success: function (result) {
                    if (result.status == false) {
                        $('#row_basket_item_id_' + basket_item_id).find('.cart_quantity').val(1);
                        LckApp.cartCalculatorPrice();
                        alert(result.message);
                    } else {
                        LckApp.getCartData();
                    }
                }, error: function (xhr, status, error) {
                    alert('Có lỗi xảy ra, vui lòng thử lại!');
                },
                cache: false
            });
        },
        getCartData: function () {
            jQuery.ajax({
                type: "GET",
                url: BASE_URL + '/ajax/gio-hang.html',
                dataType: 'json',
                success: function (result) {
                    jQuery('.cart-count').html(result.data.t);
                    jQuery('.card-content').html(result.data.html);

                }, error: function (xhr, status, error) {

                },
                cache: false
            });
        },
        cartCheckAll: function () {
            $(".check-all").change(function () {
                if (this.checked) {
                    $("input[type='checkbox']").prop('checked', true);
                } else {
                    $("input[type='checkbox']").prop('checked', false);
                }
                LckApp.cartCalculatorPrice();
            });

            $(".product_check").change(function () {

                var check_all = true;

                $('.product_check').each(function () {
                    if (!$(this).is(":checked")) {
                        check_all = false;
                    }
                });

                if (check_all) {
                    $(".check-all").prop('checked', true);
                } else {
                    $(".check-all").prop('checked', false);
                }

                var store_check_all = true;
                var store_id = $(this).data('store-id');

                $('#store_id_' + store_id + " .product_check").each(function () {
                    if (!$(this).is(":checked")) {
                        store_check_all = false;
                    }
                });

                if (store_check_all) {
                    $('#store_id_' + store_id + " .store_check_box").prop('checked', true);
                } else {
                    $('#store_id_' + store_id + " .store_check_box").prop('checked', false);
                }

                LckApp.cartCalculatorPrice();
            });

            $(".store_check_box").change(function () {

                var check_all = true;
                var store_id = $(this).data('store-id');

                var check_all_store = this.checked;

                if (check_all_store) {
                    $('#store_id_' + store_id + " input[type='checkbox']").prop('checked', true);
                } else {
                    $('#store_id_' + store_id + " input[type='checkbox']").prop('checked', false);
                }

                $('.product_check').each(function () {
                    if (!$(this).is(":checked")) {
                        check_all = false;
                    }
                });

                if (check_all) {
                    $(".check-all").prop('checked', true);
                } else {
                    $(".check-all").prop('checked', false);
                }

                LckApp.cartCalculatorPrice();
            });
        },
        cartCalculatorPrice: function () {

            var total_product = 0;
            var total_price = 0;

            $('.cart_total_text').html('Tổng tiền hàng ( ' + total_product + ' sản phẩm ):\n' +
                '                                    <span class="tong-cong-tat-ca">' + total_price + '\n' +
                '                                        <sup class="dong">đ</sup></span>');

            $('.product_check').each(function () {
                if ($(this).is(":checked")) {
                    total_product++;
                    var basket_item_id = $(this).attr('value');
                    var price = $(this).data('price');
                    var quantity = $('#row_basket_item_id_' + basket_item_id + ' .cart_quantity').val();

                    total_price += price * quantity;

                    var total_price_text = LckApp.numberFormat(total_price);

                    $('.cart_total_text').html('Tổng tiền hàng ( ' + total_product + ' sản phẩm ):\n' +
                        '                                    <span class="tong-cong-tat-ca">' + total_price_text + '\n' +
                        '                                        <sup class="dong">đ</sup></span>');

                }
            });
        },
        cartChangeQuantity: function () {
            $('.cart_quantity').change(function () {
                var quantity = $(this).val();
                var basket_item_id = $(this).data('basket-item-id');
                if (quantity < 0) {
                    alert('Số lượng không hợp lệ!');
                    quantity = 1;
                } else if (quantity < 1) {
                    var ok = confirm('Bạn muốn xóa sản phẩm khỏi giỏ hàng?');
                    if (ok) {
                        $('#row_basket_item_id_' + basket_item_id).remove();
                    } else {
                        quantity = 1;
                    }
                }
                $(this).val(quantity);
                LckApp.updateBasket(basket_item_id, quantity);
                LckApp.cartCalculatorPrice();
            });

            $('.cart_minus').click(function () {
                var basket_item_id = $(this).data('basket-item-id');
                var quantity_input = $(this).parent().find('.cart_quantity');
                var quantity = quantity_input.val();
                quantity--;
                if (quantity < 1) {
                    var ok = confirm('Bạn muốn xóa sản phẩm khỏi giỏ hàng?');
                    if (ok) {
                        $('#row_basket_item_id_' + basket_item_id).remove();
                    } else {
                        quantity = 1;
                    }
                }
                LckApp.updateBasket(basket_item_id, quantity);
                quantity_input.val(quantity);
                LckApp.cartCalculatorPrice();
            });

            $('.cart_plus').click(function () {
                var basket_item_id = $(this).data('basket-item-id');
                var quantity_input = $(this).parent().find('.cart_quantity');
                var quantity = quantity_input.val();
                quantity++;

                LckApp.updateBasket(basket_item_id, quantity);
                quantity_input.val(quantity);
                LckApp.cartCalculatorPrice();
            });
        },
        changeAddress: function () {
            $('.btn_change_address').click(function (e) {
                e.preventDefault();
                $('.selected_address').hide();
                $('.select_address').slideDown();
            });
            $('.address_btn_back').click(function (e) {
                e.preventDefault();
                $('.select_address').slideUp(function () {
                    $('.selected_address').show();
                });
            });

            $('.address_btn_accept').click(function (e) {
                e.preventDefault();
                $('#select_address_form').submit();
            });
        },
        addAddress: function () {
            $('.address_btn_add').click(function () {
                jQuery.ajax({
                    type: "GET",
                    url: BASE_URL + '/ajax/address.html',
                    dataType: 'json',
                    success: function (result) {
                        jQuery('div.ajax-modal_content').html(result.data.html);
                        jQuery('#myModal').modal('toggle');
                        LckApp.selectLocation('#form_add_address ');
                    }, error: function (xhr, status, error) {
                        alert('Có lỗi xảy ra, vui lòng thử lại!');
                    },
                    cache: false
                });
            });

            $(document).on('click', '#add-address-btn', function () {

                $(document).find('#form_add_address span.help-block').text('').addClass('hidden');

                jQuery.ajax({
                    type: "POST",
                    url: BASE_URL + '/ajax/address.html',
                    data: jQuery('#form_add_address').serialize(),
                    dataType: 'json',
                    success: function (result) {
                        if (result.status) {
                            location.reload()
                        } else {
                            $.each(result.message, function (i, v) {
                                $('#form_add_address #address_error_' + i).text(v).removeClass('hidden');
                            });
                        }

                    }, error: function (xhr, status, error) {
                        alert('Có lỗi xảy ra, vui lòng thử lại!');
                    },
                    cache: false
                });
            });
        }
    };

    $('body').prepend('<div class="loading-overlay"><div class="bounce-loader"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');

    //Variables
    var $loadButton = $('.loadmore .btn');

    // Ready Event
    jQuery(document).ready(function () {
        // Init our app
        LckApp.init();
        LckApp.selectLocation('#form_address ');
        if (IS_LOGGED) {
            LckApp.getCartData();
        }
    });

    // Load Event
    $(window).on('load', function () {
        $('body').addClass("loaded");
        LckApp.scrollBtnAppear();
    });

    // Scroll Event
    $(window).on('scroll', function () {
        LckApp.scrollBtnAppear();
    });
})(jQuery);