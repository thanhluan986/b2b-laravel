<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Address
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $name
 * @property string|null $phone
 * @property int|null $province_id
 * @property int|null $district_id
 * @property int|null $ward_id
 * @property string|null $street_name
 * @property string|null $full_address
 * @property int|null $is_default_recipient
 * @property int|null $is_warehouse
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFullAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereIsDefaultRecipient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereIsWarehouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereStreetName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereWardId($value)
 */
	class Address extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\BaseModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel query()
 */
	class BaseModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Basket
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property int|null $product_variation_id
 * @property int|null $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ProductVariation $product_variations
 * @property-read \App\Models\Product $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket whereProductVariationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Basket whereUserId($value)
 */
	class Basket extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property string|null $description
 * @property int|null $parent_id
 * @property int|null $status
 * @property int|null $priority
 * @property int|null $thumbnail_file_id
 * @property int|null $icon_file_id
 * @property int|null $type
 * @property string|null $seo_title
 * @property string|null $seo_descriptions
 * @property string|null $seo_keywords
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $user_id
 * @property-read \App\Models\Files|null $icon
 * @property-read \App\Models\Files|null $thumbnail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereIconFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSeoDescriptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSeoKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSeoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereThumbnailFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereUserId($value)
 */
	class Category extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CoreUsers
 *
 * @property int $id
 * @property string|null $fullname
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $username
 * @property string|null $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status
 * @property string|null $fcm_token
 * @property string|null $device_id
 * @property string|null $device_os
 * @property string|null $recommender
 * @property int|null $gender
 * @property string|null $birthday
 * @property string|null $cardid
 * @property string|null $avatar_file_path
 * @property string|null $cover_file_path
 * @property float|null $rating
 * @property float|null $balance
 * @property float|null $point
 * @property string|null $address
 * @property string|null $facebook_id
 * @property string|null $google_id
 * @property int|null $account_position
 * @property int|null $account_type
 * @property string|null $description
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereAccountPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereAccountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereAvatarFilePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereCardid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereCoverFilePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereDeviceOs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereFacebookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereFcmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereGoogleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers wherePoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereRecommender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsers whereUsername($value)
 */
	class CoreUsers extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CoreUsersActivation
 *
 * @property int $user_id
 * @property string|null $token
 * @property int|null $otp_code
 * @property string|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsersActivation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsersActivation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsersActivation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsersActivation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsersActivation whereOtpCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsersActivation whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CoreUsersActivation whereUserId($value)
 */
	class CoreUsersActivation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Favorite
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorite newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorite newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorite query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorite whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorite whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorite whereUserId($value)
 */
	class Favorite extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Files
 *
 * @property int $id
 * @property int $user_id
 * @property string $file_path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $is_temp
 * @property int|null $type
 * @property-read mixed $file_src
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files whereFilePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files whereIsTemp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Files whereUserId($value)
 */
	class Files extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\HistoryView
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryView newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryView newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryView query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryView whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryView whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryView whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryView whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryView whereUserId($value)
 */
	class HistoryView extends \Eloquent {}
}

namespace App\Models\Location{
/**
 * App\Models\Location\District
 *
 * @OA\Schema (@OA\Xml(name="District"))
 * @OA\Property (property="id",type="integer",description="id"),
 * @OA\Property (property="name",type="string",description="Tên quận/huyện"),
 * @property int $id
 * @property string $name
 * @property string|null $name_ascii
 * @property string|null $name_origin
 * @property string $location
 * @property string $type
 * @property int|null $province_id
 * @property int|null $position
 * @property int|null $user_id_created
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Location\Province|null $province
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\District onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereNameAscii($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereNameOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\District whereUserIdCreated($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\District withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\District withoutTrashed()
 */
	class District extends \Eloquent {}
}

namespace App\Models\Location{
/**
 * App\Models\Location\Province
 *
 * @OA\Schema (@OA\Xml(name="Province"))
 * @OA\Property (property="id",type="integer",description="id"),
 * @OA\Property (property="name",type="string",description="Tên tỉnh/tp"),
 * @property int $id
 * @property string $name
 * @property string|null $name_ascii
 * @property string|null $name_origin
 * @property string $location
 * @property string $type
 * @property int|null $user_id_created
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\Province onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereNameAscii($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereNameOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Province whereUserIdCreated($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\Province withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\Province withoutTrashed()
 */
	class Province extends \Eloquent {}
}

namespace App\Models\Location{
/**
 * App\Models\Location\Street
 *
 * @OA\Schema (@OA\Xml(name="Street"))
 * @OA\Property (property="id",type="integer",description="id"),
 * @OA\Property (property="name",type="string",description="Tên đường phố"),
 * @property int $id
 * @property string|null $name
 * @property string|null $name_ascii
 * @property string|null $location
 * @property int|null $ward_id
 * @property int $district_id
 * @property int|null $user_id_created
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Location\District $district
 * @property-read \App\Models\Location\Ward|null $ward
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\Street onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereNameAscii($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereUserIdCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Street whereWardId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\Street withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\Street withoutTrashed()
 */
	class Street extends \Eloquent {}
}

namespace App\Models\Location{
/**
 * App\Models\Location\Ward
 *
 * @OA\Schema (@OA\Xml(name="Ward"))
 * @OA\Property (property="id",type="integer",description="id"),
 * @OA\Property (property="name",type="string",description="Tên phường/xã"),
 * @property int $id
 * @property string $name
 * @property string|null $name_ascii
 * @property string|null $name_origin
 * @property string $location
 * @property string $type
 * @property int $district_id
 * @property int|null $user_id_created
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Location\District $district
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\Ward onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereNameAscii($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereNameOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Location\Ward whereUserIdCreated($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\Ward withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Location\Ward withoutTrashed()
 */
	class Ward extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notification
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $content
 * @property int|null $chanel 1: broadcast, 2: private
 * @property int|null $type 1: common, 
 * 2: message, 
 * 3: shared, 
 * 4: call request, 
 * 5: reward  point, 
 * 6: mission, 
 * 7: new product, 
 * 8: product update,
 * 9: like product
 * @property int|null $relate_id
 * @property int|null $from_user_id
 * @property int|null $to_user_id
 * @property int|null $user_id_created
 * @property string|null $extra
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CoreUsers|null $from_user
 * @property-read mixed $is_read
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NotificationRead[] $notification_read
 * @property-read \App\Models\CoreUsers|null $to_user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereChanel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereFromUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereRelateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereToUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereUserIdCreated($value)
 */
	class Notification extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\NotificationRead
 *
 * @property int $id
 * @property int|null $notification_id
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationRead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationRead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationRead query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationRead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationRead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationRead whereNotificationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationRead whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NotificationRead whereUserId($value)
 */
	class NotificationRead extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Orders
 *
 * @property int $id
 * @property string|null $order_code
 * @property int|null $total_product_price
 * @property int|null $discount_amount
 * @property int|null $discount_percent
 * @property string|null $discount_code
 * @property int|null $shipping_fee
 * @property int|null $free_shipping
 * @property int|null $payment_method
 * @property int|null $status_payment
 * @property int|null $shipping_method
 * @property string|null $shipping_code
 * @property int|null $total_weight
 * @property string|null $shipping_name
 * @property string|null $shipping_phone
 * @property int|null $shipping_province_id
 * @property int|null $shipping_district_id
 * @property int|null $shipping_ward_id
 * @property string|null $shipping_street_name
 * @property string|null $shipping_address
 * @property string|null $warehouse_name
 * @property string|null $warehouse_phone
 * @property int|null $warehouse_province_id
 * @property int|null $warehouse_district_id
 * @property int|null $warehouse_ward_id
 * @property string|null $warehouse_street_name
 * @property string|null $warehouse_address
 * @property int|null $status
 * @property int|null $total_price
 * @property string|null $cancel_reason
 * @property string|null $note
 * @property int|null $user_id
 * @property int|null $store_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrdersDetail[] $orders_detail
 * @property-read \App\Models\CoreUsers|null $store
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereCancelReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereDiscountAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereDiscountCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereDiscountPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereFreeShipping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereOrderCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingStreetName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereShippingWardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereStatusPayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereTotalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereTotalProductPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereTotalWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereWarehouseAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereWarehouseDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereWarehouseName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereWarehousePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereWarehouseProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereWarehouseStreetName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orders whereWarehouseWardId($value)
 */
	class Orders extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OrdersDetail
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $product_id
 * @property string|null $product_name
 * @property string|null $thumbnail_file_path
 * @property string|null $category_name
 * @property string|null $variation_name
 * @property int|null $price_old
 * @property int|null $price
 * @property int|null $discount_amount
 * @property int|null $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $thumbnail_src
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereCategoryName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereDiscountAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail wherePriceOld($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereProductName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereThumbnailFilePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdersDetail whereVariationName($value)
 */
	class OrdersDetail extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Post
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property string|null $excerpt
 * @property string|null $detail
 * @property string|null $image
 * @property string|null $image_extra
 * @property string|null $image_fb
 * @property int|null $user_id
 * @property int|null $views
 * @property int|null $status
 * @property string|null $video_url
 * @property string|null $custom_link
 * @property string|null $tags
 * @property int|null $sticky
 * @property string|null $seo_title
 * @property string|null $seo_descriptions
 * @property string|null $seo_keywords
 * @property int|null $category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\PostCategory|null $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCustomLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereImageExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereImageFb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSeoDescriptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSeoKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSeoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereSticky($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereVideoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereViews($value)
 */
	class Post extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PostCategory
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property string|null $description
 * @property int|null $parent_id
 * @property int|null $status
 * @property int|null $priority
 * @property string|null $seo_title
 * @property string|null $seo_descriptions
 * @property string|null $seo_keywords
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereSeoDescriptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereSeoKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereSeoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PostCategory whereUserId($value)
 */
	class PostCategory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Product
 *
 * @property int $id
 * @property string|null $name tên sản phẩm
 * @property string|null $description mô tả sản phẩm
 * @property string|null $detail chi tiết sản phẩm
 * @property int|null $price giá bán
 * @property int|null $status trạng thái sp
 * @property int|null $thumbnail_file_id id file hình đại diện
 * @property int|null $user_id id tài khoản cửa hàng
 * @property int|null $category_id danh mục sản phẩm
 * @property int|null $store_category_id danh mục cửa hành
 * @property string|null $sku mã sản phẩm
 * @property int|null $inventory số lượng tồn kho
 * @property int|null $length chiều dài (cm)
 * @property int|null $width chiều rộng (cm)
 * @property int|null $height chiều cao (cm)
 * @property int|null $weight khối lượng (gram)
 * @property int|null $shipping_cost
 * @property string|null $origin nơi xuất xứ
 * @property string|null $factory_address địa chỉ sản xuất/nhập khẩu
 * @property float|null $rating xếp hạng sao
 * @property int|null $is_used_product
 * @property int|null $must_pre_order
 * @property int|null $pre_order_time
 * @property \Illuminate\Support\Carbon|null $created_at ngay tao
 * @property \Illuminate\Support\Carbon|null $updated_at ngay cap nhat
 * @property string|null $deleted_at ngay xoa
 * @property-read \App\Models\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Files[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Files[] $images_extra
 * @property-read \App\Models\CoreUsers|null $owner
 * @property-read \App\Models\CoreUsers|null $store
 * @property-read \App\Models\StoreCategory|null $store_category
 * @property-read \App\Models\Files|null $thumbnail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductWholesales[] $wholesale
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereFactoryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereInventory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereIsUsedProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereMustPreOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product wherePreOrderTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereShippingCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereStoreCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereThumbnailFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product whereWidth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product withoutTrashed()
 */
	class Product extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductImages
 *
 * @property int $id
 * @property int $product_id
 * @property int $file_id
 * @property int|null $priority
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImages newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImages newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImages query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImages whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImages wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImages whereProductId($value)
 */
	class ProductImages extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductImagesExtra
 *
 * @property int $id
 * @property int $product_id
 * @property int $file_id
 * @property int|null $priority
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImagesExtra newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImagesExtra newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImagesExtra query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImagesExtra whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImagesExtra whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImagesExtra wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductImagesExtra whereProductId($value)
 */
	class ProductImagesExtra extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductVariation
 *
 * @property int $id
 * @property int|null $product_id
 * @property string|null $name
 * @property int|null $price
 * @property string|null $sku
 * @property int|null $inventory
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation whereInventory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductVariation whereUpdatedAt($value)
 */
	class ProductVariation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductWholesales
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $quantity_from
 * @property int|null $quantity_to
 * @property int|null $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductWholesales newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductWholesales newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductWholesales query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductWholesales whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductWholesales wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductWholesales whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductWholesales whereQuantityFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductWholesales whereQuantityTo($value)
 */
	class ProductWholesales extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StoreCategory
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property string|null $description
 * @property int|null $parent_id
 * @property int|null $status
 * @property int|null $priority
 * @property int|null $thumbnail_file_id
 * @property int|null $icon_file_id
 * @property int|null $type
 * @property string|null $seo_title
 * @property string|null $seo_descriptions
 * @property string|null $seo_keywords
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereIconFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereSeoDescriptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereSeoKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereSeoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereThumbnailFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreCategory whereUserId($value)
 */
	class StoreCategory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\VariationCombine
 *
 * @property int $product_variation_id
 * @property int $variation_value_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationCombine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationCombine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationCombine query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationCombine whereProductVariationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationCombine whereVariationValueId($value)
 */
	class VariationCombine extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Variations
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $user_id
 * @property int|null $product_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variations newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variations newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variations query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variations whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variations whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variations whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Variations whereUserId($value)
 */
	class Variations extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\VariationValues
 *
 * @property int $id
 * @property int|null $variation_id mã biến thể
 * @property string|null $value giá trị biến thể
 * @property int|null $product_id mã sp
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationValues newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationValues newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationValues query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationValues whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationValues whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationValues whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VariationValues whereVariationId($value)
 */
	class VariationValues extends \Eloquent {}
}

