<?php

return [
    'account_kit_app_id'     => '2220797271491128',
    'account_kit_app_secret' => '3d79d25158cc151dfe982f1f5322e68d',
    'account_kit_version'    => 'v1.1',

    'upload_dir'      => [
        'root' => storage_path('app/public/uploads'),
        'url'  => config('app.url') . '/storage/uploads',
    ],
    'upload_dir_temp' => [
        'root' => storage_path('app/public/uploads/temp'),
        'url'  => config('app.url') . '/storage/uploads/temp',
    ],

    'firebase_api_key'    => 'AAAAmA6hiuA:APA91bEIVf4nvoI6He9aCG_LWEcsICDuK9yy9qwt7iQwDI6pRa5Z28lLmNt34TnNN0A6qRWiSahzH4htIGMzJEVp6dC4ZvzcITI3DONMo2bdN48z825xIQhm5aD9CfMnNnQwLgH4V7xQ',
    'item_perpage'        => 10,
    'item_per_page_admin' => 30,
    'google_maps_key'     => 'AIzaSyB711stPiEwDrN_Biq6Tcx7KHhtu-QPxm0',
    'limit_records'       => [10, 30, 50, 100, 500, 1000, 5000, 10000],
    'version'             => '1.0'
];