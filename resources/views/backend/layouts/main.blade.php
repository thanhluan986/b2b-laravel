<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon icon -->
        <link rel="shortcut icon" href="{{ url('/favicon.ico')}}" type="image/x-icon"/>

        <title>@if($title!=''){{ $title.' | ' }}@endif{{ config('app.name', 'HomeDoctor') }}</title>

        <meta name="description" content="@isset($description){{ $description }}@endisset">
        <meta name="keywords" content="@isset($keywords){{ $keywords }}@endisset">
        <meta name="author" content="@isset($author){{ $author }}@endisset">

        <!-- Bootstrap Core CSS -->
        <link href="{{ asset('/storage/backend/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

        <link href="{{ asset('/storage/backend/assets/plugins/dropzone-master/dist/dropzone.css')}}" rel="stylesheet"
              type="text/css"/>

    @yield('style_top')

    <!-- Custom CSS -->
        <link href="{{ asset('/storage/backend')}}/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="{{ asset('/storage/backend/main/css/style.css')}}" rel="stylesheet">
        <link href="{{ asset('/storage/backend/main/css/custom.css'.'?v='.config('constants.version'))}}"
              rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="{{ asset('/storage/backend/main/css/colors/blue.css')}}" id="theme" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Popup CSS -->
        <link href="{{ asset('/storage/backend')}}/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css"
              rel="stylesheet">

        <link href="{{ asset('/storage/backend')}}/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
        <link href="{{ asset('/storage/backend')}}/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
        <link href="{{ asset('/storage/backend')}}/assets/plugins/datepicker/jquery.datetimepicker.css"
              rel="stylesheet">
        <link href="{{ asset('/storage/backend')}}/assets/plugins/datepicker-simple/datepicker.min.css"
              rel="stylesheet">

        @yield('style')

        <script>
            var BASE_URL = "{{config('app.url')}}";

            var STATIC_URL = "{{ asset('/storage')}}";

            var LOCATION_PROVINCE_URL = "{{route('location.province')}}";
            var LOCATION_DISTRICT_URL = "{{route('location.district')}}";
            var LOCATION_WARD_URL = "{{route('location.ward')}}";
            var LOCATION_STREET_URL = "{{route('location.street')}}";

            var PRODUCT_UNAPPROVED_URL = "{{route('backend.products.ajax.un_approved')}}";
            var PRODUCT_APPROVED_URL = "{{route('backend.products.ajax.approved')}}";
            var PRODUCT_DELETE_URL = "{{route('backend.products.ajax.delete')}}";

            var PRODUCT_MERGE_URL = "{{route('backend.products.merge.make')}}";
            var PRODUCT_MERGE_DETAIL_URL = "{{route('backend.products.merge.detailProduct')}}";

            var USER_SEARCH_URL = "{{route('backend.ajax.searchUser')}}";
            var PRODUCT_CALLBACK_REQUEST_URL = "{{route('backend.products.callback-request')}}";
            var PRODUCT_GET_NOTE_URL = "{{route('backend.products.ajax.getNote')}}";

            var PRODUCT_CALLBACK_MAKE_URL = "{{route('backend.products.callback.make')}}";
            var PRODUCT_CALLBACK_DETAIL_URL = "{{route('backend.products.callback.detailProduct')}}";
        </script>

        @yield('script_top')

    </head>
    <body class="<?php echo Route::currentRouteName() != 'backend.login' ? 'fix-header fix-sidebar card-no-border logo-center' : ''; ?>">

    <?php if( Route::currentRouteName() != 'backend.login'){?>

    <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">

        @include('backend.partials.header')

        <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">

            @yield('content')

            <!-- footer -->
                <!-- ============================================================== -->
                <footer class="footer">
                    © <?php echo date('Y');?>
                </footer>
                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
    <?php }else{?>

    @yield('content')

    <?php }?>

    <!-- All Jquery -->
        <!-- ============================================================== -->
        <script src="{{ asset('/storage/backend/assets/plugins/jquery/jquery.min.js')}}"></script>

        <!-- Bootstrap tether Core JavaScript -->
        <script src="{{ asset('/storage/backend/assets/plugins/bootstrap/js/popper.min.js')}}"></script>
        <script src="{{ asset('/storage/backend/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{ asset('/storage/backend/main/js/jquery.slimscroll.js')}}"></script>

        <!--Wave Effects -->
        <script src="{{ asset('/storage/backend/main/js/waves.js')}}"></script>

        <!--Menu sidebar -->
        <script src="{{ asset('/storage/backend/main/js/sidebarmenu.js')}}"></script>

        <!--stickey kit -->
        <script src="{{ asset('/storage/backend/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
        <script src="{{ asset('/storage/backend/assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

        <!--Custom JavaScript -->
        <script src="{{ asset('/storage/backend/main/js/custom.min.js')}}"></script>


        <script src="{{ asset('/storage/backend/main/js/mustache.min.js')}}"></script>
        <!-- ============================================================== -->

        <!-- Magnific popup JavaScript -->
        <script src="{{ asset('/storage/backend')}}/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
        <script src="{{ asset('/storage/backend')}}/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>

        <!-- chartist chart -->
        <script src="{{ asset('/storage/backend')}}/assets/plugins/chartist-js/dist/chartist.min.js"></script>
        <script src="{{ asset('/storage/backend')}}/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>

        <script src="{{ asset('/storage/backend')}}/assets/plugins/select2/dist/js/select2.full.min.js"></script>
        <script src="{{ asset('/storage/backend')}}/assets/plugins/datepicker/jquery.datetimepicker.js"></script>
        <script src="{{ asset('/storage/backend')}}/assets/plugins/datepicker-simple/datepicker.min.js"></script>
        <script src="{{ asset('/storage/backend')}}/js/custom.js?v={{config('constants.version')}}"></script>

        <!-- ============================================================== -->
        @yield('script')
        @yield('script2')
        @yield('script3')
        @yield('script4')

    </body>
</html>
