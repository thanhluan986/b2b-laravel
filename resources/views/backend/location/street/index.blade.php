@extends('backend.layouts.main')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{$subtitle}}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            {{ Breadcrumbs::render('backend.location.street.index') }}
        </div>

        <div class="col-md-12">
            <div class="card card-outline-info">
                <div class="card-body">

                    @if(auth()->guard('backend')->user()->can('street.add'))
                        <div class="row">
                            <div class="col-md-2 pull-right">
                                <a href="{{Route('backend.location.street.add')}}"
                                   class="btn waves-effect waves-light btn-block btn-info">
                                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Thêm mới
                                </a>
                            </div>
                        </div>
                    @endif

                    <form action="" method="get" id="form-filter">
                        <div class="form-body">
                            <div class="row p-t-20">

                                <div class="col-md-12">
                                    @include('backend.partials.msg')
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="province_id">Tỉnh/TP</label>
                                        <select class="form-control select_province select2 form-control-sm"
                                                style="width: 100%; height:36px;" name="province_id">
                                            <option value="">Tất cả</option>
                                            @foreach($provinces as $province)
                                                <option value="{{$province->id}}"
                                                        {!! request('province_id')==$province->id?'selected="selected"':'' !!}
                                                >{{$province->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="district_id">Quận/Huyện</label>
                                        <select class="form-control  select_district select2" name="district_id">
                                            <option value="">Tất cả</option>
                                            @foreach($districts as $districts)
                                                <option value="{{$districts->id}}"
                                                        {!! request('district_id')==$districts->id?'selected="selected"':'' !!}>{{$districts->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label" for="name">Tên</label>
                                        <input type="text"
                                               name="name"
                                               value="{{$filter['name']}}"
                                               id="name"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Số record</label>
                                        <select class="form-control"
                                                name="limit">
                                            @foreach($_limits as $st)
                                                <option value="{{$st}}"
                                                        {!! $filter['limit']==$st?'selected="selected"':'' !!}>{{number_format($st)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="btn-group" role="group" style="display: inherit">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-search"></i>Tìm
                                        </button>

                                        <a title="Clear search"
                                           href="{{Route('backend.location.street.index')}}"
                                           class="btn btn-danger">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table color-table muted-table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Tên đường</th>
                                            <th>Tên mới</th>
                                            <th>Quận/Huyện</th>
                                            <th>Tỉnh/TP</th>
                                            <th>Ngày tạo</th>
                                            <th>Ngày cập nhật</th>
                                            <th class="text-right">Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($streets as $key => $street)
                                            <tr>
                                                <td>{{++$start}}</td>
                                                <td>{{$street->id}}</td>
                                                <td class="street_name">{{$street->name}}</td>
                                                <td data-id="{{$street->id}}">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control form-control-sm" placeholder="Nhập tên mới...">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-info btn-sm save-name" type="button">
                                                                <i class="fa fa-save"></i></button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{$street->district->name}}</td>
                                                <td>{{$street->district->province->name}}</td>
                                                <td>{{$street->created_at}}</td>
                                                <td>{{$street->updated_at}}</td>

                                                <td class="text-right">
                                                    @if(auth()->guard('backend')->user()->can('street.edit'))
                                                        <a href="{{Route('backend.location.street.edit',[$street->id]). '?_ref=' .$current_url }}"
                                                           class="btn waves-effect waves-light btn-info btn-sm">
                                                            <i class="fa fa-pencil-square-o"></i> Sửa</a>
                                                    @endif

                                                    @if(auth()->guard('backend')->user()->can('street.del'))
                                                        <a href="{{Route('backend.location.street.del',[$street->id]) . '?_ref=' .$current_url }}"
                                                           class="btn waves-effect waves-light btn-danger btn-sm" data-bb="confirm">
                                                            <i class="fa fa-trash-o"></i> Xóa</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="8">-</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>

                            <div class="text-center">
                                {{ $streets->links() }}
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('style_top')
    <link href="{{ asset('/storage/backend/assets/plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@stop

@section('script')

    <script src="{{ asset('/storage/backend/assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script>
        $('.select2').select2();
        new SelectLocation('#form-filter ');

        function SelectLocation(form_id) {
            $(form_id + '.select_province').change(function () {
                $(form_id + '.select_district').html('<option value="">Chọn</option>');
                $(form_id + '.select_ward').html('<option value="">Chọn</option>');
                var province_id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '{{route('location.district')}}',
                    data: {province_id: province_id},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data.data, function (index, element) {
                            $(form_id + '.select_district').append('<option value=' + element.id + '>' + element.name + '</option>');
                        });
                    }
                });
            });

            $(form_id + '.select_district').change(function () {
                $(form_id + '.select_ward').html('<option value="">Chọn</option>');
                $(form_id + '.select_street').html('<option value="">Chọn</option>');
                var district_id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '{{route('location.ward')}}',
                    data: {district_id: district_id},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data.data, function (index, element) {
                            $(form_id + '.select_ward').append('<option value=' + element.id + '>' + element.name + '</option>');
                        });
                    }
                });
            });
        };

        $(document).on('click', '.save-name', function () {
            var d = $(this);
            var id = $(this).parent().parent().parent().data('id');
            var name = $(this).parent().parent().find('input').val();
            var _token = $('meta[name="csrf-token"]').attr('content');

            if (name == '') {
                alert('Vui lòng nhập tên mới!');
                return;
            }

            $.ajax({
                type: 'POST',
                url: '{{route('backend.location.street.ajax-edit')}}',
                data: {id: id, name: name, _token: _token},
                dataType: 'json',
                success: function (data) {
                    if (data.e == 0) {
                        $('meta[name="csrf-token"]').attr('content', data._token);
                        d.parent().parent().parent().parent().find('.street_name').html(name);
                        d.parent().parent().find('input').val('');
                    } else {
                        alert(data.r);
                    }
                }
            });
        });
    </script>
@stop