@extends('backend.layouts.main')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{$subtitle}}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            {{ Breadcrumbs::render('backend.banners.index') }}
        </div>

        <div class="col-md-12">
            <div class="card card-outline-info">
                <div class="card-body">
                    @if(auth()->guard('backend')->user()->can('banners.add')||1)
                        <div class="row">
                            <div class="col-md-2 pull-right">
                                <a href="{{Route('backend.banners.add')}}"
                                   class="btn waves-effect waves-light btn-block btn-info">
                                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Thêm mới
                                </a>
                            </div>
                        </div>
                    @endif
                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            @include('backend.partials.msg')

                            <div class="table-responsive">
                                <table class="table color-table muted-table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Hình</th>
                                            <th>Hình App</th>
                                            <th>Tiêu đề</th>
                                            <th>Loại</th>
                                            <th>Danh mục</th>
                                            <th>Trạng thái</th>
                                            <th>Ngày tạo</th>
                                            <th>Ngày cập nhật</th>
                                            <th class="text-right">Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($list_data as $key => $item)
                                            <tr>
                                                <td>{{++$start}}</td>
                                                <td>
                                                    @if($item->image)
                                                        <img src="{{$item->image->file_src}}" height="40"
                                                             class="image-popup-no-margins"
                                                             href="{{$item->image->file_src}}"/>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($item->image_app)
                                                        <img src="{{$item->image_app->file_src}}" height="40"
                                                             class="image-popup-no-margins"
                                                             href="{{$item->image_app->file_src}}"/>
                                                    @endif
                                                </td>
                                                <td>{{$item->name}}</td>

                                                <td>
                                                    @if($item->type=='category')
                                                        Danh mục
                                                    @elseif($item->type=='store')
                                                        Cửa hàng
                                                    @elseif($item->type=='promo')
                                                        CTKM
                                                    @else
                                                        Trang chủ
                                                    @endif
                                                </td>

                                                <td>{{isset($item->category->name)?$item->category->name:'-'}}</td>

                                                <td>
                                                    @foreach($status as $st)
                                                        @if($st['id']==$item->status)
                                                            {{$st['name']}}
                                                            @break
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>{{$item->created_at}}</td>
                                                <td>{{$item->updated_at}}</td>

                                                <td class="text-right">
                                                    @if(auth()->guard('backend')->user()->can('banners.edit')||1)
                                                        <a href="{{Route('backend.banners.edit',[$item->id]). '?_ref=' .$current_url }}"
                                                           class="btn waves-effect waves-light btn-info btn-sm">
                                                            <i class="fa fa-pencil-square-o"></i> Sửa</a>
                                                    @endif

                                                    @if(auth()->guard('backend')->user()->can('banners.del')||1)
                                                        <a href="{{Route('backend.banners.del',[$item->id]) . '?_ref=' .$current_url }}"
                                                           class="btn waves-effect waves-light btn-danger btn-sm"
                                                           data-bb="confirm">
                                                            <i class="fa fa-trash-o"></i> Xóa</a>
                                                    @endif

                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="10">-</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection