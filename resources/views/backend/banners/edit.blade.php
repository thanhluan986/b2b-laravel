@extends('backend.layouts.main')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{$subtitle}}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            {{ Breadcrumbs::render('backend.banners.edit') }}
        </div>

        <div class="col-md-12">
            <div class="card card-outline-info">
                <div class="card-body">

                    <form class="form-horizontal form-bordered" action="" method="post">

                        @include('backend.partials.msg')
                        @include('backend.partials.errors')

                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-8" style="margin: auto">

                                <div class="form-group row">
                                    <label class="form-control-label text-left col-md-2">Tên banner
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="col-md-10">
                                        <input type="text"
                                               class="form-control"
                                               name="name" required="required"
                                               value="{{old('name', $data->name)}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label text-left col-md-2">Loại banner
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="col-md-10">
                                        <select class="form-control form-control-line " name="type" id="sle_type">
                                            <option value="">Chọn</option>
                                            @foreach($type as $k=>$v)
                                                <option value="{{$v['id']}}"
                                                        {{old('type', $data->type)==$v['id']?'selected="selected"':''}}
                                                >{{$v['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row @if($data->type!='category') hidden-lg-up @endif" id="sle_category">
                                    <label class="form-control-label text-left col-md-2">Danh mục
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="col-md-10">
                                        <select class="form-control form-control-line " name="category_id">
                                            <option value="">Chọn</option>
                                            {!! $categories_html !!}}
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label text-left col-md-2">Trạng thái
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="col-md-10">
                                        @foreach($status as $k=>$v)
                                            <input type="radio" id="status_{{$v['id']}}" name="status" value="{{$v['id']}}"
                                                    {{old('status', $data->status)==$v['id']?'checked':''}}/>
                                            <label for="status_{{$v['id']}}">{{$v['name']}}</label>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label text-left col-md-2">Link</label>

                                    <div class="col-md-10">
                                        <input type="text"
                                               class="form-control"
                                               name="link"
                                               value="{{old('link', $data->link)}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label text-left col-md-2">Hình ảnh
                                        <span class="text-danger">*</span>
                                    </label>

                                    <div class="col-md-5">
                                        <label>Hình web</label>
                                        <div class="dropzone" id="myDropzone" action="{{route('backend.ajax.uploadImage')}}">
                                            <div class="dz-message">
                                                <div class="col-xs-8">
                                                    <div class="message">
                                                        <h6>Kéo thả tập tin vào hoặc Click để tải lên</h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="result">
                                                @if($image)
                                                    <div class="thumb-nail-view">
                                                        <img src="{{$image->file_src}}" class="img-thumbnail">
                                                        <input name="image_file_id" value="{{$image->id}}" type="hidden">
                                                        <a href="javascript:;" onclick="removeFile($(this))" data-id="{{$image->id}}">Xóa</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="fallback">
                                                <input type="file" name="file" multiple style="opacity: 0">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <label>Hình App</label>
                                        <div class="dropzone" id="myDropzone2" action="{{route('backend.ajax.uploadImage')}}">
                                            <div class="dz-message">
                                                <div class="col-xs-8">
                                                    <div class="message">
                                                        <h6>Kéo thả tập tin vào hoặc Click để tải lên</h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="result">
                                                @if($app_image)
                                                    <div class="thumb-nail-view">
                                                        <img src="{{$app_image->file_src}}" class="img-thumbnail">
                                                        <input name="app_image_file_id" value="{{$app_image->id}}" type="hidden">
                                                        <a href="javascript:;" onclick="removeFile($(this))" data-id="{{$app_image->id}}">Xóa</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="fallback">
                                                <input type="file" name="file" multiple style="opacity: 0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row text-center p-t-10">
                                    <div class="col-md-12 text-center">
                                        <button class="btn btn-info" type="submit">Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script_top')
    <script src="{{ asset('/storage/backend')}}/assets/plugins/ckeditor/ckeditor.js"></script>
@stop

@section('style')
    <style>
        #form-add label {
            font-size: 13px;
        }

        .result {
            display: inline-block;
        }

        .thumb-nail-view {
            /*width: 32%;*/
            /*height: 100px;*/
            margin: auto;
            padding: 5px;
            float: left;
        }

        .thumb-nail-view a {
            text-align: center;
            font-size: 10px;
            cursor: pointer !important;
            display: inline-block;
            width: 100%;
            color: red;
            font-weight: bold;
        }

        .dropzone .dz-message {
            padding: 0;
        }

        .dropzone .dz-preview .dz-image {
            border-radius: 5px;
            overflow: hidden;
            width: 70px;
            height: 70px;
        }

        .dropzone .dz-preview .dz-image img {
            width: 100%;
        }

        .dropzone .dz-preview .dz-progress {
            width: 60px;
            margin-left: -30px;
        }

        .dropzone .dz-preview .dz-details .dz-size {
            display: none;
        }

        .dropzone {
            min-height: 100px;
        }
    </style>
@stop

@section('script')

    <script src="{{ asset('/storage/backend/assets/plugins/dropzone-master/dist/dropzone.js')}}" type="text/javascript"></script>

    <script>

        $('#sle_type').change(function () {
            if ($(this).val() == 'category') {
                $('#sle_category').removeClass('hidden-lg-up');
            } else {
                $('#sle_category').addClass('hidden-lg-up');
            }
        });

        $.fn.modal.Constructor.prototype._enforceFocus = function () {
        };

        Dropzone.options.myDropzone = {
            uploadMultiple: true,
            parallelUploads: 1,
            maxFilesize: 16,
            addRemoveLinks: false,
            dictFileTooBig: 'Dung lượng hình lớn hơn 16MB',
            timeout: 10000,
            params: {
                _token: $('[name="_token"]').val(),
                type: 3
            },
            init: function () {
                this.on("complete", function (file, reponse) {
                    $('#myDropzone .dz-preview').remove();
                    $('#myDropzone').removeClass('dz-started');
                });
            },
            success: function (file, reponse) {
                $('#myDropzone .result').html('');
                $.each(reponse.r, function (i, item) {
                    appendImage(item.id, item.url, '#myDropzone .result', 'image_file_id');
                });
            }
        }

        Dropzone.options.myDropzone2 = {
            uploadMultiple: true,
            parallelUploads: 1,
            maxFilesize: 16,
            addRemoveLinks: false,
            dictFileTooBig: 'Dung lượng hình lớn hơn 16MB',
            timeout: 10000,
            params: {
                _token: $('[name="_token"]').val(),
                type: 3
            },
            init: function () {
                this.on("complete", function (file) {
                    $('#myDropzone2 .dz-preview').remove();
                    $('#myDropzone2').removeClass('dz-started');
                });
            },
            success: function (file, reponse) {
                $('#myDropzone2 .result').html('');
                $.each(reponse.r, function (i, item) {
                    appendImage(item.id, item.url, '#myDropzone2 .result', 'app_image_file_id');
                });
            }
        };

        function removeFile(d) {
            var file_id = d.data('id');
            $.post({
                url: '{{route('backend.ajax.removeImage')}}',
                data: {id: file_id, _token: $('[name="_token"]').val()},
                dataType: 'json',
                success: function (data) {
                    d.parent().remove();
                }
            });
        }

        function appendImage(id, src, parent_div, input_name) {
            $(parent_div).append('<div class="thumb-nail-view">\n' +
                '        <img src="' + src + '" class="img-thumbnail"/>\n' +
                '        <input name="' + input_name + '" value="' + id + '" type="hidden"/>\n' +
                '        <a href="javascript:;" onclick="removeFile($(this))" data-id="' + id + '">Xóa</a>\n' +
                '    </div>')
        }
    </script>
@stop