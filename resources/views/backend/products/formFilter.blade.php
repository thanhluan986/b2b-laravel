<form action="" method="get" id="form-filter">
    <div class="form-body">

        <div class="row">
            <div class="col-md-12">
                @include('backend.partials.msg')
            </div>


            <div class="col-md-2">
                <div class="form-group">
                    <input type="text"
                           name="date_from"
                           value="{{request('date_from')}}"
                           id="date_from" data-toggle="datepicker-simple"
                           class="form-control form-control-sm" placeholder="Từ ngày">
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <input type="text"
                           name="date_to"
                           value="{{request('date_to')}}"
                           id="date_to" data-toggle="datepicker-simple"
                           class="form-control form-control-sm" placeholder="Đến ngày">
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <select class="form-control form-control-sm"
                            name="category_id">
                        <option value="">Danh mục</option>
                        {!! $product_category_html !!}}
                    </select>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <select class="form-control form-control-sm"
                            name="status">
                        <option value="">Trạng thái</option>
                        @foreach($status as $st)
                            <option value="{{$st['id']}}"
                                    {!! request('status')===$st['id']?'selected="selected"':'' !!}>{{$st['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">

            <input type="hidden" name="sort" value="" class="input_sort">

            <div class="col-md-2">
                <div class="form-group">
                    <input type="number"
                           name="id"
                           value="{{request('id')}}"
                           id="product_id"
                           class="form-control form-control-sm" placeholder="ID">
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <input type="text"
                           name="keywords"
                           value="{{request('keywords')}}"
                           id="keywords"
                           class="form-control form-control-sm" placeholder="Từ khóa SP">
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <input type="text"
                           name="user_id"
                           value="{{request('user_id')}}"
                           id="user_id"
                           class="form-control form-control-sm" placeholder="ID shop">
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <input type="text"
                           name="fullname"
                           value="{{request('fullname')}}"
                           id="fullname"
                           class="form-control form-control-sm" placeholder="Tên/SĐT shop">
                </div>
            </div>


            <div class="col-md-2">
                <div class="form-group">
                    <select class="form-control form-control-sm"
                            name="limit">
                        <option>Số record</option>
                        @foreach($_limits as $st)
                            <option value="{{$st}}"
                                    {!! $filter['limit']==$st?'selected="selected"':'' !!}>{{number_format($st)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</form>
