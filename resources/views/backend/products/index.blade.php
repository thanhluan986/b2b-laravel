@extends('backend.layouts.main')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{$subtitle}}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            {{ Breadcrumbs::render('backend.products.index') }}
        </div>

        <div class="col-md-12">
            <div class="card card-outline-info">
                <div class="card-body">
                    @include('backend.products.formFilter')

                    <div class="ajax-result">
                        @include('backend.products.ajaxList')
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('style_top')
    <link href="{{ asset('/storage/backend/assets/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .select2 {
            width: 100% !important;
            height: 36px !important;
        }

        .checkbox-basic {
            position: initial !important;
            left: initial !important;
            opacity: 1 !important;
        }

        @media (min-width: 576px) {
            #note-modal .modal-dialog {
                max-width: 1000px;
                margin: 1.75rem auto;
            }
        }

        .thumbnail-outer {
            display: inline-flex;
        }

        .thumbnail {
            width: 80px;
            height: 80px;
            position: relative;
            float: left;
            margin: 2px;
        }

        .thumbnail img {
            max-width: 100%;
            max-height: 100%;
            border: solid 1px #ccc;
            cursor: pointer;
        }

        .thumbnail a.btn_delete,
        .thumbnail a.btn_rotate_left,
        .thumbnail a.btn_rotate_right {
            display: block;
            width: 20px;
            height: 20px;
            position: absolute;
            overflow: hidden;
            padding: 1px 5px;
            font-weight: bold;
            font-size: 13px;
        }

        .thumbnail a.btn_rotate_left,
        .thumbnail a.btn_rotate_right {
            background: #2125294a;
        }

        .thumbnail a.btn_delete {
            top: 1px;
            right: 1px;
            background: #FDFF21;
            color: red;
        }

        .thumbnail a.btn_rotate_left {
            bottom: 1px;
            left: 1px;
        }

        .thumbnail a.btn_rotate_right {
            bottom: 1px;
            right: 1px;
        }

        p.upload {
            clear: both;
            padding-top: 15px;
            padding-bottom: 15px;
            margin-bottom: 0;
        }

        a.sort.active {
            color: red;
        }

        .image_select {
            cursor: pointer;
        }

        .sort_btn {
            /*display: inline-block;*/
            /*float: left;*/
            margin-top: 10px;
        }

        .my_upload {
            display: inline-block;
            float: left;
            margin-top: 10px;
            cursor: pointer;
            clear: both;
        }

        .my_upload .image_select {
            position: absolute;
            left: 0;
            opacity: 0;
            top: 0;
            bottom: 0;
            width: 100%;
        }

        .my_upload div {
            position: absolute;
            left: 0;
            top: 0;
            bottom: 0;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            background: #ccc;
            border: 3px dotted #bebebe;
            border-radius: 2px;
        }

        .my_upload label {
            display: inline-block;
            position: relative;
            height: 40px;
            width: 100px;
            cursor: pointer;
        }

        .my_upload div.dragover {
            background-color: #aaa;
        }

        #grid_table td {
            text-align: center;
            vertical-align: middle;
            font-size: 14px !important;
        }

        /*#my-table td{*/
            /*vertical-align: middle;*/
        /*}*/
        .image-border{
            border: solid 2px #ddd;
        }
    </style>
@stop

@section('script')
    <script src="{{ asset('/storage/backend/assets/plugins/dropzone-master/dist/dropzone.js')}}" type="text/javascript"></script>
    <script src="{{ asset('/storage/backend/assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script>

        $(document).on('click', 'ul.pagination li a', function (event) {
            event.preventDefault();
            var url = $(this).attr('href');
            load_product(url);
        });

        $(document).on('click', 'a.sort', function (event) {
            event.preventDefault();
            var sort = $(this).data('sort');
            $('#form-filter .input_sort').val(sort);
            var data = $('#form-filter').serialize();
            var url = $('#form-filter').attr('action');
            load_product(url, data)
        });

        function load_product(url, params) {
            var params = params !== undefined ? params : null;
            $.ajax({
                url: url,
                type: 'get',
                data: params,
                success: function (data) {
                    if (data.e == 0) {
                        $('div.ajax-result').html(data.r);
                        $('html, body').animate({scrollTop: 0}, 0);
                        image_popup();
                    }

                }
            });
        }

        $('#form-filter').on('change', 'select', function () {
            var data = $('#form-filter').serialize();
            var url = $('#form-filter').attr('action');
            load_product(url, data)
        });

        var timeoutID = null;

        $('#form-filter').on('keyup', 'input', function () {
            var data = $('#form-filter').serialize();
            var url = $('#form-filter').attr('action');
            clearTimeout(timeoutID);

            timeoutID = setTimeout(function () {
                load_product(url, data)
            }, 500);
        });

        $('[data-toggle="datepicker-simple"]').on('change', function () {
            var data = $('#form-filter').serialize();
            var url = $('#form-filter').attr('action');
            clearTimeout(timeoutID);

            timeoutID = setTimeout(function () {
                load_product(url, data)
            }, 1000);
        });

        $(document).on('click', '#un_approved_btn', function (event) {
            var x = getChecked();
            if (!x)
                return;
            var r = confirm("Ẩn các sản phẩm đã chọn?");
            if (r == true) {
                $.ajax({
                    url: '{{route('backend.products.ajax.un_approved')}}',
                    type: 'post',
                    data: {product_ids: val, _token: '{{csrf_token()}}'},
                    success: function (data) {
                        alert('Ẩn sản phẩm thành công!');
                        window.location.reload();
                    }
                });
            }
        });

        $(document).on('click', '#delete_btn', function (event) {
            var x = getChecked();
            if (!x)
                return;
            var r = confirm("Xóa các sản phẩm đã chọn?");
            if (r == true) {
                $.ajax({
                    url: '{{route('backend.products.ajax.delete')}}',
                    type: 'post',
                    data: {product_ids: val, _token: '{{csrf_token()}}'},
                    success: function (data) {
                        alert('Đã xóa thành công!');
                        window.location.reload();
                    }
                });
            }
        });

        $(document).on('click', '#approved_btn', function (event) {
            var x = getChecked();
            if (!x)
                return;
            var r = confirm("Hiện các sản phẩm đã chọn?");
            if (r == true) {
                $.ajax({
                    url: '{{route('backend.products.ajax.approved')}}',
                    type: 'post',
                    data: {product_ids: val, _token: '{{csrf_token()}}'},
                    success: function (data) {
                        alert('Hiện sản phẩm thành công!');
                        window.location.reload();
                    }
                });
            }
        });
    </script>
@stop