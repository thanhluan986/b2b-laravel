<div class="table-responsive">
    <table class="table color-table muted-table table-striped" id="my-table">
        <thead>
            <tr>
                <th style="width: 20px;"><input type="checkbox" class="checkbox-basic check-all">
                </th>
                <th style="width: 50px;">Hình</th>
                <th>Tên sản phẩm</th>
                <th style="width: 100px;">Giá &nbsp;&nbsp;
                    <a href="" title="Tăng dần" data-sort="price_low_high"
                       class="sort {{$sort=='price_low_high'?'active':''}}"><i class="fa fa-long-arrow-up"></i></a>
                    <a href="" title="Giảm dần" data-sort="price_high_low"
                       class="sort {{$sort=='price_high_low'?'active':''}}"><i class="fa fa-long-arrow-down"></i></a>
                </th>
                <th>Dạnh mục</th>
                <th>Shop</th>
                <th style="width: 140px;">Trạng thái</th>
                <th style="width: 140px;">Ngày tạo</th>
            </tr>
        </thead>
        <tbody>
            @forelse($products as $key => $product)
                <tr>
                    <td>
                        <input type="checkbox" name="ids[]" value="{{$product->id}}" id="{{$product->id}}"
                               class="checkbox-basic check-all-child">
                    </td>

                    <td>
                        @if($product->thumbnail)
                            <img src="{{$product->thumbnail->file_src}}" height="40" class="image-popup-no-margins"
                                 href="{{$product->thumbnail->file_src}}"/>
                        @endif
                    </td>

                    <td>
                        <span class="row-title">{{$product->name}}</span>

                        <div class="row-actions">
                            <span class="id">ID: {{$product->id}}</span> |
                            <span class="shop-id">Shop ID: {{$product->user_id}}</span>
                        </div>
                    </td>

                    <td class="price">{{number_format($product->price)}}</td>

                    <td>{{$product->category->name}}</td>
                    <td>{{$product->store->fullname?$product->store->fullname:$product->store->phone}}</td>

                    <td>
                        @foreach($status as $st)
                            @if($st['id']==$product->status)
                                {{$st['name']}}
                                @break
                            @endif
                        @endforeach
                    </td>

                    <td>{{date('H:i d/m/y',strtotime($product->created_at))}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="19">-</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>

<div style="margin-top: 10px">
    <label> Chọn tất cả <input type="checkbox" class="checkbox-basic check-all"></label>

    @if(auth()->guard('backend')->user()->can('products.censorship.index'))

        <button type="button" class="btn waves-effect waves-light btn-xs btn-success"
                id="approved_btn">Hiện
        </button>

        <button type="button" class="btn waves-effect waves-light btn-xs btn-warning"
                id="un_approved_btn">Ẩn
        </button>
    @endif

    @if(auth()->guard('backend')->user()->can('products.delete'))
        <button type="button" class="btn waves-effect waves-light btn-xs btn-danger"
                id="delete_btn">Xóa
        </button>
    @endif
</div>

<br>
<div class="text-center">
    {{ $products->links() }}

    @if($products->total())
        <small>Hiển thị {{number_format($products->firstItem())}} - {{number_format($products->lastItem())}} tên
            tổng số {{number_format($products->total())}}
        </small>
    @endif
</div>