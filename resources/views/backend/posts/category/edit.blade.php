@extends('backend.layouts.main')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{$subtitle}}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            {{ Breadcrumbs::render('backend.posts.category.edit') }}
        </div>

        <div class="col-md-12">
            <div class="card card-outline-info">
                <div class="card-body">

                    <form class="form-horizontal" action="" method="post">

                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-md-6" style="margin: auto">

                                @include('backend.partials.msg')
                                @include('backend.partials.errors')

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-control-label">Tên danh mục
                                                <span class="text-danger">*</span>
                                            </label>

                                            <input type="text"
                                                   class="form-control"
                                                   name="name" required="required"
                                                   value="{{old('name', $data->name)}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p><label class="form-control-label">Trạng thái</label></p>
                                            @foreach($status as $k=>$v)
                                                <input type="radio" id="status_{{$v['id']}}" name="status" value="{{$v['id']}}"
                                                        {{old('status', $data->status)==$v['id']?'checked':''}}/>
                                                <label for="status_{{$v['id']}}">{{$v['name']}}</label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Mô tả</label>

                                    <textarea type="text"
                                              class="form-control form-control-line" rows="5"
                                              name="description">{{old('description', $data->description)}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Seo title</label>

                                    <input type="text"
                                           class="form-control"
                                           name="seo_title"
                                           value="{{old('seo_title', $data->seo_title)}}">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Seo descriptions</label>

                                    <textarea type="text"
                                              class="form-control form-control-line" rows="5"
                                              name="seo_descriptions">{{old('seo_descriptions', $data->seo_descriptions)}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Seo keywords</label>

                                    <input type="text"
                                           class="form-control"
                                           name="seo_keywords"
                                           value="{{old('seo_keywords', $data->seo_keywords)}}">
                                </div>

                                <div class="form-group text-center">
                                    <button class="btn btn-info" type="submit">Sửa</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection