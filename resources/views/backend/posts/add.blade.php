@extends('backend.layouts.main')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{$subtitle}}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            {{ Breadcrumbs::render('backend.posts.add') }}
        </div>

        <div class="col-md-12">
            <div class="card card-outline-info">
                <div class="card-body">

                    <form class="form-horizontal" action="" method="post">

                        @include('backend.partials.msg')
                        @include('backend.partials.errors')

                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-md-9">
                                <div class="form-group form-group-sm">
                                    <label class="form-control-label">Tiêu đề
                                        <span class="text-danger">*</span>
                                    </label>

                                    <input type="text"
                                           class="form-control form-control-sm"
                                           name="name" required="required"
                                           value="{{old('name')}}">
                                </div>

                                <div class="form-group form-group-sm">
                                    <label class="form-control-label">Mô tả</label>

                                    <textarea type="text"
                                              class="form-control form-control-sm form-control-line" rows="3"
                                              name="excerpt">{{old('excerpt')}}</textarea>
                                </div>

                                <div class="form-group form-group-sm">
                                    <label class="form-control-label">Nội dung</label>

                                    <textarea type="text"
                                              class="form-control form-control-sm form-control-line" rows="5" id="detail"
                                              name="detail">{!! old('detail') !!}</textarea>
                                </div>

                                <hr>
                                <div class="form-group form-group-sm row">
                                    <label class="form-control-label text-left col-md-2">Seo title</label>
                                    <div class="col-md-4">
                                        <input type="text"
                                               class="form-control form-control-sm"
                                               name="seo_title"
                                               value="{{old('seo_title')}}">
                                    </div>
                                </div>

                                <div class="form-group form-group-sm row">
                                    <label class="form-control-label text-left col-md-2">Seo descriptions</label>

                                    <div class="col-md-4">
                                        <input type="text"
                                               class="form-control form-control-sm"
                                               name="seo_descriptions"
                                               value="{{old('seo_descriptions')}}">
                                    </div>
                                </div>

                                <div class="form-group form-group-sm row">
                                    <label class="form-control-label text-left col-md-2">Seo keywords</label>

                                    <div class="col-md-4">
                                        <input type="text"
                                               class="form-control form-control-sm"
                                               name="seo_keywords"
                                               value="{{old('seo_keywords')}}">
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <button class="btn btn-info" type="submit">Thêm</button>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group form-group-sm">
                                    <label class="form-control-label">Danh mục
                                        <span class="text-danger">*</span>
                                    </label>

                                    <select class="form-control form-control-sm form-control-line " name="category_id">
                                        <option value="">Chọn</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" {{$category->id == old('category_id')?"selected='selected":''}}>
                                                {{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group form-group-sm">
                                    <p><label class="form-control-label">Trạng thái</label></p>
                                    @foreach($status as $k=>$v)
                                        <input type="radio" id="status_{{$v['id']}}" name="status" value="{{$v['id']}}"
                                                {{(empty(old('status'))&&$v['id']==1)||(old('status')==$v['id'])?'checked':''}}/>
                                        <label for="status_{{$v['id']}}">{{$v['name']}}</label>
                                    @endforeach
                                </div>

                                <div class="form-group form-group-sm">
                                    <label>Hình đại diện</label>
                                    <div class="dropzone" id="myDropzone"
                                         action="{{route('backend.ajax.uploadImage')}}">
                                        <div class="dz-message">
                                            <div class="col-xs-8">
                                                <div class="message">
                                                    <h6>Kéo thả tập tin vào hoặc Click để tải lên</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="result">
                                            @if($image)
                                                <div class="thumb-nail-view">
                                                    <img src="{{$image->file_src}}" class="img-thumbnail">
                                                    <input name="image_file_id" value="{{$image->id}}" type="hidden">
                                                    <a href="javascript:;" onclick="removeFile($(this))"
                                                       data-id="{{$image->id}}">Xóa</a>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="fallback">
                                            <input type="file" name="file" multiple style="opacity: 0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script_top')
    <script src="{{ asset('/storage/backend')}}/assets/plugins/ckeditor/ckeditor.js"></script>
@stop

@section('script')
    <script src="{{ asset('/storage/backend/assets/plugins/dropzone-master/dist/dropzone.js')}}"
            type="text/javascript"></script>

    <script>
        CKEDITOR.replace('detail');

        $.fn.modal.Constructor.prototype._enforceFocus = function () {
        };

        Dropzone.options.myDropzone = {
            uploadMultiple: true,
            parallelUploads: 1,
            maxFilesize: 16,
            addRemoveLinks: false,
            dictFileTooBig: 'Dung lượng hình lớn hơn 16MB',
            timeout: 10000,
            params: {
                _token: $('[name="_token"]').val(),
                type: 3
            },
            init: function () {
                this.on("complete", function (file, reponse) {
                    $('#myDropzone .dz-preview').remove();
                    $('#myDropzone').removeClass('dz-started');
                });
            },
            success: function (file, reponse) {
                $('#myDropzone .result').html('');
                $.each(reponse.r, function (i, item) {
                    appendImage(item.id, item.url, '#myDropzone .result', 'image_file_id');
                });
            }
        }

        function removeFile(d) {
            var file_id = d.data('id');
            $.post({
                url: '{{route('backend.ajax.removeImage')}}',
                data: {id: file_id, _token: $('[name="_token"]').val()},
                dataType: 'json',
                success: function (data) {
                    d.parent().remove();
                }
            });
        }

        function appendImage(id, src, parent_div, input_name) {
            $(parent_div).append('<div class="thumb-nail-view">\n' +
                '        <img src="' + src + '" class="img-thumbnail"/>\n' +
                '        <input name="' + input_name + '" value="' + id + '" type="hidden"/>\n' +
                '        <a href="javascript:;" onclick="removeFile($(this))" data-id="' + id + '">Xóa</a>\n' +
                '    </div>')
        }
    </script>
@stop