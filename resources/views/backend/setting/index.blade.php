@extends('backend.layouts.main')

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{$subtitle}}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            {{ Breadcrumbs::render('backend.setting.index') }}

        </div>

        <div class="col-md-12">
            <div class="card card-outline-info">
                <div class="card-body">

                    <form class="form-horizontal form-bordered" action="" method="post">

                        @include('backend.partials.msg')
                        @include('backend.partials.errors')

                        {{ csrf_field() }}

                        @foreach($settings as $v)
                            <div class="x" @if(!$v['visible']) style="display: none" @endif>
                                <div class="form-group row {{ isset(Session::get('my_errors')[$v['setting_key']]) ? 'has-danger' : ''}}">

                                    <label class="control-label text-left col-md-2" for="{{$v['setting_key']}}">
                                        {{$v['setting_desc']}} @if($v['require'])
                                            <span style="color: red"> (*)</span> @endif
                                    </label>

                                    <div class="col-md-10">
                                        @if($v['setting_type'] == 'textarea')
                                            <textarea name="{{$v['setting_key']}}" class="form-control" rows="3"
                                                      placeholder="{{$v['setting_desc']}}"
                                                      id="{{$v['setting_key']}}">{{html_entity_decode($v['setting_value'])}}</textarea>
                                        @else
                                            <input type="text" name="{{$v['setting_key']}}"
                                                   value="{{html_entity_decode($v['setting_value'])}}"
                                                   class="form-control" id="{{$v['setting_key']}}"
                                                   placeholder="{{$v['setting_desc']}}">
                                        @endif

                                        @if(isset(Session::get('my_errors')[$v['setting_key']]))
                                            <small class="form-control-feedback">{{Session::get('my_errors')[$v['setting_key']]}}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <div class="form-group">
                                    <button class="btn btn-info" type="submit">Cập nhật</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection