<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="{{Route('backend.dashboard')}}">
                <!-- Logo icon -->
                <b>
                    <img src="{{ asset('/storage/backend/assets') }}/images/logo.png" height="40" alt="homepage" class="light-logo"/>
                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                {{--<span>--}}
                {{--<img src="{{ asset('/storage/backend/assets') }}/images/logo-text.png" height="20" class="light-logo" alt="homepage"/>--}}
                {{--</span>--}}
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto mt-md-0">
                <!-- This is  -->
                <li class="nav-item">
                    <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a>
                </li>

                <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href=""
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-message"></i>
                        <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                    </a>
                </li>
                <!-- ============================================================== -->
                <!-- End Comment -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Messages -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-email"></i>
                        <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                    </a>
                </li>
                <!-- ============================================================== -->
                <!-- End Messages -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Messages -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown mega-dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href=""
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-view-grid"></i></a>
                </li>
                <!-- ============================================================== -->
                <!-- End Messages -->
                <!-- ============================================================== -->
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">

                <!-- Profile -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark"
                       href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ Auth()->guard('backend')->user()->avatar_src() }}" alt="user" class="profile-pic"/></a>
                    <div class="dropdown-menu dropdown-menu-right scale-up">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img">
                                        <img src="{{ Auth()->guard('backend')->user()->avatar_src() }}" alt="user">
                                    </div>
                                    <div class="u-text">

                                        <h4>{{Auth()->guard('backend')->user()->fullname}}</h4>
                                        <p class="text-muted">{{Auth()->guard('backend')->user()->phone}}</p>

                                        <a href="{{route('backend.users.profile')}}"
                                           class="btn btn-rounded btn-danger btn-sm">Thông tin tài khoản</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{route('backend.logout')}}"><i class="fa fa-power-off"></i> Thoát</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-home"></i>
                        <span class="hide-menu">Sản phẩm</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">

                        @if(auth()->guard('backend')->user()->can('products.index'))
                            <li><a href="{{Route('backend.products.index')}}">Tất cả</a></li>
                        @endif

                        @if(auth()->guard('backend')->user()->can('products.type.index'))
                            <li><a href="{{Route('backend.products.type.index')}}">Danh mục sản phẩm</a></li>
                        @endif

                    </ul>
                </li>

                @if(auth()->guard('backend')->user()->can('banners.index')||1)
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-image"></i>
                            <span class="hide-menu">Banner</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            @if(auth()->guard('backend')->user()->can('banners.index')||1)
                                <li><a href="{{Route('backend.banners.index')}}">Danh sách</a></li>
                            @endif

                            @if(auth()->guard('backend')->user()->can('banners.index')||1)
                                <li><a href="{{Route('backend.banners.add')}}">Thêm mới</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(auth()->guard('backend')->user()->can('province.index')||auth()->guard('backend')->user()->can('district.index')
                ||auth()->guard('backend')->user()->can('ward.index')||auth()->guard('backend')->user()->can('street.index'))
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-map-marker-multiple"></i>
                            <span class="hide-menu">Khu vực</span></a>
                        <ul aria-expanded="false" class="collapse">
                            @if(auth()->guard('backend')->user()->can('province.index'))
                                <li><a href="{{Route('backend.location.province.index')}}">Tỉnh/TP </a></li>
                            @endif
                            @if(auth()->guard('backend')->user()->can('district.index'))
                                <li><a href="{{Route('backend.location.district.index')}}">Quận/Huyện</a></li>
                            @endif
                            @if(auth()->guard('backend')->user()->can('ward.index'))
                                <li><a href="{{Route('backend.location.ward.index')}}">Phường/Xã</a></li>
                            @endif
                            {{--@if(auth()->guard('backend')->user()->can('street.index'))--}}
                            {{--<li><a href="{{Route('backend.location.street.index')}}">Tên đường</a></li>--}}
                            {{--@endif--}}
                        </ul>
                    </li>
                @endif

                @if(auth()->guard('backend')->user()->can('posts.index'))
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-document"></i>
                            <span class="hide-menu">Bài viết</span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            @if(auth()->guard('backend')->user()->can('posts.index'))
                                <li><a href="{{Route('backend.posts.index')}}">Danh sách</a></li>
                            @endif

                            @if(auth()->guard('backend')->user()->can('posts.index'))
                                <li><a href="{{Route('backend.posts.add')}}">Thêm mới</a></li>
                            @endif

                            @if(auth()->guard('backend')->user()->can('posts.category.index'))
                                <li><a href="{{Route('backend.posts.category.index')}}">Danh mục bài viết</a></li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(auth()->guard('backend')->user()->can('notification.index'))
                    <li>
                        <a class="waves-effect" href="{{Route('backend.notification.index')}}">
                            <i class="mdi mdi-bell"></i><span class="hide-menu">Thông báo</span>
                        </a>
                    </li>
                @endif

                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Cài đặt</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{Route('backend.setting.index')}}">Chung </a></li>
                        {{--<li><a href="{{Route('backend.menu.index')}}">Menu </a></li>--}}
                    </ul>
                </li>

                @if(auth()->guard('backend')->user()->can('staff.index'))
                    <li>
                        <a class="waves-effect" href="{{Route('backend.staff.index')}}">
                            <i class="mdi mdi-lock"></i><span class="hide-menu">Quản trị viên</span>
                        </a>
                    </li>
                @endif

                @if(auth()->guard('backend')->user()->can('users.index'))
                    <li>
                        <a class="waves-effect" href="{{Route('backend.users.index')}}">
                            <i class="mdi mdi-account-multiple"></i><span class="hide-menu">Tài khoản</span>
                        </a>
                    </li>
                @endif
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->