@extends('frontend.layouts.frontend')

@section('content')
    <main class="main">

        @include('frontend.parts.breadcrumb')

        <div class="container">

            <div class="row mt-4">
                <div class="col-md-12">
                    @include('frontend.parts.msg')
                    @include('frontend.parts.errors')
                </div>

                <div class="card  no-border margin-bottom-2 w-100">
                    <div class="card-body no-min-height  shadow-sm">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <tbody>
                                    <tr>
                                        <td colspan="4" class="align-middle">
                                            <span class="dia-chi-nhan-hang"><i class="fas fa-map-marker-alt"></i> Địa Chỉ Nhận Hàng</span>
                                        </td>
                                    </tr>

                                    @php
                                        $address_id=null;
                                    @endphp

                                    @forelse($address as $adds)
                                        @if((empty($address_id)&&$adds->is_default_recipient)||$address_id==$adds->id)
                                            @php
                                                $address_id=$adds->id;
                                            @endphp

                                            <tr class="text-left selected_address">
                                                <td class="align-middle" colspan="2">
                                                    <b>{{$adds->name}} {{$adds->phone}}</b>
                                                    &nbsp;&nbsp;
                                                    {{$adds->full_address}}
                                                </td>
                                                <td class="align-middle color-don-gia" style="width: 10%">Mặc Định</td>
                                                <td class="align-middle" style="width: 30%">
                                                    <a href="#" class="color-thay-doi btn_change_address">
                                                        THAY ĐỔI</a>
                                                </td>
                                            </tr>
                                            @php break; @endphp;
                                        @endif
                                    @empty
                                        <tr class="text-left selected_address">
                                            <td class="align-left" colspan="4">
                                                <button type="button" class="btn btn-default address_btn flat address_btn_add">
                                                    + Thêm địa chỉ mới
                                                </button>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>

                            <div class="select_address" style="display: none">
                                <br>
                                <form action="" method="post" id="select_address_form">
                                    @csrf
                                    <input type="hidden" name="submit_type" value="change_address"/>
                                    @foreach($address as $adds)
                                        <label class="radio_container"><b>{{$adds->name}} {{$adds->phone}}</b>
                                            &nbsp;&nbsp; {{$adds->full_address}}
                                            <input type="radio" {!! (empty($address_id)&&$adds->is_default_recipient)||$address_id==$adds->id?'checked="checked"':'' !!}
                                            name="address_id" value="{{$adds->id}}">
                                            <span class="checkmark"></span>
                                        </label>
                                    @endforeach
                                </form>
                                <br>

                                <button type="button" class="btn btn-success btn-b2b address_btn flat address_btn_accept">
                                    Đồng ý
                                </button>
                                <button type="button" class="btn btn-default address_btn flat address_btn_back">Trở
                                    lại
                                </button>
                                <button type="button" class="btn btn-default address_btn flat address_btn_add">+ Thêm
                                    địa chỉ mới
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="card no-border no-margin-bottom w-100">
                    <div class="card-body no-min-height no-padding-bottom shadow-sm">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Sản phẩm</th>
                                        <th scope="col" class="color-so-luong" style="width: 10%">Đơn giá</th>
                                        <th scope="col" class="color-so-luong" style="width: 20%">Số lượng</th>
                                        <th scope="col" class="color-so-luong" style="width: 8%">Thành tiền</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @php
                $total_product = 0;
                $total_quantity = 0;
                $total_price = 0;
                $total_shipping_fee = 0;
            @endphp

            <form action="" method="post">
                @csrf

                <input type="hidden" name="address_id" value="{{$address_id}}"/>

                @foreach($baskets as $store)

                    @php
                        $total_product_store = 0;
                        $total_quantity_store = 0;
                        $total_price_store = 0;
                        $total_shipping_fee_store = 0;
                    @endphp

                    <div class="row">
                        <div class="card no-border no-margin-bottom w-100">
                            <div class="card-body no-min-height no-padding-bottom shadow-sm">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td colspan="4">
                                                    <span>
                                                        <a href="{{route('frontend.store.index',$store->username)}}" target="_blank">
                                                        <i class="fas fa-store"></i>
                                                            {{$store->username}}
                                                        </a>
                                                    </span>
                                                    <span style="color: #E9E9E9;"> | </span>
                                                    <a href="#!" class="badge-primary-chat-checkout">
                                                        <i class="far fa-comment-dots"></i> Chat ngay </a>
                                                </td>
                                            </tr>

                                            @foreach($store->products as $product)
                                                @php
                                                    $total_product +=1;
                                                    $total_quantity += $product['quantity'];
                                                    $total_price += $product['quantity']*$product['price'];

                                                    $total_product_store+=1;
                                                    $total_quantity_store+= $product['quantity'];
                                                    $total_price_store+=$product['quantity']*$product['price'];

                                                    $link = route('frontend.product.detail',['slug'=>\App\Utils\Filter::setSeoLink($product['name']), 'id'=>$product['product_id']]);
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <div class="media">
                                                            <a href="{{$link}}" target="_blank">
                                                                <img src="{{$product['thumbnail']['file_src']}}" class="align-self-end mr-3 media-img-checkout" alt="...">
                                                            </a>
                                                            <div class="media-body">
                                                                <a href="{{$link}}" target="_blank">
                                                                    <h5 class="mt-1 mb-0 mt-title-customize">{{$product['name']}}</h5>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="width: 10%">
                                                        {{number_format($product['price'])}}
                                                        <sup class="dong">đ</sup>
                                                    </td>
                                                    <td style="width: 20%" class="pl-5">{{$product['quantity']}}</td>
                                                    <td style="width: 8%" class="text-right">
                                                        {{number_format($product['price']*$product['quantity'])}}
                                                        <sup class="dong">đ</sup>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="card w-100 shadow-sm  no-margin-bottom card-body-checkout">
                            <div class="card-body no-min-height no-padding">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td class="align-middle"><span class="float-left">Lời nhắn:</span>
                                                <input type="text" name="notes[{{$store->id}}]"
                                                       id="msg" class="form-control info-email info-email-with float-right">
                                            </td>
                                            <td class="text-center don-vi-van-chuyen">Đơn vị vận chuyển:</td>
                                            <td>
                                                <p class="mx-0 my-0">Giao Hàng Tiết Kiệm</p>
                                                <p>Giao hàng trong 2-4 ngày</p>
                                                <input type="hidden" name="shipping_methods[{{$store->id}}]" value="1"/>
                                            </td>
                                            <td><a href="#" class="color-thay-doi">THAY ĐỔI</a></td>
                                            <td style="width: 11%;text-align: center"><p>
                                                    <span>{{number_format($total_shipping_fee_store)}}</span>
                                                    <sup class="dong">đ</sup>
                                                </p></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="card w-100 shadow-sm margin-bottom-2 card-body-checkout">
                            <div class="card-body no-min-height text-right">
                                <div class="d-flex justify-content-end">
                                    <div class="">Tổng số tiền ({{number_format($total_product_store)}} sản phẩm ):
                                    </div>
                                    <div class="pr-5 pl-5 price-order">
                                        <span>{{number_format($total_price_store)}}</span><sup class="dong">đ</sup>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach

                <div class="row">
                    <div class="card w-100 no-margin-bottom no-border shadow-sm">
                        <div class="card-header card-header-checkout card-header-background">
                            <div class="d-flex">
                                <div class="p-3 flex-md-grow-1">Phương thức thanh toán</div>
                                <div class="p-3 mr-5">Thanh toán khi nhận hàng</div>
                                <div class="p-3"><a href="#" class="color-thay-doi">THAY ĐỔI</a></div>
                            </div>
                        </div>
                        <div class="card-body no-min-height card-body-background-order">
                            <div class="d-flex justify-content-end">
                                <div class="p-2 mr-5">Tổng tiền hàng</div>
                                <div class="p-2 ml-5" style="min-width: 150px">{{number_format($total_price)}}
                                    <sup class="dong">đ</sup></div>
                            </div>

                            <div class="d-flex justify-content-end">
                                <div class="p-2 mr-5">Phí vận chuyển</div>
                                <div class="p-2 ml-5" style="min-width: 150px">{{number_format($total_shipping_fee)}}
                                    <sup class="dong">đ</sup>
                                </div>
                            </div>

                            <div class="d-flex justify-content-end">
                                <div class="p-2 mr-5">Tổng thanh toán:</div>
                                <div class="p-2 ml-5" style="min-width: 150px">
                                    <span class="tong-tien-hang">
                                    {{number_format($total_price+$total_shipping_fee)}}
                                        <sup class="dong">đ</sup>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="card w-100 shadow-sm card-body-checkout-total">
                        <div class="card-body no-min-height">
                            <div class="d-flex justify-content-end">
                                <div class="p-2">
                                    <button type="submit" name="submit_type" value="checkout" class="btn btn-primary btn-lg rounded  mr-1 xem-danh-gia-shop">
                                        Đặt Hàng
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>

    </main><!-- End .main -->
@endsection