@extends('frontend.layouts.frontend')

@section('content')

    @include('frontend.parts.breadcrumb')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="product-single-container product-single-default">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 product-single-gallery">
                            <div class="product-slider-container product-item">
                                <div class="product-single-carousel owl-carousel owl-theme">
                                    @foreach($product->images as $img)
                                        <div class="product-item">
                                            <img class="product-single-image" src="{{$img->file_src}}" data-zoom-image="{{$img->file_src}}"/>
                                        </div>
                                    @endforeach
                                </div>

                                <span class="prod-full-screen">
                                    <i class="icon-plus"></i>
                                </span>
                            </div>

                            <div class="prod-thumbnail row owl-dots" id='carousel-custom-dots'>
                                @foreach($product->images as $img)
                                    <div class="col-3 owl-dot">
                                        <img src="{{$img->file_src}}"/>
                                    </div>
                                @endforeach
                            </div>
                        </div><!-- End .col-lg-7 -->

                        <div class="col-lg-8 col-md-6">
                            <div class="product-single-details">
                                <h1 class="product-title">{{$product->name}}</h1>

                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:60%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->

                                    <a href="#" class="rating-link">( 6 đánh giá )</a>
                                </div><!-- End .product-container -->

                                <div class="price-box">
                                    @if($product->price_old)
                                        <span class="old-price">{{number_format($product->price_old)}} đ</span>
                                    @endif
                                    <span class="product-price">{{number_format($product->price)}} đ</span>
                                </div><!-- End .price-box -->

                                <div class="product-desc">
                                    <p>{{$product->description}}</p>

                                    <p>Vận chuyển: Miễn Phí Vận Chuyển khi đạt mức giá trị đơn hàng tối
                                        thiểu</p>

                                </div><!-- End .product-desc -->

                                <div class="product-filters-container mb-1" style="min-height: 70px">
                                    {{--<div class="product-single-filter">
                                        <label>Màu:</label>
                                        <ul class="config-swatch-list">
                                            <li class="active">
                                                <a href="#" style="background-color: #6085a5;"></a>
                                            </li>
                                            <li>
                                                <a href="#" style="background-color: #ab6e6e;"></a>
                                            </li>
                                            <li>
                                                <a href="#" style="background-color: #b19970;"></a>
                                            </li>
                                            <li>
                                                <a href="#" style="background-color: #11426b;"></a>
                                            </li>
                                        </ul>
                                    </div><!-- End .product-single-filter -->

                                    <div class="product-single-filter">
                                        <label>Sizes:</label>
                                        <ul class="config-size-list">
                                            <li class="active"><a href="#">S</a></li>
                                            <li><a href="#">M</a></li>
                                            <li><a href="#">L</a></li>
                                            <li><a href="#">XL</a></li>
                                        </ul>
                                    </div>--}}
                                </div>
                                <p class="inventory">Kho: {{number_format($product->inventory)}}</p>
                                <div class="product-action product-all-icons">

                                    @if(empty($user_data)||$user_data->id!=$product->user_id)
                                        <div class="product-single-qty">
                                            <input class="horizontal-quantity form-control quantity" type="text" data-max="{{$product->inventory}}">
                                        </div>

                                        <a href="javascript:;" class="paction add-cart" title="Thêm vào giỏ" data-id="{{$product->id}}">
                                            <span>Thêm vào giỏ hàng</span>
                                        </a>
                                        <a href="javascript:;" class="paction add-wishlist" title="Yêu thích">
                                            <span>Yêu thích</span>
                                        </a>
                                    @else
                                        <a href="{{route('frontend.store.products.update').'?id='.$product->id}}"
                                           class="paction edit-product" title="Sửa sản phẩm">
                                            <span style="display: inline">Sửa sản phẩm</span>
                                        </a>
                                    @endif

                                </div>

                            </div><!-- End .product-single-details -->
                        </div><!-- End .col-lg-5 -->
                    </div><!-- End .row -->
                </div><!-- End .product-single-container -->

            </div><!-- End .col-lg-12 -->
        </div><!-- End .row -->

        <div class="row mb-5" style="background: #f5f5f5;">

            <div class="col-lg-4 p-3">
                <div class="shop-avatar-w">
                    <img src="{{$product->store->avatar_src()}}" class="shop_avatar"/>
                </div>

                <div class="shop-info ml-3 pl-3 border-left">
                    <h4 class="text-primary">{{$product->store->username}}</h4>
                    <h5 class="text-info mb-0">Online 2 Phút Trước</h5>

                    <p class="text-left m-0">
                        <a href="#" class="btn btn-sm btn-light btn-primary">
                            Chat ngay
                        </a>
                        <a href="{{route('frontend.store.index',$product->store->username)}}" class="btn btn-sm btn-light">
                            Xem shop
                        </a>
                    </p>
                </div>
            </div>

            <div class="col-lg-2 p-3">
                <div class="shop-info-item border-left ml-3 pl-3 ">
                    <p class="mb-1 mt-1">Đánh giá: 1000</p>
                    <p class="mb-1">Sản phẩm: 1000</p>
                </div>
            </div>

            <div class="col-lg-3 p-3">
                <div class="shop-info-item border-left ml-3 pl-3 ">
                    <p class="mb-1 mt-1">Tỉ lệ phản hồi: 90%</p>
                    <p class="mb-1">Thời gian phản hồi: trong vài giờ</p>
                </div>
            </div>

            <div class="col-lg-3 p-3">
                <div class="shop-info-item border-left ml-3 pl-3 ">
                    <p class="mb-1 mt-1">Tham gia: 12 tháng trước</p>
                    <p class="mb-1">Người theo dõi: 200</p>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-lg-10 col-md-10">
                <div class="product-single-tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="product-tab-desc" data-toggle="tab" href="#product-desc-content"
                               role="tab" aria-controls="product-desc-content" aria-selected="true">Chi tiết
                                sản phẩm</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel" aria-labelledby="product-tab-desc">
                            <table class="table table-info-product">
                                <tbody>
                                    <tr>
                                        <td>Danh Mục</td>
                                        <td>Thời Trang > Thời Trang Nữ</td>
                                    </tr>
                                    <tr>
                                        <td>Thương hiệu</td>
                                        <td>Lorem ipsum</td>
                                    </tr>
                                    <tr>
                                        <td>Chất liệu</td>
                                        <td>Lorem ipsum</td>
                                    </tr>
                                    <tr>
                                        <td>Kho</td>
                                        <td>Lorem ipsum</td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="product-desc-content">
                                {{$product->detail}}
                            </div>

                        </div><!-- End .tab-pane -->
                    </div><!-- End .tab-content -->
                </div><!-- End .product-single-tabs -->

                <div class="product-single-tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#!">Đánh giá sản phẩm</a>
                        </li>
                    </ul>

                </div><!-- End .product-single-tabs -->

                @if(count($products_shop))
                    <h2 class="carousel-title">Các sản phẩm khác của shop</h2>
                    <div class="detail-featured-products owl-carousel owl-theme owl-dots-top">
                        @foreach($products_shop as $product)
                            @include('frontend.template.product')
                        @endforeach
                    </div>
                @endif

                @if(count($products_same_category))
                    <h2 class="carousel-title">Các sản phẩm tương tự</h2>
                    <div class="detail-featured-products owl-carousel owl-theme owl-dots-top">
                        @foreach($products_same_category as $product)
                            @include('frontend.template.product')
                        @endforeach
                    </div>
                @endif

            </div>

            @if(count($top_sale_products))
                <div class="col-lg-2 col-md-2">
                    <div class="widget widget-featured">
                        <h3 class="widget-title widget-title-h4">Sản phẩm bán chạy</h3>
                        <div class="widget-body">
                            @foreach($top_sale_products as $product)
                                @include('frontend.template.productTwo')
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div><!-- End .container -->

@endsection