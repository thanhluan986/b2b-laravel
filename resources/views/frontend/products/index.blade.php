@extends('frontend.layouts.frontend')

@section('content')

    @include('frontend.parts.breadcrumb')

    <div class="container">
        <div class="row">
            <div class="col-lg-9">

                @include('frontend.parts.bannerPromo')

                <nav class="toolbox">
                    <div class="toolbox-left">
                        <div class="toolbox-item toolbox-sort">
                            <div class="select-custom">
                                <select name="orderby" class="form-control">
                                    <option value="menu_order" selected="selected">Sắp xếp mặc định</option>
                                    <option value="popularity">Phổ biến</option>
                                    <option value="rating">Đánh giá</option>
                                    <option value="date">Mới nhất</option>
                                    <option value="date">Cũ nhất</option>
                                    <option value="price">Giá tăng dần</option>
                                    <option value="price-desc">Giá giảm dần</option>
                                </select>
                            </div><!-- End .select-custom -->

                            <a href="#" class="sorter-btn" title="Set Ascending Direction"><span class="sr-only">Set Ascending Direction</span></a>
                        </div><!-- End .toolbox-item -->
                    </div><!-- End .toolbox-left -->

                    <div class="toolbox-item toolbox-show">
                        <label>Hiển thị {{number_format($products->firstItem())}}
                            – {{number_format($products->lastItem())}} trong {{number_format($products->total())}} kết
                            quả</label>
                    </div>

                    <div class="layout-modes">
                        <a href="#" class="layout-btn btn-grid active" title="Lưới">
                            <i class="icon-mode-grid"></i>
                        </a>
                        <a href="#" class="layout-btn btn-list" title="Danh sách">
                            <i class="icon-mode-list"></i>
                        </a>
                    </div>
                </nav>

                @if(!empty($keywords))
                    <p>Kết quả tìm kiếm cho từ khóa <strong>"{{$keywords}}"</strong></p>
                @endif

                <div class="row row-sm">
                    @foreach($products as $product)
                        <div class="col-sm-6 col-md-3">
                            @include('frontend.template.product')
                        </div>
                    @endforeach
                </div>

                <nav class="toolbox toolbox-pagination">
                    <div class="toolbox-item toolbox-show">
                        <label>Hiển thị {{number_format($products->firstItem())}}
                            – {{number_format($products->lastItem())}} trong {{number_format($products->total())}} kết
                            quả</label>
                    </div>
                    {{$products->links()}}
                </nav>
            </div>

            @include('frontend.parts.sidebarCategory')
        </div>
    </div>

    <div class="mb-5"></div>
@endsection