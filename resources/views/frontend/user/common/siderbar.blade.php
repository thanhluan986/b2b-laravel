<div class="col col-md-3 mt-4" style="min-height: 800px">
    <div class="media">
        <i class="fal fa-user-circle fa-4x align-self-center mr-3" style="display: none"></i>
        <img src="{{$user_data->avatar_src}}" style="width: 70px ;height: 70px"
             class="align-self-center rounded-circle mr-3">
        <div class="media-body">
            <h5 class="mt-1 mb-0">{{$user_data->username}}</h5>
            <p class="mb-0"><a href="{{route('frontend.user.profile')}}"><i class="far fa-edit"></i> Sửa Hồ Sơ</a></p>
        </div>
    </div>
    <hr class="info-line">
    <ul class="list-group list-group-item-address">

        <li class="list-group-item address-info-profile">
            <span style="color: #FFC107;"><i class="fas fa-user-circle fa-lg"></i> </span>
            <a href="{{route('frontend.user.profile')}}"> Tài khoản của tôi</a>
        </li>

        <li class="list-group-item address-info">
            <a href="{{route('frontend.user.profile')}}"
               class="{{isset($menu_active)&&$menu_active=='profile'?'info-active':''}}"> Hồ sơ </a>
        </li>
        <li class="list-group-item address-info">
            <a href="{{route('frontend.user.orders')}}"
               class="{{isset($menu_active)&&$menu_active=='my_orders'?'info-active':''}}"> Đơn mua </a>
        </li>

        <li class="list-group-item address-info-profile">
            <span class="fa-stack bell-color" style="color: #FFC107;">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fal fa-shopping-cart fa-stack-1x fa-inverse"></i>
            </span>
            <a href="#!"> Bán hàng</a>
        </li>

        <li class="list-group-item address-info">
            <a href="{{route('frontend.store.products.index')}}"
               class="{{isset($menu_active)&&$menu_active=='products'?'info-active':''}}"> Sản phẩm </a>
        </li>

        <li class="list-group-item address-info">
            <a href="{{route('frontend.store.orders.index')}}"
               class="{{isset($menu_active)&&$menu_active=='orders'?'info-active':''}}"> Đơn bán </a>
        </li>

        <li class="list-group-item address-info-profile">
            <span class="fa-stack bell-color" style="color: #FFC107;">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fal fa-map-marker fa-stack-1x fa-inverse"></i>
            </span>
            <a href="{{route('frontend.address.index')}}"
               class="{{isset($menu_active)&&$menu_active=='address'?'info-active':''}}"> Địa Chỉ </a>
        </li>

        <li class="list-group-item address-info-profile">
            <span class="fa-stack vi-color" style="color: #FFC107;">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fas fa-money-check fa-stack-1x fa-inverse"></i>
            </span>
            <a href="#"> Ngân hàng </a>
        </li>

        <li class="list-group-item address-info-profile">
            <span class="fa-stack bell-color" style="color: #FFC107;">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fal fa-bell fa-stack-1x fa-inverse"></i>
            </span>
            <a href="#"> Thông Báo </a>
        </li>
        <li class="list-group-item address-info-profile">
             <span class="fa-stack vi-color" style="color: #FFC107;">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fas fa-tags fa-stack-1x fa-inverse"></i>
            </span>
            <a href="#"> Voucher </a>
        </li>
        <li class="list-group-item address-info-profile">
            <span class="fa-stack vi-color" style="color: #FFC107;">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fas fa-wallet fa-stack-1x fa-inverse"></i>
            </span>
            <a href="#"> Ví</a>
        </li>
    </ul>

</div>
