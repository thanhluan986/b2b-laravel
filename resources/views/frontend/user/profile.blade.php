@extends('frontend.layouts.frontend')

@section('content')
    <main class="main">

        @include('frontend.parts.breadcrumb')

        <div class="container">
            <div class="row">

                @include('frontend.user.common.siderbar')

                <div class="col col-md-9 mt-3">
                    <div class="card shadow-sm bg-white rounded w-100 card-info">
                        <div class="card-header card-info-header">
                            <h4 class="mt-2 mr-info">Hồ sơ Của Tôi</h4>
                            <span class="sub-info-header">Quản lý thông tin để bảo mật tài khoản</span>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <form action="" method="post" style="display: flex; flex-wrap: wrap; width: 100%;">
                                    @csrf
                                    <div class="col-md-8 border-right-profile">
                                        @include('frontend.parts.msg')
                                        @include('frontend.parts.errors')

                                        <div class="table-responsive">
                                            <table class="table table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td class="text-right td-width-20">Tài khoản</td>
                                                        <td>{{$user_data->username}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right td-width-20">Email</td>
                                                        <td class="align-middle">
                                                            {{$user_data->email}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right align-middle">Số Điện Thoại</td>
                                                        <td class="align-middle">
                                                            <input type="text" class="form-control info-email" id="phone"
                                                                   name="phone"
                                                                   value="{{$user_data->phone}}">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right align-middle">Tên Shop</td>
                                                        <td class="align-middle">
                                                            <input type="text" class="form-control info-email" id="fullname"
                                                                   name="fullname"
                                                                   value="{{$user_data->fullname}}">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">Giới Tính</td>
                                                        <td>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="gender1" name="gender"
                                                                       {{$user_data->gender==1?'checked="checked"':''}}
                                                                       class="custom-control-input" value="1">
                                                                <label class="custom-control-label" for="gender1">Nam</label>
                                                            </div>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="gender2" name="gender"
                                                                       {{$user_data->gender==2?'checked="checked"':''}}
                                                                       class="custom-control-input" value="2">
                                                                <label class="custom-control-label" for="gender2">Nữ</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">Ngày Sinh</td>
                                                        <td class="align-middle">
                                                            <input type="date" class="form-control info-email" id="birthday"
                                                                   name="birthday"
                                                                   value="{{$user_data->birthday}}">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="text-right"></td>
                                                        <td class="align-middle">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                       {{!empty(old('password'))?'checked="checked"':''}}
                                                                       id="my-change-pass-checkbox" value="1">
                                                                <label class="custom-control-label" for="my-change-pass-checkbox">Đổi
                                                                    mật khẩu</label>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr class="change_pass {{empty(old('password'))?'hidden':''}}">
                                                        <td class="text-right">Mật khẩu cũ</td>
                                                        <td class="align-middle">
                                                            <input type="password" name="password_old" class="form-control info-email" id="password_old">
                                                        </td>
                                                    </tr>
                                                    <tr class="change_pass {{empty(old('password'))?'hidden':''}}">
                                                        <td class="text-right">Mật khẩu mới</td>
                                                        <td class="align-middle">
                                                            <input type="password" name="password" class="form-control info-email" id="password">
                                                        </td>
                                                    </tr>
                                                    <tr class="change_pass {{empty(old('password'))?'hidden':''}}">
                                                        <td class="text-right">Xác nhận MK</td>
                                                        <td class="align-middle">
                                                            <input type="password" name="password_confirmation" class="form-control info-email" id="password_confirmation">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <button type="submit" class="btn btn-primary rounded btn-sm btn-primary-save">
                                                                Lưu
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-md-4-upload">
                                        <img src="{{$user_data->avatar_src}}" class="rounded-circle mt-4 rounded-circle-img-upload">
                                        <p class="mt-2">
                                            <label for="upload-photo" class="btn btn-primary shadow-sm rounded btn-sm btn-upload">
                                                <input type="file" name="photo" id="upload-photo"/>
                                                Chọn Ảnh
                                            </label>
                                        </p>
                                        <p>
                                            Dung lượng file tối đa 2MB<br>
                                            Định dạng: jpg, png
                                        </p>
                                    </div>
                                </form>
                            </div>


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </main><!-- End .main -->
@endsection