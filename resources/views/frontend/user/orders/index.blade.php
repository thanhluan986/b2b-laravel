@extends('frontend.layouts.frontend')

@section('content')
    <main class="main">

        @include('frontend.parts.breadcrumb')

        <div class="container">
            <div class="row">

                @include('frontend.user.common.siderbar')
                <div class="col col-md-9 mt-3">

                    <div class="row mb-2">
                        <nav aria-label="breadcrumb" class="rounded w-100 shadow-sm">
                            <ol class="breadcrumb breadcrumb-order">
                                <li class="breadcrumb-item breadcrumb-item-customize">
                                    <a href="#">Chờ thanh toán</a>
                                </li>
                                <li class="breadcrumb-item breadcrumb-item-customize active">
                                    <a href="#" class="active">Chờ lấy hàng</a>
                                </li>
                                <li class="breadcrumb-item breadcrumb-item-customize">
                                    <a href="#">Đang giao</a>
                                </li>
                                <li class="breadcrumb-item breadcrumb-item-customize">
                                    <a href="#">Đã giao</a>
                                </li>
                                <li class="breadcrumb-item breadcrumb-item-customize">
                                    <a href="#">Đã Hủy</a>
                                </li>
                            </ol>
                        </nav>
                    </div>

                    @forelse($orders as $order)
                        <div class="row">
                            <div class="card shadow-sm bg-white rounded w-100 mb-0 card-info">
                                <div class="card-header card-info-header px-4 py-4">
                                    <div class="row">
                                        <div class="col col-md-6 text-left">
                                            <ul class="header-left-customize">
                                                <li>
                                                    <div class="media">
                                                        <img src="{{$order->store->avatar_src}}"
                                                             class="rounded-circle rounded-circle-img-order mr-1 mr-3" height="20" width="20" alt="...">
                                                        <div class="media-body">
                                                            <a href="{{route('frontend.store.index',$order->store->username)}}"
                                                               target="_blank">
                                                                <span class="mt-0">{{$order->store->username}}</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" class="badge badge-primary badge-primary-chat">
                                                        <i class="far fa-comment-dots"></i> Chat
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('frontend.store.index',$order->store->username)}}"
                                                       target="_blank"
                                                       class="badge badge-primary badge-primary-xem-shop">
                                                        <i class="fas fa-store"></i> Xem shop
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col col-md-6 align-middle text-right">
                                            <ul class="header-right-customize">
                                                <li>
                                                    <span class="da-giao-hang"><i class="fas fa-truck-moving"></i> Chờ lấy hàng</span>
                                                </li>
                                                <li><span class="da-hoi"> <i class="far fa-question-circle"></i> </span>
                                                </li>
                                                {{--<li><span class="da-danh-gia">ĐÃ ĐÁNH GIÁ</span></li>--}}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body no-min-height">

                                    @foreach($order->orders_detail as $product)
                                        <div class="row pb-4 border-bottom-order">
                                            <div class="col-md-10">
                                                <div class="media">
                                                    <img src="{{$product->thumbnail_src}}" class="align-self-end mr-3 media-img" alt="...">
                                                    <div class="media-body">
                                                        <h5 class="mt-0 mb-0 mt-title-customize">
                                                            {{$product->product_name}}
                                                        </h5>
                                                        <p class="mb-0">x {{$product->quantity}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                @if($product->price_old&&$product->price_old>$product->price)
                                                    <p class="mb-0">
                                                        <del>{{number_format($product->price_old)}}<sup> đ </sup></del>
                                                    </p>
                                                @endif
                                                <p class="price-order">{{number_format($product->price)}}<sup> đ </sup>
                                                </p>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="card w-100 shadow-sm rounded card-body-order">
                                <div class="card-body no-min-height">
                                    <div class="tong-tien-chi-tiet mb-1">
                                        <span class="tong-so-tien"><i class="fas fa-dollar-sign"></i> Tổng số tiền:</span>
                                        <span class="tong-tien">{{number_format($order->total_price)}}
                                            <sup> đ </sup></span>
                                    </div>
                                    <div>
                                        {{--<button type="button" class="btn btn-primary btn-sm rounded mr-1 xem-danh-gia-shop">--}}
                                        {{--Xem Đánh Giá Shop--}}
                                        {{--</button>--}}
                                        {{--<button type="button" class="btn btn-primary btn-sm rounded mr-1 xem-danh-gia-nguoi-mua">--}}
                                        {{--Xem Đánh Giá Người Mua--}}
                                        {{--</button>--}}
                                        <button type="button" class="btn btn-primary btn-sm rounded xem-chi-tiet-don-hang">
                                            Xem Chi Tiết Đơn Hàng
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @empty
                        <p>Chưa có dữ liệu</p>
                    @endforelse
                </div>

            </div>
        </div>
    </main><!-- End .main -->
@endsection