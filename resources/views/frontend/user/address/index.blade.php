@extends('frontend.layouts.frontend')

@section('content')
    <main class="main">

        @include('frontend.parts.breadcrumb')

        <div class="container">
            <div class="row">

                @include('frontend.user.common.siderbar')

                <div class="col col-md-9 mt-3">
                    <div class="card shadow-sm bg-white rounded w-100 card-info">
                        <div class="card-header card-info-header">
                            <h4 class="mt-1 mr-info">Địa Chỉ Của Tôi</h4>
                            <a class="btn btn-primary btn-sm rounded btn-add" href="{{route('frontend.address.add')}}">
                                <i class="fal fa-plus"></i>Thêm Địa Chỉ Mới
                            </a>
                        </div>
                        @foreach($address as $item)
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td class="text-right td-width-10">Tên</td>
                                                <td>{{$item->name}}
                                                    @if($item->is_default_recipient)
                                                        <span class="badge badge-success mb-1 badge-info-customize">Mặc Định</span>
                                                    @endif
                                                    @if($item->is_warehouse)
                                                        <span class="badge badge-pill badge-primary-info">Địa chỉ lấy hàng</span>
                                                    @endif
                                                    @if($item->is_return)
                                                        <span class="badge badge-pill badge-primary-info">Địa chỉ trả hàng</span>
                                                    @endif
                                                </td>
                                                <td class="text-right">
                                                    <a href="{{route('frontend.address.delete').'?id='.$item->id}}" onclick="return confirm('Bạn muốn xóa địa chỉ này?')">Xóa</a>
                                                    |
                                                    <a href="{{route('frontend.address.edit').'?id='.$item->id}}">Sửa</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right td-width-10">Số ĐT</td>
                                                <td class="align-middle">{{$item->phone}}</td>
                                                <td class="text-right">
                                                    @if(!$item->is_default_recipient)
                                                        {{--<span class="badge badge-primary badge-primary-thietlap px-3 py-2">Thiết Lập Mặc Định</span>--}}
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="text-right td-width-10">Địa Chỉ</td>
                                                <td class="align-middle" colspan="2">{{$item->full_address}}
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endforeach
                    </div>


                </div>
            </div>
        </div>
    </main><!-- End .main -->
@endsection