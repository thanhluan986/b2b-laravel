@extends('frontend.layouts.frontend')

@section('content')
    <main class="main">

        @include('frontend.parts.breadcrumb')

        <div class="container">
            <div class="row">

                @include('frontend.user.common.siderbar')

                <div class="col col-md-9 mt-3">
                    <div class="card shadow-sm bg-white rounded w-100 card-info">
                        <div class="card-header card-info-header">
                            <h4 class="mt-1 mr-info">Thêm địa chỉ mới</h4>
                        </div>

                        <div class="card-body">
                            <div class="col-md-6" style="float: none;margin: auto">
                                @include('frontend.parts.msg')
                                @include('frontend.parts.errors')
                                <form role="form" action="" method="post" id="form_address">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name"><span class="required">*</span> Họ tên </label>
                                        <input type="text" name="name"
                                               value="{{$address->name}}"
                                               class="form-control" id="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone"><span class="required">*</span> Số điện thoại</label>
                                        <input type="number" name="phone"
                                               value="{{$address->phone}}"
                                               class="form-control" id="phone" required>
                                    </div>

                                    <div class="form-group">
                                        <label><span class="required">*</span> Tỉnh/TP</label>
                                        <select class="form-control select_province select2" name="province_id" required>
                                            <option value="">Chọn</option>
                                            @foreach($provinces as $province)
                                                <option value="{{$province->id}}"
                                                        {!! old('province_id',$address->province_id)==$province->id?'selected="selected"':'' !!}>{{$province->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label><span class="required">*</span> Quận/Huyện</label>
                                        <select class="form-control select_district select2" name="district_id" required>
                                            <option value="">Chọn</option>
                                            @foreach($districts as $district)
                                                <option value="{{$district->id}}"
                                                        {!! old('district_id', $address->district_id)==$district->id?'selected="selected"':'' !!}>{{$district->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label><span class="required">*</span> Phường/Xã</label>
                                        <select class="form-control select_ward select2" name="ward_id" required>
                                            <option value="">Chọn</option>
                                            @foreach($wards as $ward)
                                                <option value="{{$ward->id}}"
                                                        {!! old('ward_id', $address->ward_id)==$ward->id?'selected="selected"':'' !!}>{{$ward->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="street_name">
                                            <span class="required">*</span> Địa chỉ cụ thể (số nhà, hẻm, tên đường...)
                                        </label>
                                        <textarea name="street_name" class="form-control" id="street_name" rows="3" required>{{$address->street_name}}</textarea>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="is_default_recipient"
                                                   {!! old('is_default_recipient', $address->is_default_recipient)==1?'checked="checked"':'' !!}
                                                   value="1"> Địa chỉ nhận
                                            hàng mặc định
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="is_warehouse"
                                                   {!! old('is_warehouse', $address->is_warehouse)==1?'checked="checked"':'' !!}
                                                   value="1"> Địa lấy hàng
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="is_return"
                                                   {!! old('is_return', $address->is_return)==1?'checked="checked"':'' !!}
                                                   value="1"> Địa chỉ trả hàng
                                        </label>
                                    </div>

                                    <div class="form-group" style="text-align: center">
                                        <button type="submit" class="btn btn-primary">Lưu</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </main><!-- End .main -->
@endsection