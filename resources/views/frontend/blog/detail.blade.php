@extends('frontend.layouts.frontend')

@section('content')

    @include('frontend.parts.breadcrumb')

    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <article class="entry single">
                    <div class="entry-body" style="margin-left:0">
                        <h2 class="entry-title">
                            {!! $post->name !!}
                        </h2>

                        <div class="entry-meta">
                            <span><i class="icon-calendar"></i>{{$post->created_at->format('d/m/Y')}}</span>
                            <span><i class="icon-user"></i>By <a href="javascript:;">Admin</a></span>
                        </div><!-- End .entry-meta -->

                        <div class="entry-content">
                            {!! $post->detail !!}
                        </div>

                        <div class="entry-share">
                            <h3>
                                <i class="icon-forward"></i>
                                Share
                            </h3>

                            <div class="social-icons">
                                <a href="#" class="social-icon social-facebook" target="_blank" title="Facebook">
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon social-twitter" target="_blank" title="Twitter">
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon social-linkedin" target="_blank" title="Linkedin">
                                    <i class="icon-linkedin"></i>
                                </a>
                                <a href="#" class="social-icon social-gplus" target="_blank" title="Google +">
                                    <i class="icon-gplus"></i>
                                </a>
                                <a href="#" class="social-icon social-mail" target="_blank" title="Email">
                                    <i class="icon-mail-alt"></i>
                                </a>
                            </div><!-- End .social-icons -->
                        </div><!-- End .entry-share -->

                    </div><!-- End .entry-body -->
                </article><!-- End .entry -->

                @include('frontend.blog.related')

            </div><!-- End .col-lg-9 -->

            <aside class="sidebar col-lg-3">
                @include('frontend.blog.sidebar')
            </aside><!-- End .col-lg-3 -->
        </div><!-- End .row -->
    </div><!-- End .container -->

    <div class="mb-6"></div><!-- margin -->

@endsection