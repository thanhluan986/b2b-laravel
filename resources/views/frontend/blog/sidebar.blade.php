<div class="sidebar-wrapper">
    <div class="widget widget-categories">
        <h4 class="widget-title">Danh mục</h4>

        <ul class="list">
            {{--<li><a href="#">All about clothing</a></li>--}}
            {!! \App\Utils\Category::sidebar_menu_category($tree_categories, $all_categories, $category_id)!!}
        </ul>
    </div><!-- End .widget -->

    <div class="widget">
        <h4 class="widget-title">Bài viết mới</h4>

        <ul class="simple-entry-list">
            @foreach($top_post as $item)
                @php $link = blog_link($item->slug,$item->id);@endphp
                <li>
                    <div class="entry-media">
                        <a href="{{$link}}" title="{{$item->name}}">
                            <img src="{{isset($item->image)?$item->image->file_src:asset('storage/frontend/assets/images/noimage_600x400.png')}}"
                                 alt="{{$item->name}}">
                        </a>
                    </div>
                    <div class="entry-info">
                        <a href="{{$link}}" title="{{$item->name}}">{{$item->name}}</a>
                        <div class="entry-meta">
                            {{$item->created_at->format('d/m/Y')}}
                        </div>
                    </div>
                </li>
            @endforeach

        </ul>
    </div><!-- End .widget -->

</div><!-- End .sidebar-wrapper -->