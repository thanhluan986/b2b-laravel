<div class="related-posts">
    <h4 class="light-title">Bài viết khác</h4>

    <div class="owl-carousel owl-theme related-posts-carousel">

        @foreach($top_post as $item)
            @php $link = blog_link($item->slug,$item->id);@endphp
            <article class="entry">
                <div class="entry-media">
                    <a href="{{$link}}" title="{{$item->name}}">
                        <img src="{{isset($item->image)?$item->image->file_src:asset('storage/frontend/assets/images/noimage_600x400.png')}}"
                             alt="{{$item->name}}">
                    </a>
                </div><!-- End .entry-media -->

                <div class="entry-body">
                    <div class="entry-date">
                        <span class="day">{{$item->created_at->format('d')}}</span>
                        <span class="month">{{$item->created_at->format('m')}}</span>
                    </div><!-- End .entry-date -->

                    <h2 class="entry-title">
                        <a href="{{$link}}" title="{{$item->name}}">{{$item->name}}</a>
                    </h2>

                    <div class="entry-content">
                        <p>{!! $item->excerpt !!}</p>
                    </div><!-- End .entry-content -->

                </div><!-- End .entry-body -->
            </article>

        @endforeach
    </div><!-- End .owl-carousel -->
</div><!-- End .related-posts -->