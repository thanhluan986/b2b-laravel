@extends('frontend.layouts.frontend')

@section('content')

    @include('frontend.parts.breadcrumb')

    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    @foreach($items as $item)
                        @php $link = blog_link($item->slug,$item->id);@endphp
                        <div class="col-md-6">
                            <article class="entry">
                                <div class="entry-media">
                                    <a href="{{$link}}" title="{{$item->name}}">
                                        <img src="{{isset($item->image)?$item->image->file_src:asset('storage/frontend/assets/images/noimage_600x400.png')}}"
                                             alt="{{$item->name}}">
                                    </a>
                                </div>

                                <div class="entry-body" style="margin-left: 0">
                                    <h2 class="entry-title">
                                        <a href="{{$link}}" title="{{$item->name}}">{{$item->name}}</a>
                                    </h2>

                                    <div class="entry-content">
                                        <p>{!! $item->excerpt !!}</p>
                                    </div>

                                    <div class="entry-meta">
                                        <span><i class="icon-calendar"></i>{{$item->created_at->format('d/m/Y')}}</span>
                                        <span><i class="icon-user"></i>By <a href="javascript:;">Admin</a></span>
                                        <span><a href="{{$link}}">Xem thêm <i
                                                        class="icon-angle-double-right"></i></a></span>
                                    </div>
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>

                <nav class="toolbox toolbox-pagination">

                    {{$items->links()}}

                </nav>
            </div>

            <aside class="sidebar col-lg-3">
                @include('frontend.blog.sidebar')
            </aside><!-- End .col-lg-3 -->
        </div><!-- End .row -->
    </div><!-- End .container -->

    <div class="mb-6"></div><!-- margin -->

@endsection