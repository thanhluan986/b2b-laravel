@extends('frontend.layouts.frontend')

@section('content')
    <main class="main">

        @include('frontend.parts.breadcrumb')

        <form action="" class="add-product-form form-horizontal form-bordered" method="post">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-9 mt-3">
                        <div class="card bg-white rounded w-100 card-info">
                            <div class="card-body card-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        @include('frontend.parts.msg')
                                        {{--@include('frontend.parts.errors')--}}
                                    </div>
                                    <div class="col-md-12">
                                        <h3>Thông tin cơ bản</h3>
                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="name">
                                                <span style="color: red">*</span> Tên sản phẩm</label>

                                            <div class="col-md-9">
                                                <input type="text" name="name" class="form-control" id="name" value="{{old('name', $product->name)}}" minlength="10" required>
                                                @if($errors->get('name'))
                                                    <span class="help-block error">
                                                         @foreach ($errors->get('name') as $message)
                                                            {{$message}} <br>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="description">
                                                <span style="color: red">*</span> Mô tả sản phẩm</label>

                                            <div class="col-md-9">
                                                <textarea class="form-control" name="description" placeholder="" id="description">{{old('description', $product->description)}}</textarea>
                                                @if($errors->get('description'))
                                                    <span class="help-block error">
                                                         @foreach ($errors->get('description') as $message)
                                                            {{$message}} <br>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        {{--<div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="detail">
                                                <span style="color: red">*</span> Chi tiết sản phẩm</label>

                                            <div class="col-md-9">
                                                <textarea class="form-control" placeholder="" name="detail"></textarea>
                                            </div>
                                        </div>--}}

                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="category_id">
                                                <span style="color: red">*</span> Danh mục</label>

                                            <div class="col-md-3">
                                                <select class="form-control select2 select_parent_id" id="category_id" name="parent_category_id" required>
                                                    <option value="">Chọn</option>
                                                    @foreach($array_categories as $category)
                                                        @php if(!empty($category['parent_id'])) continue; @endphp
                                                        <option value="{{$category['id']}}"
                                                                {{old('parent_category_id',$parent_category_id)==$category['id']?'selected="selected"':''}}>
                                                            {{$category['name']}}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->get('category_id'))
                                                    <span class="help-block error">
                                                         @foreach ($errors->get('category_id') as $message)
                                                            {{$message}} <br>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control select2x select_category_id" name="category_id" style="{{count($child_categories)>0?'':'display: none'}}">
                                                    @foreach($child_categories as $v)
                                                        <option value="{{$v->id}}" {{old('category_id', $product->category_id)==$v->id?'selected="selected"':''}}>{{$v->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="trademark_id">
                                                <span style="color: red">*</span> Thương hiệu</label>

                                            <div class="col-md-6">
                                                <select class="form-control select2" name="trademark_id" id="trademark_id" required>
                                                    <option value="">Chọn</option>
                                                    @foreach($trademarks as $v)
                                                        <option value="{{$v->id}}" {{old('trademark_id', $product->trademark_id)==$v->id?'selected="selected"':''}}>{{$v->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->get('trademark_id'))
                                                    <span class="help-block error">
                                                         @foreach ($errors->get('trademark_id') as $message)
                                                            {{$message}} <br>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        {{--<div class="form-group row align-items-center">--}}
                                        {{--<label class="control-label text-right col-md-3" for="material">--}}
                                        {{--Chất liệu--}}
                                        {{--</label>--}}

                                        {{--<div class="col-md-6">--}}
                                        {{--<input type="text" name="material" class="form-control" id="material">--}}
                                        {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="origin">
                                                <span style="color: red">*</span> Xuất xứ</label>

                                            <div class="col-md-6">
                                                <input type="text" name="origin" class="form-control" id="origin"
                                                       value="{{old('origin', $product->origin)}}" required>
                                                @if($errors->get('origin'))
                                                    <span class="help-block error">
                                                         @foreach ($errors->get('origin') as $message)
                                                            {{$message}} <br>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="factory_address">
                                                Địa chỉ nơi sản xuất
                                            </label>

                                            <div class="col-md-6">
                                                <input type="text" name="factory_address" class="form-control" id="factory_address"
                                                       value="{{old('factory_address', $product->factory_address)}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Thông tin bán hàng</h3>
                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="price">
                                                <span style="color: red">*</span> Giá</label>

                                            <div class="col-md-6">
                                                <input type="number" name="price" class="form-control" id="price"
                                                       value="{{old('price', $product->price)}}" required>
                                                @if($errors->get('price'))
                                                    <span class="help-block error">
                                                         @foreach ($errors->get('price') as $message)
                                                            {{$message}} <br>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="inventory">
                                                <span style="color: red">*</span> Kho hàng</label>

                                            <div class="col-md-6">
                                                <input type="number" min="1" name="inventory" class="form-control" id="inventory"
                                                       value="{{old('inventory', $product->inventory)}}" required>
                                                @if($errors->get('inventory'))
                                                    <span class="help-block error">
                                                         @foreach ($errors->get('inventory') as $message)
                                                            {{$message}} <br>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        {{--<div class="form-group row align-items-center">--}}
                                        {{--<label class="control-label text-right col-md-3" for="META_TITLE">--}}
                                        {{--Phân loại hàng--}}
                                        {{--</label>--}}

                                        {{--<div class="col-md-6">--}}

                                        {{--</div>--}}
                                        {{--</div>--}}

                                        {{--<div class="form-group row align-items-center">--}}
                                        {{--<label class="control-label text-right col-md-3" for="META_TITLE">--}}
                                        {{--Mua nhiều giảm giá--}}
                                        {{--</label>--}}

                                        {{--<div class="col-md-6">--}}

                                        {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Hình ảnh sản phẩm</h3>

                                        <div class="dropzone" id="myDropzone" action="{{route('frontend.ajax.image.upload')}}">
                                            <div class="dz-message">
                                                <div class="col-xs-8">
                                                    <div class="message">
                                                        <h6>Kéo thả tập tin vào hoặc Click để tải lên</h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="result sortable">
                                                @foreach($file_images as $file)
                                                    <div class="dz-preview dz-image-preview dz-success dz-complete my-preview">
                                                        <div class="dz-image">
                                                            <img data-dz-thumbnail="" src="{{$file->file_src}}">
                                                            <a href="javascript:;" onclick="removeFile($(this))" data-id="{{$file->id}}" title="xóa" class="image-remove"><i class="fa fa-trash"></i></a>
                                                        </div>
                                                        <input name="image_file_ids[]" value="{{$file->id}}" type="hidden">
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="fallback">
                                                <input type="file" name="file" multiple style="opacity: 0">
                                            </div>
                                        </div>

                                        @if($errors->get('image_file_ids'))
                                            <div class="form-group row align-items-center">
                                                <div class="col-md-12">
                                                    <span class="help-block error">
                                                         @foreach ($errors->get('image_file_ids') as $message)
                                                            {{$message}} <br>
                                                        @endforeach
                                                    </span>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Vận chuyển</h3>
                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="weight">
                                                <span style="color: red">*</span> Cân nặng (gram)</label>

                                            <div class="col-md-6">
                                                <input type="number" min="0" name="weight" class="form-control"
                                                       value="{{old('weight', $product->weight)}}" required>
                                                @if($errors->get('weight'))
                                                    <span class="help-block error">
                                                         @foreach ($errors->get('weight') as $message)
                                                            {{$message}} <br>
                                                        @endforeach
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3">
                                                Kích thước (cm)
                                            </label>

                                            <div class="col-md-2">
                                                <input type="number" min="0" name="width" class="form-control" placeholder="Rộng"
                                                       value="{{old('width', $product->width)}}">
                                            </div>
                                            <div class="col-md-2">
                                                <input type="number" min="0" name="length" class="form-control" placeholder="Dài"
                                                       value="{{old('length', $product->length)}}">
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" min="0" name="height" class="form-control" placeholder="Cao"
                                                       value="{{old('height', $product->height)}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body card-form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Thông tin khác</h3>
                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="must_pre_order">
                                                <span style="color: red">*</span> Đặt hàng trước</label>

                                            <div class="col-md-6">
                                                <input type="checkbox" name="must_pre_order" value="1" {{old('must_pre_order', $product->must_pre_order)==1?'checked="checked"':''}}/>
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="is_used_product">
                                                <span style="color: red">*</span> Tình trạng</label>

                                            <div class="col-md-6">
                                                <select class="form-control" name="is_used_product" id="is_used_product" required>
                                                    <option value="0" {{old('is_used_product', $product->is_used_product)===0?'selected="selected"':''}}>
                                                        Mới
                                                    </option>
                                                    <option value="1" {{old('is_used_product', $product->is_used_product)===1?'selected="selected"':''}}>
                                                        Đã sử dụng
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row align-items-center">
                                            <label class="control-label text-right col-md-3" for="sku">
                                                SKU sản phẩm
                                            </label>

                                            <div class="col-md-6">
                                                <input type="text" name="sku" class="form-control"
                                                       value="{{old('sku', $product->sku)}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-footer fixed-bottom">
                <div class="container">
                    <button type="button" class="btn btn-default">Hủy</button>
                    <button type="submit" class="btn btn-default" name="status" value="0">Lưu & Ẩn</button>
                    <button type="submit" class="btn btn-danger" name="status" value="1">Lưu & Hiển thị</button>
                </div>
            </div>
        </form>

    </main><!-- End .main -->
@endsection

@section('style')
    <link href="{{ asset('/storage/frontend')}}/assets/plugins/dropzone/min/basic.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/storage/frontend')}}/assets/plugins/dropzone/min/dropzone.min.css" rel="stylesheet" type="text/css"/>
    <style>

        a.image-remove {
            display: block;
            width: 30px;
            height: 30px;
            position: absolute;
            overflow: hidden;
            padding: 1px 5px;
            font-weight: bold;
            font-size: 13px;
            top: 1px;
            right: 1px;
            background: #ff5722ba;
            color: #fff;
            border-radius: 2px;
            padding: 2px 10px;
            cursor: pointer !important;
        }

        a.image-remove:hover {
            background: #ff5722;
        }

        .image-remove i {
            cursor: pointer !important;
        }

        .dropzone {
            border: 1px dashed #b1b8bb;
        }

        .dz-message h6 {
            font-size: 11pt;
            /*color: #ccc;*/
            font-weight: normal;
        }

        .dropzone .dz-message {
            padding: 0;
        }

        .dropzone .dz-preview .dz-image {
            border-radius: 5px;
            border: dotted 1px #094794;
            overflow: hidden;
            cursor: pointer !important;
            /*width: 70px;*/
            /*height: 70px;*/
        }

        .dropzone .dz-preview .dz-image img {
            width: 100%;
            cursor: pointer !important;
        }

        .dropzone .dz-preview .dz-progress {
            width: 60px;
            margin-left: -30px;
        }

        .dropzone .dz-preview .dz-details .dz-size {
            display: none;
        }

        .dropzone {
            min-height: 100px;
        }

        .dropzone .dz-preview:hover .dz-image img {
            -webkit-filter: blur(1px);
            filter: blur(1px)
        }

        .sortable {
            padding: 0;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .sortable li {
            float: left;
            width: 120px;
            height: 120px;
            overflow: hidden;
            border: 1px solid red;
            text-align: center;
            margin: 5px;
        }

        li.sortable-placeholder {
            border: 1px dashed #CCC;
            background: none;
        }
    </style>
@stop

@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="{{ asset('/storage/frontend')}}/assets/plugins/dropzone/min/dropzone.min.js" type="text/javascript"></script>

    <script>
        // Dropzone.autoDiscover = false;

        $(document).ready(function () {
            $('.sortable').sortable();
        });

        Dropzone.options.myDropzone = {
            uploadMultiple: true,
            parallelUploads: 1,
            maxFilesize: 2,
            addRemoveLinks: false,
            dictFileTooBig: 'Dung lượng hình lớn hơn 2MB',
            timeout: 10000,
            params: {
                _token: $('[name="_token"]').val()
            },
            init: function () {
                this.on("complete", function (file, reponse) {
                    $('#myDropzone').removeClass('dz-started');
                    $('.sortable').sortable('enable');
                });
            },
            success: function (file, reponse) {
                $('#myDropzone .dz-preview').not('.my-preview').remove();
                $.each(reponse.r, function (i, item) {
                    appendImage(item.id, item.url, '#myDropzone .result', 'image_file_ids[]');
                });
            }
        }

        function removeFile(d) {
            var file_id = d.data('id');
            $.post({
                url: '{{route('frontend.ajax.image.remove')}}',
                data: {id: file_id, _token: $('[name="_token"]').val()},
                dataType: 'json',
                success: function (data) {
                    d.parent().parent().remove();
                }
            });
        }

        function appendImage(id, src, parent_div, input_name) {
            $(parent_div).append('<div class="dz-preview dz-image-preview dz-success dz-complete my-preview">\n' +
                '                      <div class="dz-image">\n' +
                '                           <img data-dz-thumbnail="" alt=""\n' +
                '                                src="' + src + '">\n' +
                '                           <a href="javascript:;" onclick="removeFile($(this))" data-id="' + id + '" title="xóa"\n' +
                '                              class="image-remove"><i class="fa fa-trash"></i></a>\n' +
                '                      </div>\n' +
                '                      <input name="' + input_name + '" value="' + id + '" type="hidden"/>\n' +
                '                 </div>')
        }

        $('.select_parent_id').change(function () {
            var parent_id = $(this).val();

            $('.select_category_id').html('').hide();
            if (parent_id != '') {
                $.get({
                    url: '{{route('category.getAll')}}',
                    data: {parent_id: parent_id, only_root: 0},
                    dataType: 'json',
                    success: function (data) {
                        if (data.data.length < 1)
                            return;
                        $.each(data.data, function (k, v) {
                            $('.select_category_id').append('<option value="' + v.id + '">' + v.name + '</option>');
                        })
                        $('.select_category_id').show();
                    }
                });
            }
        });
    </script>
@stop