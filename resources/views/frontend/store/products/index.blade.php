@extends('frontend.layouts.frontend')

@section('content')
    <main class="main">

        @include('frontend.parts.breadcrumb')

        <div class="container">
            <div class="row">

                @include('frontend.user.common.siderbar')

                <div class="col col-md-9 mt-3">
                    <div class="card shadow-sm bg-white rounded w-100 card-info">
                        <div class="card-header card-info-header">
                            <h4 class="mt-2 mr-info">Sản phẩm đăng bán</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="product add-new">
                                        <figure class="product-image-container">
                                            <a href="{{route('frontend.store.products.create')}}" class="product-image" title="Thêm 1 sản phẩm mới">
                                                <img src="{{ asset('/storage/frontend')}}/assets/images/plus-256.png" alt="Thêm 1 sản phẩm mới">
                                            </a>
                                            <p class="text-center mb-0">
                                                <a href="{{route('frontend.store.products.create')}}" class="d-inline-block product_name" title="Thêm 1 sản phẩm mới"
                                                   style="font-size: 10pt;">Thêm 1 sản phẩm mới</a>
                                            </p>
                                            <p class="text-center mb-1">
                                                <span class="product-price d-inline-block ">&nbsp;</span>
                                            </p>
                                        </figure>
                                    </div><!-- End .product -->
                                </div>
                                @foreach($products as $product)
                                    <div class="col-sm-6 col-md-3">
                                        @include('frontend.template.store.product')
                                    </div>
                                @endforeach
                            </div>

                            <nav class="toolbox toolbox-pagination">
                                <div class="toolbox-item toolbox-show">
                                    <label>Hiển thị {{number_format($products->firstItem())}}
                                        – {{number_format($products->lastItem())}}
                                        trong {{number_format($products->total())}} kết
                                        quả</label>
                                </div>
                                {{$products->links()}}
                            </nav>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </main><!-- End .main -->
@endsection