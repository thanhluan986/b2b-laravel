@extends('frontend.layouts.frontend')

@section('content')

    <div class="container-fluid bg-white pt-5 pb-4">

        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card w-100 card-background-info rounded">
                        <div class="card-body no-min-height card-body-avatar-padding">
                            <div class="media" style="position: relative">
                                <a href="#" class="badge badge-primary love"><i class="fas fa-check"></i> Yêu thích</a>
                                <img src="{{$store_data->avatar_src}}" width="80" class="mr-3 rounded-circle" alt="...">
                                <div class="media-body">
                                    <h5 class="mt-1 mb-0 title-avatar">{{$store_data->username}}</h5>
                                    <p class="mt-0 title-online">Online 14 phút trước</p>
                                </div>
                            </div>
                            <div class="d-flex mt-1">
                                <div class="flex-fill text-center ">
                                    <a href="#" class="badge badge-primary theodoi">
                                        <i class="fas fa-plus"></i> THEO DÕI
                                    </a>
                                </div>
                                <div class="flex-fill text-center">
                                    <a href="#" class="badge badge-primary chat-theodoi">
                                        <i class="far fa-comment-dots"></i> CHAT
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-3 pr-1">
                    <ul class="list-group">
                        <li class="list-group-item shop_page_info_list">
                            <span class="font-size-info"><i class="fas fa-store"></i></span>
                            <span class="ml-1"> Sản Phẩm:</span>
                            <span class="price-order">100</span>
                        </li>
                        <li class="list-group-item shop_page_info_list">
                            <span class="font-size-info"><i class="fas fa-truck"></i></span>
                            <span class="ml-1">Thời Gian Chuẩn Bị Hàng:</span>
                            <span class="price-order">Chậm</span>
                        </li>
                        <li class="list-group-item shop_page_info_list">
                            <span class="font-size-info"><i class="fas fa-user-plus"></i></span>
                            <span class="ml-1">Đang Theo Dõi:</span>
                            <span class="price-order">44</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 pr-1 pl-1">
                    <ul class="list-group">
                        <li class="list-group-item shop_page_info_list">
                            <span class="font-size-info"><i class="fas fa-comment-dots"></i></span>
                            <span class="ml-1">Tỉ Lệ Phản Hồi Chát:</span>
                            <span class="price-order">57%</span>
                        </li>
                        <li class="list-group-item shop_page_info_list">
                            <span class="font-size-info"><i class="far fa-newspaper"></i></span>
                            <span class="ml-1">Tỉ Lệ Hủy Đơn:</span>
                            <span class="price-order">7%</span>
                        </li>
                        <li class="list-group-item shop_page_info_list">
                            <span class="font-size-info"><i class="fas fa-user-friends"></i></span>
                            <span class="ml-1">Người Theo Dõi:</span>
                            <span class="price-order">79999</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2 pr-1 pl-1">
                    <ul class="list-group">
                        <li class="list-group-item shop_page_info_list">
                            <span class="font-size-info"><i class="far fa-star"></i></span>
                            <span class="ml-1">Đánh Giá:</span>
                            <span class="price-order">79999</span>
                        </li>
                        <li class="list-group-item shop_page_info_list">
                            <span class="font-size-info"><i class="fas fa-user-check"></i></span>
                            <span class="ml-1">Tham Gia:</span>
                            <span class="price-order">19/08/2019</span>

                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-white color-top-shop">
        <div class="container">
            <nav aria-label="breadcrumb" class="rounded w-50 shadow-sm">
                <ol class="breadcrumb breadcrumb-order">
                    <li class="breadcrumb-item breadcrumb-item-customize active">
                        <a href="{{route('frontend.store.index',$store_data->username)}}" class="active">DẠO</a>
                    </li>
                    <li class="breadcrumb-item breadcrumb-item-customize">
                        <a href="{{route('frontend.store.product',$store_data->username)}}">TẤT CẢ SẢN PHẨM</a>
                    </li>
                    <li class="breadcrumb-item breadcrumb-item-customize">
                        <a href="{{route('frontend.store.product',$store_data->username)}}">HÀNG MỚI VỀ</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container mt-2">
        <div class="row">
            <div class="card no-border shadow-sm w-100">
                <div class="card-body">
                    <h5 class="ml-4">THÔNG TIN SHOP</h5>
                    <div class="media">
                        <img src="{{ asset('/storage/frontend')}}/assets/images/740137.jpg" class="mr-5 ml-4 mb-1" alt="...">
                        <div class="media-body">
                            <h5 class="mt-2 price-order">Giới thiệu</h5>
                            {{$store_data->description}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="d-flex ">
                    <div class="p-2 flex-grow-1">HÀNG MỚI VỀ</div>
                    <div class="p-2"> Xem Tất Cả</div>
                </div>
            </div>
        </div>

        <div class="row mb-3">
            @if(count($new_products))
                <div class="row">
                    @foreach($new_products as $product)
                        <div class="col-md-2 col-sm-4 col-6">
                            @include('frontend.template.product')
                        </div>
                    @endforeach
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-md-2">
                <h3><i class="fas fa-list"></i> Danh Mục</h3>
                <hr class="mt-2 mb-2">
                <ul class="list-group">
                    <li class="list-group-item border-0 bg-transparent">
                        <a href="#" class="active-shop-left"><i class="fas fa-caret-right"></i> Sản Phẩm</a>
                    </li>
                    <li class="list-group-item border-0 bg-transparent">
                        <a href="#">Hàng Mới Về</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">

                <div class="row">
                    @if(count($all_products))
                        @foreach($all_products as $product)
                            <div class="col-md-2 col-sm-4 col-6">
                                @include('frontend.template.product')
                            </div>
                        @endforeach
                    @endif
                </div>


                <div class="row">
                    <div class="col-md-12 mt-4">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <li class="page-item disabled">
                                    <span class="page-link page-link-shop-page">&#8249;</span>
                                </li>
                                <li class="page-item"><a class="page-link page-link-shop-page" href="#">1</a></li>
                                <li class="page-item page-item-shop-page active" aria-current="page">
                                  <span class="page-link page-link-shop-page">
                                    2
                                    <span class="sr-only">(current)</span>
                                  </span>
                                </li>
                                <li class="page-item page-item-shop-page">
                                    <a class="page-link page-link-shop-page" href="#">3</a></li>
                                <li class="page-item page-item-shop-page">
                                    <a class="page-link page-link-shop-page" href="#">4</a></li>
                                <li class="page-item page-item-shop-page">
                                    <a class="page-link page-link-shop-page" href="#">&#8250;</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>


            </div>
        </div>

    </div>

@endsection