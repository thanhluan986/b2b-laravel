<div class="product">
    <figure class="product-image-container">
        @php
            $link = route('frontend.store.products.update').'?id='.$product->id;
        @endphp

        <a href="{{$link}}" class="product-image" title="{{$product->name}}">
            <img src="{{$product->thumbnail->file_src}}" alt="{{$product->name}}">
        </a>
        <p class="text-center mb-0">
            <a href="{{$link}}" class="d-inline-block product_name" title="{{$product->name}}"
               style="font-size: 10pt;">{{\App\Utils\Common::shorten_string($product->name, 6)}}</a>
        </p>
        <p class="text-center mb-1">
            <span class="product-price d-inline-block ">{{number_format($product->price)}} đ</span>
            {!! $product->status==1?'':'<span class="d-inline-block ">Đã ẩn</span>' !!}
        </p>
    </figure>
</div><!-- End .product -->