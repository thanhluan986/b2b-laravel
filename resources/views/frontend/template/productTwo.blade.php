<div class="product">
    <figure class="product-image-container">
        @php
            $link = route('frontend.product.detail',['slug'=>\App\Utils\Filter::setSeoLink($product->name), 'id'=>$product->id]);
        @endphp

        <a href="{{$link}}" class="product-image" title="{{$product->name}}">
            <img src="{{$product->thumbnail->file_src}}" alt="{{$product->name}}">
        </a>
        <p class="text-center mb-0">
            <a href="{{$link}}" class="d-inline-block" title="{{$product->name}}"
               style="font-size: 10pt;">{{\App\Utils\Common::shorten_string($product->name, 6)}}</a>
        </p>
        <p class="text-center mb-1">
            <span class="product-price d-inline-block ">{{number_format($product->price)}} đ</span>
        </p>
    </figure>
</div><!-- End .product -->