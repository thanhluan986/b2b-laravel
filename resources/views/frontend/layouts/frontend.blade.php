<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="robots" content="INDEX,FOLLOW,NOODP,NOYDIR">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@if($title){{$title}} | {{$META_TITLE}}@else{{$META_TITLE}}@endif</title>

        <meta name="distribution" content="Global">
        <meta name="description" content="@if($description){{$description}}@else{{$META_DESCRIPTIONS}}@endif">
        <meta name="keywords" content="@if($keywords){{$keywords}}@else{{$META_KEYWORDS}}@endif">
        <meta name="author" content="{{$META_AUTHOR}}">

        <!-- Plugins CSS File -->
        <link rel="stylesheet" href="{{ asset('/storage/frontend')}}/assets/css/font-awesome/fontawesome-all.css">
        <link rel="stylesheet" href="{{ asset('/storage/frontend')}}/assets/css/bootstrap.min.css">

        <!-- Main CSS File -->
        <link rel="stylesheet" href="{{ asset('/storage/frontend')}}/assets/css/style.min.css">
        <link href="{{ asset('/storage/frontend')}}/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('/storage/frontend')}}/assets/css/address.css">
        <link rel="stylesheet" href="{{ asset('/storage/frontend')}}/assets/css/custom.css">

        @yield('style')

        <script>
            var BASE_URL = "{{config('app.url')}}";
            var LOCATION_PROVINCE_URL = "{{route('location.province')}}";
            var LOCATION_DISTRICT_URL = "{{route('location.district')}}";
            var LOCATION_WARD_URL = "{{route('location.ward')}}";
            var IS_LOGGED = {{$is_logged?'true':'false'}};
        </script>

    </head>
    <body>
        <div class="page-wrapper">

            @include('frontend.parts.header')

            @yield('content')

            @include('frontend.parts.footer')

        </div>

        @include('frontend.parts.mobileMenu')

        @include('frontend.parts.popupNewsletter')

        @include('frontend.parts.modal')

        <a id="scroll-top" href="#top" title="Top" role="button"><i class="icon-angle-up"></i></a>

        <script src="{{ asset('/storage/frontend')}}/assets/js/jquery.min.js"></script>
        <script src="{{ asset('/storage/frontend')}}/assets/js/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('/storage/frontend')}}/assets/js/plugins.min.js"></script>
        <script src="{{ asset('/storage/frontend')}}/assets/plugins/select2/dist/js/select2.full.min.js"></script>

        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=1825695670984277&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        {{--<script src="{{ asset('/storage/frontend')}}/assets/js/main.js"></script>--}}
        <script src="{{ asset('/storage/frontend')}}/assets/js/function/app.js"></script>

        @yield('script')
    </body>
</html>