<div class="modal-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title mb-2">Đăng nhập</h2>

                <form action="{{route('frontend.ajax.login')}}" method="post" id="form_login">
                    @csrf
                    <label for="login-credential">Tên đăng nhập / Email <span class="required">*</span></label>
                    <input type="text" name="credential" class="form-input form-wide mb-1" id="login-credential" required/>
                    <span class="help-block error mb-1 hidden" id="login_error_credential"></span>

                    <label for="login-password">Mật khẩu <span class="required">*</span></label>
                    <input type="password" name="password" class="form-input form-wide mb-1" id="login-password" required/>
                    <span class="help-block error mb-1 hidden" id="login_error_password"></span>

                    <span class="help-block error mb-1 hidden" id="login_error_system"></span>

                    <a href="{{route('frontend.user.forgotpass')}}" class="forget-password"> Quên mật khẩu?</a>

                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary btn-md">ĐĂNG NHẬP</button>

                        <div class="custom-control custom-checkbox form-footer-right">
                            <input type="checkbox" class="custom-control-input" id="lost-password">
                            <label class="custom-control-label form-footer-right" for="lost-password">
                                Ghi nhớ đăng nhập
                            </label>
                        </div>
                    </div><!-- End .form-footer -->

                    <p>Bạn chưa có tài khoản? <a href="#" class="forget-password register-link">Đăng ký ngay</a></p>
                </form>

                <div class="social-login-wrapper">
                    <p>Đăng nhập bằng tài khoản mạng xã hội.</p>
                    <div class="btn-group">
                        <a class="btn btn-social-login btn-md btn-gplus mb-1"><i class="icon-gplus"></i><span>Google</span></a>
                        <a class="btn btn-social-login btn-md btn-facebook mb-1"><i class="icon-facebook"></i><span>Facebook</span></a>
                    </div>
                </div>
            </div><!-- End .col-md-6 -->
        </div><!-- End .row -->
    </div><!-- End .container -->
</div>