<!-- Modal Header -->
<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">
        Thêm địa chỉ giao hàng! </h4>
    <i class="close fa fa-times btooltip" data-toggle="tooltip" data-placement="top" title="" data-dismiss="modal" aria-hidden="true" data-original-title="Close"></i>
</div>

<!-- Modal Body -->
<div class="modal-body">
    <form role="form" id="form_add_address">
        @csrf
        <div class="form-group">
            <label for="name"><span class="required">*</span> Họ tên </label>
            <input type="text" name="name"
                   value="{{$address->name}}"
                   class="form-control" id="name" required>
            <span class="help-block error mb-1 hidden" id="address_error_name"></span>
        </div>
        <div class="form-group">
            <label for="phone"><span class="required">*</span> Số điện thoại</label>
            <input type="number" name="phone"
                   value="{{$address->phone}}"
                   class="form-control" id="phone" required>
            <span class="help-block error mb-1 hidden" id="address_error_phone"></span>
        </div>

        <div class="form-group">
            <label><span class="required">*</span> Tỉnh/TP</label>
            <select class="form-control select_province select2" name="province_id" required>
                <option value="">Chọn</option>
                @foreach($provinces as $province)
                    <option value="{{$province->id}}"
                            {!! old('province_id',$address->province_id)==$province->id?'selected="selected"':'' !!}>{{$province->name}}</option>
                @endforeach
            </select>
            <span class="help-block error mb-1 hidden" id="address_error_province_id"></span>
        </div>

        <div class="form-group">
            <label><span class="required">*</span> Quận/Huyện</label>
            <select class="form-control select_district select2" name="district_id" required>
                <option value="">Chọn</option>
                @foreach($districts as $district)
                    <option value="{{$district->id}}"
                            {!! old('district_id', $address->district_id)==$district->id?'selected="selected"':'' !!}>{{$district->name}}</option>
                @endforeach
            </select>
            <span class="help-block error mb-1 hidden" id="address_error_district_id"></span>
        </div>

        <div class="form-group">
            <label><span class="required">*</span> Phường/Xã</label>
            <select class="form-control select_ward select2" name="ward_id" required>
                <option value="">Chọn</option>
                @foreach($wards as $ward)
                    <option value="{{$ward->id}}"
                            {!! old('ward_id', $address->ward_id)==$ward->id?'selected="selected"':'' !!}>{{$ward->name}}</option>
                @endforeach
            </select>
            <span class="help-block error mb-1 hidden" id="address_error_ward_id"></span>
        </div>

        <div class="form-group">
            <label for="street_name">
                <span class="required">*</span> Địa chỉ cụ thể (số nhà, hẻm, tên đường...)
            </label>
            <textarea name="street_name" class="form-control" id="street_name" rows="3" required>{{$address->street_name}}</textarea>
            <span class="help-block error mb-1 hidden" id="address_error_street_name"></span>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="is_default_recipient"
                       {!! old('is_default_recipient', $address->is_default_recipient)==1?'checked="checked"':'' !!}
                       value="1"> Địa chỉ nhận
                hàng mặc định
            </label>
        </div>
    </form>
</div>

<!-- Modal Footer -->
<div class="modal-footer">
    <div class="text-center">
        <button class="btn btn-primary flat" id="add-address-btn">
            Thêm địa chỉ
        </button>
        <button type="button" class="btn btn-default flat" data-dismiss="modal">Đóng
        </button>
    </div>
</div>