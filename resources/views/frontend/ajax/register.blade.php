<div class="modal-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title mb-2">Đăng ký tài khoản</h2>

                <form action="{{route('frontend.ajax.register')}}" method="post" id="form_register">
                    @csrf

                    <label for="register-username">Tên đăng nhập <span class="required">*</span></label>
                    <input type="text" name="username" class="form-input form-wide mb-2" id="register-username" required>
                    <span class="help-block error mb-1 hidden" id="login_error_username"></span>

                    <label for="register-email">Email <span class="required">*</span></label>
                    <input type="email" name="email" class="form-input form-wide mb-2" id="register-email" required>
                    <span class="help-block error mb-1 hidden" id="login_error_email"></span>

                    <label for="register-phone">Số điện thoại</label>
                    <input type="number" name="phone" class="form-input form-wide mb-2" id="register-phone">
                    <span class="help-block error mb-1 hidden" id="login_error_phone"></span>

                    <label for="register-password">Mật khẩu <span class="required">*</span></label>
                    <input type="password" name="password" class="form-input form-wide mb-2" id="register-password" required>
                    <span class="help-block error mb-1 hidden" id="login_error_password"></span>

                    <label for="register-password_confirmation">Xác nhận mật khẩu
                        <span class="required">*</span></label>
                    <input type="password" name="password_confirmation" class="form-input form-wide mb-2" id="register-password_confirmation" required>
                    <span class="help-block error mb-1 hidden" id="login_error_password_confirmation"></span>

                    <span class="help-block error mb-1 hidden" id="login_error_system"></span>

                    <span class="help-block success mb-1 hidden" id="login_message"></span>

                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary btn-md" id="register_btn">Đăng ký</button>
                    </div><!-- End .form-footer -->

                    <p>Bạn đã có tài khoản? <a href="#" class="forget-password login-link">Đăng nhập ngay</a></p>
                </form>
            </div><!-- End .col-md-6 -->
        </div><!-- End .row -->
    </div><!-- End .container -->
</div>