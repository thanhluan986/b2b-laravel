<div class="modal-header">
    <i class="close fa fa-times btooltip" data-toggle="tooltip"
       data-placement="top" title="" data-dismiss="modal" aria-hidden="true"
       data-original-title="Close"></i>
</div>
@if($error)
    <div class="modal-body">
        <p><?php echo $error; ?></p>
    </div>
    <div class="modal-footer">
        <div class="text-center" style="display: inline-block; width: 100%;">
            <button type="button" class="btn btn-danger flat" data-dismiss="modal">Đóng</button>
        </div>
    </div>
@else
    <div class="modal-body">
        <p><?php echo $message; ?></p>
    </div>
    <div class="modal-footer">
        <div class="text-center" style="display: inline-block; width: 100%;">
            <button type="button" class="btn btn-success flat" data-dismiss="modal">Tiếp tục mua hàng</button>
            <a href="{{route('frontend.basket.index')}}" class="btn btn-info flat">Đến giỏ hàng</a>
        </div>
    </div>
@endif