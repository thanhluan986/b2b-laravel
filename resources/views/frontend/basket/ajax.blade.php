@if(count($basket))
    <div class="dropdown-menu">
        <div class="dropdownmenu-wrapper">
            <div class="dropdown-cart-products">
                @php $i=0; @endphp
                @foreach($basket as $k=> $product)
                    @php
                        $link = route('frontend.product.detail',['slug'=>\App\Utils\Filter::setSeoLink($product->name), 'id'=>$product->product_id]);
                    @endphp
                    <div class="product">
                        <div class="product-details">
                            <h4 class="product-title">
                                <a href="{{$link}}" target="_blank">{{$product->name}}</a>
                            </h4>

                            <span class="cart-product-info">
                                  <span class="cart-product-qty">{{$product->quantity}}</span>
                                                    x {{number_format($product->price)}} đ
                            </span>
                        </div>

                        <figure class="product-image-container">
                            <a href="{{$link}}" class="product-image" target="_blank">
                                <img src="{{$product->thumbnail->file_src}}" alt="product">
                            </a>
                            {{--<a href="#" class="btn-remove" title="Remove Product"><i class="icon-cancel"></i></a>--}}
                        </figure>
                    </div><!-- End .product -->
                    @php $i=$k; @endphp
                    @if($k==4)
                        @php break; @endphp
                    @endif
                @endforeach

            </div><!-- End .cart-product -->

            @if(count($basket)-$i-1)
                <div class="dropdown-cart-total">
                    <span>Và {{count($basket)-$i-1}} sản phẩm khác</span>
                </div>
            @endif

            <div class="dropdown-cart-action">
                <a href="{{route('frontend.basket.index')}}" class="btn">Xem giỏ hàng</a>
                {{--<a href="{{route('frontend.checkout.index')}}" class="btn">Đặt hàng</a>--}}
            </div><!-- End .dropdown-cart-total -->
        </div><!-- End .dropdownmenu-wrapper -->
    </div><!-- End .dropdown-menu -->
@endif