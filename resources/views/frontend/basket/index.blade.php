@extends('frontend.layouts.frontend')

@section('content')
    <main class="main">

        @include('frontend.parts.breadcrumb')

        <div class="container">

            <div class="row">
                <div class="card no-border margin-bottom-2 w-100">
                    <div class="card-body no-min-height  card-body-125 no-padding-bottom shadow-sm">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" class="align-middle td-width-4">
                                            <div class="custom-control custom-checkbox pl-0 mt-0 mb-0">
                                                <input type="checkbox"
                                                       checked="checked"
                                                       class="custom-control-input check-all" id="customCheckAll">
                                                <label class="custom-control-label" for="customCheckAll"></label>
                                            </div>
                                        </th>
                                        <th scope="col" class="align-middle">Sản Phẩm</th>
                                        <th scope="col" class="color-so-luong align-middle text-center" style="width: 15%">
                                            Đơn Giá
                                        </th>
                                        <th scope="col" class="color-so-luong align-middle text-center" style="width: 15%">
                                            Số Lượng
                                        </th>
                                        <th scope="col" class="color-so-luong align-middle text-center" style="width: 8%">
                                            Số Tiền
                                        </th>
                                        <th scope="col" class="color-so-luong align-middle text-center" style="width: 8%">
                                            Thao Tác
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @php
                $total_product = 0;
                $total_quantity = 0;
                $total_price = 0;
            @endphp
            <form action="" method="post">
                @csrf
                @foreach($baskets as $store)
                    <div class="row" id="store_id_{{$store->id}}">
                        <div class="card no-border no-margin-bottom w-100">
                            <div class="card-body no-min-height no-padding-bottom shadow-sm">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td scope="col" class="align-middle td-width-4">
                                                <div class="custom-control custom-checkbox pl-0 mt-0 mb-0">
                                                    <input type="checkbox" class="custom-control-input store_check_box"
                                                           checked="checked"
                                                           data-store-id="{{$store->id}}"
                                                           id="customCheck_{{$store->id}}">
                                                    <label class="custom-control-label" for="customCheck_{{$store->id}}"></label>
                                                </div>
                                            </td>
                                            <td colspan="5">
                                                <span>
                                                     <a href="{{route('frontend.store.index',$store->username)}}" target="_blank">
                                                        <i class="fas fa-store"></i>
                                                         {{$store->username}}
                                                        </a>
                                                <span style="color: #E9E9E9;"> | </span>
                                                </span>
                                                <a href="javascript:;" class="badge-primary-chat-checkout">
                                                    <i class="far fa-comment-dots"></i> Chat ngay
                                                </a>
                                            </td>
                                        </tr>

                                        @foreach($store->products as $product)
                                            @php
                                                $total_product +=1;
                                                $total_quantity += $product['quantity'];
                                                $total_price += $product['quantity']*$product['price'];
                                                $link = route('frontend.product.detail',['slug'=>\App\Utils\Filter::setSeoLink($product['name']), 'id'=>$product['product_id']]);
                                            @endphp
                                            <tr data-basket-item-id="{{$product['basket_item_id']}}" id="row_basket_item_id_{{$product['basket_item_id']}}">
                                                <td scope="col" class="align-middle td-width-4">
                                                    <div class="custom-control custom-checkbox pl-0 mt-0 mb-0">
                                                        <input type="checkbox" class="custom-control-input product_check"
                                                               checked="checked"
                                                               data-store-id="{{$store->id}}"
                                                               data-price="{{$product['price']}}"
                                                               name="basket_item_ids[]"
                                                               id="customCheck-{{$product['basket_item_id']}}" value="{{$product['basket_item_id']}}">
                                                        <label class="custom-control-label" for="customCheck-{{$product['basket_item_id']}}"></label>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="media">
                                                        <a href="{{$link}}">
                                                            <img src="{{$product['thumbnail']['file_src']}}" class="align-self-end mr-3 media-img-checkout" alt="...">
                                                        </a>

                                                        <div class="media-body">
                                                            <a href="{{$link}}" target="_blank">
                                                                <h5 class="mt-1 mb-0 mt-title-customize">{{$product['name']}}</h5>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center" style="width: 15%">
                                                    @if($product['price_old']&&$product['price_old']>$product['price'])
                                                        <span class="color-so-luong"><del>{{number_format($product['price_old'])}}
                                                                <sup class="dong">đ</sup></del></span>
                                                    @endif
                                                    <span class="ml-2">{{number_format($product['price'])}}
                                                        <sup class="dong">đ</sup> </span>
                                                </td>
                                                <td class="pl-5" style="width: 15%">
                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                        <button type="button" class="btn btn-secondary btn-secondary-shopping-cart cart_minus"
                                                                data-basket-item-id="{{$product['basket_item_id']}}">
                                                            -
                                                        </button>
                                                        <input type="text" class="btn btn-secondary btn-secondary-shopping-cart cart_quantity"
                                                               data-basket-item-id="{{$product['basket_item_id']}}"
                                                               value="{{$product['quantity']}}" style="width: 50px">
                                                        <button type="button" class="btn btn-secondary btn-secondary-shopping-cart cart_plus"
                                                                data-basket-item-id="{{$product['basket_item_id']}}">
                                                            +
                                                        </button>
                                                    </div>
                                                </td>
                                                <td class="text-center price-order" style="width: 8%">
                                                    {{number_format($product['price'])}} <sup class="dong">đ</sup>
                                                </td>
                                                <td class="text-center" style="width: 8%">
                                                    <a href="{{route('frontend.basket.remove')}}?id={{$product['basket_item_id']}}" class="delete-product" title="Xóa">
                                                        Xóa</a>
                                                </td>
                                            </tr>

                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="row">
                    <div class="card w-100 no-margin-bottom no-border shadow-sm">

                        <div class="card-body no-min-height card-body-background-order">
                            <div class="d-flex justify-content-end">
                                <div class="p-2">
                                    <span class="color-cart-discount-shopping"> <i class="fas fa-money-check"></i> </span>
                                    Mã giảm giá
                                </div>
                                <div class="p-2">
                                    <a href="#" class="badge badge-primary badge-primary-magiamgia p-3">Chọn Mã Vận
                                        Chuyển/Mã Giảm Giá</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="card w-100 shadow-sm card-body-checkout-total">
                        <div class="card-body no-min-height">
                            <div class="d-flex">
                                <div class="mr-auto p-2 mt-1">
                                    <div class="custom-control custom-checkbox pl-0 mt-0 mb-0">
                                        <input type="checkbox" checked="checked" class="custom-control-input check-all" id="customCheckAll2">
                                        <label class="custom-control-label ml-5" for="customCheckAll2">Chọn Tất Cả
                                            ( {{number_format($total_product)}} )</label>
                                    </div>
                                </div>
                                <div class="p-2 mr-3">
                                    <p class="m-0 p-0 cart_total_text">Tổng tiền hàng
                                        ( {{number_format($total_product)}}
                                        sản phẩm ):
                                        <span class="tong-cong-tat-ca">{{number_format($total_price)}}
                                            <sup class="dong">đ</sup></span>
                                    </p>
                                    {{--<p class="nhan-them-xu m-0 p-0 text-right">Nhận thêm: 0 Xu</p>--}}
                                </div>
                                <div class="p-2">
                                    <button type="submit" class="btn btn-primary btn-sm btn-sm-tong-size rounded pr-4 mt-1 xem-danh-gia-shop">
                                        Đặt Hàng
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </main><!-- End .main -->
@endsection