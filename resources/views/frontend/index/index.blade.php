@extends('frontend.layouts.frontend')

@section('content')
    <main class="main mt-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">

                    @include('frontend.parts.bannerHome')

                    @include('frontend.parts.bannerPromo')

                    <div class="mb-3"></div>

                    @if(count($trending_products))
                        <h2 class="carousel-title">XU HƯỚNG TÌM KIẾM</h2>
                        <div class="home-featured-products owl-carousel owl-theme owl-dots-top">
                            @foreach($trending_products as $product)
                                @include('frontend.template.product')
                            @endforeach
                        </div>
                    @endif

                    @if(count($top_sale_products))
                        <h2 class="carousel-title">BÁN CHẠY NHẤT</h2>
                        <div class="row">
                            @foreach($top_sale_products as $product)
                                <div class="col-md-3 col-sm-6 col-6">
                                    @include('frontend.template.product')
                                </div>
                            @endforeach
                        </div>
                    @endif

                    @if(count($suggested_products))
                        <h2 class="carousel-title">GỢI Ý HÔM NAY</h2>
                        <div class="home-featured-products owl-carousel owl-theme owl-dots-top">
                            @foreach($suggested_products as $product)
                                @include('frontend.template.product')
                            @endforeach
                        </div>
                    @endif

                    @if(count($trending_products))
                        <h2 class="carousel-title">THƯƠNG HIỆU NỔI BẬT</h2>
                        <div class="home-featured-products owl-carousel owl-theme owl-dots-top">
                            @foreach($trending_products as $product)
                                @include('frontend.template.product')
                            @endforeach
                        </div>
                    @endif

                    <div class="mb-3"></div>
                </div>

                @include('frontend.parts.siderbarHome')

            </div>

        </div>
        <div class="mb-4"></div>
    </main>
@endsection