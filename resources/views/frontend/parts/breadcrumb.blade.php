<nav aria-label="breadcrumb" class="breadcrumb-nav">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{config('app.url')}}"><i class="icon-home"></i></a></li>

            @if(isset($breadcrumbs))
                @php $total =count($breadcrumbs) @endphp
                @foreach($breadcrumbs as $k=>$item)
                    @if($k==$total-1)
                        <li class="breadcrumb-item active" aria-current="page">{{$item['name']}}</li>
                    @else
                        <li class="breadcrumb-item"><a href="{{$item['link']}}">{{$item['name']}}</a></li>
                    @endif
                @endforeach
            @endif
        </ol>
    </div><!-- End .container -->
</nav>