<footer class="footer">

    {!! html_entity_decode($FOOTER_HTML) !!}

    <div class="container">
        <div class="footer-bottom">
            <p class="footer-copyright">{{$COMPANY_NAME}}. &copy; {{date('Y')}}. All Rights Reserved</p>
            <img src="{{ asset('/storage/frontend')}}/assets/images/payments.png" alt="payment methods"
                 class="footer-payments">
        </div>
    </div>
</footer>