@if(count($banners_promo))
    <div class="row">

        @foreach($banners_promo as $banner)
            @if(empty($banner->image))continue;@endif
            <div class="col-md-4">
                <div class="banner banner-image">
                    <a href="{{$banner->link}}" title="{{$banner->name}}">
                        <img src="{{$banner->image->file_src}}" alt="{{$banner->name}}">
                    </a>
                </div><!-- End .banner -->
            </div><!-- End .col-md-4 -->
        @endforeach

    </div><!-- End .row -->

@endif