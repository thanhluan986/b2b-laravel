<header class="header sticky-header">
    <div class="header-top">
        <div class="container">
            <div class="header-right">
                <div class="header-dropdown dropdown-expanded">
                    <a href="#">Links</a>
                    <div class="header-menu">
                        <ul>
                            <li><a href="#">Thông báo</a></li>

                            @if($is_logged)
                                <li><a href="{{route('frontend.user.wishlist')}}">Sản phẩm yêu thích </a></li>
                                <li><a href="{{route('frontend.user.profile')}}">Chào {{$user_data->username}} </a></li>
                                <li><a href="{{route('frontend.user.logout')}}?_ref={{url()->current()}}">Thoát</a></li>
                            @else
                                <li><a href="#" class="login-link">Đăng nhập</a></li>
                                <li><a href="#" class="register-link">Đăng ký</a></li>
                            @endif
                        </ul>
                    </div><!-- End .header-menu -->
                </div><!-- End .header-dropown -->
            </div><!-- End .header-right -->
        </div><!-- End .container -->
    </div><!-- End .header-top -->

    <div class="header-middle">
        <div class="container">
            <div class="header-left">
                <a href="/" class="logo" title="{{$COMPANY_NAME}}">
                    <img src="{{ asset('/storage/frontend')}}/assets/images/logo.png" width="220" alt="{{$COMPANY_NAME}}">
                </a>
            </div><!-- End .header-left -->

            <div class="header-center">
                <div class="header-search">
                    <a href="#" class="search-toggle" role="button"><i class="icon-magnifier"></i></a>
                    <form action="{{route('frontend.product.search')}}" method="get">
                        <div class="header-search-wrapper">
                            <input type="search" class="form-control" name="q" id="q" placeholder="Bạn muốn mua gì?" required>
                            <button class="btn" type="submit"><i class="icon-magnifier"></i></button>
                        </div><!-- End .header-search-wrapper -->
                    </form>
                </div><!-- End .header-search -->

            </div><!-- End .headeer-center -->

            <div class="header-right">
                <button class="mobile-menu-toggler" type="button">
                    <i class="icon-menu"></i>
                </button>
                <div class="header-contact">
                    <span>Hotline</span>
                    <a href="tel:{{$HOTLINE}}"><strong>{{$HOTLINE}}</strong></a>
                </div><!-- End .header-contact -->

                <div class="dropdown cart-dropdown">
                    <a href="{{route('frontend.basket.index')}}" class="dropdown-toggle"
                       role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                        <span class="cart-count">0</span>
                    </a>
                    <div class="card-content">

                    </div>
                </div><!-- End .dropdown -->
            </div><!-- End .header-right -->
        </div><!-- End .container -->
    </div><!-- End .header-middle -->
</header><!-- End .header -->