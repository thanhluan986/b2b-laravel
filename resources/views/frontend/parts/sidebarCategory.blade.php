<aside class="sidebar-shop col-lg-3 order-lg-first">
    <div class="sidebar-wrapper">
        <div class="widget"  @if(!$root_category_id)style="padding: 2.3rem 1rem 1.8rem;" @endif>
            @if($root_category_id)
                <h3 class="widget-title">
                    <a data-toggle="collapse" href="#widget-body-1" role="button" aria-expanded="true"
                       aria-controls="widget-body-1">
                        {{$array_categories[$root_category_id]['name']}}
                    </a>
                </h3>

                @if(isset($array_tree_categories[$root_category_id]['child']))
                    <div class="collapse show" id="widget-body-1">
                        <div class="widget-body">
                            <ul class="cat-list">
                                @foreach($array_tree_categories[$root_category_id]['child'] as $v)
                                    <li class="@if($category_id==$v['id'])active @endif">
                                        <a href="{{\App\Utils\Links::CategoryLink($v)}}">{{$v['name']}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            @else
                <h3 class="widget-title">
                    <a data-toggle="collapse" href="#widget-body-1" role="button" aria-expanded="true"
                       aria-controls="widget-body-1">
                        Danh mục
                    </a>
                </h3>
                <div class="collapse show" id="widget-body-1">
                    <div class="widget-body">
                        <nav class="side-nav">
                            <ul class="menu menu-vertical sf-arrows">
                                @foreach($array_tree_categories as $category)
                                    @if(empty($category['child']))
                                        <li class="active_">
                                            <a href="{{\App\Utils\Links::CategoryLink($category)}}" title="{{$category['name']}}">{{$category['name']}}
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{\App\Utils\Links::CategoryLink($category)}}" class="sf-with-ul" title="{{$category['name']}}"
                                               @if($category['icon']) style="background: url({{$category['icon']['file_src']}}) no-repeat left center;padding-left: 25px;background-size: 25px;" @endif>
                                                {{$category['name']}}</a>
                                            <ul>
                                                @foreach($category['child'] as $item)
                                                    <li>
                                                        <a href="{{\App\Utils\Links::CategoryLink($item)}}" title="{{$item['name']}}">{{$item['name']}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
            @endif
        </div>

        <div class="widget">
            <h3 class="widget-title">
                <a data-toggle="collapse" href="#widget-body-2" role="button" aria-expanded="true" aria-controls="widget-body-2">Khoảng
                    giá</a>
            </h3>

            <div class="collapse show" id="widget-body-2">
                <div class="widget-body">
                    <form action="#">
                        <div class="price-slider-wrapper my-price-range-filter--vn">
                            <div class="my-price-range-filter__inputs">
                                <input type="text" maxlength="13" class="my-price-range-filter__input" placeholder="₫ TỪ" value="">
                                <div class="my-price-range-filter__range-line"></div>
                                <input type="text" maxlength="13" class="my-price-range-filter__input" placeholder="₫ ĐẾN" value="">
                            </div>
                        </div><!-- End .price-slider-wrapper -->

                        <div class="filter-price-action">
                            <button type="submit" class="btn btn-primary">Áp dụng</button>

                            <div class="filter-price-text">
                                <span id="filter-price-range"></span>
                            </div><!-- End .filter-price-text -->
                        </div><!-- End .filter-price-action -->
                    </form>
                </div><!-- End .widget-body -->
            </div><!-- End .collapse -->
        </div><!-- End .widget -->

        <div class="widget">
            <h3 class="widget-title">
                <a data-toggle="collapse" href="#widget-body-4" role="button" aria-expanded="true" aria-controls="widget-body-4">Nơi
                    bán</a>
            </h3>

            <div class="collapse show" id="widget-body-4">
                <div class="widget-body">
                    <ul class="cat-list">
                        <li><a href="#">Hà Nội <span>100</span></a></li>
                        <li><a href="#">TP. HCM <span>95</span></a></li>
                        <li><a href="#">Thái Nguyên <span>50</span></a></li>
                        <li><a href="#">Vĩnh Phúc <span>45</span></a></li>
                        <li><a href="#">Hải Phòng <span>23</span></a></li>
                        <li><a href="#">Xem thêm...</a></li>
                    </ul>
                </div><!-- End .widget-body -->
            </div><!-- End .collapse -->
        </div><!-- End .widget -->

        <div class="widget widget-newsletters">
            <h3 class="widget-title">ĐĂNG KÝ NHẬN TIN</h3>
            <p>Nhận tất cả các thông tin mới nhất về Sự kiện, Bán hàng và Ưu đãi. </p>
            <form action="" method="post">
                <div class="form-group">
                    <input type="email" class="form-control" id="wemail">
                    <label for="wemail"><i class="icon-envolope"></i>Email Address</label>
                </div><!-- Endd .form-group -->
                <input type="button" class="btn btn-block" value="Đăng ký">
            </form>
        </div><!-- End .widget -->

        <div class="widget widget-contact">
            <h3 class="widget-title">THÔNG TIN LIÊN HỆ</h3>
            <p>CÔNG TY TNHH GOLDEN RIVER<br>
                Mã Số Thuế: 0315005650
            </p>
            <p>
                <i class="icon-paper-plane"></i> {{$ADDRESS}}
            </p>

            <p>
                <i class="icon-phone"></i> Hotline 1: {{$HOTLINE}}
            </p>
            <p>
                <i class="icon-phone"></i> Hotline 2: {{$PHONE}}
            </p>
            <p>
                <i class="icon-envolope"></i> Email: {{$EMAIL}}
            </p>
        </div><!-- End .widget -->
    </div><!-- End .sidebar-wrapper -->
</aside><!-- End .col-lg-3 -->
