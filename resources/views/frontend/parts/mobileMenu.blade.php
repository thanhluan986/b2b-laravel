<div class="mobile-menu-overlay"></div>

<div class="mobile-menu-container">
    <div class="mobile-menu-wrapper">
        <span class="mobile-menu-close"><i class="icon-cancel"></i></span>
        <nav class="mobile-nav">
            <ul class="mobile-menu">
                <li class="active"><a href="/">Trang chủ</a></li>
                <li>
                    <a href="javascript:;">Danh mục</a>
                    <ul>

                        @foreach($array_tree_categories as $category)
                            @if(empty($category['child']))
                                <li class="">
                                    <a href="{{\App\Utils\Links::CategoryLink($category)}}" title="{{$category['name']}}">{{$category['name']}}
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{\App\Utils\Links::CategoryLink($category)}}" class="sf-with-ul" title="{{$category['name']}}"
                                       @if($category['icon']) style="background: url({{$category['icon']['file_src']}}) no-repeat left center;padding-left: 25px;background-size: 25px;" @endif>
                                        {{$category['name']}}</a>
                                    <ul>
                                        @foreach($category['child'] as $item)
                                            <li>
                                                <a href="{{\App\Utils\Links::CategoryLink($item)}}" title="{{$item['name']}}">{{$item['name']}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </li>

                <li><a href="{{route('frontend.page.about')}}">Giới thiệu</a></li>
                <li><a href="{{route('frontend.page.contact')}}">Liên hệ</a></li>
            </ul>
        </nav><!-- End .mobile-nav -->

        <div class="social-icons">
            <a href="{{$FACEBOOK}}" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
            <a href="{{$TWITTER}}" class="social-icon" target="_blank"><i class="icon-twitter"></i></a>
            <a href="{{$INSTAGRAM}}" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
        </div><!-- End .social-icons -->
    </div><!-- End .mobile-menu-wrapper -->
</div><!-- End .mobile-menu-container -->