@if(count($banners))
    <div class="home-slider owl-carousel owl-carousel-lazy owl-theme owl-theme-light">
        @foreach($banners as $banner)
            @if(empty($banner->image))continue;@endif
            <div class="home-slide">
                <div class="owl-lazy slide-bg" data-src="{{$banner->image->file_src}}"></div>
                <div class="home-slide-content">
                    <a href="{{$banner->link}}" title="{{$banner->name}}" class="btn btn-dark">Chi tiết</a>
                </div><!-- End .home-slide-content -->
            </div><!-- End .home-slide -->
        @endforeach
    </div><!-- End .home-slider -->
@endif
