<!DOCTYPE html>
<html>
    <head>
        <title>Activation Email - Sàn TMĐT Golden River</title>
    </head>
    <body>
        <p>
            Chào mừng {{ $user->username }} đã đăng ký thành viên tại Sàn TMĐT Golden River. Bạn hãy click vào đường link sau đây để
            hoàn tất việc đăng ký.
            </br>
            <a href="{{ $user->activation_link }}">{{ $user->activation_link }}</a>
        </p>
    </body>
</html>