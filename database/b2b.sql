-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 23, 2019 at 08:29 AM
-- Server version: 5.7.27-30
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `b2b`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lck_address`
--

CREATE TABLE `lck_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `full_address` varchar(255) DEFAULT NULL,
  `is_default_recipient` tinyint(1) DEFAULT '0',
  `is_warehouse` tinyint(1) DEFAULT '0',
  `is_return` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_address`
--

INSERT INTO `lck_address` (`id`, `user_id`, `name`, `phone`, `province_id`, `district_id`, `ward_id`, `street_name`, `full_address`, `is_default_recipient`, `is_warehouse`, `is_return`, `created_at`, `updated_at`) VALUES
(4, 2, 'Bap Cui', '84979427220', 79, 760, 26737, '61 Lê Quý Đôn', '61 Lê Quý Đôn, Phường Đa Kao, Quận 1, Thành phố Hồ Chí Minh', 0, 0, NULL, '2019-03-09 17:09:02', '2019-04-20 09:47:34'),
(6, 2, 'Bap Cui', '84979427220', 79, 760, 26737, '61 Lê Quý Đôn', '61 Lê Quý Đôn, Phường Đa Kao, Quận 1, Thành phố Hồ Chí Minh', 0, 0, NULL, '2019-03-09 17:09:02', '2019-04-20 09:47:34'),
(11, 3, 'Nguyen Thanh Luan', '0979427220', 79, 766, 26968, '4/3A Đồ Sơn', '4/3A Đồ Sơn, Phường 04, Quận Tân Bình, Thành phố Hồ Chí Minh', 0, NULL, NULL, '2019-05-02 23:25:33', '2019-05-15 14:14:49'),
(14, 5, 'Phùng Duy Thành', '0966714834', 79, 770, 27154, '549 Điện Biên Phủ', '549 Điện Biên Phủ, Phường 03, Quận 3, Thành phố Hồ Chí Minh', 1, 1, 0, '2019-05-15 16:16:02', '2019-05-17 15:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `lck_banners`
--

CREATE TABLE `lck_banners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `descripttion` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `image_file_id` int(11) DEFAULT NULL,
  `app_image_file_id` int(11) DEFAULT NULL,
  `app_destination` varchar(255) DEFAULT NULL,
  `app_params` varchar(255) DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_banners`
--

INSERT INTO `lck_banners` (`id`, `name`, `descripttion`, `link`, `image_file_id`, `app_image_file_id`, `app_destination`, `app_params`, `start_datetime`, `end_datetime`, `category_id`, `user_id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Banner 1', NULL, NULL, 58, 59, NULL, NULL, NULL, NULL, NULL, NULL, 'home', 1, '2019-03-20 17:04:13', '2019-04-18 09:36:54'),
(2, 'Banner 2', NULL, NULL, 136, 137, NULL, NULL, NULL, NULL, NULL, NULL, 'home', 1, '2019-03-20 17:04:13', '2019-05-10 17:23:53'),
(3, 'Banner 3', NULL, NULL, 138, 139, NULL, NULL, NULL, NULL, NULL, NULL, 'home', 0, '2019-03-20 17:04:13', '2019-05-10 17:24:04'),
(4, 'Banner 4', NULL, NULL, 38, 38, NULL, NULL, NULL, NULL, NULL, NULL, 'home', 1, '2019-03-20 17:04:13', '2019-03-20 17:04:13'),
(5, 'Banner 5', NULL, NULL, 38, 38, NULL, NULL, NULL, NULL, NULL, NULL, 'home', 1, '2019-03-20 17:04:13', '2019-03-20 17:04:13'),
(6, 'Banner 1', NULL, NULL, 140, 141, NULL, NULL, NULL, NULL, 1, NULL, 'category', 1, '2019-03-20 17:04:13', '2019-05-10 17:24:25'),
(7, 'Banner 2', NULL, NULL, 142, 143, NULL, NULL, NULL, NULL, 52, NULL, 'category', 1, '2019-03-20 17:04:13', '2019-05-10 17:24:34'),
(8, 'Banner 3', NULL, NULL, 144, 150, NULL, NULL, NULL, NULL, 1, NULL, 'category', 1, '2019-03-20 17:04:13', '2019-05-10 17:25:15'),
(9, 'Banner 1', NULL, NULL, 146, 147, NULL, NULL, NULL, NULL, NULL, NULL, 'store', 1, '2019-03-20 17:04:13', '2019-05-10 17:24:53'),
(10, 'Banner 2', NULL, NULL, 148, 149, NULL, NULL, NULL, NULL, NULL, NULL, 'store', 1, '2019-03-20 17:04:13', '2019-05-10 17:25:02');

-- --------------------------------------------------------

--
-- Table structure for table `lck_basket`
--

CREATE TABLE `lck_basket` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_variation_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lck_basket`
--

INSERT INTO `lck_basket` (`id`, `user_id`, `product_id`, `product_variation_id`, `quantity`, `created_at`, `updated_at`) VALUES
(2, 3, 2, NULL, 1, '2019-05-03 23:17:02', '2019-05-03 23:17:02'),
(3, 5, 28, NULL, 2, '2019-05-14 14:32:14', '2019-05-17 15:30:58'),
(4, 5, 29, NULL, 3, '2019-05-14 14:32:24', '2019-05-14 14:32:24'),
(5, 5, 1, 2, 1, '2019-05-16 10:49:10', '2019-05-16 10:49:10'),
(6, 5, 1, 10, 1, '2019-05-16 11:12:09', '2019-05-16 11:12:09'),
(7, 5, 26, 21, 3, '2019-05-30 14:17:54', '2019-05-30 14:17:54');

-- --------------------------------------------------------

--
-- Table structure for table `lck_category`
--

CREATE TABLE `lck_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `parent_id` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `priority` int(11) DEFAULT '0',
  `thumbnail_file_id` int(11) DEFAULT NULL,
  `icon_file_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_descriptions` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `total_product` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Loai san pham' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_category`
--

INSERT INTO `lck_category` (`id`, `name`, `slug`, `description`, `parent_id`, `status`, `priority`, `thumbnail_file_id`, `icon_file_id`, `type`, `seo_title`, `seo_descriptions`, `seo_keywords`, `total_product`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Thời trang nam', 'thoi-trang-nam', 'THỜI TRANG NAM', 0, 1, 1, NULL, 39, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(2, 'áo thun', 'ao-thun', 'Áo thun ', 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(3, 'áo sơ mi ', 'ao-so-mi', 'Áo sơ mi  ', 1, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(4, 'áo khoác & áo vest', 'ao-khoac--ao-vest', 'Áo khoác & Áo vest ', 1, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(5, 'áo nỉ/ áo len', 'ao-ni-ao-len', 'Áo nỉ/ Áo len ', 1, 1, 5, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(6, 'đồ bộ/ đồ mặc nhà', 'do-bo-do-mac-nha', 'Đồ bộ/ Đồ mặc nhà ', 1, 1, 6, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(7, 'đồ đôi', 'do-doi', 'Đồ đôi ', 1, 1, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(8, 'Quần', 'quan', 'Quần ', 1, 1, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(9, 'Balo/ túi/ ví', 'balo-tui-vi', 'Balo/ Túi/ Ví ', 1, 1, 9, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(10, 'Mắt kính', 'mat-kinh', 'Mắt kính ', 1, 1, 10, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(11, 'Phụ kiện nam', 'phu-kien-nam', 'Phụ kiện nam ', 1, 1, 11, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(12, 'đồ trung niên', 'do-trung-nien', 'Đồ Trung Niên ', 1, 1, 12, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(13, 'Trang sức nam ', 'trang-suc-nam', 'Trang Sức Nam  ', 1, 1, 13, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(14, 'Thắt lưng', 'that-lung', 'Thắt Lưng ', 1, 1, 14, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(15, 'đồ lót', 'do-lot', 'Đồ lót ', 1, 1, 15, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(16, 'Khác', 'khac', 'Khác ', 1, 1, 16, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(17, 'Nhà cửa & đời sống', 'nha-cua--doi-song', 'NHÀ CỬA & ĐỜI SỐNG', 0, 1, 17, NULL, 40, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(18, 'Dụng cụ & thiết bị tiện ích', 'dung-cu--thiet-bi-tien-ich', 'Dụng cụ & Thiết bị tiện ích ', 17, 1, 18, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(19, 'đồ dùng nhà bếp & phòng ăn ', 'do-dung-nha-bep--phong-an', 'Đồ dùng nhà bếp & Phòng ăn  ', 17, 1, 19, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(20, 'Chăn, ga, gối & nệm', 'chan-ga-goi--nem', 'Chăn, Ga, Gối & Nệm ', 17, 1, 20, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(21, 'Tủ đựng & hộp lưu trữ ', 'tu-dung--hop-luu-tru', 'Tủ đựng & Hộp lưu trữ  ', 17, 1, 21, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(22, 'Trang trí nhà cửa', 'trang-tri-nha-cua', 'Trang trí nhà cửa ', 17, 1, 22, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(23, 'đồ dùng phòng tắm ', 'do-dung-phong-tam', 'Đồ dùng phòng tắm  ', 17, 1, 23, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(24, 'đồ nội thất', 'do-noi-that', 'Đồ nội thất ', 17, 1, 24, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(25, 'đèn', 'den', 'Đèn ', 17, 1, 25, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(26, 'Ngoài trời & sân vườn', 'ngoai-troi--san-vuon', 'Ngoài trời & Sân vườn ', 17, 1, 26, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:42', NULL),
(27, 'Dụng cụ cầm tay', 'dung-cu-cam-tay', 'Dụng cụ cầm tay ', 17, 1, 27, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(28, 'Khác', 'khac', 'Khác ', 17, 1, 28, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(29, 'đồng hồ', 'dong-ho', 'ĐỒNG HỒ', 0, 1, 29, NULL, 41, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(30, 'đồng hồ nam', 'dong-ho-nam', 'Đồng hồ nam ', 29, 1, 30, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(31, 'đồng hồ nữ', 'dong-ho-nu', 'Đồng hồ nữ ', 29, 1, 31, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(32, 'đồng hồ trẻ em', 'dong-ho-tre-em', 'Đồng hồ trẻ em ', 29, 1, 32, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(33, 'Phụ kiện đồng hồ', 'phu-kien-dong-ho', 'Phụ Kiện Đồng Hồ ', 29, 1, 33, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(34, 'Khác', 'khac', 'Khác ', 29, 1, 34, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(35, 'Bách hoá online', 'bach-hoa-online', 'BÁCH HOÁ ONLINE', 0, 1, 35, NULL, 42, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(36, 'ăn vặt & bánh kẹo', 'an-vat--banh-keo', 'Ăn vặt & Bánh kẹo ', 35, 1, 36, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(37, 'đồ uống', 'do-uong', 'Đồ uống ', 35, 1, 37, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(38, 'đồ hộp & đóng gói', 'do-hop--dong-goi', 'Đồ hộp & Đóng gói ', 35, 1, 38, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(39, 'Nấu ăn & làm bánh ', 'nau-an--lam-banh', 'Nấu ăn & Làm bánh  ', 35, 1, 39, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(40, 'đậu & hạt', 'dau--hat', 'Đậu & Hạt ', 35, 1, 40, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(41, 'đặc sản việt', 'dac-san-viet', 'Đặc sản Việt ', 35, 1, 41, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(42, 'Quà biếu', 'qua-bieu', 'Quà biếu ', 35, 1, 42, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(43, 'Khác', 'khac', 'Khác ', 35, 1, 43, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(44, 'đồ chơi', 'do-choi', 'ĐỒ CHƠI', 0, 1, 44, NULL, 43, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(45, 'đồ chơi cho trẻ sơ sinh & trẻ nhỏ', 'do-choi-cho-tre-so-sinh--tre-nho', 'Đồ chơi cho trẻ sơ sinh & trẻ nhỏ ', 44, 1, 45, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(46, 'đồ chơi giáo dục', 'do-choi-giao-duc', 'Đồ chơi giáo dục ', 44, 1, 46, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(47, 'đồ chơi vận động & ngoài trời', 'do-choi-van-dong--ngoai-troi', 'Đồ chơi vận động & Ngoài trời ', 44, 1, 47, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(48, 'Búp bê & đồ chơi nhồi bông', 'bup-be--do-choi-nhoi-bong', 'Búp bê & Đồ chơi nhồi bông ', 44, 1, 48, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(49, 'đồ chơi mô hình', 'do-choi-mo-hinh', 'Đồ Chơi Mô Hình ', 44, 1, 49, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(50, 'đồ chơi giải trí', 'do-choi-giai-tri', 'Đồ Chơi Giải Trí ', 44, 1, 50, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(51, 'Khác', 'khac', 'Khác ', 44, 1, 51, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(52, 'Thời trang nữ', 'thoi-trang-nu', 'THỜI TRANG NỮ', 0, 1, 52, NULL, 44, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(53, 'áo', 'ao', 'Áo ', 52, 1, 53, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(54, 'đầm', 'dam', 'Đầm ', 52, 1, 54, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(55, 'Chân váy', 'chan-vay', 'Chân váy ', 52, 1, 55, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(56, 'Quần', 'quan', 'Quần ', 52, 1, 56, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(57, 'Set trang phục & jumpsuit', 'set-trang-phuc--jumpsuit', 'Set trang phục & Jumpsuit ', 52, 1, 57, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(58, 'đồ đôi', 'do-doi', 'Đồ đôi ', 52, 1, 58, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(59, 'đồ lót, đồ ngủ & đồ mặc nhà', 'do-lot-do-ngu--do-mac-nha', 'Đồ lót, Đồ ngủ & Đồ mặc nhà ', 52, 1, 59, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(60, 'đồ bơi', 'do-boi', 'Đồ bơi ', 52, 1, 60, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(61, 'Trang phục thể thao', 'trang-phuc-the-thao', 'Trang phục thể thao ', 52, 1, 61, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(62, 'Phụ kiện may mặc', 'phu-kien-may-mac', 'Phụ kiện may mặc ', 52, 1, 62, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(63, 'Thời trang trung niên', 'thoi-trang-trung-nien', 'Thời trang trung niên ', 52, 1, 63, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(64, 'áo khoác & áo vest ', 'ao-khoac--ao-vest', 'Áo khoác & Áo vest  ', 52, 1, 64, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(65, 'Trang phục đông', 'trang-phuc-dong', 'Trang Phục Đông ', 52, 1, 65, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(66, 'Thời trang bầu và sau sinh', 'thoi-trang-bau-va-sau-sinh', 'Thời trang bầu và sau sinh ', 52, 1, 66, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(67, 'Khác', 'khac', 'Khác ', 52, 1, 67, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(68, 'Máy tính & laptop', 'may-tinh--laptop', 'MÁY TÍNH & LAPTOP', 0, 1, 68, NULL, 45, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(69, 'Laptop', 'laptop', 'Laptop ', 68, 1, 69, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(70, 'Máy tính bàn', 'may-tinh-ban', 'Máy Tính Bàn ', 68, 1, 70, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(71, 'Linh kiện máy tính', 'linh-kien-may-tinh', 'Linh Kiện Máy Tính ', 68, 1, 71, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(72, 'Chuột, bàn phím', 'chuot-ban-phim', 'Chuột, Bàn Phím ', 68, 1, 72, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(73, 'Thiết bị mạng ', 'thiet-bi-mang', 'Thiết Bị Mạng  ', 68, 1, 73, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(74, 'Usb & ổ cứng', 'usb--o-cung', 'USB & Ổ Cứng ', 68, 1, 74, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(75, 'Máy in, máy scan & máy chiếu', 'may-in-may-scan--may-chieu', 'Máy In, Máy Scan & Máy Chiếu ', 68, 1, 75, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(76, 'Phần mềm', 'phan-mem', 'Phần Mềm ', 68, 1, 76, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(77, 'Phụ kiện máy tính khác', 'phu-kien-may-tinh-khac', 'Phụ Kiện Máy Tính Khác ', 68, 1, 77, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(78, 'Audio & video accessories', 'audio--video-accessories', 'Audio & Video Accessories ', 68, 1, 78, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(79, 'Khác', 'khac', 'Khác ', 68, 1, 79, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(80, 'Túi ví', 'tui-vi', 'TÚI VÍ', 0, 1, 80, NULL, 46, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(81, 'Túi đeo chéo nữ', 'tui-deo-cheo-nu', 'Túi đeo chéo nữ ', 80, 1, 81, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(82, 'Túi xách nữ', 'tui-xach-nu', 'Túi xách nữ ', 80, 1, 82, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(83, 'Balo thời trang', 'balo-thoi-trang', 'Balo thời trang ', 80, 1, 83, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(84, 'Ví/bóp nữ', 'vibop-nu', 'Ví/Bóp nữ ', 80, 1, 84, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(85, 'Cặp văn phòng', 'cap-van-phong', 'Cặp văn phòng ', 80, 1, 85, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(86, 'Túi đựng tiện ích', 'tui-dung-tien-ich', 'Túi đựng tiện ích ', 80, 1, 86, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(87, 'Túi vải/túi tote', 'tui-vaitui-tote', 'Túi vải/Túi tote ', 80, 1, 87, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(88, 'Phụ kiện túi ví khác', 'phu-kien-tui-vi-khac', 'Phụ kiện túi ví khác ', 80, 1, 88, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(89, 'Khác', 'khac', 'Khác ', 80, 1, 89, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(90, 'Thể thao & du lịch', 'the-thao--du-lich', 'THỂ THAO & DU LỊCH', 0, 1, 90, NULL, 47, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(91, 'Trang phục thể thao', 'trang-phuc-the-thao', 'Trang Phục thể thao ', 90, 1, 91, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(92, 'Giày thể thao', 'giay-the-thao', 'Giày Thể Thao ', 90, 1, 92, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(93, 'Phụ kiện thể thao', 'phu-kien-the-thao', 'Phụ Kiện Thể Thao ', 90, 1, 93, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(94, 'Thể thao & thể hình ', 'the-thao--the-hinh', 'Thể Thao & Thể Hình  ', 90, 1, 94, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(95, 'Thể thao ngoài trời', 'the-thao-ngoai-troi', 'Thể thao ngoài trời ', 90, 1, 95, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(96, 'đồ dùng thể thao trong nhà', 'do-dung-the-thao-trong-nha', 'Đồ dùng thể thao trong nhà ', 90, 1, 96, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(97, 'Thể thao dưới nước', 'the-thao-duoi-nuoc', 'Thể thao dưới nước ', 90, 1, 97, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(98, 'Các môn thể thao khác ', 'cac-mon-the-thao-khac', 'Các Môn Thể Thao Khác  ', 90, 1, 98, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(99, 'Vali', 'vali', 'Vali ', 90, 1, 99, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(100, 'Hành lý', 'hanh-ly', 'Hành lý ', 90, 1, 100, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(101, 'Hoạt động dã ngoại', 'hoat-dong-da-ngoai', 'Hoạt Động Dã Ngoại ', 90, 1, 101, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(102, 'Khác', 'khac', 'Khác ', 90, 1, 102, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(103, 'Balo', 'balo', 'Balo ', 90, 1, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(104, 'Túi', 'tui', 'Túi ', 90, 1, 104, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(105, 'Khác', 'khac', 'Khác ', 90, 1, 105, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(106, 'Giặt giũ & chăm sóc nhà cửa', 'giat-giu--cham-soc-nha-cua', 'GIẶT GIŨ & CHĂM SÓC NHÀ CỬA', 0, 1, 106, NULL, 48, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(107, 'Giặt giũ', 'giat-giu', 'Giặt giũ ', 106, 1, 107, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(108, 'Giấy vệ sinh, khăn giấy', 'giay-ve-sinh-khan-giay', 'Giấy vệ sinh, khăn giấy ', 106, 1, 108, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(109, 'Vệ sinh nhà cửa', 've-sinh-nha-cua', 'Vệ sinh nhà cửa ', 106, 1, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(110, 'Vệ sinh bát đĩa ', 've-sinh-bat-dia', 'Vệ sinh bát đĩa  ', 106, 1, 110, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(111, 'Dụng cụ vệ sinh', 'dung-cu-ve-sinh', 'Dụng cụ vệ sinh ', 106, 1, 111, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(112, 'Chất khử mùi, làm thơm', 'chat-khu-mui-lam-thom', 'Chất khử mùi, làm thơm ', 106, 1, 112, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(113, 'Thuốc diệt côn trùng', 'thuoc-diet-con-trung', 'Thuốc diệt côn trùng ', 106, 1, 113, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(114, 'Túi, màng bọc thực phẩm', 'tui-mang-boc-thuc-pham', 'Túi, màng bọc thực phẩm ', 106, 1, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(115, 'Bao bì, túi đựng rác', 'bao-bi-tui-dung-rac', 'Bao bì, túi đựng rác ', 106, 1, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(116, 'Khác', 'khac', 'Khác ', 106, 1, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(117, 'điện thoại & phụ kiện', 'dien-thoai--phu-kien', 'ĐIỆN THOẠI & PHỤ KIỆN', 0, 1, 117, NULL, 49, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 11:22:26', NULL),
(118, 'điện thoại', 'dien-thoai', 'Điện thoại ', 117, 1, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(119, 'Máy tính bảng', 'may-tinh-bang', 'Máy tính bảng ', 117, 1, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(120, 'Vỏ, bao & ốp lưng', 'vo-bao--op-lung', 'Vỏ, Bao & Ốp lưng ', 117, 1, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(121, 'Miếng dán màn hình', 'mieng-dan-man-hinh', 'Miếng dán màn hình ', 117, 1, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(122, 'Pin sạc dự phòng', 'pin-sac-du-phong', 'Pin sạc dự phòng ', 117, 1, 122, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(123, 'Pin, cáp & bộ sạc', 'pin-cap--bo-sac', 'Pin, Cáp & Bộ sạc ', 117, 1, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(124, 'Giá đỡ & kẹp', 'gia-do--kep', 'Giá đỡ & Kẹp ', 117, 1, 124, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(125, 'Gậy chụp hình', 'gay-chup-hinh', 'Gậy chụp hình ', 117, 1, 125, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(126, 'Sim', 'sim', 'Sim ', 117, 1, 126, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(127, 'Khác', 'khac', 'Khác ', 117, 1, 127, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(128, 'Sức khỏe & sắc đẹp', 'suc-khoe--sac-dep', 'SỨC KHỎE & SẮC ĐẸP ', 0, 1, 128, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(129, 'Chăm sóc da ', 'cham-soc-da', 'Chăm sóc da  ', 128, 1, 129, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(130, 'Son & chăm sóc môi', 'son--cham-soc-moi', 'Son & Chăm sóc môi ', 128, 1, 130, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(131, 'Trang điểm da ', 'trang-diem-da', 'Trang điểm da  ', 128, 1, 131, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(132, 'Trang điểm mắt', 'trang-diem-mat', 'Trang điểm mắt ', 128, 1, 132, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(133, 'Mỹ phẩm nam', 'my-pham-nam', 'Mỹ phẩm nam ', 128, 1, 133, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(134, 'Tắm & chăm sóc cơ thể', 'tam--cham-soc-co-the', 'Tắm & Chăm sóc cơ thể ', 128, 1, 134, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(135, 'Chăm sóc tóc', 'cham-soc-toc', 'Chăm sóc tóc ', 128, 1, 135, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(136, 'Chăm sóc răng miệng ', 'cham-soc-rang-mieng', 'Chăm sóc răng miệng  ', 128, 1, 136, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(137, 'Máy massage & thiết bị y tế', 'may-massage--thiet-bi-y-te', 'Máy massage & Thiết bị y tế ', 128, 1, 137, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(138, 'Dụng cụ làm đẹp', 'dung-cu-lam-dep', 'Dụng cụ làm đẹp ', 128, 1, 138, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(139, 'Vệ sinh phụ nữ & hỗ trợ tình dục ', 've-sinh-phu-nu--ho-tro-tinh-duc', 'Vệ sinh phụ nữ & Hỗ trợ tình dục  ', 128, 1, 139, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(140, 'Nước hoa ', 'nuoc-hoa', 'Nước hoa  ', 128, 1, 140, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(141, 'Vitamin & thực phẩm chức năng ', 'vitamin--thuc-pham-chuc-nang', 'Vitamin & Thực phẩm chức năng  ', 128, 1, 141, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(142, 'Hoá mỹ phẩm & khác', 'hoa-my-pham--khac', 'Hoá Mỹ Phẩm & Khác ', 128, 1, 142, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(143, 'Khác', 'khac', 'Khác ', 128, 1, 143, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(144, 'Giày dép nam', 'giay-dep-nam', 'GIÀY DÉP NAM ', 0, 1, 144, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(145, 'Giày thể thao', 'giay-the-thao', 'Giày thể thao ', 144, 1, 145, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(146, 'Sneakers', 'sneakers', 'Sneakers ', 144, 1, 146, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(147, 'Giày lười  ', 'giay-luoi', 'Giày lười   ', 144, 1, 147, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(148, 'Giày tây ', 'giay-tay', 'Giày tây  ', 144, 1, 148, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(149, 'Xăng-đan ', 'xang-dan', 'Xăng-đan  ', 144, 1, 149, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(150, 'Dép ', 'dep', 'Dép  ', 144, 1, 150, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(151, 'Giày dép unisex ', 'giay-dep-unisex', 'Giày dép Unisex  ', 144, 1, 151, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(152, 'Phụ kiện giày dép', 'phu-kien-giay-dep', 'Phụ kiện giày dép ', 144, 1, 152, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(153, 'Khác', 'khac', 'Khác ', 144, 1, 153, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(154, 'Voucher & dịch vụ', 'voucher--dich-vu', 'VOUCHER & DỊCH VỤ ', 0, 1, 154, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(155, 'Nhà hàng - ăn uống  ', 'nha-hang-an-uong', 'Nhà hàng - Ăn uống   ', 154, 1, 155, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(156, 'Du lịch - khách sạn  ', 'du-lich-khach-san', 'Du lịch - Khách sạn   ', 154, 1, 156, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(157, 'Sức khỏe - làm đẹp  ', 'suc-khoe-lam-dep', 'Sức khỏe - Làm đẹp   ', 154, 1, 157, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(158, 'Sự kiện - giải trí ', 'su-kien-giai-tri', 'Sự kiện - Giải trí  ', 154, 1, 158, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(159, 'Khóa học ', 'khoa-hoc', 'Khóa học  ', 154, 1, 159, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(160, 'Nạp tiền tài khoản', 'nap-tien-tai-khoan', 'Nạp tiền tài khoản ', 154, 1, 160, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(161, 'Khác', 'khac', 'Khác ', 154, 1, 161, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(162, 'Chăm sóc thú cưng', 'cham-soc-thu-cung', 'CHĂM SÓC THÚ CƯNG ', 0, 1, 162, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(163, 'Chó  ', 'cho', 'Chó   ', 162, 1, 163, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(164, 'Mèo  ', 'meo', 'Mèo   ', 162, 1, 164, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(165, 'Fish ', 'fish', 'Fish  ', 162, 1, 165, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(166, 'Chim  ', 'chim', 'Chim   ', 162, 1, 166, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(167, 'Hamsters ', 'hamsters', 'Hamsters  ', 162, 1, 167, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(168, 'Khác', 'khac', 'Khác ', 162, 1, 168, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(169, 'Mẹ & bé', 'me--be', 'MẸ & BÉ ', 0, 1, 169, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(170, 'đồ dùng cho bé ', 'do-dung-cho-be', 'Đồ dùng cho bé  ', 169, 1, 170, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(171, 'Tã & bỉm ', 'ta--bim', 'Tã & Bỉm  ', 169, 1, 171, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(172, 'Ghế, nôi, cũi, xe đẩy & địu ', 'ghe-noi-cui-xe-day--diu', 'Ghế, Nôi, Cũi, Xe đẩy & Địu  ', 169, 1, 172, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(173, 'Sữa công thức 0-24 tháng tuổi ', 'sua-cong-thuc-0-24-thang-tuoi', 'Sữa công thức 0-24 tháng tuổi  ', 169, 1, 173, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(174, 'Thực phẩm bổ sung cho trẻ dưới 6 tháng tuổi ', 'thuc-pham-bo-sung-cho-tre-duoi-6-thang-tuoi', 'Thực phẩm bổ sung cho trẻ dưới 6 tháng tuổi  ', 169, 1, 174, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(175, 'Sữa trên 24 tháng tuổi ', 'sua-tren-24-thang-tuoi', 'Sữa trên 24 tháng tuổi  ', 169, 1, 175, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(176, 'Thực phẩm cho bé từ 6 tháng tuổi ', 'thuc-pham-cho-be-tu-6-thang-tuoi', 'Thực phẩm cho bé từ 6 tháng tuổi  ', 169, 1, 176, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(177, 'Vitamin, thực phẩm bổ sung và đồ dùng cho mẹ', 'vitamin-thuc-pham-bo-sung-va-do-dung-cho-me', 'Vitamin, thực phẩm bổ sung và đồ dùng cho mẹ ', 169, 1, 177, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(178, 'Khác', 'khac', 'Khác ', 169, 1, 178, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(179, 'Máy ảnh - máy quay phim', 'may-anh-may-quay-phim', 'MÁY ẢNH - MÁY QUAY PHIM ', 0, 1, 179, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(180, 'Máy ảnh du lịch & chụp lấy liền', 'may-anh-du-lich--chup-lay-lien', 'Máy ảnh du lịch & chụp lấy liền ', 179, 1, 180, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(181, 'Thẻ nhớ', 'the-nho', 'Thẻ nhớ ', 179, 1, 181, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(182, 'Camera giám sát & webcam', 'camera-giam-sat--webcam', 'Camera giám sát & Webcam ', 179, 1, 182, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(183, 'Máy ảnh dslr', 'may-anh-dslr', 'Máy ảnh DSLR ', 179, 1, 183, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(184, 'Máy ảnh không gương lật ', 'may-anh-khong-guong-lat', 'Máy ảnh không gương lật  ', 179, 1, 184, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(185, 'Máy quay phim ', 'may-quay-phim', 'Máy quay phim  ', 179, 1, 185, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(186, 'Phụ kiện máy ảnh ', 'phu-kien-may-anh', 'Phụ kiện máy ảnh  ', 179, 1, 186, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(187, 'Chân máy ảnh & gậy ', 'chan-may-anh--gay', 'Chân máy ảnh & Gậy  ', 179, 1, 187, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(188, 'Balo, túi đựng, bao da', 'balo-tui-dung-bao-da', 'Balo, Túi đựng, Bao da ', 179, 1, 188, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(189, 'Khác', 'khac', 'Khác ', 179, 1, 189, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(190, 'Phụ kiện thời trang', 'phu-kien-thoi-trang', 'PHỤ KIỆN THỜI TRANG ', 0, 1, 190, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(191, 'Trang sức', 'trang-suc', 'Trang sức ', 190, 1, 191, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(192, 'Kính mắt ', 'kinh-mat', 'Kính mắt  ', 190, 1, 192, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(193, 'Nón & dù', 'non--du', 'Nón & Dù ', 190, 1, 193, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(194, 'Khăn, tất & găng tay', 'khan-tat--gang-tay', 'Khăn, Tất & Găng tay ', 190, 1, 194, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(195, 'Phụ kiện tóc', 'phu-kien-toc', 'Phụ kiện tóc ', 190, 1, 195, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(196, 'Dây lưng', 'day-lung', 'Dây lưng ', 190, 1, 196, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(197, 'Hình xăm ', 'hinh-xam', 'Hình xăm  ', 190, 1, 197, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(198, 'Phụ kiện cưới', 'phu-kien-cuoi', 'Phụ kiện cưới ', 190, 1, 198, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(199, 'Khẩu trang', 'khau-trang', 'Khẩu trang ', 190, 1, 199, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(200, 'Khác', 'khac', 'Khác ', 190, 1, 200, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(201, 'ô tô - xe máy - xe đạp', 'o-to-xe-may-xe-dap', 'Ô TÔ - XE MÁY - XE ĐẠP ', 0, 1, 201, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(202, 'Chăm sóc, sửa chữa xe', 'cham-soc-sua-chua-xe', 'Chăm sóc, sửa chữa xe ', 201, 1, 202, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(203, 'Chăm sóc ô tô ', 'cham-soc-o-to', 'Chăm sóc ô tô  ', 201, 1, 203, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(204, 'Phương tiện ', 'phuong-tien', 'Phương tiện  ', 201, 1, 204, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(205, 'Mũ bảo hiểm', 'mu-bao-hiem', 'Mũ bảo hiểm ', 201, 1, 205, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(206, 'Phụ kiện xe máy ', 'phu-kien-xe-may', 'Phụ kiện xe máy  ', 201, 1, 206, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(207, 'Phụ tùng xe máy ', 'phu-tung-xe-may', 'Phụ tùng xe máy  ', 201, 1, 207, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(208, 'Chăm sóc xe máy', 'cham-soc-xe-may', 'Chăm sóc xe máy ', 201, 1, 208, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(209, 'Phụ tùng, phụ kiện xe đạp, xe đạp điện', 'phu-tung-phu-kien-xe-dap-xe-dap-dien', 'Phụ tùng, phụ kiện xe đạp, xe đạp điện ', 201, 1, 209, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(210, 'Khác', 'khac', 'Khác ', 201, 1, 210, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(211, 'Thời trang trẻ em', 'thoi-trang-tre-em', 'THỜI TRANG TRẺ EM ', 0, 1, 211, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(212, 'Bé gái (5-14 tuổi)', 'be-gai-5-14-tuoi', 'Bé gái (5-14 tuổi) ', 211, 1, 212, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(213, 'Bé trai (5-14 tuổi) ', 'be-trai-5-14-tuoi', 'Bé trai (5-14 tuổi)  ', 211, 1, 213, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(214, 'Bé gái (3 tháng-4 tuổi)', 'be-gai-3-thang-4-tuoi', 'Bé gái (3 tháng-4 tuổi) ', 211, 1, 214, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(215, 'Bé trai (3 tháng-4 tuổi)', 'be-trai-3-thang-4-tuoi', 'Bé trai (3 tháng-4 tuổi) ', 211, 1, 215, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(216, 'Sơ sinh (0-12 tháng)', 'so-sinh-0-12-thang', 'Sơ sinh (0-12 tháng) ', 211, 1, 216, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(217, 'Phụ kiện ', 'phu-kien', 'Phụ kiện  ', 211, 1, 217, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(218, 'Giày dép', 'giay-dep', 'Giày dép ', 211, 1, 218, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(219, 'Khác', 'khac', 'Khác ', 211, 1, 219, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(220, 'Thiết bị điện tử', 'thiet-bi-dien-tu', 'THIẾT BỊ ĐIỆN TỬ ', 0, 1, 220, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(221, 'Thiết bị âm thanh', 'thiet-bi-am-thanh', 'Thiết bị âm thanh ', 220, 1, 221, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(222, 'Thiết bị đeo thông minh', 'thiet-bi-deo-thong-minh', 'Thiết bị đeo thông minh ', 220, 1, 222, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(223, 'Android tivi box', 'android-tivi-box', 'Android Tivi Box ', 220, 1, 223, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(224, 'Tivi', 'tivi', 'Tivi ', 220, 1, 224, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(225, 'Phụ kiện tivi ', 'phu-kien-tivi', 'Phụ kiện tivi  ', 220, 1, 225, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(226, 'Tai nghe', 'tai-nghe', 'Tai Nghe ', 220, 1, 226, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(227, 'Phụ kiện & thiết bị game', 'phu-kien--thiet-bi-game', 'Phụ kiện & Thiết bị game ', 220, 1, 227, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(228, 'Khác', 'khac', 'Khác ', 220, 1, 228, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(229, 'Giày dép nữ', 'giay-dep-nu', 'GIÀY DÉP NỮ ', 0, 1, 229, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(230, 'Giày cao gót ', 'giay-cao-got', 'Giày cao gót  ', 229, 1, 230, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(231, 'Giày đế bằng', 'giay-de-bang', 'Giày đế bằng ', 229, 1, 231, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(232, 'Sandal', 'sandal', 'Sandal ', 229, 1, 232, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(233, 'Giày cao gót', 'giay-cao-got', 'Giày cao gót ', 229, 1, 233, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(234, 'Giày đế xuồng', 'giay-de-xuong', 'Giày đế xuồng ', 229, 1, 234, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(235, 'Bốt', 'bot', 'Bốt ', 229, 1, 235, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(236, 'Guốc', 'guoc', 'Guốc ', 229, 1, 236, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(237, 'Dép nữ', 'dep-nu', 'Dép nữ ', 229, 1, 237, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(238, 'Giày sneaker và giày thể thao', 'giay-sneaker-va-giay-the-thao', 'Giày sneaker và Giày thể thao ', 229, 1, 238, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(239, 'Phụ kiện giày', 'phu-kien-giay', 'Phụ kiện giày ', 229, 1, 239, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(240, 'Khác', 'khac', 'Khác ', 229, 1, 240, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(241, 'Thiết bị điện gia dụng', 'thiet-bi-dien-gia-dung', 'THIẾT BỊ ĐIỆN GIA DỤNG ', 0, 1, 241, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(242, 'đồ gia dụng nhà bếp', 'do-gia-dung-nha-bep', 'Đồ gia dụng nhà bếp ', 241, 1, 242, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(243, 'Quạt & máy nóng lạnh', 'quat--may-nong-lanh', 'Quạt & Máy nóng lạnh ', 241, 1, 243, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(244, 'Thiết bị chăm sóc quần áo', 'thiet-bi-cham-soc-quan-ao', 'Thiết bị chăm sóc quần áo ', 241, 1, 244, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(245, 'Máy hút bụi & thiết bị làm sạch', 'may-hut-bui--thiet-bi-lam-sach', 'Máy hút bụi & Thiết bị làm sạch ', 241, 1, 245, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(246, 'đồ gia dụng lớn ', 'do-gia-dung-lon', 'Đồ gia dụng lớn  ', 241, 1, 246, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(247, 'Khác', 'khac', 'Khác ', 241, 1, 247, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(248, 'Nhà sách online', 'nha-sach-online', 'NHÀ SÁCH ONLINE ', 0, 1, 248, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:43', NULL),
(249, 'Văn phòng phẩm', 'van-phong-pham', 'Văn Phòng Phẩm ', 248, 1, 249, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:44', NULL),
(250, 'Flashcards ', 'flashcards', 'Flashcards  ', 248, 1, 250, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:44', NULL),
(251, 'Nhạc cụ ', 'nhac-cu', 'Nhạc Cụ  ', 248, 1, 251, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:44', NULL),
(252, 'Quà lưu niệm ', 'qua-luu-niem', 'Quà Lưu Niệm  ', 248, 1, 252, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:44', NULL),
(253, 'Sách ngoại văn', 'sach-ngoai-van', 'Sách Ngoại Văn ', 248, 1, 253, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:44', NULL),
(254, 'Sách tiếng việt', 'sach-tieng-viet', 'Sách Tiếng Việt ', 248, 1, 254, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:44', NULL),
(255, 'Khác', 'khac', 'Khác ', 248, 1, 255, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:44', NULL),
(256, 'Sản phẩm khác', 'san-pham-khac', 'SẢN PHẨM KHÁC ', 0, 1, 256, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-27 11:46:25', '2019-04-16 15:06:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lck_core_users`
--

CREATE TABLE `lck_core_users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(191) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `fcm_token` varchar(255) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `device_os` varchar(255) DEFAULT NULL,
  `recommender` varchar(20) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `cardid` varchar(20) DEFAULT NULL,
  `avatar_file_path` varchar(255) DEFAULT NULL,
  `cover_file_path` varchar(255) DEFAULT NULL,
  `rating` decimal(2,1) UNSIGNED DEFAULT '0.0' COMMENT 'Đánh giá',
  `balance` double DEFAULT '0',
  `point` float(2,1) DEFAULT '0.0',
  `address` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `account_position` int(2) DEFAULT NULL,
  `account_type` int(2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL COMMENT 'Mô tả shop',
  `chat_response_rate` decimal(10,2) DEFAULT '0.00' COMMENT 'Tỉ lệ phản hồi chat (%)',
  `preparing_time` int(11) DEFAULT '0' COMMENT 'Thời gian chuẩn bị hàng (ngày)',
  `cancel_order_rate` decimal(10,2) DEFAULT '0.00' COMMENT 'Tỉ lệ hủy đơn (%)',
  `total_products` int(11) DEFAULT '0' COMMENT 'Tổng sản phẩm',
  `last_login` datetime DEFAULT NULL COMMENT 'Lần cuối login',
  `following_count` int(11) DEFAULT '0' COMMENT 'Số người đang theo dõi',
  `follower_count` int(11) DEFAULT '0' COMMENT 'Số người theo dõi shop',
  `is_verified` tinyint(1) DEFAULT '0' COMMENT 'Tài khoản đã xác thực hay chưa'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tai khoan' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_core_users`
--

INSERT INTO `lck_core_users` (`id`, `fullname`, `email`, `phone`, `username`, `password`, `remember_token`, `created_at`, `updated_at`, `status`, `fcm_token`, `device_id`, `device_os`, `recommender`, `gender`, `birthday`, `cardid`, `avatar_file_path`, `cover_file_path`, `rating`, `balance`, `point`, `address`, `facebook_id`, `google_id`, `account_position`, `account_type`, `description`, `chat_response_rate`, `preparing_time`, `cancel_order_rate`, `total_products`, `last_login`, `following_count`, `follower_count`, `is_verified`) VALUES
(2, 'Thanh Luan', 'bapcui1102@gmail.com', '0979427220', NULL, '$2y$10$EbaxZPLrYWwNojCmOG8iDueG7R/YtqkZPh4pF9UNRawsd5iyJC372', 'lqErMMYgtrllopXIn8joHMP5NWFLNuPbEBHLhAeYkCQlnsWwyHDpsl1IKVVv', '2018-09-09 15:51:34', '2019-01-18 09:50:38', 1, 'c3fwbTP3soY:APA91bE96I_iItvFKjQjCY22elyx4HUF-ttQuPG2ICDgtWat77i1W5j4EcwMUcdCvUdU6tfOA9iTch7pdSV30gJJSmNJ0Yh0rcyAYPH8pWIh2IzBBPKDo4aiXayZjPUdFi6ckp7Qu-6p', '34c910b54e166bde', 'android_26', NULL, 1, '2004-01-01', NULL, NULL, NULL, '0.0', 0, 0.0, 'hcm châhmb', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 'Thành Luân', NULL, '0979427220', 'thanhluan986', '$2y$10$ZSdas9CX/LTWhsK3kIDw8Of7SHrSThIlEjpNtF6lISXTt53E8H1Tq', 'IbtuoqOEgNvawB8NjChALSfpdGMaUpP0HWIrTgMjz3JsQbcozwVNR1q7hnma', '2019-02-27 09:54:59', '2019-04-20 10:02:20', 1, NULL, NULL, NULL, NULL, 1, '1993-11-12', NULL, NULL, NULL, '1.1', 0, 0.0, NULL, NULL, NULL, 1, NULL, 'Shop abcd', '5.50', 3, '40.50', 10, '2019-04-23 14:52:38', 50, 20, 0),
(5, 'Phùng Duy Thành', 'phungduythanh99999@gmail.com', '0123456789', 'pduythanh', '$2y$10$dWgudMw4JEQrMo76SbZU.eRhkA8cxh1Hvh7MsrXXfi7jyNTAP6xwG', NULL, '2019-03-11 16:41:37', '2019-05-17 15:41:10', 1, NULL, NULL, NULL, NULL, NULL, '2019-03-27', NULL, '2019/05/17/f8324ce8546cb90e9fa6dab1fea85b45.jpeg', '2019/03/21/f4c8022120ad17fe24e01cb9af233660.jpeg', '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, 'phan thanh ca', 'phanthanhca@gmail.com', '0976337424', 'thanhca', '$2y$10$wH3Fv0VkI9p67YC0tWu7i.lh10l.0p48f0OTEEnGiaPuRqhW11SgW', NULL, '2019-03-11 17:28:14', '2019-03-11 17:30:01', 1, 'sdsddsasdaadssdasdaas', 'ddssdsdsd', 'android', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(7, 'Minh tuấn', 'crash147852369@gmail.com', '034546813', 'tuan', '$2y$10$tDZcwB2FbI8qhQJiAhzgK.wPyJPuThlo7ttgOSKe7aJR2kNmqndZa', NULL, '2019-03-19 09:52:13', '2019-06-12 17:12:54', 1, NULL, NULL, NULL, NULL, NULL, '1995-05-30', NULL, '2019/06/12/6fe6515140b113cf65afd074eff474ce.png', NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, NULL, 'pinkp7996@gmail.com', '1254678543', 'tendangnhap', '$2y$10$Mn3SBXQqt3H44Z4iLCcFwu1I6EwrAvtoTpuBOvXrR./IMwGdaf8We', NULL, '2019-04-05 17:06:19', '2019-04-05 17:06:47', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, NULL, 'hausb@nsue.com', '637864846944', 'akihhwu', '$2y$10$JPRsaqcLCfhUUKCwk7OrKuntd57LBDSSexV48jdyM6l.4G0/WU6/a', NULL, '2019-04-05 17:14:44', '2019-04-05 17:14:44', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, NULL, 'hauhs@neies.com', '946483484644', 'bjsbssss', '$2y$10$6b6uFfqCmOBkKQwHZgWcOunCdcstC/MaY4myfzEx0BSSSL7Js5K7S', NULL, '2019-04-05 17:16:33', '2019-04-05 17:16:33', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(13, NULL, 'thanhluan986@gmail.com', '979427220', 'cuimia1102', '$2y$10$vkuZ2PSZ7yEKLzCuI52yr.TmQ1VSJFEmEnDHO6YcpBlxBbz8Nr/he', 'XQp98NVIOxwFGNJapImEzTLrs7vk4aKzHDD8J3q3GaCq85Iuy6qKYhavLtxc', '2019-04-19 15:48:53', '2019-04-19 15:48:53', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(14, NULL, 'bapcui11022@gmail.com', '09794272202', 'thanhluan986x', '$2y$10$FMLLhGoMVmc8XnVx9WcCB.o4pC.j6zbIeAYGcQp/L/it1sUvoihnS', NULL, '2019-04-19 15:56:12', '2019-04-19 15:56:12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(15, NULL, 'bapcui110222@gmail.com', '097942722021', 'thanhluan986x2', '$2y$10$vy1hT3iB4NCSCd/ZnsgZ8.z4ZquZabKCtLY5KyD9FZCN1urFj7KA.', NULL, '2019-04-19 15:56:35', '2019-04-19 15:56:35', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(16, NULL, 'bapcui1102c@gmail.com', '9794272200', 'bapcui1102xs', '$2y$10$eSBXQqJK3XCQJB9Dhq686OHAG/LG68eGHNV.ncS.D.AW6XHhQdyyO', NULL, '2019-04-19 15:59:25', '2019-04-19 15:59:25', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(17, NULL, 'bapcui1102dd@gmail.com', '9794272204', 'bapcui1102x', '$2y$10$Rjf9g2gFWeIxcgb5dmdfIeF3RSucJhQed2VGTIJsyYbIaLD8HgMAq', NULL, '2019-04-19 16:00:11', '2019-04-19 16:00:11', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(18, NULL, 'bapcui110s2@gmail.com', NULL, 'cuibapkkk2s', '$2y$10$RkLiLSjUlsUh8T5OR.rrzO5YgqiCvkkeIv5W060XRazSUvcMlUxMW', NULL, '2019-04-19 16:01:31', '2019-04-19 16:01:31', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(19, NULL, 'bapcui1102x@gmail.com', '0979427233', 'thanhluan986xxx', '$2y$10$LnE14u5uSnx8t31t3LBtT.ShFQcNgR8duHAWTUwo.dn75/S9OtI/i', NULL, '2019-04-19 16:02:48', '2019-04-19 16:02:48', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(20, 'Shop bán đồ', 'thanhluan986df@gmail.com', '979427220', 'thanhluan986cccc', '$2y$10$97cn2LR5SsqzHjbaOMjg4uJ21A2.w8Hm25VwKnet7iSe27V4aEGLy', 'LkGZ9bprhQeyPhDXO0QJCscAE4c6Ca8hqJ7oqKZ1eTpvtsuA26BwFjsCZyF1', '2019-04-19 16:05:57', '2019-04-19 17:46:06', 0, NULL, NULL, NULL, NULL, 1, '1993-05-06', NULL, NULL, NULL, '0.0', 0, 0.0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lck_core_users_activations`
--

CREATE TABLE `lck_core_users_activations` (
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `otp_code` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_core_users_activations`
--

INSERT INTO `lck_core_users_activations` (`user_id`, `token`, `otp_code`, `created_at`) VALUES
(9, '70c37d701018e09cf1ac0ffdfc281cae00edf858f5ce9ce76d9aa0fedd0e8cd0', 675013, '2019-04-05 17:14:44'),
(10, '75c02c6dd8e45562f0f6255bd742329d2770ca30e40bbd2b523229a5909b2be1', 307966, '2019-04-05 17:16:33'),
(13, 'fbb2f8a21b4b8062eb955bda8655c78aa9d3ac53af5ab9f8e52fefdaaf0e160a', 632249, '2019-04-19 15:48:53'),
(14, 'bc09a5fb3874bd7691ead35df8eeb86043f5e7d420fa55089d3a8842e3f85d05', 994025, '2019-04-19 15:56:12'),
(15, '178262de61614ab624fac557b42f67c7a5a34f6e1db3dd7b6e63af316be92fb9', 239827, '2019-04-19 15:56:35'),
(16, '29a7f5791f8f419872228d9070d4b9448da5eb3ccc2abe44f3b16762376d13a3', 795263, '2019-04-19 15:59:25'),
(17, '46b44cf4ec9e2a028edcc0d4cc1196fc0f615612eb09495b8a98b8f52a8dc991', 916476, '2019-04-19 16:00:11'),
(18, '8eacb8c584fd7ae04f37e39c28bdc9ef7af6f3365b514b50ec558a2c920593ae', 586572, '2019-04-19 16:01:31'),
(19, '5e1fc88c32dc5a3e51e2d570b153cc27fe908ff2d9207e116c409b55efafb469', 967434, '2019-04-19 16:02:48'),
(20, '7845b210e5ce4fb16c66f43626d59af60d0f840c9d018a78e5dd846460c83ef5', 750950, '2019-04-19 16:05:57');

-- --------------------------------------------------------

--
-- Table structure for table `lck_favorite`
--

CREATE TABLE `lck_favorite` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='San pham yeu thich';

-- --------------------------------------------------------

--
-- Table structure for table `lck_files`
--

CREATE TABLE `lck_files` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_temp` tinyint(1) DEFAULT '1',
  `type` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='File da tai len' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_files`
--

INSERT INTO `lck_files` (`id`, `user_id`, `file_path`, `created_at`, `updated_at`, `is_temp`, `type`) VALUES
(1, 2, '2019/01/18/06639264529636367db55bd0357391ab.jpg', '2019-01-18 17:11:53', '2019-01-18 17:30:59', NULL, 2),
(2, 3, '2019/at1.jpg', '2019-01-18 17:11:53', '2019-01-18 17:30:59', NULL, 3),
(3, 3, '2019/at2.jpg', '2019-01-18 17:11:53', '2019-01-18 17:11:53', NULL, 3),
(4, 3, '2019/at3.jpg', '2019-01-18 17:11:53', '2019-01-18 17:11:53', NULL, 3),
(5, 3, '2019/at4.jpg', '2019-01-18 17:11:53', '2019-01-18 17:11:53', NULL, 3),
(6, 3, '2019/qj1.jpg', '2019-01-18 17:11:53', '2019-01-18 17:30:59', NULL, 3),
(7, 3, '2019/qj2.jpg', '2019-01-18 17:11:53', '2019-01-18 17:11:53', NULL, 3),
(8, 3, '2019/qj3.jpg', '2019-01-18 17:11:53', '2019-01-18 17:11:53', NULL, 3),
(9, 3, '2019/qj4.jpg', '2019-01-18 17:11:53', '2019-01-18 17:11:53', NULL, 3),
(10, 3, '2019/qj5.jpg', '2019-01-18 17:11:53', '2019-01-18 17:11:53', NULL, 3),
(11, 2, '2019/03/08/4c907379bb608f144f9d92dfabf9ecc9.JPG', '2019-03-08 13:59:14', '2019-03-08 13:59:14', 1, 3),
(12, 2, '2019/03/08/9e0cc5dca19aa97b3e7f31dd1e5a809a.JPG', '2019-03-08 14:00:10', '2019-03-08 14:00:10', 1, 3),
(13, 2, '2019/03/08/9e0adea4131a819795abba074e72ee40.JPG', '2019-03-08 14:00:14', '2019-03-08 14:00:14', 1, 3),
(14, 2, '2019/03/08/d173422523f627f0c67de186c7d425fa.JPG', '2019-03-08 14:00:26', '2019-03-08 14:00:26', 1, 3),
(15, 2, '2019/03/08/3b4cb48847c027c44d7a9db395944244.JPG', '2019-03-08 14:01:57', '2019-03-08 14:01:57', 1, 3),
(16, 2, '2019/03/08/9d36efb09e29fd1189a8f1aa190e0f96.jpg', '2019-03-08 14:02:00', '2019-03-08 14:02:00', 1, 3),
(17, 2, '2019/03/08/1ed9016ca1626878eb8d264e7fedd7dc.JPG', '2019-03-08 14:02:35', '2019-03-08 14:02:35', 1, 3),
(18, 2, '2019/03/08/98484ab15b80dae7b6095a58d1fea098.jpg', '2019-03-08 14:02:39', '2019-03-08 14:02:39', 1, 3),
(19, 2, '2019/03/08/351a5b61ec69681bb57554021dc39c1b.JPG', '2019-03-08 14:03:08', '2019-03-08 14:03:08', 1, 3),
(20, 2, '2019/03/08/453ac3aeffc5e6f3871548ef81c308c5.JPG', '2019-03-08 14:03:28', '2019-03-08 14:03:28', 1, 3),
(21, 2, '2019/03/08/b21db7e7a3fb5f45e9a546d012d65fbc.jpg', '2019-03-08 14:03:30', '2019-03-08 14:03:30', 1, 3),
(22, 2, '2019/03/08/d295aac1ae738aef849c202ac86ad851.JPG', '2019-03-08 14:08:31', '2019-03-08 14:08:31', 1, 3),
(23, 2, '2019/03/08/6ce33a7771b43d8f615ba8302f570a8a.jpg', '2019-03-08 14:08:33', '2019-03-08 14:08:33', 1, 3),
(24, 2, '2019/03/08/3a477bcddbfeb5ad6de6ca1091f9965b.png', '2019-03-08 14:13:09', '2019-03-08 14:13:09', 1, 3),
(25, 2, '2019/03/08/1cfa9aa05e1d107bdf944e9aa0061cdb.png', '2019-03-08 14:31:37', '2019-03-08 14:31:37', 1, 3),
(26, 3, '2019/03/13/8647e64f3e86a52f54440f8d8e0ae50a.jpeg', '2019-03-13 21:16:02', '2019-03-13 21:16:02', 1, 1),
(27, 5, '2019/03/14/1a986608938c6d60dd378e1603fe458e.png', '2019-03-14 11:37:27', '2019-03-14 11:37:27', 1, 1),
(28, 5, '2019/03/18/b899b5b1ba482db2df7cbf2e07161ab8.png', '2019-03-18 13:41:47', '2019-03-18 13:41:47', 1, 1),
(29, 5, '2019/03/18/9e49c1d7dbf4681b3abe69c2095ee156.jpeg', '2019-03-18 13:44:28', '2019-03-18 13:44:28', 1, 1),
(30, 5, '2019/03/18/896918f76b98e627a6735eb7ae69f681.jpeg', '2019-03-18 13:44:28', '2019-03-18 13:44:28', 1, 2),
(31, 5, '2019/03/18/d2791b720219df3dcdad6cb12ca4c389.jpeg', '2019-03-18 13:49:32', '2019-03-18 13:49:33', NULL, 1),
(32, 5, '2019/03/18/7277f623909b2221715f1ebb8c177454.jpeg', '2019-03-18 13:49:33', '2019-03-18 13:49:33', NULL, 2),
(33, 5, '2019/03/18/926442662bf34476a41b86214da127ee.jpeg', '2019-03-18 14:37:49', '2019-03-18 14:37:49', NULL, 1),
(34, 5, '2019/03/18/bbb0e97989bc986975d7008ef87b8f65.jpeg', '2019-03-18 14:37:49', '2019-03-18 14:37:49', NULL, 2),
(35, 2, '2019/banner/banner-1.jpg', '2019-03-08 14:31:37', '2019-03-08 14:31:37', NULL, NULL),
(36, 2, '2019/banner/banner-2.jpg', '2019-03-08 14:31:37', '2019-03-08 14:31:37', NULL, NULL),
(37, 2, '2019/banner/banner-3.png', '2019-03-08 14:31:37', '2019-03-08 14:31:37', NULL, NULL),
(38, 2, '2019/banner/banner-4.jpg', '2019-03-08 14:31:37', '2019-03-08 14:31:37', NULL, NULL),
(39, 2, '2019/03/20/b0f4f3b194054e3daf1acc9e3dbe66df.png', '2019-03-20 17:27:46', '2019-03-20 17:27:46', 1, 3),
(40, 2, '2019/03/20/bc6110d97590f7cc450f5795ec97d43f.png', '2019-03-20 17:28:08', '2019-03-20 17:28:08', 1, 3),
(41, 2, '2019/03/20/93288928baafeab1fdad5d29b4ca463a.png', '2019-03-20 17:28:20', '2019-03-20 17:28:20', 1, 3),
(42, 2, '2019/03/20/ffdbd461317bac25c239bc89d9e25c16.png', '2019-03-20 17:28:31', '2019-03-20 17:28:31', 1, 3),
(43, 2, '2019/03/20/5af3528e40faec8ceb00f6662e8d8ce3.png', '2019-03-20 17:28:43', '2019-03-20 17:28:43', 1, 3),
(44, 2, '2019/03/20/5f73ae735d2635992164f3b399669a0c.png', '2019-03-20 17:28:57', '2019-03-20 17:28:57', 1, 3),
(45, 2, '2019/03/20/50bc807c87f3e1ebc0a6eef60ef7fec2.png', '2019-03-20 17:29:08', '2019-03-20 17:29:08', 1, 3),
(46, 2, '2019/03/20/778fb187eb128897579803f4e2e44e95.png', '2019-03-20 17:29:19', '2019-03-20 17:29:19', 1, 3),
(47, 2, '2019/03/20/35d9f96fa9c80b6c34e32c0e42c4a2ff.png', '2019-03-20 17:29:58', '2019-03-20 17:29:58', 1, 3),
(48, 2, '2019/03/20/01ee5969343a2b16e0147606148790a4.png', '2019-03-20 17:30:19', '2019-03-20 17:30:19', 1, 3),
(49, 2, '2019/03/20/82f1e55b248e7a195063b1fc75061f5f.png', '2019-03-20 17:30:34', '2019-03-20 17:30:34', 1, 3),
(50, 5, '2019/03/21/a482463f192f41541cee1866d539f9e2.jpeg', '2019-03-21 11:49:35', '2019-03-21 11:49:36', NULL, 1),
(51, 5, '2019/03/21/f4c8022120ad17fe24e01cb9af233660.jpeg', '2019-03-21 11:49:59', '2019-03-21 11:50:00', NULL, 2),
(52, 2, '2019/trademark/nokia.jpg', '2019-03-08 14:31:37', '2019-03-08 14:31:37', NULL, NULL),
(53, 2, '2019/trademark/xmen.jpg', '2019-03-08 14:31:37', '2019-03-08 14:31:37', NULL, NULL),
(54, 2, '2019/trademark/ohui.jpg', '2019-03-08 14:31:37', '2019-03-08 14:31:37', NULL, NULL),
(55, 2, '2019/trademark/bitits.jpg', '2019-03-08 14:31:37', '2019-03-08 14:31:37', NULL, NULL),
(56, 2, '2019/04/05/fbc7319d261930a2e8b964d26562a95e.jpg', '2019-04-05 16:55:48', '2019-04-05 16:55:48', 1, 3),
(57, 2, '2019/04/05/2906ede99481055c12c59b75ab84c8bf.jpg', '2019-04-05 16:55:51', '2019-04-05 16:55:51', 1, 3),
(58, 2, '2019/04/18/f6cbb66f15f0e362696cd474595a0e7b.jpeg', '2019-04-18 09:36:50', '2019-04-18 09:36:50', 1, 3),
(59, 2, '2019/04/18/c709715e995a79159ddb67a1c5f9db2e.jpeg', '2019-04-18 09:36:52', '2019-04-18 09:36:52', 1, 3),
(60, 3, '2019/04/29/99071d43b972fa2772bb3387f717d143.jpg', '2019-04-29 15:54:31', '2019-04-29 15:54:31', 1, 3),
(61, 3, '2019/04/29/c60c0568ea66f59814a21ff2844446f2.jpg', '2019-04-29 15:54:33', '2019-04-29 15:54:33', 1, 3),
(62, 3, '2019/04/29/5dbdf7e0d19264ffe6714b410e5435f3.jpg', '2019-04-29 15:54:34', '2019-04-29 15:54:34', 1, 3),
(63, 3, '2019/04/29/f16f33e362956b5ca45915b249dc6cae.jpg', '2019-04-29 15:54:36', '2019-04-29 15:54:36', 1, 3),
(64, 3, '2019/04/29/6a0213dcfdda754cab0192926f508914.jpg', '2019-04-29 16:29:23', '2019-04-29 16:29:23', 1, 3),
(65, 3, '2019/04/29/59dea1e374e5356d329dd662af674ce7.jpg', '2019-04-29 16:29:24', '2019-04-29 16:29:24', 1, 3),
(66, 3, '2019/04/29/42cdcab601abf85b3560307bdd5ec9ae.jpg', '2019-04-29 16:29:25', '2019-04-29 16:29:25', 1, 3),
(67, 3, '2019/04/29/c1f5406025251ec49403202d3e6c6a77.jpg', '2019-04-29 16:29:28', '2019-04-29 16:29:28', 1, 3),
(68, 3, '2019/04/29/8201a214e5607746ebfbe6e302486df3.jpg', '2019-04-29 16:31:15', '2019-04-29 16:31:15', 1, 3),
(69, 3, '2019/04/29/5b72dcfd472d316e6e7fe91fda319eb4.jpg', '2019-04-29 16:31:16', '2019-04-29 16:31:16', 1, 3),
(70, 3, '2019/04/29/003b5bcc4f01742388f36a2cbdba94e7.jpg', '2019-04-29 16:31:17', '2019-04-29 16:31:17', 1, 3),
(71, 3, '2019/04/29/6fc05cdcdc7d24c9e1cc92d03fd9db52.jpg', '2019-04-29 16:33:14', '2019-04-29 16:33:14', 1, 3),
(72, 3, '2019/04/29/f9c8fb89d58e854955b34e0419770951.jpg', '2019-04-29 16:33:15', '2019-04-29 16:33:15', 1, 3),
(73, 3, '2019/04/29/8c35839603aa995d48acb482f2888861.jpg', '2019-04-29 16:33:16', '2019-04-29 16:33:16', 1, 3),
(74, 3, '2019/04/29/64ed8506f78043811228b0970bb6cd07.jpg', '2019-04-29 16:33:17', '2019-04-29 16:33:17', 1, 3),
(75, 3, '2019/04/29/bb4354fa4d7de91b6ce85eaf00b90607.jpg', '2019-04-29 16:34:40', '2019-04-29 16:34:40', 1, 3),
(76, 3, '2019/04/29/eff261bd492d8cc28eff57b48d00efa9.jpg', '2019-04-29 16:37:58', '2019-04-29 16:37:58', 1, 3),
(77, 3, '2019/04/29/62acb3ba868c17c0e7d6918c7916b795.jpg', '2019-04-29 16:38:07', '2019-04-29 16:38:07', 1, 3),
(78, 3, '2019/04/29/761ebe030d12b64ac5bb8c23506093f7.jpg', '2019-04-29 16:40:41', '2019-04-29 16:40:41', 1, 3),
(79, 3, '2019/04/29/9b90aa9f699e37e37f109d347475dc43.jpg', '2019-04-29 16:40:50', '2019-04-29 16:40:50', 1, 3),
(80, 3, '2019/04/29/a2d8d927de535032a94091c7f3bf33d6.jpg', '2019-04-29 16:40:51', '2019-04-29 16:40:51', 1, 3),
(81, 3, '2019/04/29/b974a5b1230ef21422cc0ded37ee5167.jpg', '2019-04-29 16:40:52', '2019-04-29 16:40:52', 1, 3),
(82, 3, '2019/04/29/6e45c068b8eab504734c4b33e5b17736.jpg', '2019-04-29 16:41:29', '2019-04-29 16:41:29', 1, 3),
(83, 3, '2019/04/29/4ce225622fd3cdecab3eb805e5a96014.jpg', '2019-04-29 16:41:30', '2019-04-29 16:41:30', 1, 3),
(84, 3, '2019/04/29/dfb4a4973681ff3a249f6d756b89e7ef.jpg', '2019-04-29 16:41:31', '2019-04-29 16:41:31', 1, 3),
(85, 3, '2019/04/29/b62212889e6bdfdeda4d424c3dd9dfc2.jpg', '2019-04-29 16:41:33', '2019-04-29 16:41:33', 1, 3),
(86, 3, '2019/04/29/17ff67c04164aa3c9d4e96d78c63bd28.jpg', '2019-04-29 16:46:16', '2019-04-29 16:46:16', 1, 3),
(87, 3, '2019/04/29/9a90e56e928453642588b860da8da78d.jpg', '2019-04-29 16:46:17', '2019-04-29 16:46:17', 1, 3),
(88, 3, '2019/04/29/6ff98504a6354f047212eedacd109d70.jpg', '2019-04-29 16:46:18', '2019-04-29 16:46:18', 1, 3),
(89, 3, '2019/04/29/2e826ad37711b0834741f1c8979abd3b.jpg', '2019-04-29 16:46:19', '2019-04-29 16:46:19', 1, 3),
(90, 3, '2019/04/29/4271fb6a7d69320915ef05b1f09fccff.jpg', '2019-04-29 16:52:40', '2019-04-29 16:52:40', 1, 3),
(91, 3, '2019/04/29/5436c4d5bf358729ccff9c8690a28535.jpg', '2019-04-29 16:59:25', '2019-04-29 16:59:25', 1, 3),
(92, 3, '2019/04/29/e80b8b5947c1dfe58a3cdcb123374da8.jpg', '2019-04-29 16:59:26', '2019-04-29 16:59:26', 1, 3),
(93, 3, '2019/04/29/0d19b46cfec73a804b8d8a1b41f52936.jpg', '2019-04-29 16:59:28', '2019-04-29 16:59:28', 1, 3),
(94, 3, '2019/04/29/eb1052237510ded611225cd495fce2b2.jpg', '2019-04-29 16:59:29', '2019-04-29 16:59:29', 1, 3),
(95, 3, '2019/04/29/324d8e6213f17c66b7d3210a3c7e0a2a.jpg', '2019-04-29 17:55:53', '2019-04-29 17:55:53', 1, 3),
(96, 3, '2019/04/29/c8146bcf790ea975e5df992526508627.jpg', '2019-04-29 17:55:54', '2019-04-29 17:55:54', 1, 3),
(97, 3, '2019/04/29/112c752ec6b70aecae18fe232b0ff2a5.jpg', '2019-04-29 17:55:55', '2019-04-29 17:55:55', 1, 3),
(98, 3, '2019/04/29/999c1c68207f7eff3206425c08f98331.jpg', '2019-04-29 17:55:57', '2019-04-29 17:55:57', 1, 3),
(99, 3, '2019/04/29/6c9c01315b855b946ca77fa60b7cf4e4.jpg', '2019-04-29 21:38:23', '2019-04-29 21:38:23', 1, 3),
(100, 3, '2019/04/29/a2a028220165b1a6425e9c3cb7bff025.jpg', '2019-04-29 21:38:28', '2019-04-29 21:38:28', 1, 3),
(101, 3, '2019/04/29/763efa0c08de50d10b370f380fcb883b.jpg', '2019-04-29 21:38:30', '2019-04-29 21:38:30', 1, 3),
(102, 3, '2019/04/29/f473b333555da407f99356bb75cec80b.jpg', '2019-04-29 21:38:31', '2019-04-29 21:38:31', 1, 3),
(103, 3, '2019/04/29/8352b0a66979627b3e327e231bd6a497.jpg', '2019-04-29 21:41:20', '2019-04-29 21:41:20', 1, 3),
(104, 3, '2019/04/29/6481bf2751005e4209b1da7a656ad6e7.jpg', '2019-04-29 21:41:21', '2019-04-29 21:41:21', 1, 3),
(105, 3, '2019/04/29/f60623295fb9d7f61e708fecacf9f0fb.jpg', '2019-04-29 21:41:23', '2019-04-29 21:41:23', 1, 3),
(106, 3, '2019/04/29/092a08a6a7b3c90c95260f8b62d461e9.jpg', '2019-04-29 21:41:24', '2019-04-29 21:41:24', 1, 3),
(107, 3, '2019/04/29/3238c8a120307ca7f0bde531e727d8c8.jpg', '2019-04-29 21:44:05', '2019-04-29 21:44:05', 1, 3),
(108, 3, '2019/04/29/fc6fd3c55e91fcff41083b579d776df2.jpg', '2019-04-29 21:44:06', '2019-04-29 21:44:06', 1, 3),
(109, 3, '2019/04/29/a9fbdd42861d69c4765ca90018861c99.jpg', '2019-04-29 21:44:07', '2019-04-29 21:44:07', 1, 3),
(110, 3, '2019/04/29/73c338b6f5aa0c83e8d735a178c354d3.jpg', '2019-04-29 21:44:08', '2019-04-29 21:44:08', 1, 3),
(111, 3, '2019/04/29/3588d07285e76950ad97de8ccdb6a4c5.jpg', '2019-04-29 21:45:18', '2019-04-29 21:45:18', 1, 3),
(112, 3, '2019/04/29/29ae85a2055391131a97ee598cd7bb6f.jpg', '2019-04-29 21:45:19', '2019-04-29 21:45:19', 1, 3),
(113, 3, '2019/04/29/42c617c30753293b794acc15799c0ada.jpg', '2019-04-29 21:45:21', '2019-04-29 21:45:21', 1, 3),
(114, 3, '2019/04/29/65c0b8a17bbf0a0a8116886a5fe64bcd.jpg', '2019-04-29 21:45:22', '2019-04-29 21:45:22', 1, 3),
(115, 3, '2019/04/29/54eb23fd7e56ed9d68004fc406b39f99.jpg', '2019-04-29 21:46:40', '2019-04-29 21:46:40', 1, 3),
(116, 3, '2019/04/29/4ab815c5a983d1ed9bf7af3ac79618bf.jpg', '2019-04-29 21:46:41', '2019-04-29 21:46:41', 1, 3),
(117, 3, '2019/04/29/7397fd3e683415942d822230638c24b0.jpg', '2019-04-29 21:46:42', '2019-04-29 21:46:42', 1, 3),
(118, 3, '2019/04/29/e8ceaf711bb667d9858ebea6e6931a35.jpg', '2019-04-29 21:46:44', '2019-04-29 21:46:44', 1, 3),
(119, 3, '2019/04/29/98eae44630c3732a72839d08e67e0003.jpg', '2019-04-29 21:47:38', '2019-04-29 21:47:38', 1, 3),
(120, 3, '2019/04/29/cabc3ab6b68b13fd0c226b6fdb247271.jpg', '2019-04-29 21:47:39', '2019-04-29 21:47:39', 1, 3),
(121, 3, '2019/04/29/2ceb0497f9219733a82f28e7becd8f6a.jpg', '2019-04-29 21:47:40', '2019-04-29 21:47:40', 1, 3),
(122, 3, '2019/04/29/0b325291fdad12dd35bde337588f9f3c.jpg', '2019-04-29 21:47:41', '2019-04-29 21:47:41', 1, 3),
(123, 3, '2019/04/29/8b708bf407590fbe1324abf2a8186b09.jpg', '2019-04-29 22:21:53', '2019-04-29 22:21:53', 1, 3),
(124, 3, '2019/04/29/88d3bc4fa88eb4d941ae1b9cf83381de.jpg', '2019-04-29 22:21:54', '2019-04-29 22:21:54', 1, 3),
(125, 3, '2019/04/29/3d6663284fdf46ebebc8e988f9c3a442.jpg', '2019-04-29 22:22:34', '2019-04-29 22:22:34', 1, 3),
(126, 3, '2019/04/29/f91e76f462c0b3308c6118f4d25084b9.jpg', '2019-04-29 22:22:35', '2019-04-29 22:22:35', 1, 3),
(127, 3, '2019/04/29/fc96ae12a9f01fd35bb412731639a335.jpg', '2019-04-29 22:22:36', '2019-04-29 22:22:36', 1, 3),
(128, 3, '2019/04/29/d63f13ee31846cfd61df50688a8fad73.jpg', '2019-04-29 23:08:29', '2019-04-29 23:08:29', 1, 3),
(129, 3, '2019/05/10/671b872e7fabbd484b77ed26819b2665.jpg', '2019-05-10 17:11:07', '2019-05-10 17:11:07', 1, 3),
(130, 3, '2019/05/10/610e638997a71257ac1028b5f58f0602.jpg', '2019-05-10 17:11:08', '2019-05-10 17:11:08', 1, 3),
(131, 3, '2019/05/10/a55fe10e618d9bd5e92581f6f428bcec.jpg', '2019-05-10 17:11:08', '2019-05-10 17:11:08', 1, 3),
(132, 3, '2019/05/10/ed7df04a9cb39b135fefa26508279904.jpg', '2019-05-10 17:11:09', '2019-05-10 17:11:09', 1, 3),
(133, 3, '2019/05/10/cdfe2d4cb1ec7f4a7bd128446e1633a7.jpg', '2019-05-10 17:11:53', '2019-05-10 17:11:53', 1, 3),
(134, 3, '2019/05/10/8e022a68dcd7be6e77ca79b334606ff2.jpg', '2019-05-10 17:11:54', '2019-05-10 17:11:54', 1, 3),
(135, 3, '2019/05/10/52df4f243348975524a151eaac39f646.jpg', '2019-05-10 17:11:54', '2019-05-10 17:11:54', 1, 3),
(136, 2, '2019/05/10/ff9b91595c9ede26b3e2bcc470f1fed9.jpg', '2019-05-10 17:23:43', '2019-05-10 17:23:43', 1, 3),
(137, 2, '2019/05/10/ae1d24ac63c6183e537687d10c94b8fc.jpg', '2019-05-10 17:23:46', '2019-05-10 17:23:46', 1, 3),
(138, 2, '2019/05/10/9edb8f26c64197ac73e3b52dd5d59edd.jpg', '2019-05-10 17:24:01', '2019-05-10 17:24:01', 1, 3),
(139, 2, '2019/05/10/0f7c031635d37d52345e55eae5f8130c.jpg', '2019-05-10 17:24:03', '2019-05-10 17:24:03', 1, 3),
(140, 2, '2019/05/10/c84bf810522be11559945b464cf219bb.jpg', '2019-05-10 17:24:22', '2019-05-10 17:24:22', 1, 3),
(141, 2, '2019/05/10/8fbb835fefb2ea395f9617c088db502a.jpg', '2019-05-10 17:24:25', '2019-05-10 17:24:25', 1, 3),
(142, 2, '2019/05/10/6565a6467095425fe4af9dd9ae022215.jpg', '2019-05-10 17:24:31', '2019-05-10 17:24:31', 1, 3),
(143, 2, '2019/05/10/83c0cbb828d909911bce1d22869d8d31.jpg', '2019-05-10 17:24:33', '2019-05-10 17:24:33', 1, 3),
(144, 2, '2019/05/10/21015db2a407cd7107e15c38882405e4.jpg', '2019-05-10 17:24:42', '2019-05-10 17:24:42', 1, 3),
(145, 2, '2019/05/10/6493886098c58fd273770af9cd9c056f.jpg', '2019-05-10 17:24:44', '2019-05-10 17:24:44', 1, 3),
(146, 2, '2019/05/10/91bc96f6815eb42cf3e46aea2dfc79a3.jpg', '2019-05-10 17:24:50', '2019-05-10 17:24:50', 1, 3),
(147, 2, '2019/05/10/1ca639a5f6d997d17b7337ca4cae9744.jpg', '2019-05-10 17:24:53', '2019-05-10 17:24:53', 1, 3),
(148, 2, '2019/05/10/24df267fd46f6055433dbb69f718e564.jpg', '2019-05-10 17:24:59', '2019-05-10 17:24:59', 1, 3),
(149, 2, '2019/05/10/6f3c3686c0b1b8d5b504afb0fbff1642.jpg', '2019-05-10 17:25:01', '2019-05-10 17:25:01', 1, 3),
(150, 2, '2019/05/10/ac1d5fe9a1bee218bce7bc5c3c075635.jpg', '2019-05-10 17:25:14', '2019-05-10 17:25:14', 1, 3),
(151, 5, '2019/05/17/f8324ce8546cb90e9fa6dab1fea85b45.jpeg', '2019-05-17 15:41:09', '2019-05-17 15:41:10', NULL, 1),
(152, 7, '2019/05/30/ba4ea87ae800bc520e1068521e2e95f7.png', '2019-05-30 17:46:58', '2019-05-30 17:46:58', 1, 2),
(153, 7, '2019/06/12/9211455028631f425af9f473121ffddf.png', '2019-06-12 17:11:45', '2019-06-12 17:11:45', 1, 2),
(154, 7, '2019/06/12/ff274606680537334a409054860ad8cb.png', '2019-06-12 17:12:11', '2019-06-12 17:12:12', NULL, 1),
(155, 7, '2019/06/12/6fe6515140b113cf65afd074eff474ce.png', '2019-06-12 17:12:53', '2019-06-12 17:12:54', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lck_history_view`
--

CREATE TABLE `lck_history_view` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='San pham da xem';

--
-- Dumping data for table `lck_history_view`
--

INSERT INTO `lck_history_view` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 5, 1, '2019-03-26 13:37:05', '2019-05-17 16:00:13'),
(2, 6, 9, '2019-03-26 22:12:20', '2019-03-26 22:12:20'),
(3, 6, 17, '2019-03-26 22:13:34', '2019-03-26 22:13:34'),
(4, 6, 15, '2019-03-26 22:13:39', '2019-04-02 10:25:26'),
(5, 6, 10, '2019-03-26 22:13:52', '2019-03-26 22:13:52'),
(6, 6, 1, '2019-03-27 09:52:45', '2019-03-27 09:53:12'),
(7, 5, 27, '2019-03-27 14:04:09', '2019-08-05 11:12:14'),
(8, 5, 26, '2019-03-27 14:13:49', '2019-05-30 14:17:41'),
(9, 5, 12, '2019-03-27 14:37:22', '2019-03-27 14:37:22'),
(10, 5, 2, '2019-03-27 15:11:09', '2019-05-16 11:11:51'),
(11, 5, 14, '2019-03-27 15:13:43', '2019-03-27 15:13:43'),
(12, 5, 3, '2019-03-29 09:51:40', '2019-05-11 15:31:14'),
(13, 5, 4, '2019-03-29 09:51:50', '2019-04-05 17:03:27'),
(14, 5, 11, '2019-03-29 09:51:59', '2019-03-29 09:51:59'),
(15, 5, 7, '2019-03-29 09:57:27', '2019-04-03 10:20:01'),
(16, 5, 5, '2019-03-29 09:57:49', '2019-04-04 09:24:44'),
(17, 5, 17, '2019-03-29 10:07:58', '2019-03-29 10:07:58'),
(18, 3, 26, '2019-04-02 16:26:55', '2019-04-05 11:12:59'),
(19, 5, 6, '2019-04-03 10:19:59', '2019-04-03 10:27:44'),
(20, 5, 9, '2019-04-03 10:20:04', '2019-04-03 10:20:04'),
(21, 5, 8, '2019-04-03 10:20:06', '2019-04-03 10:20:06'),
(22, 5, 10, '2019-04-03 10:20:08', '2019-04-03 10:20:08'),
(23, 5, 16, '2019-04-03 15:18:32', '2019-04-03 15:18:32'),
(24, 8, 26, '2019-04-05 17:07:17', '2019-04-05 17:07:17'),
(25, 5, 28, '2019-05-11 15:29:55', '2019-08-05 11:12:10'),
(26, 5, 29, '2019-05-11 15:30:06', '2019-08-05 11:12:06'),
(27, 7, 2, '2019-05-13 13:37:27', '2019-05-13 13:37:27'),
(28, 7, 29, '2019-05-21 08:29:52', '2019-06-03 17:43:13'),
(29, 7, 28, '2019-05-21 08:30:12', '2019-05-21 08:30:12'),
(30, 7, 27, '2019-05-21 08:30:17', '2019-05-21 08:30:35'),
(31, 5, 19, '2019-05-30 14:17:37', '2019-05-30 14:17:37'),
(32, 7, 26, '2019-05-30 22:50:19', '2019-06-01 19:22:28'),
(33, 7, 1, '2019-06-01 19:22:22', '2019-06-01 19:22:22');

-- --------------------------------------------------------

--
-- Table structure for table `lck_location_district`
--

CREATE TABLE `lck_location_district` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ascii` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT '1',
  `user_id_created` int(11) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_location_district`
--

INSERT INTO `lck_location_district` (`id`, `name`, `name_ascii`, `name_origin`, `location`, `type`, `province_id`, `position`, `user_id_created`, `created_at`, `updated_at`, `deleted_at`) VALUES
(760, 'Quận 1', 'Quan 1', 'Quận 1', '10.7756587-106.7004238', 'Quận', 79, 1, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:26.000000', NULL),
(761, 'Quận 12', '12', '12', '10.8671531-106.6413322', 'Quận', 79, 12, NULL, '2018-10-22 23:42:17.000000', '2018-10-23 13:48:41.000000', NULL),
(762, 'Quận nhà bè', 'nha be', 'nhà bè', '10.8494094-106.7537055', 'Quận', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2019-01-19 11:08:07.000000', NULL),
(763, 'Quận 9', 'Quan 9', 'Quận 9', '10.8428402-106.8286851', 'Quận', 79, 9, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:26.000000', NULL),
(764, 'Quận Gò Vấp', 'Quan Go Vap', 'Quận Gò Vấp', '10.8386779-106.6652904', 'Quận', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:26.000000', NULL),
(765, 'Quận Bình Thạnh', 'Quan Binh Thanh', 'Quận Bình Thạnh', '10.8105831-106.7091422', 'Quận', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:26.000000', NULL),
(766, 'Quận Tân Bình', 'Quan Tan Binh', 'Quận Tân Bình', '10.8014659-106.6525974', 'Quận', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:26.000000', NULL),
(767, 'Quận Tân Phú', 'Quan Tan Phu', 'Quận Tân Phú', '10.7900517-106.6281901', 'Quận', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:26.000000', NULL),
(768, 'Quận Phú Nhuận', 'Quan Phu Nhuan', 'Quận Phú Nhuận', '10.7991944-106.6802639', 'Quận', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:26.000000', NULL),
(769, 'Quận 2', 'Quan 2', 'Quận 2', '10.7872729-106.7498105', 'Quận', 79, 2, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(770, 'Quận 3', '3', '3', '10.7843695-106.6844089', 'Quận', 79, 3, NULL, '2018-10-22 23:42:17.000000', '2018-10-23 13:49:15.000000', NULL),
(771, 'Quận 10', 'Quan 10', 'Quận 10', '10.7745965-106.6679542', 'Quận', 79, 10, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(772, 'Quận 11', 'Quan 11', 'Quận 11', '10.7629739-106.650084', 'Quận', 79, 11, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(773, 'Quận 4', 'Quan 4', 'Quận 4', '10.7578263-106.7012968', 'Quận', 79, 4, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(774, 'Quận 5', 'Quan 5', 'Quận 5', '10.7540279-106.6633746', 'Quận', 79, 5, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(775, 'Quận 6', 'Quan 6', 'Quận 6', '10.7480929-106.6352362', 'Quận', 79, 6, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(776, 'Quận 8', 'Quan 8', 'Quận 8', '10.7240878-106.6286259', 'Quận', 79, 8, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(777, 'Quận Bình Tân', 'Quan Binh Tan', 'Quận Bình Tân', '10.7652581-106.6038535', 'Quận', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(778, 'Quận 7', 'Quan 7', 'Quận 7', '10.7340344-106.7215787', 'Quận', 79, 7, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(783, 'Huyện Củ Chi', 'Huyen Cu Chi', 'Huyện Củ Chi', '11.0066683-106.5131967', 'Huyện', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(784, 'Huyện Hóc Môn', 'Huyen Hoc Mon', 'Huyện Hóc Môn', '10.8839675-106.5870611', 'Huyện', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:27.000000', NULL),
(785, 'Huyện Bình Chánh', 'Huyen Binh Chanh', 'Huyện Bình Chánh', '10.687392-106.5938538', 'Huyện', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:28.000000', NULL),
(786, 'Huyện Nhà Bè', 'Huyen Nha Be', 'Huyện Nhà Bè', '10.6952642-106.704874', 'Huyện', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:28.000000', NULL),
(787, 'Huyện Cần Giờ', 'Huyen Can Gio', 'Huyện Cần Giờ', '10.5083266-106.8635004', 'Huyện', 79, 13, NULL, '2018-10-22 23:42:17.000000', '2018-10-22 23:52:28.000000', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lck_location_province`
--

CREATE TABLE `lck_location_province` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ascii` varchar(255) DEFAULT NULL,
  `name_origin` varchar(255) DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_location_province`
--

INSERT INTO `lck_location_province` (`id`, `name`, `name_ascii`, `name_origin`, `location`, `type`, `user_id_created`, `created_at`, `updated_at`, `deleted_at`) VALUES
(79, 'Thành phố Hồ Chí Minh', 'Ho Chi Minh', 'Hồ Chí Minh', '10.8230989-106.6296638', 'Thành phố', NULL, '2018-10-22 23:42:17', '2018-10-23 11:25:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lck_location_street`
--

CREATE TABLE `lck_location_street` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `name_ascii` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `district_id` int(11) NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_location_street`
--

INSERT INTO `lck_location_street` (`id`, `name`, `name_ascii`, `location`, `ward_id`, `district_id`, `user_id_created`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Pasteur', 'Pasteur', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(2, 'Alexandre De Rhodes', 'Alexandre De Rhodes', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(3, 'Bà Lê Chân', 'Ba Le Chan', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-08 16:45:32', NULL),
(4, 'Lê Văn Hưu', 'Le Van Huu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(5, 'Bùi Viện', 'Bui Vien', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(6, 'Hồ Hảo Hớn', 'Ho Hao Hon', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(7, 'Nguyễn Hữu Cầu', 'Nguyen Huu Cau', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(8, 'Phan Tôn', 'Phan Ton', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(9, 'Võ Văn Kiệt', 'Vo Van Kiet', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(10, 'Cống Quỳnh', 'Cong Quynh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(11, 'Đề Thám', 'De Tham', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(12, 'Đỗ Quang Đẩu', 'Do Quang Dau', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(13, 'Nguyễn Phi Khanh', 'Nguyen Phi Khanh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(14, 'Trần Quang Khải', 'Tran Quang Khai', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(15, 'Lê Thị Riêng', 'Le Thi Rieng', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(16, 'Bùi Thị Xuân', 'Bui Thi Xuan', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(17, 'Trần Đình Xu', 'Tran Dinh Xu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(18, 'Nguyễn Văn Nguyễn', 'Nguyen Van Nguyen', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(19, 'Nguyễn Trãi', 'Nguyen Trai', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(20, 'Nguyễn Bỉnh Khiêm', 'Nguyen Binh Khiem', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(21, 'Trần Khắc Chân', 'Tran Khac Chan', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(22, 'Trần Hưng Đạo', 'Tran Hung Dao', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(23, 'Thạch Thị Thanh', 'Thach Thi Thanh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(24, 'Trần Khánh Dư', 'Tran Khanh Du', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(25, 'Nguyễn Cảnh Chân', 'Nguyen Canh Chan', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(26, 'Nguyễn Công Trứ', 'Nguyen Cong Tru', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(27, 'Cô Giang', 'Co Giang', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:14', NULL),
(28, 'Võ Thị Sáu', 'Vo Thi Sau', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(29, 'Nguyễn Văn Thủ', 'Nguyen Van Thu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(30, 'Trần Quý Khoách', 'Tran Quy Khoach', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(31, 'Lý Tự Trọng', 'Ly Tu Trong', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(32, 'Đinh Công Tráng', 'Dinh Cong Tráng', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(33, 'Lương Hữu Khánh', 'Luong Huu Khanh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(34, 'Trịnh Văn Cấn', 'Trinh Van Can', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(35, 'Phan Bội Châu', 'Phan Boi Chau', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(36, 'Lê Thánh Tôn', 'Le Thanh Ton', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(37, 'Nguyễn Thị Minh Khai', 'Nguyen Thi Minh Khai', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(38, 'Nguyễn Cư Trinh', 'Nguyen Cu Trinh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(39, 'Phan Ngữ', 'Phan Ngu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(40, 'Tôn Thất Đạm', 'Ton That Dam', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(41, 'Nguyễn An Ninh', 'Nguyen An Ninh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(42, 'Huỳnh Thúc Kháng', 'Huynh Thuc Khang', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(43, 'Đặng Dung', 'Dang Dung', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(44, 'Hai Bà Trưng', 'Hai Ba Trung', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(45, 'Thái Văn Lung', 'Thai Van Lung', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(46, 'Mai Thị Lựu', 'Mai Thi Luu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(47, 'Nguyễn Đình Chiểu', 'Nguyen Dinh Chieu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(48, 'Calmette', 'Calmette', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(49, 'Tôn Thất Tùng', 'Ton That Tung', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(50, 'Đông Du', 'Dong Du', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(51, 'Bến Chương Dương', 'Ben Chuong Duong', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(52, 'Nguyễn Thái Bình', 'Nguyen Thai Binh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(53, 'Yersin', 'Yersin', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(54, 'Hàm Nghi', 'Ham Nghi', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(55, 'Huỳnh Khương Ninh', 'Huynh Khuong Ninh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(56, 'Đinh Tiên Hoàng', 'Dinh Tien Hoang', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(57, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(58, 'Ký Con', 'Ky Con', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(59, 'Điện Biên Phủ', 'Dien Bien Phu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(60, 'Cô Bắc', 'Co Bac', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(61, 'Lê Công Kiều', 'Le Cong Kieu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(62, 'Nguyễn Huệ', 'Nguyen Hue', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(63, 'Nam Kỳ Khởi Nghĩa', 'Nam Ky Khoi Nghia', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(64, 'Hàn Thuyên', 'Han Thuyen', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(65, 'Nguyễn Văn Tráng', 'Nguyen Van Trang', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(66, 'Mạc Đĩnh Chi', 'Mac Dinh Chi', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(67, 'Đồng Khởi', 'Dong Khoi', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(68, 'Trương Định', 'Truong Dinh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(69, 'Phạm Ngũ Lão', 'Pham Ngu Lao', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(70, 'Đặng Thị Nhu', 'Dang Thi Nhu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(71, 'Hồ Tùng Mậu', 'Ho Tung Mau', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(72, 'Tôn Đức Thắng', 'Ton Duc Thang', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(73, 'Lê Anh Xuân', 'Le Anh Xuan', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(74, 'Thi Sách', 'Thi Sach', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(75, 'Ngô Đức Kế', 'Ngo Duc Ke', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(76, 'Mạc Thị Bưởi', 'Mac Thi Buoi', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(77, 'Hải Triều', 'Hai Trieu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(78, 'Ngô Văn Nam', 'Ngo Van Nam', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(79, 'xxx', 'xxx', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(80, 'Hồ Huấn Nghiệp', 'Hò Huán Nghiẹp', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(81, 'Chu Mạnh Trinh', 'Chu Manh Trinh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(82, 'Nguyễn Thái Học', 'Nguyen Thai Hoc', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(83, 'Nguyễn Du', 'Nguyen Du', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(84, 'Lê Duẩn', 'Le Duan', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(85, 'Thủ Khoa Huân', 'Thu Khoa Huan', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(86, 'Lê Lợi', 'Le Loi', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(87, 'Nguyễn Trung Ngạn', 'Nguyen Trung Ngan', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(88, 'Nguyễn Siêu', 'Nguyen Sieu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-02 11:49:01', NULL),
(89, 'Trường Sa', 'Truong Sa', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(90, 'Nguyễn Hữu Cảnh', 'Nguyen Huu Canh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(91, 'Phạm Ngọc Thạch', 'Pham Ngoc Thach', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(92, 'Hải Nguyên', 'Hai Nguyen', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(93, 'Nguyễn Trung Trực', 'Nguyen Trung Truc', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(94, 'Tôn Thất Thiệp', 'Ton Thát Thiẹp', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(95, 'Bến Vân Đồn', 'Ben Van Don', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(96, 'Nguyễn Văn Cừ', 'Nguyen Van Cu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(97, 'Lê Lai', 'Le Lai', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(98, 'Phó Đức Chính', 'Pho Duc Chinh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(99, 'Phạm Viết Chánh', 'Pham Viet Chanh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(100, 'Cao Bá Quát', 'Cao Bá Quát', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(101, 'Sương Nguyệt Ánh', 'Suong Nguyet Anh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(102, 'Nguyễn Thiệp', 'Nguyen Thiep', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(103, 'Lê Thị Hồng Gấm', 'Le Thi Hong Gam', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(104, 'Đặng Trần Côn', 'Dang Tran Con', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(105, 'Phan Chu Trinh', 'Phan Chu Trinh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(106, 'Phạm Hồng Thái', 'Phạm Hòng Thái', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(107, 'Nam Quốc Cang', 'Nam Quoc Cang', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(108, 'Lưu Văn Lang', 'Luu Van Lang', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(109, 'Nguyễn Thị Nghĩa', 'Nguyen Thi Nghia', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(110, 'Phan Văn Trường', 'Phan Van Truong', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(111, 'Nguyễn Khắc Nhu', 'Nguyen Khac Nhu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(112, 'Nguyễn Văn Giai', 'Nguyen Van Giai', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(113, 'Phan Liêm', 'Phan Liem', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(114, 'Phan Kế Bính', 'Phan Ke Binh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(115, 'Hoàng Sa', 'Hoang Sa', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(116, 'Phùng Khắc Khoan', 'Phùng Khác Khoan', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(117, 'Nguyễn Huy Tự', 'Nguyen Huy Tu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(118, 'Trương Hán Siêu', 'Truong Han Sieu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(119, 'Trần Cao Vân', 'Tran Cao Van', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(120, 'Nguyễn Thành Ý', 'Nguyen Thanh Y', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(121, 'Cây Điệp', 'Cay Diẹp', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(122, 'Trần Doãn Khanh', 'Tran Doan Khanh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(123, 'Hòa Mỹ', 'Hòa Mỹ', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(124, 'Lý Văn Phức', 'Ly Van Phuc', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(125, 'Trần Nhật Duật', 'Tran Nhat Duat', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(126, 'Bùi Hữu Nghĩa', 'Bui Huu Nghia', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(127, 'Cao Bá Nhạ', 'Cao Ba Nha', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(128, 'Trần  Hưng Đạo', 'Tràn  Hung Dạo', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(129, 'Trần Quang Diệu', 'Tran Quang Dieu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(130, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(131, 'Điện Biên Phủ', 'Dien Bien Phu', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(132, 'Hoàng Sa', 'Hoang Sa', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(133, 'Nguyễn Đình Chiểu', 'Nguyen Dinh Chieu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(134, 'Phạm Văn Đồng', 'Pham Van Dong', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(135, 'Hai Bà Trưng', 'Hai Ba Trung', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(136, 'Lý Thái Tổ', 'Ly Thai To', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(137, 'Lý Chính Thắng', 'Ly Chinh Thang', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(138, 'Vạn Kiếp', 'Van Kiep', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(139, 'Lam Sơn', 'Lam Son', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(140, 'Nguyễn Thiện Thuật', 'Nguyen Thien Thuat', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(141, 'Vũ Huy Tấn', 'Vu Huy Tan', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(142, 'Bùi Hữu Nghĩa', 'Bui Huu Nghia', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(143, 'Vũ Tùng', 'Vu Tung', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(144, 'Vườn Chuối', 'Vuon Chuoi', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(145, 'Pasteur', 'Pasteur', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(146, 'Xô Viết Nghệ Tĩnh', 'Xo Viét Nghẹ Tĩnh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(147, 'Phan Văn Hân', 'Phan Van Han', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(148, 'Đường Số 4 - CX Đô Thành', 'Duong So 4 - CX Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(149, 'Lê Quang Định', 'Le Quang Dịnh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(150, 'Nơ Trang Long', 'No Trang Long', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(151, 'Phan Chu Trinh', 'Phan Chu Trinh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(152, 'Bùi Đình Túy', 'Bui Dinh Tuy', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(153, 'Cao Thắng', 'Cao Thang', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(154, 'Đinh Tiên Hoàng', 'Dinh Tien Hoang', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(155, 'Trần Văn Đang', 'Tran Van Dang', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(156, 'Nguyễn Hiền', 'Nguyen Hien', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(157, 'Cư Xá Phan Đăng Lưu', 'Cu Xa Phan Dang Luu', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(158, 'Nguyễn Văn Đậu', 'Nguyen Van Dau', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(159, 'Trường Sa', 'Truong Sa', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(160, 'Phùng Văn Cung', 'Phung Van Cung', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(161, 'Phan Văn Trị', 'Phan Van Tri', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(162, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(163, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(164, 'Nam Kỳ Khởi Nghĩa', 'Nam Ky Khoi Nghia', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(165, 'Điện Biên Phủ', 'Dien Bien Phu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(166, 'Bàn Cờ', 'Ban Co', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(167, 'Nguyễn Sơn Hà', 'Nguyen Son Ha', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(168, 'Cư Xá Đô Thành', 'Cu Xa Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(169, 'Phó Đức Chính', 'Pho Duc Chinh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(170, 'Võ Văn Tần', 'Vo Van Tan', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(171, 'Lê Văn Sỹ', 'Le Van Sỹ', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(172, 'Bình Quới', 'Binh Quoi', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(173, 'Ngô Thời Nhiệm', 'Ngo Thoi Nhiem', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(174, 'Bạch Đằng', 'Bach Dang', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(175, 'Lê Văn Sỹ', 'Le Van Sỹ', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(176, 'Trần Kế Xương', 'Tran Ke Xuong', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(177, 'Quốc Lộ 13', 'Quoc Lo 13', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(178, 'Hoàng Hoa Thám', 'Hoang Hoa Tham', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(179, 'Nguyễn Cửu Vân', 'Nguyen Cuu Van', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(180, 'Chu Văn An', 'Chu Van An', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(181, 'Huỳnh Văn Bánh', 'Huynh Van Banh', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(182, 'Tăng Bạt Hổ', 'Tang Bat Ho', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(183, 'Đinh Bộ Lĩnh', 'Dinh Bo Linh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(184, 'Võ Thị Sáu', 'Vo Thi Sau', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(185, 'Trần Quốc Thảo', 'Tran Quoc Thao', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(186, 'Nguyễn Đình Chiểu', 'Nguyen Dinh Chieu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(187, 'Yên Đỗ', 'Yen Do', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(188, 'Nguyễn Xí', 'Nguyen Xi', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(189, 'Phan Đăng Lưu', 'Phan Dang Luu', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(190, 'Nguyên Hồng', 'Nguyen Hong', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(191, 'Đường Số 3 - CX Đô Thành', 'Duong So 3 - CX Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(192, 'Nguyễn Thượng Hiền', 'Nguyen Thuong Hien', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(193, 'Nguyễn Duy', 'Nguyen Duy', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(194, 'Hoa Sứ', 'Hoa Su', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(195, 'Võ Duy Ninh', 'Vo Duy Ninh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(196, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(197, 'Huỳnh Tịnh Của', 'Huynh Tinh Cua', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(198, 'Trần Kế Xương', 'Tran Ke Xuong', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(199, 'Cô Bắc', 'Co Bac', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(200, 'Cô Giang', 'Co Giang', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(201, 'Kỳ Đồng', 'Ky Dong', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(202, 'Đường Số 1 - CX Đô Thành', 'Duong So 1 - CX Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(203, 'Nguyễn Thượng Hiền', 'Nguyen Thuong Hien', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(204, 'Lê Văn Sỹ', 'Le Van Sỹ', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(205, 'Ngô Tất Tố', 'Ngo Tát Tó', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(206, 'Phan Đăng Lưu', 'Phan Dang Luu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(207, 'Trần Quý Cáp', 'Tran Quy Cap', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(208, 'Nguyễn Lâm', 'Nguyen Lam', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(209, 'Phan Xích Long', 'Phan Xich Long', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(210, 'Đường Số 3', 'Duong So 3', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(211, 'Nguyễn Hữu Cảnh', 'Nguyen Huu Canh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(212, 'Huyền Quang', 'Huyen Quang', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(213, 'Huỳnh Đình Hai', 'Huynh Dinh Hai', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(214, 'Trường Sa', 'Truong Sa', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(215, 'Nguyễn Lâm', 'Nguyen Lam', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(216, 'Nguyễn Thiện Thuật', 'Nguyen Thien Thuat', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(217, 'D2', 'D2', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(218, 'Rạch Bùng Binh', 'Rach Bung Binh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(219, 'Ung Văn Khiêm', 'Ung Van Khiem', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(220, 'Trần Bình Trọng', 'Tran Binh Trong', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(221, 'Bình Lợi', 'Binh Loi', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(222, 'Đường Trục 30', 'Duong Truc 30', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(223, 'Đường Số 1 - Chu Văn An', 'Duong So 1 - Chu Van An', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(224, 'Nguyễn Thông', 'Nguyen Thong', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(225, 'Nguyễn Công Hoan', 'Nguyen Cong Hoan', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(226, 'Nguyễn Gia Thiều', 'Nguyẽn Gia Thièu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(227, 'Hoa Lài', 'Hoa Lai', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(228, 'Đường D2', 'Duong D2', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(229, 'Võ Trường Toản', 'Vo Truong Toan', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(230, 'Cù Lao', 'Cu Lao', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(231, 'Nguyễn Văn Mai', 'Nguyen Van Mai', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(232, 'Trần Quốc Toản', 'Tran Quoc Toan', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(233, 'Đặng Văn Ngữ', 'Dang Van Ngu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(234, 'Hồ Văn Huê', 'Ho Van Hue', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(235, 'Đoàn Thị Điểm', 'Doan Thi Diem', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(236, 'Miếu Nổi', 'Mieu Noi', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(237, 'Nguyễn Công Hoan', 'Nguyen Cong Hoan', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(238, 'Hoa Lan', 'Hoa Lan', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(239, 'Hoa Cúc', 'Hoa Cuc', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(240, 'Đường Số 2 - CX Đô Thành', 'Duong So 2 - CX Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(241, 'Bà Huyện Thanh Quan', 'Ba Huyen Thanh Quan', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(242, 'Đường Số 11', 'Duong So 11', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(243, 'Đường Số 7', 'Duong So 7', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(244, 'D1', 'D1', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(245, 'Phan Xích Long', 'Phan Xich Long', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(246, 'Đường Số 12', 'Duong So 12', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(247, 'Đường Số 1', 'Duong So 1', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(248, 'Phạm Đình Toái', 'Pham Dinh Toai', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(249, 'Phan Đình Phùng', 'Phan Dinh Phung', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(250, 'Nguyễn Thị Minh Khai', 'Nguyen Thi Minh Khai', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(251, 'xxx', 'xxx', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(252, 'Nguyễn Huy Tưởng', 'Nguyen Huy Tuong', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(253, 'D5', 'D5', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(254, 'Trần Bình Trọng', 'Tran Binh Trong', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(255, 'Nguyễn Thị Minh Khai', 'Nguyen Thi Minh Khai', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(256, 'Trương Quốc Dung', 'Truong Quóc Dung', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(257, 'Phú Mỹ', 'Phu My', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(258, 'Nguyễn Trung Trực', 'Nguyen Trung Truc', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(259, 'Mai Xuân Thưởng', 'Mai Xuan Thuong', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(260, 'Nguyễn Ngọc Phương', 'Nguyen Ngoc Phuong', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(261, 'Nguyễn Công Trứ', 'Nguyen Cong Tru', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(262, 'Nguyễn Sơn', 'Nguyen Son', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(263, 'Lê Trực', 'Le Truc', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(264, 'Nhiêu Tứ', 'Nhieu Tu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(265, 'Nguyễn Phúc Nguyên', 'Nguyen Phuc Nguyen', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(266, 'Sư Thiện Chiếu', 'Su Thien Chieu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(267, 'Trần Quang Diệu', 'Tran Quang Dieu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(268, 'Phạm Ngọc Thạch', 'Pham Ngoc Thach', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(269, 'Trần Hữu Trang', 'Tran Huu Trang', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(270, 'Ngô Đức Kế', 'Ngo Duc Ke', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(271, 'Đường Số 6 - CX Đô Thành', 'Duong So 6 - CX Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(272, 'Trương Quyền', 'Truong Quyen', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(273, 'Thanh Đa', 'Thanh Da', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(274, 'Đường Số 3 -CX Đô Thành', 'Duong So 3 -CX Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(275, 'Nguyễn Trọng Tuyển', 'Nguyen Trong Tuyen', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(276, 'Nguyễn Văn Trỗi', 'Nguyen Van Troi', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(277, 'D3', 'D3', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(278, 'Lê Quý Đôn', 'Le Quy Don', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(279, 'Đường Số 12 - Chu Văn An', 'Duong So 12 - Chu Van An', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(280, 'Trương Định', 'Truong Dinh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(281, 'Lê Ngô Cát', 'Le Ngo Cat', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(282, 'Phan Tây Hồ', 'Phan Tay Hò', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(283, 'Mai Văn Ngọc', 'Mai Van Ngoc', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(284, 'Ký Con', 'Ky Con', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(285, 'Trường Sa', 'Truong Sa', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(286, 'Hoa Huệ', 'Hoa Hue', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(287, 'Trần Huy Liệu', 'Tran Huy Lieu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(288, 'Tú Xương', 'Tu Xuong', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(289, 'Nguyễn Văn Đậu', 'Nguyen Van Dau', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:15', NULL),
(290, 'Hùng Vương', 'Hung Vuong', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(291, 'Hoàng Văn Thụ', 'Hoang Van Thu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(292, 'Trần Hưng Đạo', 'Tran Hung Dao', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(293, 'Nguyễn Trãi', 'Nguyen Trai', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(294, 'Đặng Thai Mai', 'Dang Thai Mai', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(295, 'Hoàng Sa', 'Hoang Sa', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(296, 'Đào Duy Anh', 'Dao Duy Anh', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(297, 'Thích Quảng Đức', 'Thich Quang Duc', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(298, 'Hoàng Diệu', 'Hoang Dieu', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(299, 'Đặng Tất', 'Dang Tat', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(300, 'Nguyễn Kiệm', 'Nguyen Kiem', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(301, 'Đường Số 45', 'Duong So 45', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(302, 'Nguyễn Đình Chiểu', 'Nguyen Dinh Chieu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(303, 'Đường Số 5 - CX Đô Thành', 'Duong So 5 - CX Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(304, 'Điện Biên Phủ', 'Dien Bien Phu', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(305, 'Thành Thái', 'Thanh Thai', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(306, 'Trần Nhân Tôn', 'Tran Nhan Ton', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(307, 'Nguyễn Giản Thanh', 'Nguyen Gian Thanh', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(308, 'Sư Vạn Hạnh', 'Su Van Hanh', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(309, 'Lê Hồng Phong', 'Le Hong Phong', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(310, 'Tô Hiến Thành', 'To Hien Thanh', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(311, 'Nguyễn Tri Phương', 'Nguyen Tri Phuong', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(312, 'Cao Thắng', 'Cao Thang', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(313, 'Hồng Lĩnh', 'Hong Linh', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(314, 'Châu Thới', 'Chau Thói', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(315, 'Nguyễn Thị Diệu', 'Nguyen Thi Dieu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(316, 'Hồ Xuân Hương', 'Ho Xuan Huong', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(317, 'Ngô Quyền', 'Ngo Quyen', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(318, 'Hòa Hưng', 'Hoa Hung', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(319, 'Lý Thường Kiệt', 'Ly Thuong Kiet', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(320, 'Trần Thiện Chánh', 'Tran Thien Chanh', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(321, 'Bà Hạt', 'Ba Hat', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(322, 'Nguyễn Chí Thanh', 'Nguyen Chi Thanh', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(323, 'Lý Thái Tổ', 'Ly Thai To', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(324, 'Đồng Tiến', 'Dong Tien', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(325, 'Nhật Tảo', 'Nhat Tao', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(326, 'Nguyễn Tiểu La', 'Nguyen Tieu La', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(327, 'Nguyễn Kim', 'Nguyen Kim', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(328, 'Hòa Hảo', 'Hoa Hao', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(329, 'Nguyễn Duy Dương', 'Nguyen Duy Duong', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(330, 'Vĩnh Viễn', 'Vinh Vien', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(331, 'Tam Đảo', 'Tam Dao', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(332, 'Cửu Long', 'Cuu Long', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(333, 'Hồ Bá Kiện', 'Ho Ba Kien', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(334, 'Nguyễn Ngọc Lộc', 'Nguyen Ngoc Loc', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(335, 'Hoàng Dư Khương', 'Hoang Du Khuong', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(336, 'Trường Sơn', 'Truong Son', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(337, 'Hồ Biểu Chánh', 'Ho Bieu Chanh', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(338, 'Phạm Văn Hai', 'Pham Van Hai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(339, 'Cư Xá Lữ Gia', 'Cu Xa Lu Gia', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(340, 'Đào Duy Từ', 'Dao Duy Tu', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(341, 'Đô Thành', 'Do Thành', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(342, 'Đồng Nai', 'Dong Nai', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(343, 'Hùng Vương', 'Hung Vuong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(344, 'Hoa Sữa', 'Hoa Sua', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(345, 'xxx', 'xxx', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(346, 'Trần Minh Quyền', 'Tran Minh Quyen', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(347, 'Ngô Gia Tự', 'Ngo Gia Tụ', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(348, 'Trần Quang Diệu', 'Tran Quang Dieu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(349, 'Trần Bình Trọng', 'Tran Binh Trong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(350, 'Nguyễn Đình Chính', 'Nguyen Dinh Chinh', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(351, 'Cộng Hòa', 'Cong Hoa', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(352, 'Hoàng Diệu', 'Hoang Dieu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(353, 'Bà Lê Chân', 'Ba Le Chan', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(354, 'Hà Tôn Quyền', 'Ha Ton Quyen', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(355, 'Nguyễn Thái Học', 'Nguyen Thai Hoc', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(356, 'Trần Phú', 'Tran Phu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(357, 'Lam Sơn', 'Lam Son', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(358, 'Phan Huy Ích', 'Phan Huy Ích', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(359, 'Nguyễn Tri Phương', 'Nguyen Tri Phuong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(360, 'Hoa Đào', 'Hoa Dao', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(361, 'Lê Tự Tài', 'Le Tu Tai', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(362, 'Nguyễn Hồng Đào', 'Nguyen Hong Dao', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(363, 'Trường Sơn', 'Truong Son', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(364, 'Hoàng Hoa Thám', 'Hoang Hoa Tham', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(365, 'An Điểm', 'An Diẻm', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(366, 'Bắc Hải', 'Bac Hai', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(367, 'Bạch Đằng', 'Bach Dang', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(368, 'Trần Cao Vân', 'Tran Cao Van', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(369, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(370, 'Đất Thánh', 'Dat Thanh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(371, 'Nguyễn Lâm', 'Nguyen Lam', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(372, 'Trần Bình Trọng', 'Tran Binh Trong', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(373, 'Đỗ Tấn Phong', 'Do Tan Phong', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(374, 'Ba Vì', 'Ba Vi', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(375, 'Nguyễn Đình Chiểu', 'Nguyen Dinh Chieu', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(376, 'xxx', 'xxx', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(377, 'Chấn Hưng', 'Chan Hung', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(378, 'Thất Sơn', 'That Son', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(379, 'Nguyễn Thượng Hiền', 'Nguyen Thuong Hien', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(380, 'Nguyễn Biểu', 'Nguyen Bieu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(381, 'Nguyễn Trọng Tuyển', 'Nguyen Trong Tuyen', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(382, 'Đường Số 7 - CX Đô Thành', 'Duong So 7 - CX Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(383, 'Duy Tân', 'Duy Tan', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(384, 'Cao Đạt', 'Cao Dat', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(385, 'Bạch Vân', 'Bach Van', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(386, 'Võ Văn Kiệt', 'Vo Van Kiet', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(387, 'Ngô Quyền', 'Ngo Quyen', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(388, 'Phan Văn Trị', 'Phan Van Tri', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(389, 'Trần Tuấn Khải', 'Tran Tuan Khai', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(390, 'Nguyễn Thị Nhỏ', 'Nguyen Thi Nho', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(391, 'An Dương Vương', 'An Duong Vuong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(392, 'Phạm Đôn', 'Pham Don', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(393, 'Nguyễn Thời Trung', 'Nguyen Thoi Trung', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(394, 'Quốc Hương', 'Quoc Huong', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(395, 'Hồng Bàng', 'Hong Bang', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(396, 'Nguyễn Chí Thanh', 'Nguyen Chi Thanh', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(397, 'Bà Triệu', 'Ba Trieu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(398, 'Hoa Hồng', 'Hoa Hong', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(399, 'Bùi Thị Xuân', 'Bui Thi Xuan', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(400, 'Thái Phiên', 'Thai Phien', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(401, 'Phan Huy Chú', 'Phan Huy Chú', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(402, 'Châu Văn Liêm', 'Chau Van Liem', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(403, 'Phan Huy Ích', 'Phan Huy Ích', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(404, 'Lý Thường Kiệt', 'Ly Thuong Kiet', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(405, 'Tân Phước', 'Tan Phuoc', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(406, 'Hồ Thị Kỷ', 'Ho Thi Ky', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(407, 'Huỳnh Mẫn Đạt', 'Huynh Man Dat', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(408, 'An Bình', 'An Binh', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(409, 'Lão Tử', 'Lao Tu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(410, 'Trần Nhân Tôn', 'Tran Nhan Ton', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(411, 'Dương Tử Giang', 'Duong Tu Giang', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(412, 'Triệu Quang Phục', 'Trieu Quang Phuc', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(413, 'Bùi Hữu Nghĩa', 'Bui Huu Nghia', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(414, 'Tân Thành', 'Tan Thanh', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(415, 'Hải Thượng Lãn Ông', 'Hai Thuong Lan Ong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(416, 'Đào Tấn', 'Dao Tan', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(417, 'Đào Duy Từ', 'Dao Duy Tu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(418, 'Cầm Bá Thước', 'Cam Ba Thuoc', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(419, 'Lê Hồng Phong', 'Le Hong Phong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(420, 'Nghĩa Thục', 'Nghia Thuc', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(421, 'Phùng Hưng', 'Phung Hung', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(422, 'Chiến Thắng', 'Chien Thang', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(423, 'Trần Xuân Hoàn', 'Tran Xuan Hoan', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(424, 'Kỳ Hòa', 'Ky Hoa', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(425, 'Phạm Viết Chánh', 'Pham Viet Chanh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(426, 'Lưu Xuân Tín', 'Luu Xuan Tín', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(427, 'Hoàng Minh Giám', 'Hoang Minh Giam', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(428, 'Sư Vạn Hạnh', 'Su Van Hanh', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(429, 'Ngô Gia Tự', 'Ngo Gia Tụ', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(430, 'Trang Tử', 'Trang Tu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(431, 'Mạc Cửu', 'Mac Cuu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(432, 'Tân Hưng', 'Tan Hung', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(433, 'Hồng Hà', 'Hong Ha', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(434, 'Trịnh Hoài Đức', 'Trinh Hoai Duc', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(435, 'Chiêu Anh Các', 'Chieu Anh Cac', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(436, 'Âu Cơ', 'Au Co', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(437, 'Nguyễn Thái Bình', 'Nguyen Thai Binh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(438, 'Bàu Cát 1', 'Bau Cat 1', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(439, 'Hiệp Nhất', 'Hiep Nhat', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(440, 'Lê Văn Phan', 'Le Van Phan', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(441, 'Trường Chinh', 'Truong Chinh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(442, 'Lê Quý Đôn', 'Le Quy Don', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(443, 'Mê Linh', 'Me Linh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(444, 'Nguyễn Phúc Chu', 'Nguyen Phuc Chu', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(445, 'Ni Sư Huỳnh Liên', 'Ni Su Huỳnh Lien', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL);
INSERT INTO `lck_location_street` (`id`, `name`, `name_ascii`, `location`, `ward_id`, `district_id`, `user_id_created`, `created_at`, `updated_at`, `deleted_at`) VALUES
(446, 'Hồng Lạc', 'Hong Lac', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(447, 'Nguyễn Đình Khơi', 'Nguyen Dinh Khoi', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(448, 'Ngô Thị Thu Minh', 'Ngo Thi Thu Minh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(449, 'Cống Lở', 'Cong Lo', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(450, 'Bạch Đằng', 'Bach Dang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(451, 'Tống Văn Hên', 'Tong Van Hen', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(452, 'Hoàng Hoa Thám', 'Hoang Hoa Tham', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(453, 'Hoàng Việt', 'Hoang Viet', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(454, 'Trương Công Định', 'Truong Cong Dịnh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(455, 'Quang Trung', 'Quang Trung', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(456, 'Nguyễn Sơn', 'Nguyen Son', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(457, 'Giải Phóng', 'Giai Phong', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(458, 'Phổ Quang', 'Pho Quang', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(459, 'Duy Tân', 'Duy Tan', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(460, 'Đồng Đen', 'Dong Den', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(461, 'Lạc Long Quân', 'Lac Long Quan', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(462, 'Bàu Cát', 'Bau Cat', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(463, 'Trần Văn Quang', 'Tran Van Quang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(464, 'Tân Trụ', 'Tan Tru', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(465, 'Trần Văn Hoàng', 'Tran Van Hoang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(466, 'Lê Bình', 'Le Bình', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(467, 'Tân Sơn Hòa', 'Tan Son Hoa', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(468, 'Khánh Hội', 'Khanh Hoi', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(469, 'Hoàng Văn Thụ', 'Hoang Van Thu', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(470, 'Võ Thành Trang', 'Vo Thanh Trang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(471, 'C18', 'C18', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(472, 'Lê Quang Định', 'Le Quang Dịnh', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(473, 'Bùi Thế Mỹ', 'Bui The My', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(474, 'Đường Số 1', 'Duong So 1', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(475, 'Đồng Xoài', 'Dong Xoai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(476, 'Thăng Long', 'Thang Long', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(477, 'Trung Lang', 'Trung Lang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(478, 'Bành Văn Trân', 'Banh Van Tran', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(479, 'Nguyễn Xí', 'Nguyen Xi', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(480, 'Bầu Bàng', 'Bau Bang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(481, 'Chấn Hưng', 'Chan Hung', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(482, 'Tiền Giang', 'Tièn Giang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(483, 'Nguyễn Cảnh Dị', 'Nguyen Canh Di', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(484, 'Long Hưng', 'Long Hung', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(485, 'Ngô Bé', 'Ngo Bé', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(486, 'Lê Duy Nhật', 'Le Duy Nhạt', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(487, 'Phạm Văn Bạch', 'Pham Van Bach', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(488, 'Nguyễn Văn Cừ', 'Nguyen Van Cu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(489, 'Nhất Chi Mai', 'Nhat Chi Mai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(490, 'Bùi Bằng Đoàn', 'Bùi Bàng Doàn', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(491, 'Yên Thế', 'Yen The', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(492, 'Lý Thường Kiệt', 'Ly Thuong Kiet', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(493, 'Đặng Lộ', 'Dang Lo', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(494, 'Đường Số 16', 'Duòng Só 16', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(495, 'Hương Giang', 'Huong Giang', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(496, 'An Dương Vương', 'An Duong Vuong', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(497, 'Bình Giã', 'Binh Gia', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(498, 'Mã Lò', 'Mã Lò', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(499, 'Phổ Quang', 'Pho Quang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(500, 'Sao Mai', 'Sao Mai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(501, 'Lê Đại Hành', 'Le Dai Hanh', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(502, 'Trần Cao Vân', 'Tran Cao Van', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(503, '3 Tháng 2', '3 Thang 2', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(504, 'Hàn Hải Nguyên', 'Han Hai Nguyen', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(505, 'Huỳnh Văn Nghệ', 'Huynh Van Nghe', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(506, 'Phan Phú Tiên', 'Phan Phu Tien', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(507, 'Trần Hưng Đạo', 'Tran Hung Dao', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(508, 'Tản Đà', 'Tan Da', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(509, 'Lê Minh Xuân', 'Le Minh Xuan', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(510, 'Nguyễn Duy Dương', 'Nguyen Duy Duong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(511, 'Lê Ngân', 'Le Ngan', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(512, 'Nguyễn Văn Đừng', 'Nguyen Van Dung', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(513, 'Tân Canh', 'Tan Canh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(514, 'Trần Văn Dư', 'Tran Van Du', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(515, 'Trần Thái Tông', 'Tran Thai Tong', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(516, 'Phan Sào Nam', 'Phan Sao Nam', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(517, 'Cửu Long', 'Cuu Long', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(518, 'Phan Thúc Duyện', 'Phan Thuc Duyen', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(519, 'Hồng Bàng', 'Hong Bang', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(520, 'Cao Thắng', 'Cao Thang', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(521, 'Nguyễn Thị Huỳnh', 'Nguyen Thi Huynh', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(522, 'Đường Số 6', 'Duòng Só 6', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(523, 'Đặng Thái Thân', 'Dang Thai Than', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(524, 'Thống Nhất', 'Thong Nhat', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(525, 'Âu Cơ', 'Au Co', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(526, 'Út Tịch', 'Ut Tich', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(527, 'Đường 40', 'Duong 40', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(528, 'Nguyễn Lương Bằng', 'Nguyẽn Luong Bàng', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(529, 'Tân Hải', 'Tan Hai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(530, 'Phó Cơ Điều', 'Pho Co Dieu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(531, 'Nguyễn Minh Hoàng', 'Nguyen Minh Hoang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(532, 'Khu K300', 'Khu K300', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(533, 'Bạch Mã', 'Bach Ma', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(534, 'Nhiêu Tâm', 'Nhieu Tam', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(535, 'Lê Đức Thọ', 'Le Duc Tho', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(536, 'Đường Số 28', 'Duòng Só 28', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(537, 'Nguyễn Hiển Lê', 'Nguyen Hien Le', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(538, 'Tân Trang', 'Tan Trang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(539, 'Tân Tiến', 'Tan Tién', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(540, 'Hòa Bình', 'Hoa Binh', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(541, 'Lương Nhữ Học', 'Luong Nhũ Học', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(542, 'Nguyễn Văn Trỗi', 'Nguyen Van Troi', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(543, 'Trương Hoàng Thanh', 'Truong Hoang Thanh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:16', NULL),
(544, 'Nguyễn Văn Vĩnh', 'Nguyen Van Vinh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(545, 'Hoa Thị', 'Hoa Thi', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(546, 'Đinh Tiên Hoàng', 'Dinh Tien Hoang', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(547, 'Tân Sơn', 'Tan Son', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(548, 'Hậu Giang', 'Hau Giang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(549, 'Nguyễn Bá Tuyển', 'Nguyen Ba Tuyen', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(550, 'Đường Số 2', 'Duong So 2', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(551, 'Lê Ngã', 'Le Ngã', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(552, 'Bùi Văn Thêm', 'Bui Van Them', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(553, 'Hoa Cau', 'Hoa Cau', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(554, 'Mai Khôi', 'Mai Khoi', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(555, 'Sông Đà', 'Song Dà', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(556, 'Đống Đa', 'Dong Da', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(557, 'Lữ Gia', 'Lu Gia', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(558, 'Nghĩa Phát', 'Nghia Phat', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(559, 'Đường Số 8', 'Duong So 8', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(560, 'Phước Hưng', 'Phuóc Hung', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(561, 'Lê Lợi', 'Le Loi', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(562, 'Đường Số 20', 'Duòng Só 20', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(563, 'Đường Số 10', 'Duong So 10', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(564, 'Xuân Diệu', 'Xuan Dieu', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(565, 'Hát Giang', 'Hat Giang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(566, 'Lưu Nhân Chú', 'Luu Nhan Chu', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(567, 'Bàu Cát 6', 'Bau Cat 6', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(568, 'Thành Mỹ', 'Thanh My', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(569, 'Lê Duy Nhuận', 'Le Duy Nhuạn', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(570, 'Bắc Hải', 'Bac Hai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(571, 'lô e2 . đường nội bộ', 'lo e2 . duong noi bo', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(572, 'Nguyễn Trường Tộ', 'Nguyen Truong To', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(573, 'Nguyễn Sỹ Sách', 'Nguyen Sy Sach', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(574, 'Nghĩa Hòa', 'Nghia Hoa', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(575, 'Nguyễn Huy Lượng', 'Nguyen Huy Luong', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(576, 'Lê Văn Thọ', 'Le Van Tho', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(577, 'Hồng Hà', 'Hong Ha', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(578, 'Huỳnh Mẫn Đạt', 'Huynh Man Dat', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(579, 'Văn Cao', 'Van Cao', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(580, 'Trần Khắc Chân', 'Tran Khac Chan', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(581, 'Nguyễn Thi', 'Nguyen Thi', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(582, 'Bàu Cát 3', 'Bau Cat 3', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(583, 'Lạc Long Quân', 'Lac Long Quan', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(584, 'An Tôn', 'An Ton', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(585, 'Dương Tử Giang', 'Duong Tu Giang', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(586, 'Minh Phụng', 'Minh Phung', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(587, 'Tân Phước', 'Tan Phuoc', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(588, 'Nguyễn Kiệm', 'Nguyen Kiem', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(589, 'Hà Tôn Quyền', 'Ha Ton Quyen', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(590, 'Bình Thới', 'Binh Thoi', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(591, 'Lê Thị Bạch Cát', 'Le Thi Bach Cat', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(592, 'Tạ Uyên', 'Ta Uyen', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(593, 'Núi Thành', 'Nui Thanh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(594, 'Ông Ích Khiêm', 'Ong Ich Khiem', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(595, 'Đội Cung', 'Doi Cung', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(596, 'Đường Số 59', 'Duong So 59', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(597, 'Phạm Huy Thông', 'Pham Huy Thong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(598, 'Bàu Cát 2', 'Bau Cat 2', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(599, 'Phạm Văn Chiêu', 'Pham Van Chieu', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(600, 'Dương Quảng Hàm', 'Duong Quang Ham', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(601, 'An Nhơn', 'An Nhon', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(602, 'Huỳnh Khương An', 'Huynh Khuong An', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(603, 'Nguyễn Bỉnh Khiêm', 'Nguyen Binh Khiem', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(604, 'Phạm Phú Thứ', 'Pham Phu Thu', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(605, 'Trần Văn Danh', 'Tran Van Danh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(606, 'Năm Châu', 'Nam Chau', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(607, 'Đoàn Nhữ Hải', 'Doan Nhu Hai', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(608, 'Phó Cơ Điều', 'Pho Co Dieu', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(609, 'Nguyễn Thái Học', 'Nguyen Thai Hoc', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(610, 'Nguyễn Bá Tòng', 'Nguyen Ba Tong', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(611, 'Nguyễn Văn Công', 'Nguyen Van Cong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(612, 'Nguyễn Văn Lượng', 'Nguyen Van Luong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(613, 'Phạm Ngũ Lão', 'Pham Ngu Lao', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(614, 'Phạm Văn Bạch', 'Pham Van Bach', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(615, 'Hạnh Thông', 'Hạnh Thong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(616, 'Tạ Uyên', 'Ta Uyen', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(617, 'Hà Huy Giáp', 'Ha Huy Giap', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(618, 'Thiên Phước', 'Thien Phuoc', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(619, 'Lê Văn Huân', 'Le Van Huan', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(620, 'Hà Tôn Quyền', 'Ha Ton Quyen', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(621, 'Đường Số 7', 'Duong So 7', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(622, 'Hậu Giang', 'Hau Giang', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(623, 'Trần Thánh Tông', 'Tran Thanh Tong', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(624, 'Lê Lai', 'Le Lai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(625, 'Bàu Cát 4', 'Bau Cat 4', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(626, 'Nguyễn Tất Thành', 'Nguyen Tat Thanh', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(627, 'Phan Văn Khỏe', 'Phan Van Khoe', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(628, 'Thép Mới', 'Thep Moi', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(629, 'Tân Hóa', 'Tan Hoa', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(630, 'Xóm Đất', 'Xom Dat', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(631, 'Lê Quang Định', 'Le Quang Dịnh', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(632, 'Nguyễn Thái Sơn', 'Nguyen Thai Son', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(633, 'Cây Trâm', 'Cay Tram', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(634, 'Đường Số 5', 'Duòng Só 5', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(635, 'Hà Tôn Quyền', 'Ha Ton Quyen', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(636, 'Quách Văn Tuấn', 'Quach Van Tuan', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(637, 'Tự Lập', 'Tu Lap', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(638, 'Lam Sơn', 'Lam Son', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(639, 'Thuận Kiều', 'Thuan Kieu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(640, 'Trần Huy Liệu', 'Tran Huy Lieu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(641, 'Hoa Mai', 'Hoa Mai', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(642, 'Bửu Long', 'Buu Long', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(643, 'Trần Chánh Chiếu', 'Tran Chanh Chieu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(644, 'Đoàn Văn Bơ', 'Doan Van Bo', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(645, 'Tứ Hải', 'Tu Hai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(646, 'Bùi Thị Xuân', 'Bui Thi Xuan', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(647, 'Vĩnh Viễn', 'Vinh Vien', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(648, 'Đỗ Ngọc Thạch', 'Do Ngoc Thach', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(649, 'Phạm Hữu Chí', 'Pham Huu Chi', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(650, 'Vành Đai Trong', 'Vanh Dai Trong', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(651, 'Võ Trường Toản', 'Vo Truong Toan', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(652, 'Vạn Kiếp', 'Van Kiep', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(653, 'Nguyễn Thị Nhỏ', 'Nguyen Thi Nho', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(654, 'Nhi Bình', 'Nhi Bình', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(655, 'Đất Thánh', 'Dat Thanh', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(656, 'Nguyễn Kiệm', 'Nguyen Kiem', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(657, 'Hoa Phượng', 'Hoa Phuong', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(658, 'Tăng Bạt Hổ', 'Tang Bat Ho', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(659, 'Vũ Chí Hiếu', 'Vũ Chí Hiéu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(660, 'xxx', 'xxx', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(661, 'Nguyễn Chí Thanh', 'Nguyen Chi Thanh', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(662, 'Kinh Dương Vương', 'Kinh Duong Vuong', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(663, 'Tuệ Tĩnh', 'Tuẹ Tĩnh', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(664, 'Trần Văn Đang', 'Tran Van Dang', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(665, 'Tự Cường', 'Tụ Cuòng', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(666, 'Lâm Văn Bền', 'Lam Van Ben', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(667, 'Phan Anh', 'Phan Anh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(668, 'Nguyễn Kim', 'Nguyen Kim', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(669, 'Lê Lai', 'Le Lai', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(670, 'Hàm Tử', 'Ham Tu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(671, 'Lý Thường Kiệt', 'Ly Thuong Kiet', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(672, 'Phạm Cự Lượng', 'Pham Cu Luong', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(673, 'Ca Văn Thinh', 'Ca Van Thinh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(674, 'Văn Chung', 'Van Chung', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(675, 'Lê Trung Nghĩa', 'Le Trung Nghia', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(676, 'Hồng Bàng', 'Hong Bang', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(677, 'Ba Vân', 'Ba Van', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(678, 'Ba Đình', 'Ba Dình', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(679, 'Hoa Trà', 'Hoa Tra', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(680, 'Phan Văn Trị', 'Phan Van Tri', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(681, 'Vĩnh Hội', 'Vĩnh Họi', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(682, 'Bến Vân Đồn', 'Ben Van Don', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(683, 'Đông Hồ', 'Dong Ho', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(684, 'Trần Mai Ninh', 'Tran Mai Ninh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(685, 'Nguyễn Đức Thuận', 'Nguyen Duc Thuan', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(686, 'Nguyễn Chánh Sắt', 'Nguyẽn Chánh Sát', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(687, 'Tôn Thất Hiệp', 'Ton That Hiep', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(688, 'An Dương Vương', 'An Duong Vuong', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(689, 'Vườn Lài', 'Vuon Lai', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(690, 'Phạm Văn Sửu', 'Pham Van Suu', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(691, 'Ngô Quang Huy', 'Ngo Quang Huy', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(692, 'Nguyễn Văn Nghi', 'Nguyen Van Nghi', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(693, 'Hoàng Hoa Thám', 'Hoang Hoa Tham', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(694, 'Lãnh Bình Thăng', 'Lanh Binh Thang', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(695, 'Hoàng Sa', 'Hoang Sa', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(696, 'Trần Xuân Soạn', 'Tran Xuan Soan', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(697, 'Gò Công', 'Go Cong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(698, 'Tạ Quang Bửu', 'Ta Quang Buu', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(699, 'Trần Quốc Tuấn', 'Tran Quoc Tuan', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(700, 'Học Lạc', 'Hoc Lac', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(701, 'Nguyễn Oanh', 'Nguyen Oanh', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(702, 'Vân Côi', 'Van Coi', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(703, 'Đường Số 8', 'Duong So 8', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(704, 'Ba Vì', 'Ba Vi', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(705, 'Đường Số 1', 'Duong So 1', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(706, 'Trường Sa', 'Truong Sa', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(707, 'An Dương Vương', 'An Duong Vuong', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(708, 'Đường Số 7', 'Duong So 7', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(709, 'Nguyễn Trọng Lội', 'Nguyen Trong Loi', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(710, 'Trần Nhân Tôn', 'Tran Nhan Ton', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(711, 'Nguyễn Văn Quá', 'Nguyen Van Qua', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(712, 'Phú Hòa', 'Phu Hoa', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(713, 'Huỳnh Tịnh Của', 'Huynh Tinh Cua', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(714, 'B3', 'B3', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(715, 'Đường A4 An Tôn', 'Duong A4 An Ton', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(716, 'Thân Nhân Trung', 'Than Nhan Trung', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(717, 'Nguyễn Thanh Tuyền', 'Nguyen Thanh Tuyen', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(718, 'Đường Số 120', 'Duong So 120', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(719, 'Đường A4', 'Duong A4', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(720, 'C1', 'C1', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(721, 'Đường Số 2 - CX Lữ Gia', 'Duong So 2 - CX Lu Gia', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(722, 'Nguyễn Văn Luông', 'Nguyen Van Luong', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(723, 'Ngô Thời Nhiệm', 'Ngo Thoi Nhiem', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(724, 'Đường A4 Khu K300', 'Duong A4 Khu K300', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(725, 'Tên Lửa', 'Ten Lua', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(726, 'Minh Phụng', 'Minh Phung', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(727, 'Đường 41', 'Duong 41', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(728, 'Sầm Sơn', 'Sam Son', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(729, 'Trần Não', 'Tran Nao', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(730, 'Phú Định', 'Phu Dinh', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(731, 'Nguyễn Khoái', 'Nguyen Khoai', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(732, 'Kênh Tân Hóa', 'Kenh Tan Hoa', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(733, 'Trần Quý', 'Tran Quy', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(734, 'Giang Văn Minh', 'Giang Van Minh', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(735, 'Ấp Bắc', 'Ap Bac', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(736, 'Đặng Minh Trứ', 'Dang Minh Tru', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(737, 'Tùng Thiện Vương', 'Tung Thien Vuong', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(738, 'Nguyễn Án', 'Nguyen An', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(739, 'Thành Thái', 'Thanh Thai', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(740, 'Tôn Đản', 'Ton Dan', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(741, 'Tân Lập', 'Tan Lap', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(742, 'Thuận Kiều', 'Thuan Kieu', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(743, 'Nguyễn Văn Mai', 'Nguyen Van Mai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(744, 'Nguyễn Thượng Hiền', 'Nguyen Thuong Hien', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(745, 'Tân Châu', 'Tan Chau', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(746, 'Đỗ Ngọc Thạch', 'Do Ngoc Thach', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(747, 'Cư Xá Trần Quang Diệu', 'Cu Xa Tran Quang Dieu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(748, 'Hòa Bình', 'Hoa Binh', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(749, 'Vạn Kiếp', 'Van Kiep', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(750, 'Đường Số 10 Trần Trọng Cung', 'Duong So 10 Tran Trong Cung', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(751, 'Hoàng Bật Đạt', 'Hoang Bat Dat', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(752, 'Trương Đăng Quê', 'Truong Dang Que', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(753, 'Nguyễn Thị Nhỏ', 'Nguyen Thi Nho', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(754, 'Trần Tướng Công', 'Tran Tuong Cong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(755, 'Đỗ Sơn', 'Do Son', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(756, 'Đường Số 10', 'Duong So 10', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(757, 'Tân Sơn Nhì', 'Tan Son Nhi', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(758, 'Nguyễn Văn Lạc', 'Nguyen Van Lac', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(759, 'Hoàng Lê Kha', 'Hoang Le Kha', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(760, 'Nguyễn Bình', 'Nguyen Binh', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(761, 'Đường Số 59', 'Duong So 59', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(762, 'Tân Hương', 'Tan Huong', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(763, 'Bùi Đình Túy', 'Bui Dinh Tuy', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(764, 'Phan Anh', 'Phan Anh', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(765, 'Lý Chiêu Hoàng', 'Ly Chieu Hoang', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(766, 'Trần Văn Đang', 'Tran Van Dang', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(767, 'Lũy Bán Bích', 'Luy Ban Bich', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(768, 'Nguyễn Trường Tộ', 'Nguyen Truong To', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(769, 'Bàu Cát 5', 'Bau Cat 5', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(770, 'Miếu Nổi - Vũ Huy Tấn', 'Mieu Noi - Vu Huy Tan', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(771, 'Phan Xích Long', 'Phan Xich Long', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(772, 'Nguyễn Khuyến', 'Nguyen Khuyen', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(773, 'Sông Thương', 'Song Thuong', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(774, 'Hưng Hóa - Chấn Hưng', 'Hung Hoa - Chan Hung', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(775, 'Hiệp Bình', 'Hiep Binh', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(776, 'CX Nguyễn Văn Trỗi', 'CX Nguyen Van Troi', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(777, 'Gò Cầm Đêm', 'Go Cam Dem', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(778, 'Trần Văn Kiểu', 'Tran Van Kieu', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(779, 'Huyền Quang', 'Huyen Quang', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(780, 'Tôn Thất Thuyết', 'Ton That Thuyet', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(781, 'Nguyên Hồng', 'Nguyen Hong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(782, 'Nguyễn Bá Tòng', 'Nguyen Ba Tong', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(783, 'Kinh Dương Vương', 'Kinh Duong Vuong', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(784, 'Phan Tây Hồ', 'Phan Tay Hò', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(785, 'Lê Trọng Tấn', 'Le Trong Tan', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(786, 'Đường Số 14', 'Duong So 14', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(787, 'Trịnh Văn Đức', 'Trinh Van Duc', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(788, 'Đường Số 21', 'Duong So 21', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(789, 'Nguyễn Bặc', 'Nguyen Bac', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(790, 'Lý Thường Kiệt', 'Ly Thuong Kiet', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(791, 'Đường Số 1', 'Duong So 1', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(792, 'Tân Thành', 'Tan Thanh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(793, 'Lò Siêu', 'Lo Sieu', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(794, 'Vũ Ngọc Phan', 'Vu Ngoc Phan', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(795, 'Nguyễn Quang Bích', 'Nguyen Quang Bich', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(796, 'Lý Tự Trọng', 'Ly Tu Trong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(797, 'Đường Số 41', 'Duong So 41', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(798, 'Đường Số 3 - CX Lữ Gia', 'Duong So 3 - CX Lu Gia', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(799, 'Cư Xá Ngân Hàng', 'Cu Xa Ngan Hang', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(800, 'Phan Đình Phùng', 'Phan Dinh Phung', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:17', NULL),
(801, 'Kha Vạn Cân', 'Kha Van Can', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(802, 'Khu Dân Cư Him Lam', 'Khu Dan Cu Him Lam', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(803, 'Đường Số 7', 'Duong So 7', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(804, 'Sơn Cang', 'Son Cang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(805, 'Nguyễn Duy Trinh', 'Nguyen Duy Trinh', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(806, 'Dương Bá Trạc', 'Duong Ba Trac', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(807, 'Thủy Lợi', 'Thuy Loi', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(808, 'An Phú Đông 09', 'An Phu Dong 09', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(809, 'Nguyễn Đình Chiểu', 'Nguyen Dinh Chieu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(810, 'Nguyễn Văn Hưởng', 'Nguyen Van Huong', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(811, 'Mai Lão Bạng', 'Mai Lao Bang', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(812, 'Xa Lộ Hà Nội', 'Xa Lo Ha Noi', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(813, 'Lê Văn Khương', 'Le Van Khuong', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(814, 'Nguyễn Thượng Hiền', 'Nguyen Thuong Hien', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(815, 'Tân Quý', 'Tan Quy', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(816, 'Trần Văn Kỷ', 'Tran Van Ky', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(817, 'Trần Bình Trọng', 'Tran Binh Trong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(818, 'Sông Thao', 'Song Thao', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(819, 'Đồng Nai', 'Dong Nai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(820, 'Lê Sắt', 'Le Sat', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(821, 'Tân Kỳ Tân Quý', 'Tan Ky Tan Quy', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(822, 'Đường Số 4', 'Duong So 4', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(823, 'Nguyễn An Ninh', 'Nguyen An Ninh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(824, 'Đường Số 33', 'Duong So 33', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(825, 'Lê Văn Thịnh', 'Le Van Thinh', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(826, 'Đường Số 22', 'Duong So 22', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(827, 'Đường Số 42', 'Duong So 42', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(828, 'Tôn Dật Tiên', 'Ton Dat Tien', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(829, 'Trần Hòa', 'Tran Hoa', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(830, 'Đường Số 14', 'Duong So 14', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(831, 'Trần Đình Khơi', 'Tran Dinh Khoi', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(832, 'Xóm Chiếu', 'Xom Chieu', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(833, 'Phan Đình Giót', 'Phan Dinh Giot', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(834, 'Đường Số 22', 'Duong So 22', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(835, 'Vườn Lài', 'Vuon Lai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(836, 'KDC An Phú Khánh', 'KDC An Phu Khanh', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(837, 'Lê Quốc Hưng', 'Le Quoc Hung', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(838, 'Đường Số 19', 'Duong So 19', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(839, 'Đường Số1', 'Duong So1', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(840, 'Xuân Thủy', 'Xuan Thuy', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(841, 'Đường Số 12', 'Duong So 12', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(842, 'Cao Lỗ', 'Cao Lo', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(843, 'Khuông Việt', 'Khuong Viet', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(844, 'Trần Ngọc Diện', 'Tran Ngoc Dien', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(845, 'Gia Phú', 'Gia Phu', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(846, 'Nguyễn Duy Cung', 'Nguyen Duy Cung', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(847, 'Đặng Văn Bi', 'Dang Van Bi', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(848, 'Đường Số 11', 'Duong So 11', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(849, 'Đường Số 182', 'Duong So 182', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(850, 'Phạm Thế Hiển', 'Pham The Hien', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(851, 'Đường Số 49', 'Duong So 49', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(852, 'Trịnh Đình Thảo', 'Trinh Dinh Thao', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(853, 'Đường TX21', 'Duong TX21', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(854, 'Đường Số 10 - Chu Văn An', 'Duong So 10 - Chu Van An', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(855, 'Sơn Hưng', 'Son Hung', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(856, 'CX Phan Đăng Lưu', 'CX Phan Dang Luu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(857, 'Đường Số 30', 'Duong So 30', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(858, 'Cộng Hòa', 'Cong Hoa', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(859, 'Đường Số 66', 'Duong So 66', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(860, 'Thạnh Mỹ Lợi', 'Thanh My Loi', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(861, 'Đường Số 8', 'Duong So 8', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(862, 'Công Trường An Đông', 'Cong Truong An Dong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(863, 'Quốc Lộ 13', 'Quoc Lo 13', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(864, 'Đường Số 53 - Phạm Văn Chiêu', 'Duong So 53 - Pham Van Chieu', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(865, 'Hưng Phú', 'Hung Phu', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(866, 'Bến Mễ Cốc', 'Ben Me Coc', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(867, 'Tây Hòa', 'Tay Hoa', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(868, 'Tô Ngọc Vân', 'To Ngoc Van', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(869, 'Bông Sao', 'Bong Sao', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(870, 'Đường Số 1', 'Duong So 1', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(871, 'Đường Số 49', 'Duong So 49', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(872, 'Đặng Nguyên Cẩn', 'Dang Nguyen Can', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(873, 'Đường 40', 'Duong 40', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(874, 'An Bình', 'An Binh', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(875, 'Đường Số 13', 'Duong So 13', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(876, 'Đường Số 79', 'Duong So 79', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(877, 'Nguyễn Thị Định', 'Nguyen Thi Dinh', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(878, 'Tân Kỳ Tân Quý', 'Tan Ky Tan Quy', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(879, 'Lũy Bán Bích', 'Luy Ban Bich', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(880, 'Lê Quang Định', 'Le Quang Dịnh', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(881, 'Đường Số 2', 'Duong So 2', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(882, 'Đông Sơn', 'Dong Son', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(883, 'Tân Hòa Đông', 'Tan Hoa Dong', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(884, 'Đường Số 2', 'Duong So 2', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(885, 'Ấp Cây Dầu', 'Ap Cay Dau', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(886, 'Đường Số 28', 'Duong So 28', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(887, 'Đường C8', 'Duong C8', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(888, 'Thạch Lam', 'Thach Lam', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(889, 'Bãi Sậy', 'Bai Say', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(890, 'Độc Lập', 'Doc Lap', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(891, 'Lương Định Của', 'Luong Dinh Cua', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(892, 'Phan Chu Trinh', 'Phan Chu Trinh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(893, 'Mai Xuân Thưởng', 'Mai Xuan Thuong', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(894, 'Bùi Quang Là', 'Bui Quang La', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(895, 'Nguyễn Cửu Đàm', 'Nguyen Cuu Dam', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL);
INSERT INTO `lck_location_street` (`id`, `name`, `name_ascii`, `location`, `ward_id`, `district_id`, `user_id_created`, `created_at`, `updated_at`, `deleted_at`) VALUES
(896, 'Nguyễn Trãi', 'Nguyen Trai', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(897, 'Nguyễn Lộ Trạch', 'Nguyen Lo Trach', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(898, 'Nguyễn Ư Dĩ', 'Nguyen U Di', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(899, 'Lê Tấn Quốc', 'Le Tan Quoc', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(900, 'Nguyễn Súy', 'Nguyen Suy', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(901, 'Đường Số 05', 'Duong So 05', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(902, 'Thiện Hộ Dương', 'Thien Ho Duong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(903, 'Huỳnh Tấn Phát', 'Huynh Tan Phat', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(904, 'Dân Tộc', 'Dan Toc', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(905, 'Đường Số 7', 'Duong So 7', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(906, 'Trịnh Đình Trọng', 'Trinh Dinh Trong', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(907, 'Nguyễn Du', 'Nguyen Du', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(908, 'Đường Số 60', 'Duong So 60', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(909, 'Bình Hưng Hòa', 'Binh Hung Hoa', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(910, 'Đường Số 41', 'Duong So 41', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(911, 'Nguyễn Tư Giản', 'Nguyen Tu Gian', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(912, 'Đào Duy Anh', 'Dao Duy Anh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(913, 'Đường C12', 'Duong C12', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(914, 'Tân Kỳ Tân Quý', 'Tan Ky Tan Quy', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(915, 'Nguyễn Hữu Đất', 'Nguyen Huu Dat', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(916, 'Phạm Phú Thứ', 'Pham Phu Thu', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(917, 'Dương Văn Dương', 'Duong Van Duong', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(918, 'Tô Hiến Thành', 'To Hien Thanh', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(919, 'Tân Sơn', 'Tan Son', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(920, 'Đường Số 10', 'Duong So 10', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(921, 'Âu Dương Lân', 'Au Duong Lan', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(922, 'Nguyễn Tuyển', 'Nguyen Tuyen', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(923, 'Đường Số 22', 'Duong So 22', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(924, 'An Hội', 'An Hoi', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(925, 'Nguyễn Văn Linh', 'Nguyen Van Linh', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(926, 'Thân Văn Nhiếp', 'Than Van Nhiep', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(927, 'Lê Đình Thám', 'Le Dinh Tham', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(928, 'Đường Số 43', 'Duong So 43', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(929, 'Bùi Văn Ba', 'Bui Van Ba', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(930, 'Mạc Thiên Tích', 'Mac Thien Tich', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(931, 'Gò Dầu', 'Go Dau', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(932, 'Đường Số 29', 'Duong So 29', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(933, 'Lê Thị Hồng', 'Le Thi Hong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(934, 'Đường Số 4', 'Duong So 4', NULL, NULL, 785, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(935, 'Lê Hoàng Phái', 'Le Hoang Phai', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(936, 'Đỗ Ngọc Thạnh', 'Do Ngoc Thanh', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(937, 'Đường Số 1', 'Duong So 1', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(938, 'Bình Long', 'Binh Long', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(939, 'Đường Số 34', 'Duong So 34', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(940, 'Bình Long', 'Binh Long', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(941, 'Cầu Xéo', 'Cau Xeo', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(942, 'Phú Thọ Hòa', 'Phu Tho Hoa', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(943, 'Đường Số 31', 'Duong So 31', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(944, 'Trần Triệu Luật', 'Tran Trieu Luat', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(945, 'Lê Cao Lãng', 'Le Cao Lang', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(946, 'Lê Niệm', 'Le Niem', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(947, 'Đỗ Bí', 'Do Bi', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(948, 'Nguyễn Hữu Hào', 'Nguyen Huu Hao', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(949, 'Phạm Văn Xảo', 'Pham Van Xao', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(950, 'Hồ Văn Long', 'Ho Van Long', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(951, 'Lê Văn Lương', 'Le Van Luong', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(952, 'Đoàn Kết', 'Doan Ket', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(953, 'Tô Hiệu', 'To Hieu', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(954, 'Đường Số 6', 'Duong So 6', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(955, 'Đường Số 3', 'Duong So 3', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(956, 'Đường Số 16', 'Duong So 16', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(957, 'Tràn Tán', 'Tran Tan', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(958, 'Đường D10', 'Duong D10', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(959, 'Đường Số 47', 'Duong So 47', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(960, 'Đường Số 30', 'Duong So 30', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(961, 'Đường Số 40', 'Duong So 40', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(962, 'Đường Số 8', 'Duong So 8', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(963, 'Quân Sự', 'Quan Su', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(964, 'Trần Nguyên Hãn', 'Tran Nguyen Han', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(965, 'Đường B3', 'Duong B3', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(966, 'Công Trường Tự Do', 'Cong Truong Tu Do', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(967, 'Ấp Chiến Lược', 'Ap Chien Luoc', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(968, 'Nguyễn Thần Hiển', 'Nguyen Than Hien', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(969, 'Đường Số 8A', 'Duong So 8A', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(970, 'Cư Xá Tự Do', 'Cu Xa Tu Do', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(971, 'Bưng Ông Thoàn', 'Bung Ong Thoan', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(972, 'Đường Số 5', 'Duong So 5', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(973, 'Phạm Hùng', 'Pham Hung', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(974, 'Tỉnh Lộ 10', 'Tinh Lo 10', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(975, 'Đường Số 27', 'Duong So 27', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(976, 'Lê Văn Lương', 'Le Van Luong', NULL, NULL, 786, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(977, 'Đường Số 4', 'Duong So 4', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(978, 'Khu Dân Cư Sông Giồng', 'Khu Dan Cu Song Giong', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(979, 'Đường T4A', 'Duong T4A', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(980, 'Miếu Bình Đông', 'Mieu Binh Dong', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(981, 'Võ Văn Kiệt', 'Vo Van Kiet', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(982, 'Võ Văn Kiệt', 'Vo Van Kiet', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(983, 'Võ Văn Ngân', 'Vo Van Ngan', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(984, 'Đường Số 11', 'Duong So 11', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(985, 'Đường Số 5', 'Duong So 5', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(986, 'Quốc Lộ 1A', 'Quoc Lo 1A', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(987, 'Đường Số 49', 'Duong So 49', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(988, 'Âu Cơ', 'Au Co', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(989, 'Đường Số 3', 'Duong So 3', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(990, 'Lê Văn Linh', 'Le Van Linh', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(991, 'Bãi Sậy', 'Bai Say', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(992, 'Đường Số 9', 'Duong So 9', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(993, 'Nguyễn Cửu Đàm', 'Nguyen Cuu Dam', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(994, 'Số 6', 'So 6', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(995, 'Đường Số 6 -  Chu Văn An', 'Duong So 6 -  Chu Van An', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(996, 'Huỳnh Văn Chính', 'Huynh Van Chinh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(997, 'Hòa Bình', 'Hoa Binh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(998, 'Nguyễn Minh Châu', 'Nguyen Minh Chau', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(999, 'Nguyễn Thiện Thuật', 'Nguyen Thien Thuat', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1000, 'Đường Số 2 - Bình Trưng', 'Duong So 2 - Binh Trung', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1001, 'Nguyễn Văn Quỳ', 'Nguyen Van Quy', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1002, 'Nguyễn Thị Thập', 'Nguyen Thi Thap', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1003, 'Thảo Điền', 'Thao Dien', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1004, 'Đường Số 39', 'Duong So 39', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1005, 'Nguyễn Ảnh Thủ', 'Nguyen Anh Thu', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1006, 'Đường Số 9', 'Duong So 9', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1007, 'Đường 100 - Bình Thới', 'Duong 100 - Binh Thoi', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1008, 'Đỗ Ngọc Thạnh', 'Do Ngoc Thanh', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1009, 'Đường Số 13', 'Duong So 13', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1010, 'Đường 49', 'Duong 49', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1011, 'Đường Số 27', 'Duong So 27', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1012, 'Diên Hồng', 'Dien Hong', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1013, 'Đường 281 - Lý Thường Kiệt', 'Duong 281 - Ly Thuong Kiet', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1014, 'Đường Số 6 - Chu Văn An', 'Duong So 6 - Chu Van An', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1015, 'Đường Số 4', 'Duong So 4', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1016, 'Đường Số 1 - Chu Văn An', 'Duong So 1 - Chu Van An', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1017, 'Phạm Văn Đồng', 'Pham Van Dong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1018, 'Trần Xuân Hòa', 'Tran Xuan Hoa', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1019, 'Nguyễn An Khương', 'Nguyen An Khuong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1020, 'Trần Thủ Độ', 'Tran Thu Do', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1021, 'Đô Đốc Long', 'Do Doc Long', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1022, 'KDC Miếu Nổi', 'KDC Mieu Noi', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1023, 'Thạch Lam', 'Thach Lam', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1024, 'Chử Đồng Tử', 'Chu Dong Tu', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1025, 'Đường Số 1', 'Duong So 1', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1026, 'Đường Số 3', 'Duong So 3', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1027, 'Phạm Hữu Lầu', 'Pham Huu Lau', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1028, 'Nguyễn Hữu Tiến', 'Nguyen Huu Tien', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1029, 'Bến Cát', 'Ben Cat', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1030, 'Lê Đình Cẩn', 'Le Dinh Can', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1031, 'Mã Lò', 'Ma Lo', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1032, 'Đường Số 3', 'Duong So 3', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1033, 'Đường Số 1', 'Duong So 1', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1034, 'Quảng Hiền', 'Quang Hien', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1035, 'Đường Số 9', 'Duong So 9', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1036, 'Mai Văn Vĩnh', 'Mai Van Vinh', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1037, 'Nhật Tảo', 'Nhat Tao', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1038, 'Tống Văn Trân', 'Tong Van Tran', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1039, 'Đường 304', 'Duong 304', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1040, 'Đường Số 12', 'Duong So 12', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1041, 'Gò Xoài', 'Go Xoai', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1042, 'Đường Số 8', 'Duong So 8', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1043, 'Hà Bá Tường', 'Ha Ba Tuong', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1044, 'Đường Số 17', 'Duong So 17', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1045, 'Hoàng Hoa Thám', 'Hoang Hoa Tham', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1046, 'Đường 56', 'Duong 56', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1047, 'Đường Số 2', 'Duong So 2', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1048, 'Đường Số 9', 'Duong So 9', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1049, 'Trúc Đường', 'Truc Duong', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1050, 'Đường Số 64', 'Duong So 64', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1051, 'Đường Số 2 - CX Bình Thới', 'Duong So 2 - CX Binh Thoi', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1052, 'Lê Trọng Tấn', 'Le Trong Tan', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1053, 'Đường Số 2', 'Duong So 2', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1054, 'Phan Huy Thực', 'Phan Huy Thuc', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1055, 'Đường N10', 'Duong N10', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1056, 'Quang Trung', 'Quang Trung', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1057, 'Phạm Văn Chí', 'Pham Van Chi', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1058, 'Đường 14A - CX Ngân Hàng', 'Duong 14A - CX Ngan Hang', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:18', NULL),
(1059, 'Trần Đạo', 'Tran Dao', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1060, 'Công Chúa Ngọc Hân', 'Cong Chua Ngoc Han', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1061, 'Đỗ Thừa Luông', 'Do Thua Luong', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1062, 'Đường Số 102', 'Duong So 102', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1063, 'Bùi Tá Hán', 'Bui Ta Han', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1064, 'Trường Chinh', 'Truong Chinh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1065, 'Nguyễn Tuân', 'Nguyen Tuan', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1066, 'Nguyễn Hoàng', 'Nguyen Hoang', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1067, 'Đường Số 4', 'Duong So 4', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1068, 'Cao Xuân Dục', 'Cao Xuan Duc', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1069, 'Nguyễn Văn Vỹ', 'Nguyen Van Vy', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1070, 'Phạm Văn Đồng', 'Pham Van Dong', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1071, 'Dạ Nam', 'Da Nam', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1072, 'Đường Số 12', 'Duong So 12', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1073, 'Lương Thế Vinh', 'Luong The Vinh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1074, 'Nguyễn Văn Cừ', 'Nguyen Van Cu', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1075, 'Hiền Vương', 'Hien Vuong', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1076, 'Hoàng Quốc Việt', 'Hoang Quoc Viet', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1077, 'Lê Thúc Hoạch', 'Le Thuc Hoach', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1078, 'Đường Số 3', 'Duong So 3', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1079, 'Đường Số 9A', 'Duong So 9A', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1080, 'Đường Số 17', 'Duong So 17', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1081, 'Đường 26 Tháng 3', 'Duong 26 Thang 3', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1082, 'Lê Thị Hoa', 'Le Thi Hoa', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1083, 'Lê Cảnh Tuân', 'Le Canh Tuan', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1084, 'Đường 51', 'Duong 51', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1085, 'Sơn Kỳ', 'Son Ky', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1086, 'Đường Số 42', 'Duong So 42', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1087, 'Đường Số 16', 'Duong So 16', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1088, 'Lê Văn Quới', 'Le Van Quoi', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1089, 'Đỗ Xuân Hợp', 'Do Xuan Hop', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1090, 'Đường Số 3 - Chu Văn An', 'Duong So 3 - Chu Van An', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1091, 'Đường Số 2', 'Duong So 2', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1092, 'Nguyễn Văn Tăng', 'Nguyen Van Tang', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1093, 'Đường Số 28', 'Duong So 28', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1094, 'Đường Số 6', 'Duong So 6', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1095, 'Đường Số 10', 'Duong So 10', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1096, 'Phan Văn Năm', 'Phan Van Nam', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1097, 'Đường Số 5 - Chu Văn An', 'Duong So 5 - Chu Van An', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1098, 'Trần Bá Giao', 'Tran Ba Giao', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1099, 'Cô Bắc', 'Co Bac', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1100, 'Đường Số 46', 'Duong So 46', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1101, 'Đinh Điền', 'Dinh Dien', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1102, 'Quốc Lộc 1A', 'Quoc Loc 1A', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1103, 'Diệp Minh Châu', 'Diep Minh Chau', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1104, 'Bùi Văn Ngữ', 'Bui Van Ngu', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1105, 'Đường Số 7A', 'Duong So 7A', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1106, 'Phan Văn Hớn', 'Phan Van Hon', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1107, 'Nguyễn Ngọc Nhựt', 'Nguyen Ngoc Nhut', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1108, 'Tân Mỹ', 'Tan My', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1109, 'Thoại Ngọc Hầu', 'Thoai Ngoc Hau', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1110, 'Đường 7B', 'Duong 7B', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1111, 'Hưng Hóa', 'Hung Hoa', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1112, 'Lê Sao', 'Le Sao', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1113, 'Đường 50B', 'Duong 50B', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1114, 'Nguyễn Văn Sáng', 'Nguyen Van Sang', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1115, 'Đường Số 50', 'Duong So 50', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1116, 'Nguyễn Thị Sóc', 'Nguyen Thi Soc', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1117, 'Đường Số 15', 'Duong So 15', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1118, 'Đường Số 37', 'Duong So 37', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1119, 'Nguyễn Văn Khuyên', 'Nguyen Van Khuyen', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1120, 'Bờ Bao Tân Thắng', 'Bo Bao Tan Thang', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1121, 'Đường Số 18', 'Duong So 18', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1122, 'Nguyễn Văn Bảo', 'Nguyen Van Bao', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1123, 'Gò Ô Môi', 'Go O Moi', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1124, 'Lê Văn Chí', 'Le Van Chi', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1125, 'Phan Huy Chú', 'Phan Huy Chú', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1126, 'Đường Số 13', 'Duong So 13', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1127, 'Lê Liễu', 'Le Lieu', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1128, 'Nguyễn Văn Hưởng', 'Nguyen Van Huong', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1129, 'Đường Số 27', 'Duong So 27', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1130, 'Trần Văn Ơn', 'Tran Van On', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1131, 'Đường 385', 'Duong 385', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1132, 'Đường Trục', 'Duong Truc', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1133, 'Chế Lan Viên', 'Che Lan Vien', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1134, 'Đường Số 14', 'Duong So 14', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1135, 'Đường Số 11 - KDC Miếu Nổi', 'Duong So 11 - KDC Mieu Noi', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1136, 'Đường Số 15', 'Duong So 15', NULL, NULL, 785, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1137, 'Cư Xá Nguyễn Đình Chiểu', 'Cu Xa Nguyen Dinh Chieu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1138, 'Đường Số 14', 'Duong So 14', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1139, 'Song Hành', 'Song Hanh', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1140, 'Đường Số 6', 'Duong So 6', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1141, 'Đường Số 3', 'Duong So 3', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1142, 'Nguyễn Bá Lân', 'Nguyen Ba Lan', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1143, 'Nguyễn Bá Huân', 'Nguyen Ba Huan', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1144, 'Đường Số 67', 'Duong So 67', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1145, 'Tân Hóa', 'Tan Hoa', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1146, 'Tô Ngọc Vân', 'To Ngoc Van', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1147, 'Nguyễn Cừ', 'Nguyen Cu', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1148, 'Nguyễn Thị Kiểu', 'Nguyen Thi Kieu', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1149, 'Bình Trưng', 'Binh Trung', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1150, 'Đường Số 34', 'Duong So 34', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1151, 'Đường Số 16A', 'Duong So 16A', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1152, 'Trương Phước Ban', 'Truong Phuoc Ban', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1153, 'Đường Số 18', 'Duong So 18', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1154, 'CX Phú Lâm', 'CX Phu Lam', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1155, 'Đường Số 19', 'Duong So 19', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1156, 'Phú Mỹ Hưng', 'Phu My Hung', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1157, 'Bà Hom', 'Ba Hom', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1158, 'Đường Số 13 - CX Ngân Hàng', 'Duong So 13 - CX Ngan Hang', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1159, 'Trần Nhân Tông', 'Tran Nhan Tong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1160, 'Đường Số 19', 'Duong So 19', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1161, 'Đường Số 63', 'Duong So 63', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1162, 'Phù Đổng Thiên Vương', 'Phu Dong Thien Vuong', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1163, 'Dân Trí', 'Dan Tri', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1164, 'Đường Số 27', 'Duong So 27', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1165, 'Khu Dân Cư Miếu Nổi', 'Khu Dan Cu Mieu Noi', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1166, 'Tân Hóa', 'Tan Hoa', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1167, 'Khu Dân Cư Him Lam', 'Khu Dan Cu Him Lam', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1168, '30 Tháng 4', '30 Thang 4', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1169, 'Đường Số 19', 'Duong So 19', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1170, 'Đại Nghĩa', 'Dai Nghia', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1171, 'Hòa Hảo', 'Hoa Hao', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1172, 'Hoàng Xuân Nhị', 'Hoang Xuan Nhi', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1173, 'Võ Công Tồn', 'Vo Cong Ton', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1174, 'Đường Số 5', 'Duong So 5', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1175, 'Đường Số 18', 'Duong So 18', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1176, 'Nguyễn Duy', 'Nguyen Duy', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1177, 'Lý Thánh Tông', 'Ly Thanh Tong', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1178, 'Bùi Tự Toàn', 'Bui Tu Toan', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1179, 'Châu Vĩnh Tế', 'Chau Vinh Te', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1180, 'Đường Số 2-Cư Xá Đô Thành', 'Duong So 2-Cu Xa Do Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1181, 'Đường B2 - KDC Chợ Đầu Mối Bình Điền', 'Duong B2 - KDC Cho Dau Moi Binh Dien', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1182, 'Lê Quốc Trinh', 'Le Quoc Trinh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1183, 'Đô Đốc Thủ', 'Do Doc Thu', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1184, 'D52', 'D52', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1185, 'Đường D9', 'Duong D9', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1186, 'Đường Số 36', 'Duong So 36', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1187, 'Đường Số 18A', 'Duong So 18A', NULL, NULL, 785, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1188, 'Phú Thọ', 'Phu Tho', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1189, 'Chu Văn An', 'Chu Van An', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1190, 'Tây Thạnh', 'Tay Thanh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1191, 'Đường Số 21', 'Duong So 21', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1192, 'Đường Số 3', 'Duong So 3', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1193, 'Đường Số 40', 'Duong So 40', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1194, 'Dân Chủ', 'Dan Chu', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1195, 'Khu Phố 3', 'Khu Pho 3', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1196, 'Đường Số 4 - KDC Miếu Nổi', 'Duong So 4 - KDC Mieu Noi', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1197, 'Lê Thị Riêng', 'Le Thi Rieng', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1198, 'Đường Số 61', 'Duong So 61', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1199, 'Đường Số 26', 'Duong So 26', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1200, 'Đống Đa', 'Dong Da', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1201, 'Nguyễn Tư Nghiêm', 'Nguyen Tu Nghiem', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1202, 'Trần Trọng Cung', 'Tran Trong Cung', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1203, 'Vỗ Trường Toản', 'Vo Truong Toan', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1204, 'Phạm Đình Hổ', 'Pham Dinh Ho', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1205, '13', '13', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1206, 'Đường Số 52', 'Duong So 52', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1207, 'Đường Số 11', 'Duong So 11', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1208, 'Đường Số 28', 'Duong So 28', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1209, 'Đường Số 2', 'Duong So 2', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1210, 'Đường Số 20', 'Duong So 20', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1211, 'Tân Phước', 'Tan Phuoc', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1212, 'Nguyễn Nhữ Lãm', 'Nguyen Nhu Lam', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1213, 'Bình Đông', 'Binh Dong', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1214, 'Nguyễn Mỹ Ca', 'Nguyen My Ca', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1215, 'Đường Số 19', 'Duong So 19', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1216, 'Đất Mới', 'Dat Moi', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1217, 'Đỗ Thúc Tịnh', 'Do Thuc Tinh', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1218, 'Phạm Quý Thích', 'Pham Quy Thich', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1219, 'Dương Đức Hiền', 'Duong Duc Hien', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1220, 'Đường 15', 'Duong 15', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1221, 'Trường Chinh', 'Truong Chinh', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1222, 'Đường HT7', 'Duong HT7', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1223, 'Tân Kỳ', 'Tan Ky', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1224, 'Bà Hom', 'Ba Hom', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1225, 'Đường Số 19', 'Duong So 19', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1226, 'Tân Kỳ Tân Quý', 'Tan Ky Tan Quy', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1227, 'Trần Quang Cơ', 'Tran Quang Co', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1228, 'Đường Số 43', 'Duong So 43', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1229, 'Đường Số 2 - An Dương Vương', 'Duong So 2 - An Duong Vuong', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1230, 'Lý Long Tường', 'Ly Long Tuong', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1231, 'Đường Số 8', 'Duong So 8', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1232, 'Đường T8', 'Duong T8', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1233, 'Phú Thuận', 'Phu Thuan', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1234, 'Đặng Chất', 'Dang Chat', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1235, 'Hoàng Ngân', 'Hoang Ngan', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1236, 'Thành Công', 'Thanh Cong', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1237, 'Võ Văn Hát', 'Vo Van Hat', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1238, 'Thích Bửu Đăng', 'Thich Buu Dang', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1239, 'Nguyễn Tử Nha', 'Nguyen Tu Nha', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1240, 'Hoài Thanh', 'Hoai Thanh', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1241, 'Hương Lộ 2', 'Huong Lo 2', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1242, 'Đường Số 54', 'Duong So 54', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1243, 'Đường Số 16', 'Duong So 16', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1244, 'Lê Thạch', 'Le Thach', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1245, 'Nguyễn Văn Huyên', 'Nguyen Van Huyen', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1246, 'Trịnh Đình Trọng', 'Trinh Dinh Trong', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1247, 'Đường Số 32', 'Duong So 32', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1248, 'Quách Đình Bảo', 'Quach Dinh Bao', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1249, 'Đường Số 6', 'Duong So 6', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1250, 'Lò Lu', 'Lo Lu', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1251, 'Đường Số 4', 'Duong So 4', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1252, 'Vĩnh Khánh', 'Vinh Khanh', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1253, 'Tân Thới Hiệp 10', 'Tan Thoi Hiep 10', NULL, NULL, 784, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1254, 'Lê Lâm', 'Le Lam', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1255, 'Nguyễn Xiển', 'Nguyen Xien', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1256, 'Tự Do', 'Tu Do', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1257, 'Trịnh Quang Nghị', 'Trinh Quang Nghi', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1258, 'An Hạ', 'An Ha', NULL, NULL, 785, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1259, 'Đường Số 1', 'Duong So 1', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1260, 'Đường 20', 'Duong 20', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1261, 'Đỗ Nhuận', 'Do Nhuan', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1262, 'Tăng Nhơn Phú', 'Tang Nhon Phu', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1263, 'Hàn Thuyên', 'Han Thuyen', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1264, 'Đường Số 7', 'Duong So 7', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1265, 'Cư Xá Cửu Long', 'Cu Xa Cuu Long', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1266, 'Công Trường Hòa Bình', 'Cong Truong Hoa Binh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1267, 'Đường Số 63', 'Duong So 63', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1268, 'Nguyễn Quý Anh', 'Nguyen Quy Anh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1269, 'Đường Số 2', 'Duong So 2', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1270, 'Đường TX25', 'Duong TX25', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1271, 'Điện Biên Phủ', 'Dien Bien Phu', NULL, NULL, 774, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1272, 'Võ Trứ', 'Vo Tru', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1273, 'Lã Xuân Oai', 'La Xuan Oai', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1274, 'Nguyễn Trãi', 'Nguyen Trai', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1275, 'Đường Số 6', 'Duong So 6', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1276, 'Văn Thân', 'Van Than', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1277, 'Dương Đình Nghệ', 'Duong Dinh Nghe', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1278, 'Võ Văn Kiệt', 'Vo Van Kiet', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1279, 'Đường D52-K300', 'Duong D52-K300', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1280, 'Đường Số 41', 'Duong So 41', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1281, 'Bùi Minh Trực', 'Bui Minh Truc', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1282, 'Lê Sao', 'Le Sao', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1283, 'Tân Hòa 2', 'Tan Hoa 2', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1284, 'Đường Số 12', 'Duong So 12', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1285, 'Đường Số 4', 'Duong So 4', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1286, 'Tam Thới 2', 'Tam Thoi 2', NULL, NULL, 784, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1287, 'Đường TX52', 'Duong TX52', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1288, 'Đường Số 11A', 'Duong So 11A', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1289, 'Độc Lập', 'Doc Lap', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1290, 'Tân Chánh Hiệp 10', 'Tan Chanh Hiep 10', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1291, 'Thống Nhất', 'Thong Nhat', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1292, 'Huỳnh Đình Hai', 'Huynh Dinh Hai', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1293, 'Nguyễn Văn Mai', 'Nguyen Van Mai', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1294, 'Đường Số 55', 'Duong So 55', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1295, 'Lưu Chí Hiếu', 'Luu Chi Hieu', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1296, 'Đường Số 5B', 'Duong So 5B', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1297, 'Nguyễn Duy Hiệu', 'Nguyen Duy Hieu', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1298, 'Ngô Nhơn Tịnh', 'Ngo Nhon Tinh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1299, 'Hồ Đắc Di', 'Ho Dac Di', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1300, 'Nguyễn Thị Minh Khai', 'Nguyen Thi Minh Khai', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1301, 'Đường Số 20', 'Duong So 20', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1302, 'Lê Quý Đôn', 'Le Quy Don', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1303, 'Trịnh Lỗi', 'Trinh Loi', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1304, 'Nguyễn Đăng Giai', 'Nguyen Dang Giai', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1305, 'Phan Huy Ôn', 'Phan Huy On', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1306, 'Hoàng Diệu', 'Hoang Dieu', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1307, 'Man Thiện', 'Man Thien', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1308, 'Đường C3', 'Duong C3', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1309, 'Đường Số 6', 'Duong So 6', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1310, 'Đào Duy Anh', 'Dao Duy Anh', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1311, 'Nguyễn Duy Trinh', 'Nguyen Duy Trinh', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1312, 'Đường Số 8', 'Duong So 8', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1313, 'Bùi Đằng Đoàn', 'Bui Dang Doan', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:19', NULL),
(1314, 'Cao Văn Lầu', 'Cao Van Lau', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1315, 'Huỳnh Văn Nghệ', 'Huynh Van Nghe', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1316, 'Linh Đông', 'Linh Dong', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1317, 'Đường 104', 'Duong 104', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1318, 'Đặng Thị Rành', 'Dang Thi Ranh', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1319, 'Trần Nhân Tông', 'Tran Nhan Tong', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1320, 'Đường TL 19', 'Duong TL 19', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1321, 'Chánh Hưng', 'Chanh Hung', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1322, 'Miếu Gò Xoài', 'Mieu Go Xoai', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1323, 'Mễ Cốc', 'Me Coc', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1324, 'Rạch Cát', 'Rach Cat', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1325, 'Đường 47', 'Duong 47', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1326, 'Đoàn Giỏi', 'Doan Gioi', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1327, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1328, 'Trần Quang Cơ', 'Tran Quang Co', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1329, 'Trương Văn Bang', 'Truong Van Bang', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1330, 'Đường Số 5 - Khu Z756', 'Duong So 5 - Khu Z756', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1331, 'Đường Số 30', 'Duong So 30', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1332, 'Đường Số 38', 'Duong So 38', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1333, 'Đường Số 16', 'Duong So 16', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1334, 'Công Xã Pari', 'Cong Xa Pari', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1335, 'Hoàng Ngọc Phách', 'Hoang Ngoc Phach', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1336, 'Đường Số 48', 'Duong So 48', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1337, 'Bàu Cát 9', 'Bau Cat 9', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1338, 'Đường Số 18', 'Duong So 18', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1339, 'Đường 1 - Lý Phục Man', 'Duong 1 - Ly Phuc Man', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL);
INSERT INTO `lck_location_street` (`id`, `name`, `name_ascii`, `location`, `ward_id`, `district_id`, `user_id_created`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1340, 'Nguyễn Duy Trinh', 'Nguyen Duy Trinh', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1341, 'An Phú Đông 09', 'An Phu Dong 09', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1342, 'Nguyễn Thiện Thuật', 'Nguyen Thien Thuat', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1343, 'Đường 147', 'Duong 147', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1344, 'Lưu Hữu Phước', 'Luu Huu Phuoc', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1345, 'Nội Bộ', 'Noi Bo', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1346, 'Bình Đăng', 'Binh Dang', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1347, 'Nguyễn Văn Giáp', 'Nguyen Van Giap', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1348, 'Đường Số 65', 'Duong So 65', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1349, 'Phú Định', 'Phu Dinh', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1350, 'Xuân Hồng', 'Xuan Hong', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1351, 'Huỳnh Tấn Phát', 'Huynh Tan Phat', NULL, NULL, 786, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1352, 'Đường Số 22', 'Duong So 22', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1353, 'Đường Số 16', 'Duong So 16', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1354, 'Trần Quốc Hoàn', 'Tran Quoc Hoan', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1355, 'Đường N13', 'Duong N13', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1356, 'Chiến Lược', 'Chien Luoc', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1357, 'Tân Khai', 'Tan Khai', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1358, 'Khu Dân Cư Sông Gi', 'Khu Dan Cu Song Gi', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1359, 'Tây Lân', 'Tay Lan', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1360, 'Đường 29', 'Duong 29', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1361, 'Khu Kiều Đàm', 'Khu Kieu Dam', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1362, 'Trà Khúc', 'Tra Khuc', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1363, 'Đường D8', 'Duong D8', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1364, 'Lê Thiệt', 'Le Thiet', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1365, 'Đường Số 1', 'Duong So 1', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1366, 'Đường TL29', 'Duong TL29', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1367, 'Đường Số 62', 'Duong So 62', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1368, 'Cư Xá Phan Đăng Lưu', 'Cu Xa Phan Dang Luu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1369, 'Phạm Thận Duật', 'Pham Than Duat', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1370, 'Lê Văn Việt', 'Le Van Viet', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1371, 'Phạm Công Trứ', 'Pham Cong Tru', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1372, 'Trần Thị Nghị', 'Tran Thi Nghi', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1373, 'Đường Số 9', 'Duong So 9', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1374, 'Yên Đỗ', 'Yen Do', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1375, 'Quốc Lộ 52', 'Quoc Lo 52', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1376, 'Nguyễn Trường Tộ', 'Nguyen Truong To', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1377, 'Quốc Lộ 1', 'Quoc Lo 1', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1378, 'Lý Tuệ', 'Ly Tue', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1379, 'Nguyễn Văn Tố', 'Nguyen Van To', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1380, 'Phú Thọ Hoà', 'Phu Tho Hoa', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1381, 'Đường số 25', 'Duong so 25', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1382, 'Liên Khu 5-6', 'Lien Khu 5-6', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1383, 'Tân Khai', 'Tan Khai', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1384, 'Lê Văn Sỹ', 'Le Van Sy', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1385, 'Lê Văn Sỹ', 'Le Van Sy', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1386, 'Đông Hưng Thuận 42', 'Dong Hung Thuan 42', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1387, 'Lê Văn Sỹ', 'Le Van Sy', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1388, 'Đình Nghi Xuân', 'Dinh Nghi Xuan', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1389, 'Đường Số 1', 'Duong So 1', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1390, 'Gò Cát', 'Go Cat', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1391, 'Trương Vĩnh Ký', 'Truong Vinh Ky', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1392, 'Tô Hiến Thành', 'To Hien Thanh', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1393, 'Thạnh Lộc', 'Thanh Loc', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1394, 'Huỳnh Thiên Lộc', 'Huynh Thien Loc', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1395, 'Lê Vĩnh Hòa', 'Le Vinh Hoa', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1396, 'Tân Chánh Hiệp 21', 'Tan Chanh Hiep 21', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1397, 'Đường Số 5', 'Duong So 5', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1398, 'Nguyễn Thái Học', 'Nguyen Thai Hoc', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1399, 'Đường Số 12', 'Duong So 12', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1400, 'Đường HT39', 'Duong HT39', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1401, 'Quốc Lộ 50', 'Quoc Lo 50', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1402, 'Đường Số 21', 'Duong So 21', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1403, 'Đỗ Xuân Hợp', 'Do Xuan Hop', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1404, 'Hồ Ngọc Cẩn', 'Ho Ngoc Can', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1405, 'Nguyễn Sĩ Sách', 'Nguyen Si Sach', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1406, 'Đường số 51', 'Duong so 51', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1407, 'Đường số 24', 'Duong so 24', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1408, 'Đường số 61', 'Duong so 61', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1409, 'Đường Số 30', 'Duong So 30', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1410, 'Bà Trưng Trắc', 'Ba Trung Trac', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1411, 'Đường Số 18', 'Duong So 18', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1412, 'Ấp 2', 'Ap 2', NULL, NULL, 783, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1413, 'Đường số 53', 'Duong so 53', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1414, 'Đường 36', 'Duong 36', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1415, 'Nguyễn Trung Nguyệt', 'Nguyen Trung Nguyet', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1416, 'Bình Hưng', 'Binh Hung', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1417, 'Lý Nam Đế', 'Ly Nam De', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1418, 'Đường 31F', 'Duong 31F', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1419, 'Nguyễn Thị Tần', 'Nguyen Thi Tan', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1420, 'Trịnh Hoài Đức', 'Trinh Hoai Duc', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1421, 'Đường Số 11', 'Duong So 11', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1422, 'Tô Ngọc Vân', 'To Ngoc Van', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1423, 'CX Thanh Đa', 'CX Thanh Da', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1424, 'Phạm Đình Chính', 'Pham Dinh Chinh', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1425, 'Đường số 13', 'Duong so 13', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1426, 'Đường số 59', 'Duong so 59', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1427, 'Dương Văn Cam', 'Duong Van Cam', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1428, 'Nguyễn Văn Khối', 'Nguyen Van Khoi', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1429, 'Lê Hữu Kiều', 'Le Huu Kieu', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1430, 'Nguyễn Văn Dung', 'Nguyen Van Dung', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1431, 'Đào Cam Mộc', 'Dao Cam Moc', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1432, 'Đường 2', 'Duong 2', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1433, 'Phan Bội Châu', 'Phan Boi Chau', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1434, 'Hồ Ngọc Lãm', 'Ho Ngoc Lam', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1435, 'Đường Số 29', 'Duong So 29', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1436, 'Trần Quang Diện', 'Tran Quang Dien', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1437, 'Tam Đa', 'Tam Da', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1438, 'Tam Bình', 'Tam Binh', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1439, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1440, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1441, 'Đường số 30', 'Duong so 30', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1442, 'Đường số 7', 'Duong so 7', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1443, 'Phạm Thái Bường', 'Pham Thai Buong', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1444, 'Nguyên Hồng', 'Nguyen Hong', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1445, 'Đường số 7', 'Duong so 7', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1446, 'Tân Hoà Đông', 'Tan Hoa Dong', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1447, 'Đường A6', 'Duong A6', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1448, 'Đặng Thùy Trâm', 'Dang Thuy Tram', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1449, 'Đỗ Công Tường', 'Do Cong Tuong', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1450, 'Đô Đốc Chấn', 'Do Doc Chan', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1451, 'Nguyễn Hậu', 'Nguyen Hau', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1452, 'Đường Số 54', 'Duong So 54', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1453, 'Đường Số 8', 'Duong So 8', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1454, 'Hòa Bình', 'Hoa Binh', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1455, 'Đường Số 5', 'Duong So 5', NULL, NULL, 785, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1456, 'Ðào Duy Từ', 'Ðao Duy Tu', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1457, 'Nguyễn Thái Sơn', 'Nguyen Thai Son', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1458, 'Đường số 23', 'Duong so 23', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1459, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1460, 'Đường Số 47', 'Duong So 47', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1461, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1462, 'Bà Lài', 'Ba Lai', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1463, 'Đường Số 43', 'Duong So 43', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1464, 'Trương Phước Phan', 'Truong Phuoc Phan', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1465, 'Lê Quát', 'Le Quat', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1466, 'Thích Minh Nguyệt', 'Thich Minh Nguyet', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1467, 'Nguyễn Tri Phương', 'Nguyen Tri Phuong', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1468, 'Lê Lư', 'Le Lu', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1469, 'Đường số 7', 'Duong so 7', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1470, 'Tú Mỡ', 'Tu Mo', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1471, 'Đường 26 Tháng 3', 'Duong 26 Thang 3', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1472, 'Dương Khuê', 'Duong Khue', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1473, 'Tái Thiết', 'Tai Thiet', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1474, 'Lò Gốm', 'Lo Gom', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1475, 'Trần Hưng Đạo', 'Tran Hung Dao', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1476, 'Đường Số 48', 'Duong So 48', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1477, 'Lê Thành Phương', 'Le Thanh Phuong', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1478, 'Đường 20', 'Duong 20', NULL, NULL, 785, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1479, 'Lê Tấn Kế', 'Le Tan Ke', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1480, 'KDC An Phú An Khánh', 'KDC An Phu An Khanh', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1481, 'Đường Số 5', 'Duong So 5', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1482, 'Đường 26 Tháng 3', 'Duong 26 Thang 3', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1483, 'Lương Ngọc Quyến', 'Luong Ngoc Quyen', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1484, 'Ba Gia', 'Ba Gia', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1485, 'Ngô Quyền', 'Ngo Quyen', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1486, 'TX21, Khu Phố 5', 'TX21 Khu Pho 5', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1487, 'Đường Số 9', 'Duong So 9', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1488, 'Vũ Trọng Phụng', 'Vu Trong Phung', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1489, 'Bình Chiểu', 'Binh Chieu', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1490, 'Đường 970', 'Duong 970', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1491, 'Tân Thành', 'Tan Thanh', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1492, 'Dương Đình Hội', 'Duong Dinh Hoi', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1493, 'Đường TCH25', 'Duong TCH25', NULL, NULL, 786, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1494, 'Xuân Thới Đông 08', 'Xuan Thoi Dong 08', NULL, NULL, 784, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1495, 'Liên tỉnh 5', 'Lien tinh 5', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1496, 'Hồ Văn Tư', 'Ho Van Tu', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1497, 'Cây Cám', 'Cay Cam', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1498, 'Đường số 379', 'Duong so 379', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1499, 'Đại Lộ 2', 'Dai Lo 2', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1500, 'Đường 25', 'Duong 25', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1501, 'Thạnh Xuân 25', 'Thanh Xuan 25', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1502, 'Quốc Lộ 1A', 'Quoc Lo 1A', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1503, 'Tân Thới Nhất 17', 'Tan Thoi Nhat 17', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1504, 'Đường HT 17', 'Duong HT 17', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1505, 'Đồng Tâm', 'Dong Tam', NULL, NULL, 784, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1506, 'Hồ Học Lãm', 'Ho Hoc Lam', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1507, 'Quang Trung', 'Quang Trung', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1508, 'Hồ Bá Phấn', 'Ho Ba Phan', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1509, 'Tống Hữu Định', 'Tong Huu Dinh', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1510, 'KDC Nam Long', 'KDC Nam Long', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1511, 'Lưu  Hưu Phước', 'Luu  Huu Phuoc', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1512, 'Ỷ Lan', 'Y Lan', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1513, 'Đàm Thận Huy', 'Dam Than Huy', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1514, 'Hoàng Hữu Nam', 'Hoang Huu Nam', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1515, 'Huỳnh Thiên Lộc', 'Huynh Thien Loc', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1516, 'Đường 48', 'Duong 48', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1517, 'Tản Viên', 'Tan Vien', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1518, 'Đồng Tranh', 'Dong Tranh', NULL, NULL, 787, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1519, 'Thống Nhất', 'Thong Nhat', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1520, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1521, 'Hoàng Hoa Thám', 'Hoang Hoa Tham', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1522, 'Nguyễn Thị Minh Khai', 'Nguyen Thi Minh Khai', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1523, 'Phạm Ngũ Lão', 'Pham Ngu Lao', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1524, 'Nguyễn Văn Phú', 'Nguyen Van Phu', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1525, 'Mai Thị Lựu', 'Mai Thi Luu', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1526, 'Khu Dân Cư Miếu Nổi', 'Khu Dan Cu Mieu Noi', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1527, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1528, 'Hoàng Hoa Thám', 'Hoang Hoa Tham', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1529, 'Phạm Văn Chiêu', 'Pham Van Chieu', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1530, 'Ung Văn Khiêm', 'Ung Van Khiem', NULL, NULL, 765, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1531, 'Nguyễn Kiệm', 'Nguyen Kiem', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1532, 'Mạc Đĩnh Chi', 'Mac Dinh Chi', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1533, 'Nam Kỳ Khởi Nghĩa', 'Nam Ky Khoi Nghia', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1534, 'Nguyễn Bỉnh Khiêm', 'Nguyen Binh Khiem', NULL, NULL, 760, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1535, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1536, 'Nguyễn Kiệm', 'Nguyen Kiem', NULL, NULL, 764, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1537, 'Miếu Nổi', 'Mieu Noi', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1538, 'Ngô Thị Thu Minh', 'Ngo Thi Thu Minh', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1539, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1540, 'Cách Mạng', 'Cach Mang', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1541, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1542, 'Đường số 46', 'Duong so 46', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1543, 'Tân Kỳ', 'Tan Ky', NULL, NULL, 767, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1544, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1545, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1546, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1547, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1548, 'Lê Văn Sỹ', 'Le Van Sy', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1549, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1550, 'Lê Văn Sỹ', 'Le Van Sy', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1551, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1552, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1553, 'Nguyễn Cơ Thạch', 'Nguyen Co Thach', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1554, 'Lê Văn Sỹ', 'Le Van Sy', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1555, 'Lê Văn Sỹ', 'Le Van Sy', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1556, 'Số 77', 'So 77', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1557, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1558, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1559, 'An Phú', 'An Phu', NULL, NULL, 769, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1560, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1561, 'Lê Văn Sỹ', 'Le Van Sy', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1562, 'Đường Số 18', 'Duong So 18', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1563, 'Chợ Lớn', 'Cho Lon', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1564, 'Tuy Lý Vương', 'Tuy Ly Vuong', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1565, 'Đường Số 10', 'Duong So 10', NULL, NULL, 785, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1566, 'Tân Chánh Hiệp 26', 'Tan Chanh Hiep 26', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1567, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1568, 'Đường số 1A', 'Duong so 1A', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1569, '3 Tháng  2', '3 Thang  2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1570, 'Tân Vĩnh', 'Tan Vinh', NULL, NULL, 773, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:20', NULL),
(1571, 'Đường DHT 10B', 'Duong DHT 10B', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1572, 'Ni Sư Quỳnh Liên', 'Ni Su Quynh Lien', NULL, NULL, 766, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1573, 'Dã Tượng', 'Da Tuong', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1574, 'Đường Số 44', 'Duong So 44', NULL, NULL, 776, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1575, 'Trương Văn Thành', 'Truong Van Thanh', NULL, NULL, 763, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1576, 'Lê Công Phép', 'Le Cong Phep', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1577, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1578, 'Lê Văn Sĩ', 'Le Van Si', NULL, NULL, 770, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1579, 'Đường số 3A', 'Duong so 3A', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1580, 'Lê Văn Sĩ', 'Le Van Si', NULL, NULL, 768, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1581, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1582, 'Liên Khu 4-5', 'Lien Khu 4-5', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1583, 'Lý Phục Man', 'Ly Phuc Man', NULL, NULL, 778, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1584, 'Đường số 15', 'Duong so 15', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1585, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1586, 'Quốc Lộ 1A', 'Quoc Lo 1A', NULL, NULL, 762, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1587, 'Vương Văn Huống', 'Vuong Van Huong', NULL, NULL, 777, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1588, 'Đường Số 19', 'Duong So 19', NULL, NULL, 775, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1589, 'Tân Thới Hiệp 06', 'Tan Thoi Hiep 06', NULL, NULL, 761, NULL, '2018-12-06 14:35:50', '2019-01-16 22:51:21', NULL),
(1590, 'Đường Số 70', 'Duong So 70', NULL, NULL, 775, 1, '2018-12-06 14:50:44', '2019-01-16 22:53:43', NULL),
(1591, 'Lê Văn Sĩ', 'Le Van Si', NULL, NULL, 766, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1592, 'Tây Sơn', 'Tay Son', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1593, 'Đường số 85', 'Duong so 85', NULL, NULL, 778, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1594, 'Cây Keo', 'Cay Keo', NULL, NULL, 762, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1595, 'Liên tỉnh 5', 'Lien tinh 5', NULL, NULL, 785, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1596, 'Đường Số 31', 'Duong So 31', NULL, NULL, 764, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1597, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 766, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1598, 'Cách Mạng Tháng 8', 'Cach Mang Thang 8', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1599, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1600, 'B1', 'B1', NULL, NULL, 766, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1601, 'Linh Trung', 'Linh Trung', NULL, NULL, 762, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1602, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1603, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1604, 'Ngyên Hồng', 'Ngyen Hong', NULL, NULL, 764, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1605, 'Trần Quốc Tuấn', 'Tran Quoc Tuan', NULL, NULL, 765, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1606, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1607, 'Nguyễn Quý Yêm', 'Nguyen Quy Yem', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1608, 'Nguyễn Háo Vinh', 'Nguyen Hao Vinh', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1609, 'Huỳnh Lan Khanh', 'Huynh Lan Khanh', NULL, NULL, 766, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1610, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1611, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1612, 'Điện Cao Thế', 'Dien Cao The', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1613, 'Đường Số 9', 'Duong So 9', NULL, NULL, 785, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1614, '3 Tháng 2', '3 Thang 2', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1615, 'Phùng Chí Kiên', 'Phung Chi Kien', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1616, 'Tỉnh Lộ 43', 'Tinh Lo 43', NULL, NULL, 762, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1617, 'Đường 21', 'Duong 21', NULL, NULL, 785, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1618, 'Chu Văn An', 'Chu Van An', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1619, 'Lê Quang Sung', 'Le Quang Sung', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1620, 'Đường Số 16', 'Duong So 16', NULL, NULL, 763, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1621, 'Trần Bình', 'Tran Binh', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1622, 'Trần Hưng Đạo', 'Tran Hung Dao', NULL, NULL, 763, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1623, 'Đường Số 2', 'Duong So 2', NULL, NULL, 763, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1624, 'Tân Thới Hiệp 10', 'Tan Thoi Hiep 10', NULL, NULL, 761, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1625, 'Lô Tư', 'Lo Tu', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1626, 'Nguyễn Xuân Khoát', 'Nguyen Xuan Khoat', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1627, 'Phạm Ngọc', 'Pham Ngoc', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1628, 'Đường Số 35', 'Duong So 35', NULL, NULL, 762, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1629, 'Cư Xá Phú Lâm B', 'Cu Xa Phu Lam B', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1630, 'Đường Số 11', 'Duong So 11', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1631, 'Đường 37', 'Duong 37', NULL, NULL, 778, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1632, 'Lý Thường Kiệt', 'Ly Thuong Kiet', NULL, NULL, 784, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1633, 'Đường TCH21', 'Duong TCH21', NULL, NULL, 761, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1634, 'Phạm Hữu Lầu', 'Pham Huu Lau', NULL, NULL, 786, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1635, 'Đường 26/3', 'Duong 263', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1636, 'Tân Thắng', 'Tan Thang', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1637, 'Nguyễn Văn Dưỡng', 'Nguyen Van Duong', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1638, 'Đường Số 51', 'Duong So 51', NULL, NULL, 778, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1639, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1640, 'Đỗ Thị Tâm', 'Do Thi Tam', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1641, 'Trần Văn Kiểu', 'Tran Van Kieu', NULL, NULL, 774, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1642, 'Đường Số 14', 'Duong So 14', NULL, NULL, 773, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1643, 'Đường 10C', 'Duong 10C', NULL, NULL, 773, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1644, 'Bình Phú', 'Binh Phu', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1645, '3 Tháng 2', '3 Thang 2', NULL, NULL, 772, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1646, 'Cần Giuộc', 'Can Giuoc', NULL, NULL, 776, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1647, 'Bình Tiên', 'Binh Tien', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1648, 'Đường Số 6', 'Duong So 6', NULL, NULL, 785, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1649, 'Đường Số 5', 'Duong So 5', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1650, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1651, 'Hùng Vương', 'Hung Vuong', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1652, 'Tân Thới Hiệp 13', 'Tan Thoi Hiep 13', NULL, NULL, 761, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1653, 'Đường số 1B', 'Duong so 1B', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1654, 'Đường Số 12', 'Duong So 12', NULL, NULL, 778, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1655, 'Nguyễn Dư', 'Nguyen Du', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1656, 'Đường Số 4', 'Duong So 4', NULL, NULL, 762, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1657, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1658, 'Đường Vành Đai 1', 'Duong Vanh Dai 1', NULL, NULL, 776, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1659, 'Ấp Đông Lân', 'Ap Dong Lan', NULL, NULL, 784, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1660, 'Ngô Chí Quốc', 'Ngo Chi Quoc', NULL, NULL, 762, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1661, 'Dương Tự Quán', 'Duong Tu Quan', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1662, 'Đường Số 8', 'Duong So 8', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1663, 'Đường Số 36', 'Duong So 36', NULL, NULL, 762, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1664, 'Nguyễn Xuân Ôn', 'Nguyen Xuan On', NULL, NULL, 765, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1665, 'Đường Số 24', 'Duong So 24', NULL, NULL, 762, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1666, 'Phan Văn Khoẻ', 'Phan Van Khoe', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1667, 'Bạch Đằng', 'Bach Dang', NULL, NULL, 764, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1668, 'Thới An 32', 'Thoi An 32', NULL, NULL, 761, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1669, 'Bia Truyền Thống', 'Bia Truyen Thong', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1670, 'Phạm Đăng Giảng', 'Pham Dang Giang', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1671, 'Bà Ký', 'Ba Ky', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1672, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1673, 'Đường Số 47', 'Duong So 47', NULL, NULL, 762, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1674, 'Tân Thọ', 'Tan Tho', NULL, NULL, 766, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1675, '3 Tháng 2', '3 Thang 2', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1676, 'Bình Tây', 'Binh Tay', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1677, 'Đường số 37', 'Duong so 37', NULL, NULL, 769, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1678, 'Tháp Mười', 'Thap Muoi', NULL, NULL, 775, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1679, 'Hiệp Thành 13', 'Hiep Thanh 13', NULL, NULL, 761, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1680, 'Lê Trung Đình', 'Le Trung Dinh', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1681, 'Phùng Tá Chu', 'Phung Ta Chu', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1682, 'Bình Thành', 'Binh Thanh', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1683, 'Khiếu Năng Tĩnh', 'Khieu Nang Tinh', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1684, 'Phú Lộc', 'Phu Loc', NULL, NULL, 766, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1685, 'Phước Thiện', 'Phuoc Thien', NULL, NULL, 763, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1686, '43499', '43499', NULL, NULL, 771, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1687, '43499', '43499', NULL, NULL, 772, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1688, 'Đỗ Năng Tế', 'Do Nang Te', NULL, NULL, 777, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1689, 'Đường số 12', 'Duong so 12', NULL, NULL, 773, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1690, 'Đường số T7', 'Duong so T7', NULL, NULL, 767, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1691, 'Phạm Văn Sáng', 'Pham Van Sang', NULL, NULL, 785, NULL, '2019-01-16 22:53:43', '2019-01-16 22:53:43', NULL),
(1692, 'Trung Mỹ Tân Xuân', 'Trung My Tan Xuan', NULL, NULL, 784, NULL, '2019-01-16 22:53:44', '2019-01-16 22:53:44', NULL),
(1693, 'Trần Đại Nghĩa', 'Tran Dai Nghia', NULL, NULL, 785, NULL, '2019-01-16 22:53:44', '2019-01-16 22:53:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lck_location_ward`
--

CREATE TABLE `lck_location_ward` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ascii` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_origin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district_id` int(11) NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_location_ward`
--

INSERT INTO `lck_location_ward` (`id`, `name`, `name_ascii`, `name_origin`, `location`, `type`, `district_id`, `user_id_created`, `created_at`, `updated_at`, `deleted_at`) VALUES
(26734, 'Phường Tân Định', NULL, 'Phường Tân Định', '10.7930968-106.6902951', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26737, 'Phường Đa Kao', NULL, 'Phường Đa Kao', '10.7878843-106.6984026', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26740, 'Phường Bến Nghé', NULL, 'Phường Bến Nghé', '10.7808334-106.702825', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26743, 'Phường Bến Thành', NULL, 'Phường Bến Thành', '10.7735994-106.6944173', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26746, 'Phường Nguyễn Thái Bình', NULL, 'Phường Nguyễn Thái Bình', '10.7693846-106.7006138', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26749, 'Phường Phạm Ngũ Lão', NULL, 'Phường Phạm Ngũ Lão', '10.7658855-106.6908105', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26752, 'Phường Cầu Ông Lãnh', NULL, 'Phường Cầu Ông Lãnh', '10.7655446-106.6961914', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26755, 'Phường Cô Giang', NULL, 'Phường Cô Giang', '10.7616235-106.6932433', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26758, 'Phường Nguyễn Cư Trinh', NULL, 'Phường Nguyễn Cư Trinh', '10.7640301-106.68661', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26761, 'Phường Cầu Kho', NULL, 'Phường Cầu Kho', '10.7577834-106.6888211', 'Phường', 760, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26764, 'Phường Thạnh Xuân', NULL, 'Phường Thạnh Xuân', '10.8834303-106.6703963', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26767, 'Phường Thạnh Lộc', NULL, 'Phường Thạnh Lộc', '10.8712302-106.6859815', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26770, 'Phường Hiệp Thành', NULL, 'Phường Hiệp Thành', '10.8825023-106.6379724', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26773, 'Phường Thới An', NULL, 'Phường Thới An', '10.8760697-106.6556575', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26776, 'Phường Tân Chánh Hiệp', NULL, 'Phường Tân Chánh Hiệp', '10.866797-106.6261831', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26779, 'Phường An Phú Đông', NULL, 'Phường An Phú Đông', '10.8596614-106.7057733', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26782, 'Phường Tân Thới Hiệp', NULL, 'Phường Tân Thới Hiệp', '10.8603672-106.6438673', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26785, 'Phường Trung Mỹ Tây', NULL, 'Phường Trung Mỹ Tây', '10.856544-106.6143944', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26787, 'Phường Tân Hưng Thuận', NULL, 'Phường Tân Hưng Thuận', '10.8384209-106.6217623', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26788, 'Phường Đông Hưng Thuận', NULL, 'Phường Đông Hưng Thuận', '10.8433839-106.630604', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26791, 'Phường Tân Thới Nhất', NULL, 'Phường Tân Thới Nhất', '10.8292885-106.6143944', 'Phường', 761, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26794, 'Phường Linh Xuân', NULL, 'Phường Linh Xuân', '10.8804079-106.773595', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26797, 'Phường Bình Chiểu', NULL, 'Phường Bình Chiểu', '10.8830404-106.7264125', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26800, 'Phường Linh Trung', NULL, 'Phường Linh Trung', '10.8637312-106.7794935', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26803, 'Phường Tam Bình', NULL, 'Phường Tam Bình', '10.8676413-106.7337841', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26806, 'Phường Tam Phú', NULL, 'Phường Tam Phú', '10.8551341-106.7382072', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26809, 'Phường Hiệp Bình Phước', NULL, 'Phường Hiệp Bình Phước', '10.8455467-106.7146184', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26812, 'Phường Hiệp Bình Chánh', NULL, 'Phường Hiệp Bình Chánh', '10.8339953-106.7264125', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26815, 'Phường Linh Chiểu', NULL, 'Phường Linh Chiểu', '10.8538209-106.7617984', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26818, 'Phường Linh Tây', NULL, 'Phường Linh Tây', '10.8560516-106.7535475', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26821, 'Phường Linh Đông', NULL, 'Phường Linh Đông', '10.843909-106.7441048', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26824, 'Phường Bình Thọ', NULL, 'Phường Bình Thọ', '10.8467644-106.7662221', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26827, 'Phường Trường Thọ', NULL, 'Phường Trường Thọ', '10.832358-106.7559004', 'Phường', 762, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26830, 'Phường Long Bình', NULL, 'Phường Long Bình', '10.8909381-106.8283134', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26833, 'Phường Long Thạnh Mỹ', NULL, 'Phường Long Thạnh Mỹ', '10.8421949-106.8237374', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26836, 'Phường Tân Phú', NULL, 'Phường Tân Phú', '10.8569656-106.8030892', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26839, 'Phường Hiệp Phú', NULL, 'Phường Hiệp Phú', '10.8486667-106.7809682', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26842, 'Phường Tăng Nhơn Phú A', NULL, 'Phường Tăng Nhơn Phú A', '10.8409534-106.79719', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26845, 'Phường Tăng Nhơn Phú B', NULL, 'Phường Tăng Nhơn Phú B', '10.8307175-106.7853922', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26848, 'Phường Phước Long B', NULL, 'Phường Phước Long B', '10.8147076-106.7794935', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26851, 'Phường Phước Long A', NULL, 'Phường Phước Long A', '10.8224164-106.763273', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26854, 'Phường Trường Thạnh', NULL, 'Phường Trường Thạnh', '10.8117526-106.8325872', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26857, 'Phường Long Phước', NULL, 'Phường Long Phước', '10.8075496-106.8591387', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26860, 'Phường Long Trường', NULL, 'Phường Long Trường', '10.7986358-106.8237374', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26863, 'Phường Phước Bình', NULL, 'Phường Phước Bình', '10.8137558-106.7721204', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26866, 'Phường Phú Hữu', NULL, 'Phường Phú Hữu', '10.7890603-106.8001396', 'Phường', 763, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26869, 'Phường 15', NULL, 'Phường 15', '10.8514643-106.6678466', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26872, 'Phường 13', NULL, 'Phường 13', '10.8551871-106.6584938', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26875, 'Phường 17', NULL, 'Phường 17', '10.8409406-106.6748181', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26876, 'Phường 06', NULL, 'Phường 06', '10.8384391-106.6816998', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26878, 'Phường 16', NULL, 'Phường 16', '10.846657-106.6650311', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26881, 'Phường 12', NULL, 'Phường 12', '10.8347203-106.6394461', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26882, 'Phường 14', NULL, 'Phường 14', '10.8423134-106.6367', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26884, 'Phường 10', NULL, 'Phường 10', '10.8329295-106.6718702', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26887, 'Phường 05', NULL, 'Phường 05', '10.7968936-106.6867575', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26890, 'Phường 07', NULL, 'Phường 07', '10.829553-106.683662', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26893, 'Phường 04', NULL, 'Phường 04', '10.7687584-106.6839393', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26896, 'Phường 01', NULL, 'Phường 01', '10.8173779-106.6888976', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26897, 'Phường 09', NULL, 'Phường 09', '10.8475319-106.6541837', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26898, 'Phường 08', NULL, 'Phường 08', '10.8422445-106.6512361', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26899, 'Phường 11', NULL, 'Phường 11', '10.8390308-106.6600791', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26902, 'Phường 03', NULL, 'Phường 03', '10.770528-106.6862868', 'Phường', 764, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26905, 'Phường 13', NULL, 'Phường 13', '10.8256869-106.7042991', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26908, 'Phường 11', NULL, 'Phường 11', '10.8180045-106.6954544', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26911, 'Phường 27', NULL, 'Phường 27', '10.8166994-106.7190411', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26914, 'Phường 26', NULL, 'Phường 26', '10.8131851-106.7087216', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26917, 'Phường 12', NULL, 'Phường 12', '10.8122309-106.7013508', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26920, 'Phường 25', NULL, 'Phường 25', '10.8040973-106.7153875', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26923, 'Phường 05', NULL, 'Phường 05', '10.790508-106.707999', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26926, 'Phường 07', NULL, 'Phường 07', '10.790526-106.708045', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26929, 'Phường 24', NULL, 'Phường 24', '10.8058993-106.7050362', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26932, 'Phường 06', NULL, 'Phường 06', '10.789414-106.708069', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26935, 'Phường 14', NULL, 'Phường 14', '10.8049449-106.6976655', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26938, 'Phường 15', NULL, 'Phường 15', '10.8004527-106.7050362', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26941, 'Phường 02', NULL, 'Phường 02', '10.791161-106.707233', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26944, 'Phường 01', NULL, 'Phường 01', '10.790409-106.707725', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26947, 'Phường 03', NULL, 'Phường 03', '10.790481-106.70799', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26950, 'Phường 17', NULL, 'Phường 17', '10.7962866-106.7065103', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26953, 'Phường 21', NULL, 'Phường 21', '10.7957882-106.7117589', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26956, 'Phường 22', NULL, 'Phường 22', '10.7921907-106.7190411', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26959, 'Phường 19', NULL, 'Phường 19', '10.7899564-106.7101958', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26962, 'Phường 28', NULL, 'Phường 28', '10.825006-106.7411559', 'Phường', 765, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26965, 'Phường 02', NULL, 'Phường 02', '10.76246-106.668971', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26968, 'Phường 04', NULL, 'Phường 04', '10.7687584-106.6839393', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26971, 'Phường 12', NULL, 'Phường 12', '10.7931859-106.6500753', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26974, 'Phường 13', NULL, 'Phường 13', '10.8058688-106.6438673', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26977, 'Phường 01', NULL, 'Phường 01', '10.7947121-106.6675593', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26980, 'Phường 03', NULL, 'Phường 03', '10.762466-106.669001', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26983, 'Phường 11', NULL, 'Phường 11', '10.790694-106.6495246', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26986, 'Phường 07', NULL, 'Phường 07', '10.7881559-106.6563945', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26989, 'Phường 05', NULL, 'Phường 05', '10.762479-106.669061', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26992, 'Phường 10', NULL, 'Phường 10', '10.7826359-106.645341', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26995, 'Phường 06', NULL, 'Phường 06', '10.7837883-106.6582557', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(26998, 'Phường 08', NULL, 'Phường 08', '10.7829523-106.651973', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27001, 'Phường 09', NULL, 'Phường 09', '10.7686228-106.683438', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27004, 'Phường 14', NULL, 'Phường 14', '10.7936922-106.6423935', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27007, 'Phường 15', NULL, 'Phường 15', '10.8225398-106.6379724', 'Phường', 766, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27010, 'Phường Tân Sơn Nhì', NULL, 'Phường Tân Sơn Nhì', '10.7997882-106.630604', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27013, 'Phường Tây Thạnh', NULL, 'Phường Tây Thạnh', '10.8122907-106.6261831', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27016, 'Phường Sơn Kỳ', NULL, 'Phường Sơn Kỳ', '10.8033223-106.615868', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27019, 'Phường Tân Quý', NULL, 'Phường Tân Quý', '10.794826-106.6217623', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27022, 'Phường Tân Thành', NULL, 'Phường Tân Thành', '10.7914545-106.6335513', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27025, 'Phường Phú Thọ Hòa', NULL, 'Phường Phú Thọ Hòa', '10.7863307-106.6276567', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27028, 'Phường Phú Thạnh', NULL, 'Phường Phú Thạnh', '10.7785654-106.6317792', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27031, 'Phường Phú Trung', NULL, 'Phường Phú Trung', '10.7773512-106.6423935', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27034, 'Phường Hòa Thạnh', NULL, 'Phường Hòa Thạnh', '10.7783152-106.6372355', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27037, 'Phường Hiệp Tân', NULL, 'Phường Hiệp Tân', '10.7727133-106.6276567', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27040, 'Phường Tân Thới Hòa', NULL, 'Phường Tân Thới Hòa', '10.76294-106.6320777', 'Phường', 767, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27043, 'Phường 04', NULL, 'Phường 04', '10.7959603-106.6885456', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27046, 'Phường 05', NULL, 'Phường 05', '10.7968936-106.6867575', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27049, 'Phường 09', NULL, 'Phường 09', '10.7969087-106.6868524', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27052, 'Phường 07', NULL, 'Phường 07', '10.7968859-106.6868428', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27055, 'Phường 03', NULL, 'Phường 03', '10.7967751-106.6866886', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27058, 'Phường 01', NULL, 'Phường 01', '10.7986464-106.6802854', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27061, 'Phường 02', NULL, 'Phường 02', '10.7959497-106.6885666', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27064, 'Phường 08', NULL, 'Phường 08', '10.7980758-106.6740811', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27067, 'Phường 15', NULL, 'Phường 15', '10.7950274-106.679977', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27070, 'Phường 10', NULL, 'Phường 10', '10.7955148-106.6711332', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27073, 'Phường 11', NULL, 'Phường 11', '10.7926289-106.6740811', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27076, 'Phường 17', NULL, 'Phường 17', '10.7935032-106.682925', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27079, 'Phường 14', NULL, 'Phường 14', '10.791592-106.6681854', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27082, 'Phường 12', NULL, 'Phường 12', '10.7923853-106.678503', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27085, 'Phường 13', NULL, 'Phường 13', '10.790068-106.6711332', 'Phường', 768, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27088, 'Phường Thảo Điền', NULL, 'Phường Thảo Điền', '10.8064331-106.7323097', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27091, 'Phường An Phú', NULL, 'Phường An Phú', '10.8019128-106.7647475', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27094, 'Phường Bình An', NULL, 'Phường Bình An', '10.7915388-106.7308354', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27097, 'Phường Bình Trưng Đông', NULL, 'Phường Bình Trưng Đông', '10.78204-106.7794935', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27100, 'Phường Bình Trưng Tây', NULL, 'Phường Bình Trưng Tây', '10.7844627-106.7603239', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27103, 'Phường Bình Khánh', NULL, 'Phường Bình Khánh', '10.7830453-106.7367328', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27106, 'Phường An Khánh', NULL, 'Phường An Khánh', '10.7814628-106.7160926', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27109, 'Phường Cát Lái', NULL, 'Phường Cát Lái', '10.7708268-106.7853922', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27112, 'Phường Thạnh Mỹ Lợi', NULL, 'Phường Thạnh Mỹ Lợi', '10.7583621-106.7647475', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27115, 'Phường An Lợi Đông', NULL, 'Phường An Lợi Đông', '10.7631993-106.7264125', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27118, 'Phường Thủ Thiêm', NULL, 'Phường Thủ Thiêm', '10.7732956-106.7160926', 'Phường', 769, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27121, 'Phường 08', NULL, 'Phường 08', '10.7712114-106.6857666', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27124, 'Phường 07', NULL, 'Phường 07', '10.7707505-106.6817368', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27127, 'Phường 14', NULL, 'Phường 14', '10.7895808-106.679977', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27130, 'Phường 12', NULL, 'Phường 12', '10.788544-106.6740811', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27133, 'Phường 11', NULL, 'Phường 11', '10.7860642-106.6696593', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27136, 'Phường 13', NULL, 'Phường 13', '10.7855773-106.678503', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27139, 'Phường 06', NULL, 'Phường 06', '10.7369775-106.6244505', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27142, 'Phường 09', NULL, 'Phường 09', '10.7686228-106.683438', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27145, 'Phường 10', NULL, 'Phường 10', '10.7812538-106.6766605', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27148, 'Phường 04', NULL, 'Phường 04', '10.7687584-106.6839393', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27151, 'Phường 05', NULL, 'Phường 05', '10.7694773-106.6832863', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27154, 'Phường 03', NULL, 'Phường 03', '10.770528-106.6862868', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27157, 'Phường 02', NULL, 'Phường 02', '10.771435-106.681099', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27160, 'Phường 01', NULL, 'Phường 01', '10.7682626-106.6839418', 'Phường', 770, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27163, 'Phường 15', NULL, 'Phường 15', '10.7815015-106.6659746', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27166, 'Phường 13', NULL, 'Phường 13', '10.7792565-106.6696593', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27169, 'Phường 14', NULL, 'Phường 14', '10.7709337-106.6600791', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27172, 'Phường 12', NULL, 'Phường 12', '10.7730084-106.6718702', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27175, 'Phường 11', NULL, 'Phường 11', '10.7736443-106.6788715', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27178, 'Phường 10', NULL, 'Phường 10', '10.7713857-106.6676482', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27181, 'Phường 09', NULL, 'Phường 09', '10.76271-106.669118', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27184, 'Phường 01', NULL, 'Phường 01', '10.762454-106.668944', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27187, 'Phường 08', NULL, 'Phường 08', '10.763171-106.669771', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27190, 'Phường 02', NULL, 'Phường 02', '10.76246-106.668971', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27193, 'Phường 04', NULL, 'Phường 04', '10.762472-106.669031', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27196, 'Phường 07', NULL, 'Phường 07', '10.762491-106.669121', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27199, 'Phường 05', NULL, 'Phường 05', '10.762479-106.669061', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27202, 'Phường 06', NULL, 'Phường 06', '10.7373084-106.6245599', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27205, 'Phường 03', NULL, 'Phường 03', '10.762466-106.669001', 'Phường', 771, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27208, 'Phường 15', NULL, 'Phường 15', '10.7761932-106.6548706', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27211, 'Phường 05', NULL, 'Phường 05', '10.762479-106.669061', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27214, 'Phường 14', NULL, 'Phường 14', '10.7694985-106.6490254', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27217, 'Phường 11', NULL, 'Phường 11', '10.7666947-106.6504992', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27220, 'Phường 03', NULL, 'Phường 03', '10.7633465-106.6588697', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27223, 'Phường 10', NULL, 'Phường 10', '10.7630148-106.6431304', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27226, 'Phường 13', NULL, 'Phường 13', '10.7638102-106.6534468', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27229, 'Phường 08', NULL, 'Phường 08', '10.7606898-106.6482886', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27232, 'Phường 09', NULL, 'Phường 09', '10.7686228-106.683438', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27235, 'Phường 12', NULL, 'Phường 12', '10.7611684-106.651973', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27238, 'Phường 07', NULL, 'Phường 07', '10.7605453-106.6596036', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27241, 'Phường 06', NULL, 'Phường 06', '10.7370991-106.6244483', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27244, 'Phường 04', NULL, 'Phường 04', '10.7687584-106.6839393', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27247, 'Phường 01', NULL, 'Phường 01', '10.7608932-106.650142', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27250, 'Phường 02', NULL, 'Phường 02', '10.7550394-106.6431074', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27253, 'Phường 16', NULL, 'Phường 16', '10.7552446-106.6482886', 'Phường', 772, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27256, 'Phường 12', NULL, 'Phường 12', '10.7664188-106.7050362', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27259, 'Phường 13', NULL, 'Phường 13', '10.762732-106.7101958', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27262, 'Phường 09', NULL, 'Phường 09', '10.7686228-106.683438', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27265, 'Phường 06', NULL, 'Phường 06', '10.7371963-106.624516', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27268, 'Phường 08', NULL, 'Phường 08', '10.763091-106.7009979', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27271, 'Phường 10', NULL, 'Phường 10', '10.760614-106.7054047', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27274, 'Phường 05', NULL, 'Phường 05', '10.762479-106.669061', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27277, 'Phường 18', NULL, 'Phường 18', '10.7596852-106.7160926', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27280, 'Phường 14', NULL, 'Phường 14', '10.7580903-106.7079845', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27283, 'Phường 04', NULL, 'Phường 04', '10.7687584-106.6839393', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27286, 'Phường 03', NULL, 'Phường 03', '10.762466-106.669001', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27289, 'Phường 16', NULL, 'Phường 16', '10.755125-106.7124071', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27292, 'Phường 02', NULL, 'Phường 02', '10.76246-106.668971', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27295, 'Phường 15', NULL, 'Phường 15', '10.7540075-106.7079845', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27298, 'Phường 01', NULL, 'Phường 01', '10.7550849-106.689444', 'Phường', 773, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27301, 'Phường 04', NULL, 'Phường 04', '10.7510929-106.6694669', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27304, 'Phường 09', NULL, 'Phường 09', '10.7553344-106.6692256', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27307, 'Phường 03', NULL, 'Phường 03', '10.7595352-106.6693064', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27310, 'Phường 12', NULL, 'Phường 12', '10.7564453-106.6601602', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27313, 'Phường 02', NULL, 'Phường 02', '10.7562001-106.6692136', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27316, 'Phường 08', NULL, 'Phường 08', '10.7512425-106.6695266', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27319, 'Phường 15', NULL, 'Phường 15', '10.7556424-106.6534468', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27322, 'Phường 07', NULL, 'Phường 07', '10.7548791-106.6690891', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27325, 'Phường 01', NULL, 'Phường 01', '10.7549031-106.6692026', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27328, 'Phường 11', NULL, 'Phường 11', '10.7537961-106.6622899', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27331, 'Phường 14', NULL, 'Phường 14', '10.7528392-106.6549206', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27334, 'Phường 05', NULL, 'Phường 05', '10.7560917-106.6692095', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27337, 'Phường 06', NULL, 'Phường 06', '10.7370125-106.6244381', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27340, 'Phường 10', NULL, 'Phường 10', '10.7510738-106.6622899', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27343, 'Phường 13', NULL, 'Phường 13', '10.750036-106.6563945', 'Phường', 774, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27346, 'Phường 14', NULL, 'Phường 14', '10.7577874-106.6322597', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27349, 'Phường 13', NULL, 'Phường 13', '10.7536523-106.6276567', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27352, 'Phường 09', NULL, 'Phường 09', '10.7370693-106.6245142', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27355, 'Phường 06', NULL, 'Phường 06', '10.7372485-106.62456', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27358, 'Phường 12', NULL, 'Phường 12', '10.7506069-106.6335513', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27361, 'Phường 05', NULL, 'Phường 05', '10.7370125-106.6244381', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27364, 'Phường 11', NULL, 'Phường 11', '10.745162-106.6335513', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27367, 'Phường 02', NULL, 'Phường 02', '10.7371428-106.6245605', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27370, 'Phường 01', NULL, 'Phường 01', '10.783026-106.604884', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27373, 'Phường 04', NULL, 'Phường 04', '10.7371963-106.624516', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27376, 'Phường 08', NULL, 'Phường 08', '10.742838-106.6387092', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27379, 'Phường 03', NULL, 'Phường 03', '10.7369775-106.6244505', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27382, 'Phường 07', NULL, 'Phường 07', '10.8028126-106.6865481', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27385, 'Phường 10', NULL, 'Phường 10', '10.7403696-106.6238525', 'Phường', 775, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27388, 'Phường 08', NULL, 'Phường 08', '10.7167454-106.6289524', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27391, 'Phường 02', NULL, 'Phường 02', '10.737862-106.6662989', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27394, 'Phường 01', NULL, 'Phường 01', '10.749525-106.688451', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27397, 'Phường 03', NULL, 'Phường 03', '10.762466-106.669001', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27400, 'Phường 11', NULL, 'Phường 11', '10.7482708-106.6637637', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27403, 'Phường 09', NULL, 'Phường 09', '10.7165703-106.6288809', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27406, 'Phường 10', NULL, 'Phường 10', '10.7455487-106.6637637', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27409, 'Phường 04', NULL, 'Phường 04', '10.737862-106.6662989', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27412, 'Phường 13', NULL, 'Phường 13', '10.7459527-106.6563945', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27415, 'Phường 12', NULL, 'Phường 12', '10.7432306-106.6563945', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27418, 'Phường 05', NULL, 'Phường 05', '10.762479-106.669061', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27421, 'Phường 14', NULL, 'Phường 14', '10.7396318-106.6475517', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27424, 'Phường 06', NULL, 'Phường 06', '10.7372791-106.6245172', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27427, 'Phường 15', NULL, 'Phường 15', '10.7257399-106.6332948', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27430, 'Phường 16', NULL, 'Phường 16', '10.7251488-106.6261831', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27433, 'Phường 07', NULL, 'Phường 07', '10.7046969-106.6210379', 'Phường', 776, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27436, 'Phường Bình Hưng Hòa', NULL, 'Phường Bình Hưng Hòa', '10.8026884-106.6026064', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27439, 'Phường Bình Hưng Hoà A', NULL, 'Phường Bình Hưng Hoà A', '10.7850966-106.6069312', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27442, 'Phường Bình Hưng Hoà B', NULL, 'Phường Bình Hưng Hoà B', '10.8033353-106.590819', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27445, 'Phường Bình Trị Đông', NULL, 'Phường Bình Trị Đông', '10.7639086-106.6143944', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27448, 'Phường Bình Trị Đông A', NULL, 'Phường Bình Trị Đông A', '10.7703231-106.5967126', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27451, 'Phường Bình Trị Đông B', NULL, 'Phường Bình Trị Đông B', '10.7478932-106.6085003', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27454, 'Phường Tân Tạo', NULL, 'Phường Tân Tạo', '10.7597516-106.590819', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27457, 'Phường Tân Tạo A', NULL, 'Phường Tân Tạo A', '10.7360575-106.5906249', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27460, 'Phường  An Lạc', NULL, 'Phường  An Lạc', '10.7231349-106.6111242', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27463, 'Phường An Lạc A', NULL, 'Phường An Lạc A', '10.7539749-106.6217623', 'Phường', 777, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27466, 'Phường Tân Thuận Đông', NULL, 'Phường Tân Thuận Đông', '10.7571056-106.7382072', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27469, 'Phường Tân Thuận Tây', NULL, 'Phường Tân Thuận Tây', '10.7511954-106.7219896', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27472, 'Phường Tân Kiểng', NULL, 'Phường Tân Kiểng', '10.7491228-106.7101958', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27475, 'Phường Tân Hưng', NULL, 'Phường Tân Hưng', '10.7430482-106.6969285', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27478, 'Phường Bình Thuận', NULL, 'Phường Bình Thuận', '10.7417516-106.7205154', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27481, 'Phường Tân Quy', NULL, 'Phường Tân Quy', '10.7409584-106.7101958', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27484, 'Phường Phú Thuận', NULL, 'Phường Phú Thuận', '10.7299898-106.7426687', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27487, 'Phường Tân Phú', NULL, 'Phường Tân Phú', '10.7251014-106.7264125', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27490, 'Phường Tân Phong', NULL, 'Phường Tân Phong', '10.7318388-106.702825', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27493, 'Phường Phú Mỹ', NULL, 'Phường Phú Mỹ', '10.7081313-106.7382072', 'Phường', 778, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27496, 'Thị trấn Củ Chi', NULL, 'Thị trấn Củ Chi', '10.972192-106.4965434', 'Thị trấn', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27499, 'Xã Phú Mỹ Hưng', NULL, 'Xã Phú Mỹ Hưng', '11.1246502-106.458256', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27502, 'Xã An Phú', NULL, 'Xã An Phú', '11.1168711-106.4994889', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27505, 'Xã Trung Lập Thượng', NULL, 'Xã Trung Lập Thượng', '11.0603258-106.4346979', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27508, 'Xã An Nhơn Tây', NULL, 'Xã An Nhơn Tây', '11.074436-106.4759262', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27511, 'Xã Nhuận Đức', NULL, 'Xã Nhuận Đức', '11.0461163-106.493598', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27514, 'Xã Phạm Văn Cội', NULL, 'Xã Phạm Văn Cội', '11.0338732-106.5171626', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27517, 'Xã Phú Hòa Đông', NULL, 'Xã Phú Hòa Đông', '11.0203175-106.5642998', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27520, 'Xã Trung Lập Hạ', NULL, 'Xã Trung Lập Hạ', '11.026217-106.458256', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27523, 'Xã Trung An', NULL, 'Xã Trung An', '11.0048859-106.5893129', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27526, 'Xã Phước Thạnh', NULL, 'Xã Phước Thạnh', '11.011451-106.4288088', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27529, 'Xã Phước Hiệp', NULL, 'Xã Phước Hiệp', '10.9831531-106.4464766', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27532, 'Xã Tân An Hội', NULL, 'Xã Tân An Hội', '10.9593528-106.4818167', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27535, 'Xã Phước Vĩnh An', NULL, 'Xã Phước Vĩnh An', '10.984377-106.5230542', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27538, 'Xã Thái Mỹ', NULL, 'Xã Thái Mỹ', '10.990917-106.409935', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27541, 'Xã Tân Thạnh Tây', NULL, 'Xã Tân Thạnh Tây', '10.9875463-106.5642998', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27544, 'Xã Hòa Phú', NULL, 'Xã Hòa Phú', '10.9765654-106.6143944', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27547, 'Xã Tân Thạnh Đông', NULL, 'Xã Tân Thạnh Đông', '10.9586085-106.5937658', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27550, 'Xã Bình Mỹ', NULL, 'Xã Bình Mỹ', '10.9399403-106.635025', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27553, 'Xã Tân Phú Trung', NULL, 'Xã Tân Phú Trung', '10.9389375-106.5411065', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27556, 'Xã Tân Thông Hội', NULL, 'Xã Tân Thông Hội', '10.9555622-106.5133215', 'Xã', 783, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27559, 'Thị trấn Hóc Môn', NULL, 'Thị trấn Hóc Môn', '10.8863934-106.5922924', 'Thị trấn', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27562, 'Xã Tân Hiệp', NULL, 'Xã Tân Hiệp', '10.9084099-106.5912473', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27565, 'Xã Nhị Bình', NULL, 'Xã Nhị Bình', '10.9132585-106.6733441', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27568, 'Xã Đông Thạnh', NULL, 'Xã Đông Thạnh', '10.9065528-106.6468148', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27571, 'Xã Tân Thới Nhì', NULL, 'Xã Tân Thới Nhì', '10.8919577-106.5714148', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27574, 'Xã Thới Tam Thôn', NULL, 'Xã Thới Tam Thôn', '10.8976059-106.6114474', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27577, 'Xã Xuân Thới Sơn', NULL, 'Xã Xuân Thới Sơn', '10.8790427-106.5525145', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27580, 'Xã Tân Xuân', NULL, 'Xã Tân Xuân', '10.8684249-106.5967126', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27583, 'Xã Xuân Thới Đông', NULL, 'Xã Xuân Thới Đông', '10.8687479-106.5890718', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27586, 'Xã Trung Chánh', NULL, 'Xã Trung Chánh', '10.8692992-106.6148213', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27589, 'Xã Xuân Thới Thượng', NULL, 'Xã Xuân Thới Thượng', '10.8565784-106.5642998', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27592, 'Xã Bà Điểm', NULL, 'Xã Bà Điểm', '10.8411638-106.5967126', 'Xã', 784, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27595, 'Thị trấn Tân Túc', NULL, 'Thị trấn Tân Túc', '10.6844901-106.5731392', 'Thị trấn', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27598, 'Xã Phạm Văn Hai', NULL, 'Xã Phạm Văn Hai', '10.8148999-106.528946', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27601, 'Xã Vĩnh Lộc A', NULL, 'Xã Vĩnh Lộc A', '10.8238657-106.5642998', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27604, 'Xã Vĩnh Lộc B', NULL, 'Xã Vĩnh Lộc B', '10.7911649-106.5642998', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27607, 'Xã Bình Lợi', NULL, 'Xã Bình Lợi', '10.7756348-106.5096239', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27610, 'Xã Lê Minh Xuân', NULL, 'Xã Lê Minh Xuân', '10.7661714-106.5230542', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27613, 'Xã Tân Nhựt', NULL, 'Xã Tân Nhựt', '10.7155493-106.5525145', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27616, 'Xã Tân Kiên', NULL, 'Xã Tân Kiên', '10.716207-106.5848072', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27619, 'Xã Bình Hưng', NULL, 'Xã Bình Hưng', '10.7200104-106.6703963', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27622, 'Xã Phong Phú', NULL, 'Xã Phong Phú', '10.6995307-106.6468148', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27625, 'Xã An Phú Tây', NULL, 'Xã An Phú Tây', '10.6880126-106.6085003', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27628, 'Xã Hưng Long', NULL, 'Xã Hưng Long', '10.6681681-106.6232359', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27631, 'Xã Đa Phước', NULL, 'Xã Đa Phước', '10.666245-106.6586052', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27634, 'Xã Tân Quý Tây', NULL, 'Xã Tân Quý Tây', '10.666887-106.5967126', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27637, 'Xã Bình Chánh', NULL, 'Xã Bình Chánh', '10.6630417-106.5672462', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27640, 'Xã Quy Đức', NULL, 'Xã Quy Đức', '10.6425712-106.6438673', 'Xã', 785, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27643, 'Thị trấn Nhà Bè', NULL, 'Thị trấn Nhà Bè', '10.6943704-106.7411559', 'Thị trấn', 786, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27646, 'Xã Phước Kiển', NULL, 'Xã Phước Kiển', '10.70719-106.7057733', 'Xã', 786, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27649, 'Xã Phước Lộc', NULL, 'Xã Phước Lộc', '10.7001584-106.685136', 'Xã', 786, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27652, 'Xã Nhơn Đức', NULL, 'Xã Nhơn Đức', '10.6751953-106.6939803', 'Xã', 786, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27655, 'Xã Phú Xuân', NULL, 'Xã Phú Xuân', '10.6748507-106.7500025', 'Xã', 786, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27658, 'Xã Long Thới', NULL, 'Xã Long Thới', '10.6515117-106.7293611', 'Xã', 786, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27661, 'Xã Hiệp Phước', NULL, 'Xã Hiệp Phước', '10.6009892-106.7588494', 'Xã', 786, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27664, 'Thị trấn Cần Thạnh', NULL, 'Thị trấn Cần Thạnh', '10.4155124-106.9731189', 'Thị trấn', 787, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27667, 'Xã Bình Khánh', NULL, 'Xã Bình Khánh', '10.6431737-106.7824429', 'Xã', 787, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27670, 'Xã Tam Thôn Hiệp', NULL, 'Xã Tam Thôn Hiệp', '10.6037491-106.8595671', 'Xã', 787, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27673, 'Xã An Thới Đông', NULL, 'Xã An Thới Đông', '10.5549722-106.8060388', 'Xã', 787, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27676, 'Xã Thạnh An', NULL, 'Xã Thạnh An', '10.5459417-106.9712791', 'Xã', 787, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27679, 'Xã Long Hòa', NULL, 'Xã Long Hòa', '10.4630405-106.9004472', 'Xã', 787, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL),
(27682, 'Xã Lý Nhơn', NULL, 'Xã Lý Nhơn', '10.468149-106.8060388', 'Xã', 787, NULL, '2018-10-22 23:42:17', '2018-10-22 23:42:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lck_notification`
--

CREATE TABLE `lck_notification` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `chanel` tinyint(1) DEFAULT '1' COMMENT '1: broadcast, 2: private',
  `type` tinyint(1) DEFAULT '1' COMMENT '1: common, \r\n2: message, \r\n3: shared, \r\n4: call request, \r\n5: reward  point, \r\n6: mission, \r\n7: new product, \r\n8: product update,\r\n9: like product',
  `relate_id` int(11) DEFAULT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `extra` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Thong bao';

-- --------------------------------------------------------

--
-- Table structure for table `lck_notification_read`
--

CREATE TABLE `lck_notification_read` (
  `id` int(11) NOT NULL,
  `notification_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Thong bao da doc';

-- --------------------------------------------------------

--
-- Table structure for table `lck_orders`
--

CREATE TABLE `lck_orders` (
  `id` int(11) NOT NULL,
  `order_code` varchar(255) DEFAULT NULL,
  `total_product_price` int(11) DEFAULT '0',
  `discount_amount` int(11) DEFAULT '0',
  `discount_percent` int(11) DEFAULT '0',
  `discount_code` varchar(255) DEFAULT NULL,
  `shipping_fee` int(11) DEFAULT '0',
  `free_shipping` tinyint(1) DEFAULT '0',
  `payment_method` tinyint(1) DEFAULT '0',
  `status_payment` tinyint(1) DEFAULT '0',
  `shipping_method` tinyint(1) DEFAULT '0',
  `shipping_code` varchar(255) DEFAULT NULL,
  `total_weight` int(11) DEFAULT '0',
  `shipping_name` varchar(255) DEFAULT NULL,
  `shipping_phone` varchar(255) DEFAULT NULL,
  `shipping_province_id` int(11) DEFAULT NULL,
  `shipping_district_id` int(11) DEFAULT NULL,
  `shipping_ward_id` int(11) DEFAULT NULL,
  `shipping_street_name` varchar(255) DEFAULT NULL,
  `shipping_address` varchar(255) DEFAULT NULL,
  `warehouse_name` varchar(255) DEFAULT NULL,
  `warehouse_phone` varchar(255) DEFAULT NULL,
  `warehouse_province_id` int(11) DEFAULT NULL,
  `warehouse_district_id` int(11) DEFAULT NULL,
  `warehouse_ward_id` int(11) DEFAULT NULL,
  `warehouse_street_name` varchar(255) DEFAULT NULL,
  `warehouse_address` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `cancel_reason` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_orders`
--

INSERT INTO `lck_orders` (`id`, `order_code`, `total_product_price`, `discount_amount`, `discount_percent`, `discount_code`, `shipping_fee`, `free_shipping`, `payment_method`, `status_payment`, `shipping_method`, `shipping_code`, `total_weight`, `shipping_name`, `shipping_phone`, `shipping_province_id`, `shipping_district_id`, `shipping_ward_id`, `shipping_street_name`, `shipping_address`, `warehouse_name`, `warehouse_phone`, `warehouse_province_id`, `warehouse_district_id`, `warehouse_ward_id`, `warehouse_street_name`, `warehouse_address`, `status`, `total_price`, `cancel_reason`, `note`, `user_id`, `store_id`, `created_at`, `updated_at`) VALUES
(1, '50849d190eda0ae03239fc5425a4e72b', 300000, 0, 0, '0', 0, 0, 1, 0, 1, '', 0, 'Bap Cui', '84979427220', 79, 760, 26737, '61 Lê Quý Đôn', '61 Lê Quý Đôn, Phường Đa Kao, Quận 1, Thành phố Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 300000, NULL, 'string', 3, 3, '2019-04-11 16:10:09', '2019-04-11 16:10:09'),
(2, '4fda21856befc171820df824ec4e48fa', 300000, 0, 0, '0', 0, 0, 1, 0, 1, '', 0, 'Bap Cui', '84979427220', 79, 760, 26737, '61 Lê Quý Đôn', '61 Lê Quý Đôn, Phường Đa Kao, Quận 1, Thành phố Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 300000, NULL, 'string', 3, 3, '2019-04-11 17:25:56', '2019-04-11 17:25:56'),
(3, 'bd97a2e127ecbc69c32da448baa41eb8', 1575000, 0, 0, '0', 0, 0, NULL, 0, 1, '', 84705464, 'Nguyen Thanh Luan', '0979427220', 79, 766, 26968, '4/3A Đồ Sơn', '4/3A Đồ Sơn, Phường 04, Quận Tân Bình, Thành phố Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1575000, NULL, 'Gói hàng cẩn thận', 3, 3, '2019-05-03 15:00:04', '2019-05-03 15:00:04'),
(4, 'd2ca3e3ca93207a8d8875528de60f562', 130000, 0, 0, '0', 0, 0, NULL, 0, 1, '', 0, 'Nguyen Thanh Luan', '0979427220', 79, 766, 26968, '4/3A Đồ Sơn', '4/3A Đồ Sơn, Phường 04, Quận Tân Bình, Thành phố Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 130000, NULL, 'Gói hàng cẩn thận hi', 3, 5, '2019-05-03 15:00:04', '2019-05-03 15:00:04'),
(5, '576d94f55b203d69e3d7007ef9816eb1', 2500000, 0, 0, '0', 0, 0, NULL, 0, 1, '', 423524330, 'Nguyen Thanh Luan', '0979427220', 79, 766, 26968, '4/3A Đồ Sơn', '4/3A Đồ Sơn, Phường 04, Quận Tân Bình, Thành phố Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2500000, NULL, NULL, 3, 3, '2019-05-03 15:06:12', '2019-05-03 15:06:12'),
(6, 'fb3e8270816239957c8ceed497200fbe', 500000, 0, 0, '0', 0, 0, NULL, 0, 1, '', 100, 'Nguyen Thanh Luan', '0979427220', 79, 766, 26968, '4/3A Đồ Sơn', '4/3A Đồ Sơn, Phường 04, Quận Tân Bình, Thành phố Hồ Chí Minh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 500000, NULL, NULL, 3, 3, '2019-05-03 22:18:09', '2019-05-03 22:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `lck_orders_detail`
--

CREATE TABLE `lck_orders_detail` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `thumbnail_file_path` varchar(255) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `variation_name` varchar(255) DEFAULT NULL,
  `price_old` int(11) DEFAULT '0',
  `price` int(11) DEFAULT '0',
  `discount_amount` int(11) DEFAULT '0',
  `quantity` int(11) DEFAULT '0',
  `sku` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_orders_detail`
--

INSERT INTO `lck_orders_detail` (`id`, `order_id`, `product_id`, `product_name`, `thumbnail_file_path`, `category_name`, `variation_name`, `price_old`, `price`, `discount_amount`, `quantity`, `sku`, `weight`) VALUES
(1, 1, 26, 'Test cập nhật sản phẩm', '2019/at2.jpg', NULL, 'Màu Xanh - Size M', 50000, 50000, 0, 5, 'string', 0),
(2, 1, 1, 'Áo thun nữ 01', '2019/at1.jpg', NULL, NULL, 0, 50000, 0, 1, NULL, 0),
(3, 2, 26, 'Test cập nhật sản phẩm', '2019/at2.jpg', NULL, 'Màu Xanh - Size M', 50000, 50000, 0, 5, 'string', 0),
(4, 2, 1, 'Áo thun nữ 01', '2019/at1.jpg', NULL, NULL, 0, 50000, 0, 1, NULL, 0),
(5, 3, 29, 'dfgsdagssfgfdhfdsh', '2019/04/29/d63f13ee31846cfd61df50688a8fad73.jpg', NULL, '0', 0, 250000, 0, 2, 'ẻwewet', 42352433),
(6, 3, 1, 'Áo thun nữ 01', '2019/at1.jpg', NULL, '0', 0, 50000, 0, 1, NULL, 0),
(7, 3, 26, 'Test cập nhật sản phẩm', '2019/at2.jpg', NULL, '0', 0, 500000, 0, 1, 'string', 0),
(8, 3, 27, 'Test cập nhật sản phẩm', '2019/at2.jpg', NULL, '0', 0, 500000, 0, 1, 'string', 100),
(9, 3, 28, 'Quần Kaki Nam', '2019/04/29/98eae44630c3732a72839d08e67e0003.jpg', NULL, '0', 0, 25000, 0, 1, '1230', 498),
(10, 4, 2, 'Áo thun nữ 02', '2019/at1.jpg', NULL, '0', 0, 60000, 0, 1, NULL, 0),
(11, 4, 3, 'Áo thun nữ 03', '2019/at1.jpg', NULL, '0', 0, 70000, 0, 1, NULL, 0),
(12, 5, 29, 'dfgsdagssfgfdhfdsh', '2019/04/29/d63f13ee31846cfd61df50688a8fad73.jpg', NULL, '0', 0, 250000, 0, 10, 'ẻwewet', 42352433),
(13, 6, 27, 'Test cập nhật sản phẩm', '2019/at2.jpg', NULL, '0', 0, 500000, 0, 1, 'string', 100);

-- --------------------------------------------------------

--
-- Table structure for table `lck_payment_method`
--

CREATE TABLE `lck_payment_method` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `api_url` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_payment_method`
--

INSERT INTO `lck_payment_method` (`id`, `code`, `name`, `icon`, `api_key`, `api_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'visa_master_card', 'VISA/Master Card', NULL, NULL, NULL, '1', '2019-04-11 15:19:13', '2019-04-11 15:19:15'),
(2, 'paypal', 'Paypal', NULL, NULL, NULL, '1', '2019-04-11 15:19:13', '2019-04-11 15:19:15');

-- --------------------------------------------------------

--
-- Table structure for table `lck_post`
--

CREATE TABLE `lck_post` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `excerpt` text,
  `detail` longtext,
  `image` varchar(255) DEFAULT NULL,
  `image_extra` varchar(0) DEFAULT NULL,
  `image_fb` varchar(0) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  `video_url` varchar(255) DEFAULT NULL,
  `custom_link` text,
  `tags` text,
  `sticky` tinyint(4) DEFAULT '0',
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_descriptions` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_post`
--

INSERT INTO `lck_post` (`id`, `name`, `slug`, `excerpt`, `detail`, `image`, `image_extra`, `image_fb`, `user_id`, `views`, `status`, `video_url`, `custom_link`, `tags`, `sticky`, `seo_title`, `seo_descriptions`, `seo_keywords`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Loremis pum', 'loremis-pum', 'Loremis pum', '<p>Loremis pum</p>', NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, 0, 'Loremis pum', 'Loremis pum', 'Loremis pum', 2, '2019-01-15 15:40:26', '2019-01-18 16:26:20'),
(2, 'Nội Quy & Điều Khoản', 'noi-quy--dieu-khoan', NULL, '<p align=\"center\"><strong>NỘI QUY &amp; QUY ĐỊNH SỬ DỤNG ỨNG DỤNG &amp;</strong></p>\r\n\r\n<p align=\"center\"><strong>WEBSITE CỦA SÀN THƯƠNG MẠI ĐIỆN TỬ GOLDEN RIVER</strong></p>\r\n\r\n<ul>\r\n	<li>Căn cứ Bộ Luật Dân sự của nước Cộng hòa xã hội chủ nghĩa Việt Nam được Quốc hội khoá 11 thông qua ngày 14/6/2005.</li>\r\n	<li>Căn cứ Luật Thương mại số 36/2005/QH11 của nước Cộng hòa xã hội chủ nghĩa Việt Nam được Quốc hội khoá 11 thông qua ngày 14/6/2005.&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n	<li>Căn cứ khả năng và nhu cầu của hai bên (được gọi là bên cung cấp dịch vụ &amp; Bên sử dụng dịch vụ), bên cung cấp dịch vụ gọi tắt là chúng tôi xin phép đưa ra nội quy &amp; điều khoản sử dụng ứng dụng &amp; website thương mại điện tử Golden River thay thế cho hợp đồng sử dụng dịch vụ &amp; có giá trị pháp lý tương đương với hợp đồng. Nội quy &amp; điều khoản bao gồm:</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li><strong>Sứ Mệnh của chúng tôi:</strong></li>\r\n</ol>\r\n\r\n<p>Dù là “đứa con sinh sau” đẻ muộn so với các ứng dụng &amp; website thương mại điện tử khác, chúng tôi cam kết sẽ đem đến dịch vụ tốt nhất, cộng động mua &amp; bán trực tuyến lành mạnh &amp; an toàn, hạn chế rủi ro đến mức tối thiểu cho Bên sử dụng dịch vụ của chúng tôi.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li value=\"2\"><strong>Dịch Vụ của chúng tôi:</strong></li>\r\n</ol>\r\n\r\n<p>Để đảm bảo cung ứng cho quý khách sự trải nghiệm tốt nhất, chúng tôi đã chuẩn bị &amp; đem lại những dịch vụ cho quý khách bao gồm:</p>\r\n\r\n<ol>\r\n	<li value=\"2.1\">Dịch vụ mua hàng được bảo đảm trên ứng dụng &amp; website thương mại điện tử.</li>\r\n	<li value=\"2.2\">Dịch vụ đăng bán hàng được bảo đảm trên ứng dụng &amp; website thương mại điện tử và có thu phí.</li>\r\n	<li value=\"2.3\">Dịch vụ quảng cáo miễn phí thông qua các kênh quảng cáo mà chúng tôi cung cấp.</li>\r\n	<li value=\"2.4\">Dịch vụ kiểm soát hàng tồn.</li>\r\n	<li value=\"2.5\">Dịch vụ mua &amp; bán toàn cầu.</li>\r\n	<li value=\"2.6\">Dịch vụ hỗ trợ phí giao hàng tận nơi, hỗ trợ phí chuyển khoản</li>\r\n	<li value=\"2.7\">Đem lại các chương trình khuyến mãi ưu đãi cho người dùng.</li>\r\n</ol>\r\n\r\n<p style=\"margin-left:2.0cm;\">&nbsp;</p>\r\n\r\n<ol>\r\n	<li value=\"3\"><strong>Nội quy của Bên sử dụng dịch vụ</strong></li>\r\n</ol>\r\n\r\n<p><strong>3.1 Bên sử dụng dịch vụ là Bên Bán</strong></p>\r\n\r\n<p>3.1.1 Bên sử dụng được quyền đăng ký &amp; đăng nhập để sử dụng dịch vụ.</p>\r\n\r\n<p>3.1.2 Bên sử dụng đảm bảo đã đọc kỹ các nội dung &amp; điều khoản trước khi sử dụng dịch vụ mua &amp; bán trên ứng dụng &amp; website.</p>\r\n\r\n<p>3.1.3 Bên sử dụng được đăng bán sản phẩm trên sàn thương mại điện tử bao gồm hình ảnh sản phẩm, các thông tin về sản phẩm và các thông tin được đăng lên phải theo qui định của chúng tôi &amp; của pháp luật Việt Nam.</p>\r\n\r\n<p>3.1.4 Bên sử dụng được quyền cung cấp và hình ảnh cho chúng tôi để được hỗ trợ quảng cáo trên các kênh của chúng tôi phù hợp với qui định của chúng tôi &amp; pháp luật Việt Nam.</p>\r\n\r\n<p>3.1.5 Bên sử dụng được quyền truy cập vào các kênh quảng cáo của chúng tôi để cập nhật thông tin &amp; được quyền yêu cầu chúng tôi chỉnh sửa cho phù hợp.</p>\r\n\r\n<p>3.1.6 Bên sử dụng sẽ bị từ chối vĩnh viễn đăng bán trên sàn thương mại điện tử của chúng tôi vì có thái độ không tốt với khách hàng; bán sản phẩm không đúng với mô tả; cố tình gây hư hại cho sàn thương mại điện tử của chúng tôi.</p>\r\n\r\n<p>3.1.7 Bên sử dụng rút tiền bán nhiều lần thông qua dịch vụ của chúng tôi.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li value=\"3.2\"><strong>Bên sử dụng dịch vụ là Bên Mua</strong></li>\r\n</ol>\r\n\r\n<p>3.2.1 Bên sử dụng được quyền đăng ký &amp; đăng nhập để sử dụng dịch vụ.</p>\r\n\r\n<p>3.2.2 Bên sử dụng đảm bảo đã đọc kỹ các nội dung &amp; điều khoản trước khi sử dụng dịch vụ mua &amp; bán trên ứng dụng &amp; website.</p>\r\n\r\n<p>3.2.3 Bên sử dụng dịch vụ được mua hàng không giới hạn các sản phẩm của chúng tôi.</p>\r\n\r\n<p>3.2.4 Bên sử dụng sẽ bị từ chối vĩnh viễn trên sàn thương mại điện tử của chúng tôi vì có thái độ không tốt; cố tình gây hư hại cho sàn thương mại điện tử của chúng tôi; phản ánh không đúng sự thật các thông tin sản phẩm đã mua; cung cấp thông tin không đúng sự thật gây ảnh hưởng tiêu cực đến sàn thương điện tử.</p>\r\n\r\n<p>3.2.5 Bên sử dụng được quyền phản ánh thông tin chính xác đến chúng tôi để được đảm bảo quyền lợi Bên mua.</p>\r\n\r\n<p>3.2.6 Bên mua được yêu cầu trả hàng/ hoàn tiền nếu hàng nhận được bị bể, vỡ hay không đúng với thông tin sản phẩm Bên bán đã đăng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li value=\"4\"><strong>Biểu Phí &amp; Hóa đơn được áp dụng cho Bên Bán</strong>\r\n\r\n	<ol>\r\n		<li value=\"4.1\">Phí sử dụng dịch vụ 15% sẽ được cấn trừ vào tiền bán hàng hay còn gọi là doanh thu phát sinh.</li>\r\n		<li value=\"4.2\">Phí sẽ được thu vào mỗi lần phát sinh giao dịch thành công.</li>\r\n	</ol>\r\n	</li>\r\n</ol>\r\n\r\n<p>4.2 Phí đã bao gồm phí quảng cáo, phí quản lý, phí sử dụng dịch vụ thẻ nội địa &amp; quốc tế.</p>\r\n\r\n<p>4.3 Khi nhận được yêu cầu rút tiền từ Bên bán, doanh thu phát sinh sau khi cấn trừ phí sẽ được chuyển khoản cho Bên bán từ 3 đến 5 ngày làm việc (trừ thứ 7 &amp; Chủ Nhật).</p>\r\n\r\n<p>4.4 Hóa đơn điện tử tháng này sẽ xuất trong vòng 15 ngày của tháng sau (từ ngày 1 đến ngày 15 và trừ thứ 7 &amp; Chủ Nhật, ngày lễ, Tết, ngày nghỉ theo qui định của pháp luật Việt Nam) sau khi được đối chiếu giữa hai bên và thống nhất sẽ chuyển qua hệ thống nếu cùng hệ thống hoặc mail được chỉ định từ Bên bán.</p>\r\n\r\n<p>4.5 Hóa đơn xuất chưa đúng sẽ được thu hồi &amp; điều chỉnh trong vòng 15 ngày kế tiếp (từ ngày 15 đến ngày 30 và trừ thứ 7 &amp; Chủ Nhật, ngày lễ, Tết, ngày nghỉ theo qui định của pháp luật Việt Nam) cùng tháng đã xuất hóa đơn, sau khi đã đối chiếu &amp; xác nhận của các bên.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li value=\"5\"><strong>Chính sách quy đổi lúa được áp dụng cho Bên Mua</strong></li>\r\n</ol>\r\n\r\n<p>5.1 Nhằm để tri ân quý khách hàng đã mua hàng không ngừng nghỉ trên sàn Golden River, chúng tôi xin phép được giới thiệu chính sách qui đổi từ giá trị tiền thật sang lúa và được cấn trừ vào lần mua hàng kế tiếp.</p>\r\n\r\n<p>5.2 Chính sách quy đổi</p>\r\n\r\n<p>Cứ mỗi 10.000đ đổi được 1 lúa và có giá trị tiền Việt Nam đồng dùng để cấn trừ vẫn là 10.000đ</p>\r\n\r\n<p>5.3 Chính sách nhận lúa bất ngờ, tùy theo chương trình từng thời điểm của chúng tôi. Chúng tôi sẽ tặng lúa bất ngờ vào cuối tháng cho quý khách mua hàng thường xuyên.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li value=\"6\"><strong>Điều khoản chung</strong>\r\n\r\n	<ol>\r\n		<li value=\"6.1\">Hai bên cam kết hỗ trợ và phối hợp chặt chẽ trong việc thực hiện và hoàn tất các công việc quy định &amp; điều khoản được đề cập này.</li>\r\n	</ol>\r\n	</li>\r\n</ol>\r\n\r\n<p>6.2 Trong thời gian sử dụng dịch vụ, những vấn đề phát sinh được riêng cũng mỗi người sử dụng sẽ được thỏa thuận văn bản riêng (nếu có) sau khi đã có sự thống nhất &amp; xác nhận của các bên.</p>\r\n\r\n<p>6.3 Trong quá trình thực hiện nếu có vướng mắc, hai bên sẽ tích cực hợp tác thương lượng để giải quyết; trường hợp không thống nhất được sàn thương mại điện tử Golden River sẽ là người có quyền quyết định cuối cùng.</p>\r\n\r\n<p>6.4 Mọi tranh chấp dẫn đến phát sinh và gây ra kiện tụng sẽ được thực hiện dựa trên luật pháp Việt Nam &amp; sẽ thi hành ở tòa án ở thành phố Hồ Chí Minh.</p>', NULL, NULL, NULL, 2, 0, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 2, '2019-05-11 09:46:49', '2019-05-11 09:46:49');

-- --------------------------------------------------------

--
-- Table structure for table `lck_post_category`
--

CREATE TABLE `lck_post_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `parent_id` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `priority` tinyint(4) DEFAULT '0',
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_descriptions` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_post_category`
--

INSERT INTO `lck_post_category` (`id`, `name`, `slug`, `description`, `parent_id`, `status`, `priority`, `seo_title`, `seo_descriptions`, `seo_keywords`, `created_at`, `updated_at`, `user_id`) VALUES
(2, 'Tin tức', 'tin-tuc', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2019-01-15 15:36:03', '2019-01-18 16:30:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lck_product`
--

CREATE TABLE `lck_product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'tên sản phẩm',
  `description` text COMMENT 'mô tả sản phẩm',
  `detail` text COMMENT 'chi tiết sản phẩm',
  `price` int(11) DEFAULT NULL COMMENT 'giá bán',
  `price_old` int(11) DEFAULT NULL COMMENT 'giá cũ',
  `status` tinyint(1) DEFAULT NULL COMMENT 'trạng thái sp',
  `thumbnail_file_id` int(11) DEFAULT NULL COMMENT 'id file hình đại diện',
  `user_id` int(11) DEFAULT NULL COMMENT 'id tài khoản cửa hàng',
  `category_id` int(11) DEFAULT NULL COMMENT 'danh mục sản phẩm',
  `store_category_id` int(11) DEFAULT NULL COMMENT 'danh mục cửa hành',
  `trademark_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL COMMENT 'mã sản phẩm',
  `inventory` int(11) DEFAULT '0' COMMENT 'số lượng tồn kho',
  `length` int(11) DEFAULT '0' COMMENT 'chiều dài (cm)\r\n',
  `width` int(11) DEFAULT '0' COMMENT 'chiều rộng (cm)',
  `height` int(11) DEFAULT '0' COMMENT 'chiều cao (cm)',
  `weight` int(11) DEFAULT '0' COMMENT 'khối lượng (gram)',
  `shipping_cost` int(11) DEFAULT '0',
  `origin` varchar(255) DEFAULT NULL COMMENT 'nơi xuất xứ',
  `factory_address` varchar(255) DEFAULT NULL COMMENT 'địa chỉ sản xuất/nhập khẩu',
  `rating` decimal(2,1) UNSIGNED DEFAULT '0.0' COMMENT 'xếp hạng sao',
  `is_used_product` tinyint(1) DEFAULT '0',
  `must_pre_order` tinyint(1) DEFAULT '0',
  `pre_order_time` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT 'ngay tao',
  `updated_at` datetime DEFAULT NULL COMMENT 'ngay cap nhat',
  `deleted_at` datetime DEFAULT NULL COMMENT 'ngay xoa'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='San pham' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_product`
--

INSERT INTO `lck_product` (`id`, `name`, `description`, `detail`, `price`, `price_old`, `status`, `thumbnail_file_id`, `user_id`, `category_id`, `store_category_id`, `trademark_id`, `sku`, `inventory`, `length`, `width`, `height`, `weight`, `shipping_cost`, `origin`, `factory_address`, `rating`, `is_used_product`, `must_pre_order`, `pre_order_time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Áo thun nữ 01', 'Áo thun nữ 01', 'Áo thun nữ 01', 50000, 55000, 1, 2, 3, 1, NULL, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-03-08 15:21:38', NULL),
(2, 'Áo thun nữ 02', 'Áo thun nữ 02', 'Áo thun nữ 02', 60000, 70000, 1, 2, 5, 1, 1, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(3, 'Áo thun nữ 03', 'Áo thun nữ 03', 'Áo thun nữ 03', 70000, 75000, 1, 2, 5, 1, 1, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(4, 'Áo thun nữ 04', 'Áo thun nữ 04', 'Áo thun nữ 04', 80000, 85000, 1, 2, 3, 1, 1, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(5, 'Áo thun nữ 05', 'Áo thun nữ 05', 'Áo thun nữ 05', 55000, 0, 1, 2, 3, 2, 1, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(6, 'Áo thun nữ 06', 'Áo thun nữ 06', 'Áo thun nữ 06', 65000, 0, 1, 2, 3, 2, 1, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(7, 'Áo thun nữ 07', 'Áo thun nữ 07', 'Áo thun nữ 07', 150000, 0, 1, 2, 3, 1, 1, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(8, 'Áo thun nữ 08', 'Áo thun nữ 08', 'Áo thun nữ 08', 99000, 0, 1, 2, 3, 1, 1, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(9, 'Áo thun nữ 09', 'Áo thun nữ 09', 'Áo thun nữ 09', 89000, 0, 1, 2, 3, 1, 1, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(10, 'Áo thun nữ 10', 'Áo thun nữ 10', 'Áo thun nữ 10', 20000, 0, 1, 2, 3, 1, 1, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(11, 'Quần jean 01', 'Quần jean 01', 'Quần jean 01', 50000, 0, 1, 6, 3, 1, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(12, 'Quần jean 02', 'Quần jean 02', 'Quần jean 02', 60000, 0, 1, 6, 3, 1, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(13, 'Quần jean 03', 'Quần jean 03', 'Quần jean 03', 70000, 0, 1, 6, 3, 1, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(14, 'Quần jean 04', 'Quần jean 04', 'Quần jean 04', 80000, 0, 1, 6, 3, 1, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(15, 'Quần jean 05', 'Quần jean 05', 'Quần jean 05', 55000, 0, 1, 6, 3, 1, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(16, 'Quần jean 06', 'Quần jean 06', 'Quần jean 06', 65000, 0, 1, 6, 3, 1, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(17, 'Quần jean 07', 'Quần jean 07', 'Quần jean 07', 150000, 0, 1, 6, 3, 52, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(18, 'Quần jean 08', 'Quần jean 08', 'Quần jean 08', 99000, 0, 1, 6, 3, 52, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(19, 'Quần jean 09', 'Quần jean 09', 'Quần jean 09', 89000, 0, 1, 6, 3, 52, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(20, 'Quần jean 10', 'Quần jean 10', 'Quần jean 10', 20000, 0, 1, 6, 3, 52, 2, NULL, NULL, 10, 0, 0, 0, 0, 0, NULL, NULL, '0.0', 0, 0, NULL, '2019-01-18 10:50:18', '2019-01-18 17:30:59', NULL),
(26, 'Test cập nhật sản phẩm', 'Test cập nhật sản phẩm', 'Test cập nhật sản phẩm', 500000, 0, 1, 3, 3, 52, NULL, NULL, 'string', 10, 0, 0, 0, 0, 0, 'string', 'string', '0.0', 0, 0, 0, '2019-03-09 14:47:32', '2019-03-09 15:23:29', NULL),
(27, 'Test cập nhật sản phẩm', 'Test cập nhật sản phẩm', 'Test cập nhật sản phẩm', 500000, 0, 1, 3, 3, 52, NULL, NULL, 'string', 10, 20, 10, 10, 100, 0, 'string', 'string', '0.0', 0, 0, 0, '2019-03-09 14:49:45', '2019-03-09 15:53:26', NULL),
(28, 'Quần Kaki Nam', 'Quần Kaki Nam', NULL, 25000, NULL, 1, 133, 3, 8, NULL, 5, '1230', 10, NULL, NULL, NULL, 498, 0, 'VN', '52/1B, 61 Street, District 9', '0.0', 1, 1, NULL, '2019-04-29 21:47:47', '2019-05-10 17:11:55', NULL),
(29, 'Tai nghe vip', 'Tai nghe vip', NULL, 250000, NULL, 1, 129, 3, 256, NULL, 1, 'ẻwewet', 10, 2, 2, 2, 42352433, 0, 'VN', 'VN', '0.0', 0, 1, NULL, '2019-04-29 22:22:55', '2019-05-10 17:11:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lck_product_images`
--

CREATE TABLE `lck_product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `priority` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lien ket san pham - hinh anh' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_product_images`
--

INSERT INTO `lck_product_images` (`id`, `product_id`, `file_id`, `priority`) VALUES
(1, 1, 2, NULL),
(2, 1, 3, NULL),
(3, 1, 4, NULL),
(4, 1, 5, NULL),
(5, 2, 2, NULL),
(6, 2, 3, NULL),
(7, 2, 4, NULL),
(8, 2, 5, NULL),
(9, 3, 2, NULL),
(10, 3, 3, NULL),
(11, 3, 4, NULL),
(12, 3, 5, NULL),
(13, 4, 2, NULL),
(14, 4, 3, NULL),
(15, 4, 4, NULL),
(16, 4, 5, NULL),
(17, 5, 2, NULL),
(18, 5, 3, NULL),
(19, 5, 4, NULL),
(20, 5, 5, NULL),
(21, 6, 2, NULL),
(22, 6, 3, NULL),
(23, 6, 4, NULL),
(24, 6, 5, NULL),
(25, 7, 2, NULL),
(26, 7, 3, NULL),
(27, 7, 4, NULL),
(28, 7, 5, NULL),
(29, 8, 2, NULL),
(30, 8, 3, NULL),
(31, 8, 4, NULL),
(32, 8, 5, NULL),
(33, 9, 2, NULL),
(34, 9, 3, NULL),
(35, 9, 4, NULL),
(36, 9, 5, NULL),
(37, 10, 2, NULL),
(38, 10, 3, NULL),
(39, 10, 4, NULL),
(40, 10, 5, NULL),
(41, 11, 6, NULL),
(42, 11, 7, NULL),
(43, 11, 8, NULL),
(44, 11, 9, NULL),
(45, 11, 10, NULL),
(46, 12, 6, NULL),
(47, 12, 7, NULL),
(48, 12, 8, NULL),
(49, 12, 9, NULL),
(50, 12, 10, NULL),
(51, 13, 6, NULL),
(52, 13, 7, NULL),
(53, 13, 8, NULL),
(54, 13, 9, NULL),
(55, 13, 10, NULL),
(56, 14, 6, NULL),
(57, 14, 7, NULL),
(58, 14, 8, NULL),
(59, 14, 9, NULL),
(60, 14, 10, NULL),
(61, 15, 6, NULL),
(62, 15, 7, NULL),
(63, 15, 8, NULL),
(64, 15, 9, NULL),
(65, 15, 10, NULL),
(66, 16, 6, NULL),
(67, 16, 7, NULL),
(68, 16, 8, NULL),
(69, 16, 9, NULL),
(70, 16, 10, NULL),
(71, 17, 6, NULL),
(72, 17, 7, NULL),
(73, 17, 8, NULL),
(74, 17, 9, NULL),
(75, 17, 10, NULL),
(76, 18, 6, NULL),
(77, 18, 7, NULL),
(78, 18, 8, NULL),
(79, 18, 9, NULL),
(80, 18, 10, NULL),
(81, 19, 6, NULL),
(82, 19, 7, NULL),
(83, 19, 8, NULL),
(84, 19, 9, NULL),
(85, 19, 10, NULL),
(86, 20, 6, NULL),
(87, 20, 7, NULL),
(88, 20, 8, NULL),
(89, 20, 9, NULL),
(90, 20, 10, NULL),
(121, 26, 3, 0),
(122, 26, 4, 1),
(123, 26, 5, 2),
(142, 27, 3, 0),
(143, 27, 4, 1),
(144, 27, 5, 2),
(198, 29, 129, 0),
(199, 29, 130, 1),
(200, 29, 131, 2),
(201, 29, 132, 3),
(202, 28, 133, 0),
(203, 28, 134, 1),
(204, 28, 135, 2);

-- --------------------------------------------------------

--
-- Table structure for table `lck_product_images_extra`
--

CREATE TABLE `lck_product_images_extra` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `priority` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lien ket San pham -hinh anh mo rong' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `lck_product_variation`
--

CREATE TABLE `lck_product_variation` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `inventory` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_product_variation`
--

INSERT INTO `lck_product_variation` (`id`, `product_id`, `name`, `price`, `sku`, `inventory`, `created_at`, `updated_at`) VALUES
(1, 1, 'Màu Xanh', 15000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(2, 1, 'Màu Đỏ', 20000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(3, 1, 'Màu Tím', 25000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(4, 1, 'Màu Vàng', 35000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(5, 1, 'Size S', 45000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(6, 1, 'Size M', 50000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(7, 1, 'Size L', 55000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(8, 1, 'Size XL', 60000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(9, 1, 'Màu Xanh - Size S', 65000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(10, 1, 'Màu Xanh - Size M', 70000, NULL, 10, '2019-03-03 20:58:45', '2019-03-03 20:58:45'),
(19, 26, 'Màu Đỏ', 50000, 'string', 10, '2019-03-09 15:23:29', '2019-03-09 15:23:29'),
(20, 26, 'Màu Xanh', 50000, 'string', 10, '2019-03-09 15:23:29', '2019-03-09 15:23:29'),
(21, 26, 'Màu Xanh - Size M', 50000, 'string', 10, '2019-03-09 15:23:29', '2019-03-09 15:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `lck_product_wholesales`
--

CREATE TABLE `lck_product_wholesales` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity_from` int(11) DEFAULT NULL,
  `quantity_to` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_product_wholesales`
--

INSERT INTO `lck_product_wholesales` (`id`, `product_id`, `quantity_from`, `quantity_to`, `price`) VALUES
(1, 1, 3, 10, 50000),
(2, 1, 11, 50, 45000);

-- --------------------------------------------------------

--
-- Table structure for table `lck_settings`
--

CREATE TABLE `lck_settings` (
  `setting_id` int(11) NOT NULL,
  `setting_key` varchar(255) DEFAULT NULL,
  `setting_value` longtext,
  `setting_ext` varchar(255) DEFAULT NULL,
  `setting_desc` varchar(255) DEFAULT NULL,
  `setting_type` varchar(255) DEFAULT NULL,
  `priority` tinyint(4) DEFAULT '0',
  `require` tinyint(4) DEFAULT '1',
  `visible` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_settings`
--

INSERT INTO `lck_settings` (`setting_id`, `setting_key`, `setting_value`, `setting_ext`, `setting_desc`, `setting_type`, `priority`, `require`, `visible`) VALUES
(1, 'META_TITLE', 'Sàn TMĐT Golden River', NULL, 'Meta Title', 'input', 1, 1, 1),
(2, 'META_DESCRIPTIONS', 'Sàn TMĐT Golden River', NULL, 'Meta Descriptions', 'textarea', 2, 1, 1),
(3, 'META_KEYWORDS', 'Sàn TMĐT Golden River', NULL, 'Meta Keywords', 'textarea', 3, 1, 1),
(4, 'META_AUTHOR', 'Sàn TMĐT Golden River', NULL, 'Meta Author', 'input', 4, 0, 1),
(5, 'COMPANY_NAME', 'Sàn TMĐT Golden River', NULL, 'Tên Công ty', 'input', 5, 0, 1),
(6, 'COMPANY_INTRO', 'Sàn TMĐT Golden River', NULL, 'Giới thiệu ngắn', 'textarea', 6, 1, 0),
(7, 'SERVICE_INTRO', 'Sàn TMĐT Golden River', NULL, 'Block giới thiệu dịch vụ', 'textarea', 7, 0, 0),
(8, 'COMPANY_DETAIL', 'Sàn TMĐT Golden River', NULL, 'Giới thiệu chi tiết', 'textarea', 8, 0, 0),
(9, 'STATISTIC_HOME', 'Sàn TMĐT Golden River', NULL, 'Block quy trình trên trang chủ', 'textarea', 9, 0, 0),
(10, 'ADDRESS', '549 Điện Biên Phủ, Phường 3, Q. 3, TP. HCM', NULL, 'Địa chỉ Công Ty', 'input', 10, 1, 1),
(11, 'EMAIL', 'info@b2b.com', NULL, 'Email', 'input', 11, 1, 1),
(12, 'HOTLINE', '0979 427 220', NULL, 'Hotline ', 'input', 12, 1, 1),
(13, 'SITE_SLOGAN', 'Sàn TMĐT Golden River', NULL, 'Slogan', 'input', 13, 0, 0),
(14, 'PHONE', '0979 427 220', NULL, 'Điện thoại 2', 'input', 14, 0, 0),
(15, 'FAX', NULL, NULL, 'Fax', 'input', 15, 0, 1),
(16, 'FACEBOOK', 'https://www.facebook.com/', NULL, 'Facebook', 'input', 16, 0, 1),
(17, 'GOOGLE_PLUS', 'https://plus.google.com/', NULL, 'Google Plus', 'input', 17, 0, 1),
(18, 'LINKEDIN', 'https://www.linkedin.com/', NULL, 'Link In', 'input', 18, 0, 1),
(19, 'PINTEREST', 'https://www.pinterest.com/', NULL, 'Pinterest', 'input', 19, 0, 1),
(20, 'TWITTER', 'https://twitter.com/', NULL, 'Twitter', 'input', 20, 0, 1),
(21, 'YOUTUBE', 'https://www.youtube.com/', NULL, 'Youtube ', 'input', 21, 0, 1),
(22, 'INSTAGRAM', 'https://www.instagram.com/asususa/', NULL, 'Instagram link', 'input', 22, 0, 1),
(23, 'ADDRESS_FOOTER', '', NULL, 'Thông tin liên hệ dưới footer', 'textarea', 23, 0, 0),
(24, 'ADDRESS_CONTACT', '', NULL, 'Thông tin liên hệ trong trang liên hệ', 'textarea', 24, 0, 0),
(25, 'GA_CODE', '', NULL, 'Code GA', 'textarea', 25, 0, 1),
(26, 'OPTION_CODE_FOOTER', '', NULL, 'Mã Javascript tùy chọn', 'textarea', 26, 0, 1),
(27, 'IS_PUBLISHED', '1', NULL, 'Công khai trang', 'input', 27, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lck_shipping_method`
--

CREATE TABLE `lck_shipping_method` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `api_url` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_shipping_method`
--

INSERT INTO `lck_shipping_method` (`id`, `code`, `name`, `icon`, `api_key`, `api_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ghtk', 'Giao Hàng Tiết Kiệm', NULL, NULL, NULL, '1', '2019-04-11 15:19:13', '2019-04-11 15:19:15'),
(2, 'viettel_post', 'Viettel Post', NULL, NULL, NULL, '1', '2019-04-11 15:19:13', '2019-04-11 15:19:15');

-- --------------------------------------------------------

--
-- Table structure for table `lck_store_category`
--

CREATE TABLE `lck_store_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `parent_id` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `priority` tinyint(4) DEFAULT '0',
  `thumbnail_file_id` int(11) DEFAULT NULL,
  `icon_file_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_descriptions` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Loai san pham' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_store_category`
--

INSERT INTO `lck_store_category` (`id`, `name`, `slug`, `description`, `parent_id`, `status`, `priority`, `thumbnail_file_id`, `icon_file_id`, `type`, `seo_title`, `seo_descriptions`, `seo_keywords`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Tào lao sửa', 'tao-lao-sua', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-08 14:51:38', 3),
(2, 'Quần', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `lck_trademark`
--

CREATE TABLE `lck_trademark` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `status` tinyint(4) DEFAULT '0',
  `priority` tinyint(4) DEFAULT '0',
  `thumbnail_file_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_descriptions` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `total_product` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Loai san pham' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `lck_trademark`
--

INSERT INTO `lck_trademark` (`id`, `name`, `slug`, `description`, `status`, `priority`, `thumbnail_file_id`, `category_id`, `seo_title`, `seo_descriptions`, `seo_keywords`, `total_product`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Nokia', NULL, NULL, 1, 0, 52, NULL, NULL, NULL, NULL, 0, '2019-03-21 16:53:07', '2019-03-21 16:53:07', NULL),
(2, 'Xmen', NULL, NULL, 1, 0, 53, NULL, NULL, NULL, NULL, 0, '2019-03-21 16:53:07', '2019-03-21 16:53:07', NULL),
(3, 'Ohui', NULL, NULL, 1, 0, 54, NULL, NULL, NULL, NULL, 0, '2019-03-21 16:53:07', '2019-03-21 16:53:07', NULL),
(4, 'Bitis', NULL, NULL, 1, 0, 55, NULL, NULL, NULL, NULL, 0, '2019-03-21 16:53:07', '2019-03-21 16:53:07', NULL),
(5, 'Khác', NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, '2019-03-21 16:53:07', '2019-03-21 16:53:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lck_variations`
--

CREATE TABLE `lck_variations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_variations`
--

INSERT INTO `lck_variations` (`id`, `name`, `user_id`, `product_id`) VALUES
(1, 'Màu sắc', 2, NULL),
(2, 'Size', 2, NULL),
(3, 'Kiểu dáng', 2, NULL),
(11, 'Màu', 3, 26),
(12, 'Size', 3, 26);

-- --------------------------------------------------------

--
-- Table structure for table `lck_variation_combine`
--

CREATE TABLE `lck_variation_combine` (
  `product_variation_id` int(11) NOT NULL,
  `variation_value_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_variation_combine`
--

INSERT INTO `lck_variation_combine` (`product_variation_id`, `variation_value_id`) VALUES
(1, 1),
(9, 1),
(10, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(9, 5),
(6, 6),
(10, 6),
(7, 7),
(8, 8),
(20, 18),
(20, 19),
(21, 19),
(21, 21);

-- --------------------------------------------------------

--
-- Table structure for table `lck_variation_values`
--

CREATE TABLE `lck_variation_values` (
  `id` int(11) NOT NULL,
  `variation_id` int(11) DEFAULT NULL COMMENT 'mã biến thể',
  `value` varchar(255) DEFAULT NULL COMMENT 'giá trị biến thể',
  `product_id` int(11) DEFAULT NULL COMMENT 'mã sp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lck_variation_values`
--

INSERT INTO `lck_variation_values` (`id`, `variation_id`, `value`, `product_id`) VALUES
(1, 1, 'Xanh', 1),
(2, 1, 'Đỏ', 1),
(3, 1, 'Tím', 1),
(4, 1, 'Vàng', 1),
(5, 2, 'S', 1),
(6, 2, 'M', 1),
(7, 2, 'L', 1),
(8, 2, 'XL', 1),
(18, 11, 'Đỏ', 26),
(19, 11, 'Xanh', 26),
(20, 12, 'S', 26),
(21, 12, 'M', 26);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(6, 'App\\Models\\CoreUsers', 2),
(7, 'App\\Models\\CoreUsers', 145),
(8, 'App\\Models\\CoreUsers', 145),
(9, 'App\\Models\\CoreUsers', 145),
(10, 'App\\Models\\CoreUsers', 145),
(11, 'App\\Models\\CoreUsers', 145),
(11, 'App\\Models\\CoreUsers', 146),
(12, 'App\\Models\\CoreUsers', 145),
(12, 'App\\Models\\CoreUsers', 146),
(13, 'App\\Models\\CoreUsers', 145),
(13, 'App\\Models\\CoreUsers', 146),
(14, 'App\\Models\\CoreUsers', 145),
(14, 'App\\Models\\CoreUsers', 146),
(47, 'App\\Models\\CoreUsers', 146),
(48, 'App\\Models\\CoreUsers', 146),
(49, 'App\\Models\\CoreUsers', 146),
(50, 'App\\Models\\CoreUsers', 146),
(51, 'App\\Models\\CoreUsers', 146),
(52, 'App\\Models\\CoreUsers', 146),
(53, 'App\\Models\\CoreUsers', 146),
(54, 'App\\Models\\CoreUsers', 146),
(59, 'App\\Models\\CoreUsers', 2),
(60, 'App\\Models\\CoreUsers', 2),
(61, 'App\\Models\\CoreUsers', 2),
(62, 'App\\Models\\CoreUsers', 2),
(1, 'App\\Models\\CoreUsers', 3),
(2, 'App\\Models\\CoreUsers', 3),
(3, 'App\\Models\\CoreUsers', 3),
(4, 'App\\Models\\CoreUsers', 3),
(5, 'App\\Models\\CoreUsers', 3),
(7, 'App\\Models\\CoreUsers', 3),
(8, 'App\\Models\\CoreUsers', 3),
(9, 'App\\Models\\CoreUsers', 3),
(10, 'App\\Models\\CoreUsers', 3),
(11, 'App\\Models\\CoreUsers', 3),
(12, 'App\\Models\\CoreUsers', 3),
(13, 'App\\Models\\CoreUsers', 3),
(14, 'App\\Models\\CoreUsers', 3),
(15, 'App\\Models\\CoreUsers', 3),
(16, 'App\\Models\\CoreUsers', 3),
(17, 'App\\Models\\CoreUsers', 3),
(18, 'App\\Models\\CoreUsers', 3),
(19, 'App\\Models\\CoreUsers', 3),
(20, 'App\\Models\\CoreUsers', 3),
(21, 'App\\Models\\CoreUsers', 3),
(22, 'App\\Models\\CoreUsers', 3),
(23, 'App\\Models\\CoreUsers', 3),
(24, 'App\\Models\\CoreUsers', 3),
(25, 'App\\Models\\CoreUsers', 3),
(26, 'App\\Models\\CoreUsers', 3),
(27, 'App\\Models\\CoreUsers', 3),
(28, 'App\\Models\\CoreUsers', 3),
(29, 'App\\Models\\CoreUsers', 3),
(30, 'App\\Models\\CoreUsers', 3),
(31, 'App\\Models\\CoreUsers', 3),
(32, 'App\\Models\\CoreUsers', 3),
(47, 'App\\Models\\CoreUsers', 3),
(48, 'App\\Models\\CoreUsers', 3),
(49, 'App\\Models\\CoreUsers', 3),
(50, 'App\\Models\\CoreUsers', 3),
(51, 'App\\Models\\CoreUsers', 3),
(52, 'App\\Models\\CoreUsers', 3),
(53, 'App\\Models\\CoreUsers', 3),
(54, 'App\\Models\\CoreUsers', 3),
(55, 'App\\Models\\CoreUsers', 3),
(56, 'App\\Models\\CoreUsers', 3),
(57, 'App\\Models\\CoreUsers', 3),
(58, 'App\\Models\\CoreUsers', 3),
(1, 'App\\Models\\CoreUsers', 2),
(2, 'App\\Models\\CoreUsers', 2),
(3, 'App\\Models\\CoreUsers', 2),
(4, 'App\\Models\\CoreUsers', 2),
(5, 'App\\Models\\CoreUsers', 2),
(7, 'App\\Models\\CoreUsers', 2),
(8, 'App\\Models\\CoreUsers', 2),
(9, 'App\\Models\\CoreUsers', 2),
(10, 'App\\Models\\CoreUsers', 2),
(11, 'App\\Models\\CoreUsers', 2),
(12, 'App\\Models\\CoreUsers', 2),
(13, 'App\\Models\\CoreUsers', 2),
(14, 'App\\Models\\CoreUsers', 2),
(15, 'App\\Models\\CoreUsers', 2),
(16, 'App\\Models\\CoreUsers', 2),
(17, 'App\\Models\\CoreUsers', 2),
(18, 'App\\Models\\CoreUsers', 2),
(19, 'App\\Models\\CoreUsers', 2),
(20, 'App\\Models\\CoreUsers', 2),
(21, 'App\\Models\\CoreUsers', 2),
(22, 'App\\Models\\CoreUsers', 2),
(23, 'App\\Models\\CoreUsers', 2),
(24, 'App\\Models\\CoreUsers', 2),
(25, 'App\\Models\\CoreUsers', 2),
(26, 'App\\Models\\CoreUsers', 2),
(27, 'App\\Models\\CoreUsers', 2),
(28, 'App\\Models\\CoreUsers', 2),
(29, 'App\\Models\\CoreUsers', 2),
(30, 'App\\Models\\CoreUsers', 2),
(31, 'App\\Models\\CoreUsers', 2),
(32, 'App\\Models\\CoreUsers', 2),
(47, 'App\\Models\\CoreUsers', 2),
(48, 'App\\Models\\CoreUsers', 2),
(49, 'App\\Models\\CoreUsers', 2),
(50, 'App\\Models\\CoreUsers', 2),
(51, 'App\\Models\\CoreUsers', 2),
(52, 'App\\Models\\CoreUsers', 2),
(53, 'App\\Models\\CoreUsers', 2),
(54, 'App\\Models\\CoreUsers', 2),
(55, 'App\\Models\\CoreUsers', 2),
(56, 'App\\Models\\CoreUsers', 2),
(57, 'App\\Models\\CoreUsers', 2),
(58, 'App\\Models\\CoreUsers', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `group`, `name`, `guard_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Danh sách', 'Sản phẩm', 'products.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:25'),
(2, 'Thêm mới', 'Sản phẩm', 'products.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:25'),
(3, 'Sửa', 'Sản phẩm', 'products.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:25'),
(4, 'Xóa', 'Sản phẩm', 'products.delete', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:25'),
(5, 'Kiểm duyệt', 'Sản phẩm', 'products.censorship.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:25'),
(7, 'Danh sách', 'tài khoản', 'users.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:25'),
(8, 'Thêm mới', 'tài khoản', 'users.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(9, 'Sửa', 'tài khoản', 'users.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(10, 'Xóa', 'tài khoản', 'users.delete', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(11, 'Danh sách', 'quản trị viên', 'staff.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(12, 'Thêm mới', 'quản trị viên', 'staff.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(13, 'Sửa', 'quản trị viên', 'staff.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(14, 'Xóa', 'quản trị viên', 'staff.delete', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(15, 'Danh sách', 'thông báo', 'notification.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(16, 'Push', 'thông báo', 'notification.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(17, 'Danh sách', 'tỉnh/tp', 'province.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(18, 'Thêm mới', 'tỉnh/tp', 'province.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(19, 'Sửa', 'tỉnh/tp', 'province.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(20, 'Xóa', 'tỉnh/tp', 'province.del', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(21, 'Danh sách', 'quận/huyện', 'district.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:26'),
(22, 'Thêm mới', 'quận/huyện', 'district.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(23, 'Sửa', 'quận/huyện', 'district.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(24, 'Xóa', 'quận/huyện', 'district.del', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(25, 'Danh sách', 'phường/xã', 'ward.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(26, 'Thêm mới', 'phường/xã', 'ward.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(27, 'Sửa', 'phường/xã', 'ward.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(28, 'Xóa', 'phường/xã', 'ward.del', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(29, 'Danh sách', 'tên đường', 'street.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(30, 'Thêm mới', 'tên đường', 'street.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(31, 'Sửa', 'tên đường', 'street.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(32, 'Xóa', 'tên đường', 'street.del', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(47, 'Danh sách', 'Bài viết', 'posts.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(48, 'Thêm mới', 'Bài viết', 'posts.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(49, 'Sửa', 'Bài viết', 'posts.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(50, 'Xóa', 'Bài viết', 'posts.del', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(51, 'Danh sách', 'Danh mục bài viết', 'posts.category.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(52, 'Thêm mới', 'Danh mục bài viết', 'posts.category.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(53, 'Sửa', 'Danh mục bài viết', 'posts.category.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(54, 'Xóa', 'Danh mục bài viết', 'posts.category.del', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(55, 'Danh sách', 'Danh mục sản phẩm', 'products.type.index', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(56, 'Thêm mới', 'Danh mục sản phẩm', 'products.type.add', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(57, 'Sửa', 'Danh mục sản phẩm', 'products.type.edit', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27'),
(58, 'Xóa', 'Danh mục sản phẩm', 'products.type.del', 'backend', 1, '0000-00-00 00:00:00', '2018-11-01 07:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `lck_address`
--
ALTER TABLE `lck_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `lck_banners`
--
ALTER TABLE `lck_banners`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `lck_basket`
--
ALTER TABLE `lck_basket`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_variation_id` (`product_variation_id`);

--
-- Indexes for table `lck_category`
--
ALTER TABLE `lck_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `thumbnail_file_id` (`thumbnail_file_id`),
  ADD KEY `icon_file_id` (`icon_file_id`),
  ADD KEY `lck_category_ibfk_1` (`parent_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `lck_core_users`
--
ALTER TABLE `lck_core_users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `coreusers_email_unique` (`email`) USING BTREE,
  ADD UNIQUE KEY `username` (`username`) USING BTREE,
  ADD KEY `lck_core_users_ibfk_2` (`avatar_file_path`);

--
-- Indexes for table `lck_core_users_activations`
--
ALTER TABLE `lck_core_users_activations`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `lck_favorite`
--
ALTER TABLE `lck_favorite`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`product_id`) USING BTREE,
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `lck_files`
--
ALTER TABLE `lck_files`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `file_path` (`file_path`) USING BTREE,
  ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- Indexes for table `lck_history_view`
--
ALTER TABLE `lck_history_view`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`product_id`) USING BTREE,
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `lck_location_district`
--
ALTER TABLE `lck_location_district`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `huyen_tinh_id_foreign` (`province_id`) USING BTREE;

--
-- Indexes for table `lck_location_province`
--
ALTER TABLE `lck_location_province`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `lck_location_street`
--
ALTER TABLE `lck_location_street`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ward_id` (`ward_id`),
  ADD KEY `district_id` (`district_id`);

--
-- Indexes for table `lck_location_ward`
--
ALTER TABLE `lck_location_ward`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `xa_huyen_id_foreign` (`district_id`) USING BTREE;

--
-- Indexes for table `lck_notification`
--
ALTER TABLE `lck_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_user_id` (`from_user_id`),
  ADD KEY `to_user_id` (`to_user_id`);

--
-- Indexes for table `lck_notification_read`
--
ALTER TABLE `lck_notification_read`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_id` (`notification_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `lck_orders`
--
ALTER TABLE `lck_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lck_orders_detail`
--
ALTER TABLE `lck_orders_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `lck_payment_method`
--
ALTER TABLE `lck_payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lck_post`
--
ALTER TABLE `lck_post`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `PostAlias` (`slug`) USING BTREE,
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `lck_post_category`
--
ALTER TABLE `lck_post_category`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `lck_product`
--
ALTER TABLE `lck_product`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `tbl_product_user_id_79d2c8cd_fk_tbl_user_id` (`user_id`) USING BTREE,
  ADD KEY `thumbnail_file_id` (`thumbnail_file_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `store_category_id` (`store_category_id`),
  ADD KEY `trademark_id` (`trademark_id`);
ALTER TABLE `lck_product` ADD FULLTEXT KEY `title` (`name`,`description`);

--
-- Indexes for table `lck_product_images`
--
ALTER TABLE `lck_product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `file_id` (`file_id`),
  ADD KEY `product_id` (`product_id`,`file_id`) USING BTREE;

--
-- Indexes for table `lck_product_images_extra`
--
ALTER TABLE `lck_product_images_extra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `file_id` (`file_id`);

--
-- Indexes for table `lck_product_variation`
--
ALTER TABLE `lck_product_variation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `lck_product_wholesales`
--
ALTER TABLE `lck_product_wholesales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `lck_settings`
--
ALTER TABLE `lck_settings`
  ADD PRIMARY KEY (`setting_id`) USING BTREE;

--
-- Indexes for table `lck_shipping_method`
--
ALTER TABLE `lck_shipping_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lck_store_category`
--
ALTER TABLE `lck_store_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thumbnail_file_id` (`thumbnail_file_id`),
  ADD KEY `icon_file_id` (`icon_file_id`),
  ADD KEY `lck_category_ibfk_1` (`parent_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `lck_trademark`
--
ALTER TABLE `lck_trademark`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `thumbnail_file_id` (`thumbnail_file_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `lck_variations`
--
ALTER TABLE `lck_variations`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `store_id` (`user_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `name` (`name`) USING BTREE;

--
-- Indexes for table `lck_variation_combine`
--
ALTER TABLE `lck_variation_combine`
  ADD PRIMARY KEY (`product_variation_id`,`variation_value_id`) USING BTREE,
  ADD KEY `variation_value_id` (`variation_value_id`);

--
-- Indexes for table `lck_variation_values`
--
ALTER TABLE `lck_variation_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `variation_id` (`variation_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lck_address`
--
ALTER TABLE `lck_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `lck_banners`
--
ALTER TABLE `lck_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `lck_basket`
--
ALTER TABLE `lck_basket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `lck_category`
--
ALTER TABLE `lck_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- AUTO_INCREMENT for table `lck_core_users`
--
ALTER TABLE `lck_core_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `lck_favorite`
--
ALTER TABLE `lck_favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lck_files`
--
ALTER TABLE `lck_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `lck_history_view`
--
ALTER TABLE `lck_history_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `lck_location_district`
--
ALTER TABLE `lck_location_district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=788;

--
-- AUTO_INCREMENT for table `lck_location_province`
--
ALTER TABLE `lck_location_province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `lck_location_street`
--
ALTER TABLE `lck_location_street`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1694;

--
-- AUTO_INCREMENT for table `lck_location_ward`
--
ALTER TABLE `lck_location_ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27683;

--
-- AUTO_INCREMENT for table `lck_notification`
--
ALTER TABLE `lck_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lck_notification_read`
--
ALTER TABLE `lck_notification_read`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lck_orders`
--
ALTER TABLE `lck_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lck_orders_detail`
--
ALTER TABLE `lck_orders_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `lck_payment_method`
--
ALTER TABLE `lck_payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lck_post`
--
ALTER TABLE `lck_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lck_post_category`
--
ALTER TABLE `lck_post_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lck_product`
--
ALTER TABLE `lck_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `lck_product_images`
--
ALTER TABLE `lck_product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;

--
-- AUTO_INCREMENT for table `lck_product_images_extra`
--
ALTER TABLE `lck_product_images_extra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lck_product_variation`
--
ALTER TABLE `lck_product_variation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `lck_product_wholesales`
--
ALTER TABLE `lck_product_wholesales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lck_settings`
--
ALTER TABLE `lck_settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `lck_shipping_method`
--
ALTER TABLE `lck_shipping_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lck_store_category`
--
ALTER TABLE `lck_store_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lck_trademark`
--
ALTER TABLE `lck_trademark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lck_variations`
--
ALTER TABLE `lck_variations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lck_variation_values`
--
ALTER TABLE `lck_variation_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lck_address`
--
ALTER TABLE `lck_address`
  ADD CONSTRAINT `lck_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_basket`
--
ALTER TABLE `lck_basket`
  ADD CONSTRAINT `lck_basket_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_basket_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `lck_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_basket_ibfk_3` FOREIGN KEY (`product_variation_id`) REFERENCES `lck_product_variation` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_category`
--
ALTER TABLE `lck_category`
  ADD CONSTRAINT `lck_category_ibfk_2` FOREIGN KEY (`thumbnail_file_id`) REFERENCES `lck_files` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_category_ibfk_3` FOREIGN KEY (`icon_file_id`) REFERENCES `lck_files` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_category_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `lck_core_users_activations`
--
ALTER TABLE `lck_core_users_activations`
  ADD CONSTRAINT `lck_core_users_activations_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_favorite`
--
ALTER TABLE `lck_favorite`
  ADD CONSTRAINT `lck_favorite_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_favorite_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `lck_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_files`
--
ALTER TABLE `lck_files`
  ADD CONSTRAINT `lck_files_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lck_history_view`
--
ALTER TABLE `lck_history_view`
  ADD CONSTRAINT `lck_history_view_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_history_view_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `lck_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_notification_read`
--
ALTER TABLE `lck_notification_read`
  ADD CONSTRAINT `lck_notification_read_ibfk_1` FOREIGN KEY (`notification_id`) REFERENCES `lck_notification` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_notification_read_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_orders_detail`
--
ALTER TABLE `lck_orders_detail`
  ADD CONSTRAINT `lck_orders_detail_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `lck_orders` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_post`
--
ALTER TABLE `lck_post`
  ADD CONSTRAINT `lck_post_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `lck_post_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `lck_product`
--
ALTER TABLE `lck_product`
  ADD CONSTRAINT `lck_product_ibfk_1` FOREIGN KEY (`thumbnail_file_id`) REFERENCES `lck_files` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_product_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_product_ibfk_4` FOREIGN KEY (`category_id`) REFERENCES `lck_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_product_ibfk_5` FOREIGN KEY (`store_category_id`) REFERENCES `lck_store_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_product_ibfk_6` FOREIGN KEY (`trademark_id`) REFERENCES `lck_trademark` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `lck_product_images`
--
ALTER TABLE `lck_product_images`
  ADD CONSTRAINT `lck_product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `lck_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_product_images_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `lck_files` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_product_images_extra`
--
ALTER TABLE `lck_product_images_extra`
  ADD CONSTRAINT `lck_product_images_extra_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `lck_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_product_images_extra_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `lck_files` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_product_variation`
--
ALTER TABLE `lck_product_variation`
  ADD CONSTRAINT `lck_product_variation_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `lck_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_product_wholesales`
--
ALTER TABLE `lck_product_wholesales`
  ADD CONSTRAINT `lck_product_wholesales_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `lck_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_store_category`
--
ALTER TABLE `lck_store_category`
  ADD CONSTRAINT `lck_store_category_ibfk_2` FOREIGN KEY (`thumbnail_file_id`) REFERENCES `lck_files` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_store_category_ibfk_3` FOREIGN KEY (`icon_file_id`) REFERENCES `lck_files` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_store_category_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_trademark`
--
ALTER TABLE `lck_trademark`
  ADD CONSTRAINT `lck_trademark_ibfk_1` FOREIGN KEY (`thumbnail_file_id`) REFERENCES `lck_files` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_trademark_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_trademark_ibfk_4` FOREIGN KEY (`category_id`) REFERENCES `lck_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `lck_variations`
--
ALTER TABLE `lck_variations`
  ADD CONSTRAINT `lck_variations_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `lck_core_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_variations_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `lck_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_variation_combine`
--
ALTER TABLE `lck_variation_combine`
  ADD CONSTRAINT `lck_variation_combine_ibfk_1` FOREIGN KEY (`product_variation_id`) REFERENCES `lck_product_variation` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_variation_combine_ibfk_2` FOREIGN KEY (`variation_value_id`) REFERENCES `lck_variation_values` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `lck_variation_values`
--
ALTER TABLE `lck_variation_values`
  ADD CONSTRAINT `lck_variation_values_ibfk_1` FOREIGN KEY (`variation_id`) REFERENCES `lck_variations` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `lck_variation_values_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `lck_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
