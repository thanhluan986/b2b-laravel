ALTER TABLE `lck_orders_detail`
ADD COLUMN `sku` varchar(255) AFTER `quantity`;

ALTER TABLE `lck_address`
ADD COLUMN `is_return` tinyint(1) AFTER `is_warehouse`;

ALTER TABLE `b2b`.`lck_core_users`
MODIFY COLUMN `rating` decimal(2, 1) UNSIGNED DEFAULT 0.0 COMMENT 'Đánh giá' AFTER `cover_file_path`,
MODIFY COLUMN `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Mô tả shop' AFTER `account_type`,
ADD COLUMN `chat_response_rate` decimal(10, 2) COMMENT 'Tỉ lệ phản hồi chat (%)' AFTER `description`,
ADD COLUMN `preparing_time` int(11) COMMENT 'Thời gian chuẩn bị hàng (ngày)' AFTER `chat_response_rate`,
ADD COLUMN `cancel_order_rate` decimal(10, 2) COMMENT 'Tỉ lệ hủy đơn (%)' AFTER `preparing_time`,
ADD COLUMN `total_products` int(11) COMMENT 'Tổng sản phẩm' AFTER `cancel_order_rate`,
ADD COLUMN `last_login` datetime(0) COMMENT 'Lần cuối login' AFTER `total_products`,
ADD COLUMN `following_count` int(11) COMMENT 'Số người đang theo dõi' AFTER `last_login`,
ADD COLUMN `follower_count` int(11) COMMENT 'Số người theo dõi shop' AFTER `following_count`,
ADD COLUMN `is_verified` tinyint(1) DEFAULT 0 COMMENT 'Tài khoản đã xác thực hay chưa' AFTER `follower_count`;

ALTER TABLE `b2b`.`lck_core_users`
MODIFY COLUMN `chat_response_rate` decimal(10, 2) DEFAULT 0 COMMENT 'Tỉ lệ phản hồi chat (%)' AFTER `description`,
MODIFY COLUMN `preparing_time` int(11) DEFAULT 0 COMMENT 'Thời gian chuẩn bị hàng (ngày)' AFTER `chat_response_rate`,
MODIFY COLUMN `cancel_order_rate` decimal(10, 2) DEFAULT 0 COMMENT 'Tỉ lệ hủy đơn (%)' AFTER `preparing_time`,
MODIFY COLUMN `total_products` int(11) DEFAULT 0 COMMENT 'Tổng sản phẩm' AFTER `cancel_order_rate`,
MODIFY COLUMN `following_count` int(11) DEFAULT 0 COMMENT 'Số người đang theo dõi' AFTER `last_login`,
MODIFY COLUMN `follower_count` int(11) DEFAULT 0 COMMENT 'Số người theo dõi shop' AFTER `following_count`;