<?php
// Home
Breadcrumbs::for ('Dashboard', function ($trail) {
    $trail->push('Dashboard', route('backend.dashboard'));
});

// Home > login
Breadcrumbs::for ('login', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Login', route('backend.login'));
});

// Home > profile
Breadcrumbs::for ('Profile', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Thông tin tài khoản', route('backend.users.profile'));
});

Breadcrumbs::for ('backend.users.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Quản lý tài khoản', route('backend.users.index'));
});

Breadcrumbs::for ('backend.users.add', function ($trail) {
    $trail->parent('backend.users.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.users.edit', function ($trail) {
    $trail->parent('backend.users.index');
    $trail->push('Cập nhật', '');
});

// Products index
Breadcrumbs::for ('backend.products.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Sản phẩm', route('backend.products.index'));
});

Breadcrumbs::for ('backend.products.detail', function ($trail) {
    $trail->parent('backend.products.index');
    $trail->push('Chi tiết', '');
});

Breadcrumbs::for ('backend.products.add', function ($trail) {
    $trail->parent('backend.products.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.products.edit', function ($trail) {
    $trail->parent('backend.products.index');
    $trail->push('Chỉnh sửa', '');
});

// Notification
Breadcrumbs::for ('backend.notification.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Thông báo', route('backend.notification.index'));
});

Breadcrumbs::for ('backend.notification.add', function ($trail) {
    $trail->parent('backend.notification.index');
    $trail->push('Push thông báo', '');
});

// Province
Breadcrumbs::for ('backend.location.province.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Tỉnh/TP', route('backend.location.province.index'));
});

Breadcrumbs::for ('backend.location.province.add', function ($trail) {
    $trail->parent('backend.location.province.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.location.province.edit', function ($trail) {
    $trail->parent('backend.location.province.index');
    $trail->push('Chỉnh sửa', '');
});

// District
Breadcrumbs::for ('backend.location.district.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Quận/Huyện', route('backend.location.district.index'));
});

Breadcrumbs::for ('backend.location.district.add', function ($trail) {
    $trail->parent('backend.location.district.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.location.district.edit', function ($trail) {
    $trail->parent('backend.location.district.index');
    $trail->push('Chỉnh sửa', '');
});

// Ward
Breadcrumbs::for ('backend.location.ward.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Phường/Xã', route('backend.location.ward.index'));
});

Breadcrumbs::for ('backend.location.ward.add', function ($trail) {
    $trail->parent('backend.location.ward.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.location.ward.edit', function ($trail) {
    $trail->parent('backend.location.ward.index');
    $trail->push('Chỉnh sửa', '');
});

// Street
Breadcrumbs::for ('backend.location.street.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Tên đường', route('backend.location.street.index'));
});

Breadcrumbs::for ('backend.location.street.add', function ($trail) {
    $trail->parent('backend.location.street.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.location.street.edit', function ($trail) {
    $trail->parent('backend.location.street.index');
    $trail->push('Chỉnh sửa', '');
});

//staff
Breadcrumbs::for ('backend.staff.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Quản lý quản trị viên', route('backend.staff.index'));
});

Breadcrumbs::for ('backend.staff.add', function ($trail) {
    $trail->parent('backend.staff.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.staff.edit', function ($trail) {
    $trail->parent('backend.staff.index');
    $trail->push('Cập nhật', '');
});

// posts
Breadcrumbs::for ('backend.posts.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Bài viết', route('backend.posts.index'));
});

Breadcrumbs::for ('backend.posts.add', function ($trail) {
    $trail->parent('backend.posts.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.posts.edit', function ($trail) {
    $trail->parent('backend.posts.index');
    $trail->push('Chỉnh sửa', '');
});

// posts category
Breadcrumbs::for ('backend.posts.category.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Danh mục bài viết', route('backend.posts.category.index'));
});

Breadcrumbs::for ('backend.posts.category.add', function ($trail) {
    $trail->parent('backend.posts.category.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.posts.category.edit', function ($trail) {
    $trail->parent('backend.posts.category.index');
    $trail->push('Chỉnh sửa', '');
});

// products category
Breadcrumbs::for ('backend.products.type.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Danh mục sản phẩm', route('backend.products.type.index'));
});

Breadcrumbs::for ('backend.products.type.add', function ($trail) {
    $trail->parent('backend.products.type.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.products.type.edit', function ($trail) {
    $trail->parent('backend.products.type.index');
    $trail->push('Chỉnh sửa', '');
});

// banner
Breadcrumbs::for ('backend.banners.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Banner', route('backend.banners.index'));
});

Breadcrumbs::for ('backend.banners.add', function ($trail) {
    $trail->parent('backend.banners.index');
    $trail->push('Thêm mới', '');
});

Breadcrumbs::for ('backend.banners.edit', function ($trail) {
    $trail->parent('backend.banners.index');
    $trail->push('Chỉnh sửa', '');
});

// setting
Breadcrumbs::for ('backend.setting.index', function ($trail) {
    $trail->parent('Dashboard');
    $trail->push('Cài đặt chung', '');
});