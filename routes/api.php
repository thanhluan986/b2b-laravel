<?php

Route::group(['prefix' => 'v1.0'], function () {

    Route::group(['prefix' => 'user'], function () {
        Route::post('/login', 'Api\UserController@login')->name('user.login');
        Route::get('/login-facebook', 'Api\UserController@loginFacebook')->name('user.loginFacebook');
        Route::get('/login-google', 'Api\UserController@loginGoogle')->name('user.loginGoogle');
        Route::post('/forgot-password', 'Api\UserController@forgotPassword')->name('user.forgotPassword');
        Route::post('/reset-password', 'Api\UserController@resetPassword')->name('user.resetPassword');
        Route::post('/register', 'Api\UserController@register')->name('user.register');
        Route::post('/update', 'Api\UserController@update')->name('user.update');
        Route::post('/verify', 'Api\UserController@verify')->name('user.verify');
        Route::post('/exits', 'Api\UserController@accountexits')->name('user.exits');

        Route::post('/logout', 'Api\UserController@logout')->name('user.logout');
        Route::post('/refresh', 'Api\UserController@refresh')->name('user.refresh');
        Route::any('/me', 'Api\UserController@me')->name('user.me');
    });

    Route::group(['prefix' => 'banner'], function () {
        Route::get('', 'Api\BannerController@getAll')->name('banner.getAll');
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('', 'Api\CategoryController@getAll')->name('category.getAll');
    });

    Route::group(['prefix' => 'trademark'], function () {
        Route::get('', 'Api\TrademarkController@getAll')->name('trademark.getAll');
        Route::get('/top', 'Api\TrademarkController@top')->name('trademark.top');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('', 'Api\ProductController@getAll')->name('product.getAll');
        Route::get('/search', 'Api\ProductController@search')->name('product.search');
        Route::get('/{product_id}', 'Api\ProductController@detail')->name('product.detail');
    });

    Route::group(['prefix' => 'history'], function () {
        Route::get('/viewed-product', 'Api\HistoryController@getViewedProduct')->name('history.getViewedProduct');
        Route::delete('/viewed-product', 'Api\HistoryController@deleteViewedProduct')->name('history.deleteViewedProduct');
    });

    Route::group(['prefix' => 'favorite'], function () {
        Route::get('', 'Api\FavoriteController@getAll')->name('favorite.getAll');
        Route::post('', 'Api\FavoriteController@add')->name('favorite.add');
        Route::delete('', 'Api\FavoriteController@delete')->name('favorite.delete');
    });

    Route::group(['prefix' => 'basket'], function () {
        Route::get('', 'Api\BasketController@getAll')->name('basket.getAll');
        Route::post('', 'Api\BasketController@add')->name('basket.add');
        Route::get('/counter', 'Api\BasketController@counter')->name('basket.counter');
        Route::post('/update', 'Api\BasketController@update')->name('basket.update');
        Route::post('/remove-item', 'Api\BasketController@removeItems')->name('basket.removeItems');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('', 'Api\OrdersController@getAll')->name('orders.getAll');
        Route::post('', 'Api\OrdersController@add')->name('orders.add');
        Route::post('/cancel/{order_id}', 'Api\OrdersController@cancel')->name('orders.cancel');
        Route::get('/{order_id}', 'Api\OrdersController@detail')->name('orders.detail');
    });

    Route::group(['prefix' => 'notification'], function () {
        Route::get('/', 'Api\NotificationController@getAll')->name('notification.getAll');
        Route::post('/mark-read', 'Api\NotificationController@postMarkRead')->name('notification.markRead');
        Route::get('/counter', 'Api\NotificationController@getCounter')->name('notification.getCounter');
    });

    Route::group(['prefix' => 'store-list'], function () {
        Route::get('/', 'Api\StoreController@getList')->name('store-page.getList');
    });

    Route::group(['prefix' => 'store-page'], function () {

        Route::get('/{user_name}', 'Api\StoreController@info')->name('store-page.info');
        Route::get('/{user_name}/category', 'Api\StoreController@getCategory')->name('store-page.getCategory');
    });

    Route::group(['prefix' => 'store'], function () {

        Route::any('/info', 'Api\Store\IndexController@info')->name('store.info');
        Route::post('/update', 'Api\Store\IndexController@update')->name('store.update');

        Route::group(['prefix' => 'category'], function () {
            Route::post('/add-product', 'Api\Store\CategoryController@addProduct')->name('store.category.addProduct');
            Route::post('/remove-product', 'Api\Store\CategoryController@removeProduct')->name('store.category.removeProduct');

            Route::get('', 'Api\Store\CategoryController@getAll')->name('store.category.getAll');
            Route::post('', 'Api\Store\CategoryController@add')->name('store.category.add');
            Route::post('/{category_id}', 'Api\Store\CategoryController@update')->name('store.category.update');
            Route::delete('/{category_id}', 'Api\Store\CategoryController@delete')->name('store.category.delete');
        });

        Route::group(['prefix' => 'product'], function () {
            Route::get('', 'Api\Store\ProductController@getAll')->name('store.product.getAll');
            Route::post('', 'Api\Store\ProductController@add')->name('store.product.add');
            Route::post('/delete', 'Api\Store\ProductController@delete')->name('store.product.delete');
            Route::get('/{product_id}', 'Api\Store\ProductController@detail')->name('store.product.detail');
            Route::post('/{product_id}', 'Api\Store\ProductController@update')->name('store.product.update');
        });

        Route::group(['prefix' => 'orders'], function () {
            Route::get('', 'Api\Store\OrdersController@getAll')->name('store.orders.getAll');
            Route::get('/{order_id}', 'Api\Store\OrdersController@detail')->name('store.orders.detail');
            Route::post('', 'Api\Store\OrdersController@update')->name('store.orders.update');
        });
    });

    Route::group(['prefix' => 'location'], function () {
        Route::get('/province', 'Api\LocationController@province')->name('location.province');
        Route::get('/district', 'Api\LocationController@district')->name('location.district');
        Route::get('/ward', 'Api\LocationController@ward')->name('location.ward');
        Route::get('/street', 'Api\LocationController@street')->name('location.street');
    });

    Route::post('/upload/image', 'Api\UploadController@image')->name('upload.image');
    Route::get('/config', 'Api\ConfigController@index')->name('config.index');

    Route::group(['prefix' => 'address'], function () {
        Route::get('/', 'Api\AddressController@getAll')->name('address.getAll');
        Route::post('/', 'Api\AddressController@add')->name('address.add');
        Route::post('/update/{id}', 'Api\AddressController@update')->name('address.update');
        Route::get('/{id}', 'Api\AddressController@detail')->name('address.detail');
        Route::delete('/{id}', 'Api\AddressController@delete')->name('address.delete');
    });
});