<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'frontend'], function () {

    //content
    Route::group(['prefix' => 'blog'], function () {
        Route::any('/', 'Frontend\BlogController@index')->name('frontend.blog.index');
        Route::any('/{slug}.{id}', 'Frontend\BlogController@detail')->name('frontend.blog.detail');
        Route::any('/{cate_slug}_{id}', 'Frontend\BlogController@index')->name('frontend.blog.category');
        Route::any('/{cate_slug}', 'Frontend\BlogController@index')->name('frontend.blog.category');
    });

    //static page
    Route::group(['prefix' => 'page'], function () {
        Route::any('/gioi-thieu.html', 'Frontend\PageController@about')->name('frontend.page.about');
        Route::any('/lien-he.html', 'Frontend\PageController@contact')->name('frontend.page.contact');
    });

    Route::get('user/activation/{token}', 'Frontend\UserController@activateUser')->name('user.activate');
    Route::any('/', 'Frontend\IndexController@index')->name('frontend.index');

    Route::any('/tim-kiem.html', 'Frontend\ProductsController@index')->name('frontend.product.search');

    Route::any('/{slug}.{id}', 'Frontend\ProductsController@index')
        ->name('frontend.product.index')
        ->where(['slug' => '.*', 'id' => '[0-9]+']);

    Route::group(['prefix' => 'san-pham'], function () {
        Route::any('/{slug}-{id}', 'Frontend\ProductsController@detail')->name('frontend.product.detail');
    });

    Route::group(['prefix' => 'ajax'], function () {
        Route::any('/login-popup.html', 'Frontend\Ajax\UserController@login')->name('frontend.ajax.login');
        Route::any('/register-popup.html', 'Frontend\Ajax\UserController@register')->name('frontend.ajax.register');
    });

    Route::group(['middleware' => 'user'], function () {

        Route::group(['prefix' => 'ajax'], function () {
            Route::any('/upload-image.html', 'Frontend\Ajax\ImageController@upload')->name('frontend.ajax.image.upload');
            Route::post('/remove-image.html', 'Frontend\Ajax\ImageController@remove')->name('frontend.ajax.image.remove');
            Route::any('/address.html', 'Frontend\Ajax\AddressController@add')->name('frontend.ajax.address.add');
        });

        Route::any('/tai-khoan.html', 'Frontend\UserController@profile')->name('frontend.user.profile');
        Route::any('/dang-xuat.html', 'Frontend\UserController@logout')->name('frontend.user.logout');
        Route::any('/quen-mat-khau.html', 'Frontend\UserController@forgotpass')->name('frontend.user.forgotpass');
        Route::any('/san-pham-yeu-thich.html', 'Frontend\UserController@wishlist')->name('frontend.user.wishlist');
        Route::any('/don-mua.html', 'Frontend\OrdersController@index')->name('frontend.user.orders');

        Route::any('/dia-chi.html', 'Frontend\AddressController@index')->name('frontend.address.index');
        Route::any('/them-dia-chi.html', 'Frontend\AddressController@add')->name('frontend.address.add');
        Route::any('/sua-dia-chi.html', 'Frontend\AddressController@edit')->name('frontend.address.edit');
        Route::any('/xoa-dia-chi.html', 'Frontend\AddressController@delete')->name('frontend.address.delete');

        Route::group(['prefix' => 'ban-hang'], function () {
            Route::any('/san-pham.html', 'Frontend\Store\ProductsController@index')->name('frontend.store.products.index');
            Route::any('/them-san-pham.html', 'Frontend\Store\ProductsController@create')->name('frontend.store.products.create');
            Route::any('/cap-nhat-san-pham.html', 'Frontend\Store\ProductsController@update')->name('frontend.store.products.update');
            Route::any('/xoa-san-pham.html', 'Frontend\Store\ProductsController@delete')->name('frontend.store.products.delete');

            Route::any('/don-hang.html', 'Frontend\Store\OrdersController@index')->name('frontend.store.orders.index');
            Route::any('/cap-nhat-don-hang.html', 'Frontend\Store\OrdersController@update')->name('frontend.store.orders.update');

            Route::any('/thong-tin-shop.html', 'Frontend\Store\IndexController@index')->name('frontend.store.index');

            Route::any('/wallet.html', 'Frontend\Store\WalletController@index')->name('frontend.store.wallet.index');
        });

        Route::any('/gio-hang.html', 'Frontend\BasketController@index')->name('frontend.basket.index');
        Route::any('/ajax/gio-hang.html', 'Frontend\BasketController@ajax')->name('frontend.basket.ajax');
        Route::any('/xoa-khoi-gio-hang.html', 'Frontend\BasketController@remove')->name('frontend.basket.remove');
        Route::any('/cap-nhat-gio-hang.html', 'Frontend\BasketController@update')->name('frontend.basket.update');
        Route::any('/dat-hang.html', 'Frontend\CheckoutController@index')->name('frontend.checkout.index');
    });

    Route::any('/them-vao-gio-hang.html', 'Frontend\BasketController@add')->name('frontend.basket.add');

    Route::group(['prefix' => 'shop'], function () {
        Route::any('/{username}', 'Frontend\StoreController@index')->name('frontend.store.index');
        Route::any('/{username}/product.html', 'Frontend\StoreController@product')->name('frontend.store.product');
    });
});

Route::group(['prefix' => 'admin'], function () {

    Route::any('/login', 'Backend\AuthController@login')->name('backend.login');
    Route::any('/logout', 'Backend\AuthController@logout')->name('backend.logout');

    Route::group(['middleware' => 'backend'], function () {

        Route::any('/merge', 'Backend\DashboardController@merge')->name('backend.merge');
        Route::any('/merge/note', 'Backend\DashboardController@mergeNote')->name('backend.mergeNote');

        Route::any('/', 'Backend\DashboardController@index')->name('backend.dashboard');

        Route::any('/profile', 'Backend\UsersController@profile')->name('backend.users.profile');

        Route::any('/users', 'Backend\UsersController@index')->name('backend.users.index')->middleware('permission:users.index');
        Route::any('/users/add', 'Backend\UsersController@add')->name('backend.users.add')->middleware('permission:users.add');
        Route::any('/users/edit/{id}', 'Backend\UsersController@edit')->name('backend.users.edit')->middleware('permission:users.edit');
        Route::any('/users/delete/{id}', 'Backend\UsersController@delete')->name('backend.users.delete')->middleware('permission:users.delete');

        Route::group(['prefix' => 'staff'], function () {
            Route::any('', 'Backend\StaffController@index')->name('backend.staff.index')->middleware('permission:staff.index');
            Route::any('/add', 'Backend\StaffController@add')->name('backend.staff.add')->middleware('permission:staff.add');
            Route::any('/edit/{id}', 'Backend\StaffController@edit')->name('backend.staff.edit')->middleware('permission:staff.edit');
            Route::any('/delete/{id}', 'Backend\StaffController@delete')->name('backend.staff.delete')->middleware('permission:staff.delete');
        });

        Route::group(['prefix' => 'products'], function () {
            Route::any('', 'Backend\Product\ProductsController@index')->name('backend.products.index')->middleware('permission:products.index');
            Route::any('/add', 'Backend\Product\ProductsController@add')->name('backend.products.add')->middleware('permission:products.add');
            Route::any('/edit/{id}', 'Backend\Product\ProductsController@edit')->name('backend.products.edit')->middleware('permission:products.edit');
            Route::any('/delete/{id}', 'Backend\Product\ProductsController@edit')->name('backend.products.delete')->middleware('permission:products.delete');

            Route::post('/callback-request', 'Backend\Product\ProductsController@callbackRequest')->name('backend.products.callback-request')->middleware('permission:products.callback.request');

            Route::post('/ajax/approved', 'Backend\Product\ProductsController@approved')->name('backend.products.ajax.approved');
            Route::post('/ajax/un_approved', 'Backend\Product\ProductsController@un_approved')->name('backend.products.ajax.un_approved');
            Route::post('/ajax/delete', 'Backend\Product\ProductsController@ajaxdelete')->name('backend.products.ajax.delete');
            Route::get('/ajax/getnote', 'Backend\Product\ProductsController@getNote')->name('backend.products.ajax.getNote');
            Route::post('/ajax/addnote', 'Backend\Product\ProductsController@addNote')->name('backend.products.ajax.addNote');

            Route::any('/owned', 'Backend\Product\OwnedController@index')->name('backend.products.owned.index');
            Route::any('/owned/edit/{id}', 'Backend\Product\OwnedController@edit')->name('backend.products.owned.edit')->middleware('permission:products.owned.edit');
            Route::any('/owned/delete/{id}', 'Backend\Product\OwnedController@edit')->name('backend.products.owned.delete')->middleware('permission:products.owned.delete');

            Route::any('/callback', 'Backend\Product\CallbackController@index')->name('backend.products.callback.index')->middleware('permission:products.callback.index');
            Route::any('/callback/edit/{id}', 'Backend\Product\CallbackController@edit')->name('backend.products.callback.edit')->middleware('permission:products.callback.index');
            Route::any('/callback/detail/product', 'Backend\Product\CallbackController@detailProduct')->name('backend.products.callback.detailProduct')->middleware('permission:products.callback.index');
            Route::any('/callback/make', 'Backend\Product\CallbackController@make')->name('backend.products.callback.make')->middleware('permission:products.callback.index');

            Route::any('/image', 'Backend\Product\ImageController@index')->name('backend.products.image.index')->middleware('permission:products.image.index');
            Route::any('/image/edit/{id}', 'Backend\Product\ImageController@edit')->name('backend.products.image.edit')->middleware('permission:products.image.index');
            Route::post('/image/remove', 'Backend\Product\ImageController@remove')->name('backend.products.image.remove')->middleware('permission:products.image.index');
            Route::post('/image/upload', 'Backend\Product\ImageController@upload')->name('backend.products.image.upload')->middleware('permission:products.image.index');
            Route::post('/image/sort', 'Backend\Product\ImageController@sort')->name('backend.products.image.sort');
            Route::post('/image/rotate', 'Backend\Product\ImageController@rotate')->name('backend.products.image.rotate');

            Route::any('/merge', 'Backend\Product\MergeController@index')->name('backend.products.merge.index')->middleware('permission:products.merge.index');
            Route::post('/merge/make', 'Backend\Product\MergeController@make')->name('backend.products.merge.make')->middleware('permission:products.merge.index');
            Route::any('/merge/edit/{id}', 'Backend\Product\MergeController@edit')->name('backend.products.merge.edit')->middleware('permission:products.merge.index');
            Route::get('/merge/detail/product', 'Backend\Product\MergeController@detailProduct')->name('backend.products.merge.detailProduct')->middleware('permission:products.merge.index');

            Route::any('/censorship', 'Backend\Product\CensorController@index')->name('backend.products.censorship.index')->middleware('permission:products.censorship.index');
            Route::any('/censorship/edit/{id}', 'Backend\Product\CensorController@index')->name('backend.products.censorship.edit')->middleware('permission:products.censorship.edit');

//            Route::any('/{id}', 'Backend\ProductsController@detail')->name('backend.products.detail');

            Route::group(['prefix' => 'type'], function () {
                Route::any('/', 'Backend\Product\TypeController@index')->name('backend.products.type.index')->middleware('permission:products.type.index');
                Route::any('/add', 'Backend\Product\TypeController@add')->name('backend.products.type.add')->middleware('permission:products.type.add');
                Route::any('/edit/{id}', 'Backend\Product\TypeController@edit')->name('backend.products.type.edit')->middleware('permission:products.type.edit');
                Route::post('/delete', 'Backend\Product\TypeController@delete')->name('backend.products.type.del')->middleware('permission:products.type.del');
                Route::any('/sort', 'Backend\Product\TypeController@sort')->name('backend.products.type.sort')->middleware('permission:products.type.index');
            });
        });

        Route::any('/task', 'Backend\TaskController@index')->name('backend.task.index');
        Route::any('/task/add', 'Backend\TaskController@add')->name('backend.task.add');

        Route::any('/notification', 'Backend\NotificationController@index')->name('backend.notification.index')->middleware('permission:notification.index');
        Route::any('/notification/add', 'Backend\NotificationController@add')->name('backend.notification.add')->middleware('permission:notification.add');

        Route::group(['prefix' => 'location'], function () {
            Route::any('/province', 'Backend\Location\ProvinceController@index')->name('backend.location.province.index')->middleware('permission:province.index');
            Route::any('/province/add', 'Backend\Location\ProvinceController@add')->name('backend.location.province.add')->middleware('permission:province.add');
            Route::any('/province/edit/{id}', 'Backend\Location\ProvinceController@edit')->name('backend.location.province.edit')->middleware('permission:province.edit');
            Route::any('/province/del/{id}', 'Backend\Location\ProvinceController@delete')->name('backend.location.province.del')->middleware('permission:province.del');

            Route::any('/district', 'Backend\Location\DistrictController@index')->name('backend.location.district.index')->middleware('permission:district.index');
            Route::any('/district/add', 'Backend\Location\DistrictController@add')->name('backend.location.district.add')->middleware('permission:district.add');
            Route::any('/district/edit/{id}', 'Backend\Location\DistrictController@edit')->name('backend.location.district.edit')->middleware('permission:district.edit');
            Route::any('/district/del/{id}', 'Backend\Location\DistrictController@delete')->name('backend.location.district.del')->middleware('permission:district.del');

            Route::any('/ward', 'Backend\Location\WardController@index')->name('backend.location.ward.index')->middleware('permission:ward.index');
            Route::any('/ward/add', 'Backend\Location\WardController@add')->name('backend.location.ward.add')->middleware('permission:ward.add');
            Route::any('/ward/edit/{id}', 'Backend\Location\WardController@edit')->name('backend.location.ward.edit')->middleware('permission:ward.edit');
            Route::any('/ward/del/{id}', 'Backend\Location\WardController@delete')->name('backend.location.ward.del')->middleware('permission:ward.del');

            Route::any('/street', 'Backend\Location\StreetController@index')->name('backend.location.street.index')->middleware('permission:street.index');
            Route::any('/street/add', 'Backend\Location\StreetController@add')->name('backend.location.street.add')->middleware('permission:street.add');
            Route::any('/street/edit/{id}', 'Backend\Location\StreetController@edit')->name('backend.location.street.edit')->middleware('permission:street.edit');
            Route::any('/street/del/{id}', 'Backend\Location\StreetController@delete')->name('backend.location.street.del')->middleware('permission:street.del');
            Route::any('/street/ajax-edit', 'Backend\Location\StreetController@ajaxEdit')->name('backend.location.street.ajax-edit');
        });

        Route::group(['prefix' => 'info'], function () {
            Route::any('/convenience', 'Backend\Info\ConvenienceController@index')->name('backend.info.convenience.index')->middleware('permission:convenience.index');
            Route::any('/convenience/add', 'Backend\Info\ConvenienceController@add')->name('backend.info.convenience.add')->middleware('permission:convenience.add');
            Route::any('/convenience/edit/{id}', 'Backend\Info\ConvenienceController@edit')->name('backend.info.convenience.edit')->middleware('permission:convenience.edit');
            Route::any('/convenience/del/{id}', 'Backend\Info\ConvenienceController@delete')->name('backend.info.convenience.del')->middleware('permission:convenience.del');

            Route::any('/exterior', 'Backend\Info\ExteriorController@index')->name('backend.info.exterior.index')->middleware('permission:exterior.index');
            Route::any('/exterior/add', 'Backend\Info\ExteriorController@add')->name('backend.info.exterior.add')->middleware('permission:exterior.add');
            Route::any('/exterior/edit/{id}', 'Backend\Info\ExteriorController@edit')->name('backend.info.exterior.edit')->middleware('permission:exterior.edit');
            Route::any('/exterior/del/{id}', 'Backend\Info\ExteriorController@delete')->name('backend.info.exterior.del')->middleware('permission:exterior.del');
        });

        //ajax
        Route::group(['prefix' => 'ajax'], function () {
            Route::any('/search-user', 'Backend\AjaxController@searchUser')->name('backend.ajax.searchUser');
            Route::post('/add-street', 'Backend\AjaxController@addStreet')->name('backend.ajax.addStreet');
            Route::post('/upload-image', 'Backend\AjaxController@uploadImage')->name('backend.ajax.uploadImage');
            Route::post('/remove-image', 'Backend\AjaxController@removeImage')->name('backend.ajax.removeImage');
        });

        Route::group(['prefix' => 'post'], function () {
            Route::any('/', 'Backend\PostsController@index')->name('backend.posts.index')->middleware('permission:posts.index');
            Route::any('/add', 'Backend\PostsController@add')->name('backend.posts.add')->middleware('permission:posts.add');
            Route::any('/edit/{id}', 'Backend\PostsController@edit')->name('backend.posts.edit')->middleware('permission:posts.edit');
            Route::any('/delete/{id}', 'Backend\PostsController@delete')->name('backend.posts.del')->middleware('permission:posts.del');

            Route::group(['prefix' => 'category'], function () {
                Route::any('/', 'Backend\PostsCategoryController@index')->name('backend.posts.category.index')->middleware('permission:posts.category.index');
                Route::any('/add', 'Backend\PostsCategoryController@add')->name('backend.posts.category.add')->middleware('permission:posts.category.add');
                Route::any('/edit/{id}', 'Backend\PostsCategoryController@edit')->name('backend.posts.category.edit')->middleware('permission:posts.category.edit');
                Route::any('/delete/{id}', 'Backend\PostsCategoryController@delete')->name('backend.posts.category.del')->middleware('permission:posts.category.del');
            });
        });

        Route::group(['prefix' => 'banner'], function () {
            Route::any('', 'Backend\BannerController@index')->name('backend.banners.index');
            Route::any('/add', 'Backend\BannerController@add')->name('backend.banners.add');
            Route::any('/edit/{id}', 'Backend\BannerController@edit')->name('backend.banners.edit');
            Route::any('/delete/{id}', 'Backend\BannerController@del')->name('backend.banners.del');
        });

        Route::group(['prefix' => 'setting'], function () {
            Route::any('/', 'Backend\SettingController@index')->name('backend.setting.index');
        });

    });

});