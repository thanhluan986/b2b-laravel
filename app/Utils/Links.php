<?php
/**
 * Created by PhpStorm.
 * User: ThanhLuan
 * Date: 16/06/2018
 * Time: 10:28 CH
 */

namespace App\Utils;

class Links
{
    public static function ImageLink($path = null)
    {
        if (!$path) {
            return null;
        }
        return config('constants.upload_dir.url') . '/' . $path;
    }

    public static function CategoryLink(array $category)
    {
        return $category['link'] . '.' . $category['id'];
    }
}