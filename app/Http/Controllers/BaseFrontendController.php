<?php

namespace App\Http\Controllers;

use App\Models\Banners;
use App\Models\Category;
use App\Models\Settings;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;


class BaseFrontendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $_ref;
    public $_data = [];
    public $_isLogged;
    public $_userData = null;

    public function __construct()
    {
        $this->_ref = Request()->get('_ref', null);

        View::share('title', '');
        View::share('description', '');
        View::share('keywords', '');
        View::share('author', '');
        View::share('current_url', \App\Utils\Common::J_getCurUrl());

        $settings = Settings::get_all();

        foreach ($settings as $k => $v) {
            View::share($k, $v['setting_value']);
        }

        $array_categories = Category::get_all();

        $this->_data['array_categories'] = $array_categories;

        $array_tree_categories = \App\Utils\Category::buildTree($array_categories);

        $this->_data['array_tree_categories'] = $array_tree_categories;

        $this->_data['banners_promo'] = Banners::with(['image'])
            ->where('status', Banners::STATUS_SHOW)
            ->where('type', 'promo')
            ->limit(3)->get();

        View::share('is_logged', null);

        View::share('user_data', null);
    }

    public function isLogged()
    {
        return Auth()->guard('frontend')->check();
    }

    public function getUser()
    {
        return Auth()->guard('frontend')->check() ? Auth()->guard('frontend')->user() : null;
    }

    public static function returnResult($data = [], $message = null)
    {
        return Response()->json([
            'status'  => true,
            'code'    => 200,
            'data'    => $data,
            'message' => $message,
        ]);
    }

    public static function throwError($errors = 'error', $code = 400)
    {
        header('Content-type: application/json');
        echo json_encode([
            'status'  => false,
            'code'    => $code,
            'data'    => null,
            'message' => $errors,
        ]);
        exit;
    }
}
