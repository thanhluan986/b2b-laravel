<?php

namespace App\Http\Controllers;

use http\Url;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;

class BaseBackendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $_ref;

    public function __construct()
    {
        $this->_ref = Request()->get('_ref', null);

        View::share('title', '');
        View::share('description', '');
        View::share('keywords', '');
        View::share('author', '');
        View::share('current_url', url()->current());

    }
}
