<?php

namespace App\Http\Controllers;

use App\Models\CoreUsers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class BaseAPIController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\OpenApi(
     *     @OA\Info(
     *         version="1.0",
     *         title="B2B API Documentation",
     *         description="https://b2b.thietke24h.com/api/v1.0",
     *         termsOfService="https://thietke24h.com",
     *         @OA\Contact(
     *             email="hotro@thietke24h.com"
     *         ),
     *     ),
     *     @OA\Server(
     *         description="Live server",
     *         url="https://b2b.thietke24h.com/api/v1.0"
     *     ),
     *     @OA\Server(
     *         description="Local server",
     *         url="http://b2b.com/api/v1.0"
     *     ),
     *     @OA\ExternalDocumentation(
     *         description="Find out more about B2B",
     *         url="https://b2b.vn/"
     *     )
     * )
     */

    public function getAuthenticatedUser($check_registered = true)
    {
        $request = request(['token']);

        if (empty($request['token'])) {
            return $this->throwError('Empty Token', 400);
        }

        try {
            $user = auth('api')->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return $this->throwError('Token không hợp lệ', 401);
        }

        if ($user->status == CoreUsers::$status_banned) {
            return $this->throwError('Tài khoản đã bị khóa!', 403);
        }
        return $user;
    }

    public static function returnResult($data = [], $message = null)
    {
        return Response()->json([
            'status'  => true,
            'code'    => 200,
            'data'    => $data,
            'message' => $message,
        ]);
    }

    public static function throwError($errors = 'error', $code = 400)
    {
        header('Content-type: application/json');
        echo json_encode([
            'status'  => false,
            'code'    => $code,
            'data'    => null,
            'message' => $errors,
        ]);
        exit;
    }
}
