<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api\Store;

use App\Classes\ActivationService;
use App\Mail\OTPEmail;
use App\Models\CoreUsers;
use App\Http\Controllers\BaseAPIController;
use App\Models\CoreUsersActivation;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use Illuminate\Validation\Rule;
use App\Utils\File;
use App\Utils\FbAccountKit;
use Illuminate\Support\Facades\Hash;
use App\Models\Files;
use App\Utils\Avatar;
use Mail;

class IndexController extends BaseAPIController
{
    public function info()
    {
        return $this->returnResult();
    }

    public function update(Request $request)
    {
        return $this->returnResult();
    }
}