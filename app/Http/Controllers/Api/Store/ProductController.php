<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\BaseAPIController;
use App\Jobs\InsertViewedProduct;
use App\Models\Convenience;
use App\Models\Files;
use App\Models\Exterior;
use App\Models\FrontageType;
use App\Models\HistoryView;
use App\Models\Product;
use App\Models\ProductContactInfo;
use App\Models\ProductConvenience;
use App\Models\ProductFrontageType;
use App\Models\ProductImages;
use App\Models\ProductImagesExtra;
use App\Models\ProductExterior;
use App\Models\ProductNote;
use App\Models\ProductPurpose;
use App\Models\ProductVariation;
use App\Models\ProductWholesales;
use App\Models\VariationCombine;
use App\Models\Variations;
use App\Models\VariationValues;
use App\Models\Purpose;
use App\Utils\File;
use App\Utils\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Validation\Rule;
use App\Models\Location\District;
use App\Models\Location\Province;
use App\Models\Location\Street;
use App\Models\Location\Ward;

class ProductController extends BaseAPIController
{
    /**
     * @OA\Get(
     *     path="/store/product",
     *     tags={"store role"},
     *     summary="Get all products",
     *     description="",
     *     operationId="StoreProductGetAll",
     *     @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter(name="category_ids[]", description="Danh sách id danh mục, ex: [1,2,3]", required=false, in="query",
     *         @OA\Schema( type="array", @OA\Items())
     *     ),
     *     @OA\Parameter(name="store_category_ids[]", description="Danh sách id danh mục của cửa hàng, ex: [1,2,3]", required=false, in="query",
     *         @OA\Schema( type="array", @OA\Items())
     *     ),
     *     @OA\Parameter( name="status", description="status: 0: ẩn, 1:hiện", required=false, in="query",
     *         @OA\Schema( enum="0,1", )
     *     ),
     *     @OA\Parameter( name="limit", description="limit", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Parameter( name="page", description="page", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function getAll(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $aInit = [
            'category_ids'       => null,
            'store_category_ids' => null,
            'status'             => null,
            'keywords'           => null,
            'price_from'         => null,
            'price_to'           => null,
            'skip_product_ids'   => null,
            'limit'              => null,
            'sort'               => null,
        ];

        $params = array_merge(
            $aInit, $request->only(array_keys($aInit))
        );

        $validator = Validator::make($request->all(), [
            'category_ids'       => 'nullable|array',
            'store_category_ids' => 'nullable|array',
            'status'             => 'nullable|in:0,1',
            'keywords'           => 'nullable|string',
            'skip_product_ids'   => 'nullable|string',
            'price_from'         => 'nullable|integer|min:0',
            'price_to'           => 'nullable|integer|min:0',
            'limit'              => 'nullable|integer|min:1',
            'sort'               => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $params['sort'] = $request->get('sort', 'newest');
        $params = array_filter($params);
        $params['token'] = $request->get('token');
        $params['pagin_path'] = route('store.product.getAll') . '?' . http_build_query($params);

        $params['user_id'] = $user->id;
        $products = Product::get_by_where($params);

        return $this->returnResult($products);
    }

    /**
     * @OA\Get(
     *     path="/store/product/{id}",
     *     tags={"store role"},
     *     summary="Get product by id",
     *     description="",
     *     operationId="StoreProductGetByID",
     *     @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter( name="id", description="id", required=true, in="path",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function detail($id)
    {
        $user = $this->getAuthenticatedUser();

        $product = Product::with(['category', 'store_category', 'thumbnail', 'images', 'images_extra',])
            ->where('id', $id)
            ->where('user_id', $user->id)
            ->first();

        if (empty($product))
            return $this->throwError('Sản phẩm không tồn tại!', 404);

        $variations = VariationValues::get_group_variations($id);

        $product_variations = [];
        $variation_combine = [];

        if (count($variations))
            $product_variations = ProductVariation::get_product_variations_with_combine($id);

        $variations = collect(['variations' => $variations]);
        $product_variations = collect(['product_variations' => $product_variations]);

        $product_wholesales = ProductWholesales::select(['id', 'quantity_from', 'quantity_to', 'price'])->where('product_id', $id)->get();
        $product_wholesales = collect(['wholesales' => $product_wholesales]);

        $product = collect($product);

        $product = $product->merge($variations)
            ->merge($product_variations)
            ->merge($variation_combine)
            ->merge($product_wholesales);

        return $this->returnResult($product);
    }

    /**
     * @OA\Post(
     *     path="/store/product",
     *     tags={"store role"},
     *     summary="Add product",
     *     description="",
     *     operationId="StoreAddProduct",
     *      @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
     *			    @OA\Property(property="name", description="Tên sản phẩm", type="string", ),
     *			    @OA\Property(property="description", description="Mô tả sản phẩm", type="string", ),
     *			    @OA\Property(property="category_id", description="Danh mục", type="integer", ),
     *
     *			    @OA\Property(property="origin", description="Xuất xứ", type="string", ),
     *			    @OA\Property(property="factory_address", description="Địa chỉ sản xuất", type="string", ),
     *
     *			    @OA\Property(property="price", description="Giá", type="integer", ),
     *			    @OA\Property(property="inventory", description="SL tồn kho", type="integer", ),
     *
     *			    @OA\Property(property="variations", description="Khởi tạo phân loại hàng (cho phép tối đa 2 phân loại)", type="array",
     *                  @OA\Items(
     *			            @OA\Property(property="variation_name", description="Tên phân loại. Vd: Màu sắc", type="string", ),
     *			            @OA\Property(property="items", description="ds giá trị", type="array",
     *                          @OA\Items(
     *			                    @OA\Property(property="variation_value_id", description="variation_value_id - client tự khởi tạo", type="integer", ),
     *			                    @OA\Property(property="variation_value", description="Giá trị phân loại. Vd: Xanh", type="string", ),
     *                          )
     *                      ),
     *                  )
     *              ),
     *
     *			    @OA\Property(property="product_variations", description="Phân loại hàng", type="array",
     *                  @OA\Items(
     *			            @OA\Property(property="product_variation_id", description="product_variation_id - client tự khởi tạo", type="integer", ),
     *			            @OA\Property(property="name", description="name", type="string", ),
     *			            @OA\Property(property="price", description="Giá", type="integer", ),
     *			            @OA\Property(property="sku", description="SKU sản phẩm", type="string", ),
     *			            @OA\Property(property="inventory", description="SL tồn kho", type="integer", ),
     *        			    @OA\Property(property="list_variation_value_id_combine", description="Array variation_value_id. vd: [1,2]", type="array",
     *                          @OA\Items()
     *                      ),
     *                  )
     *              ),
     *
     *			    @OA\Property(property="wholesales", description="SL - giá bán sỉ", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="quantity_from", description="Số lượng nhỏ nhất", type="integer", ),
     *			            @OA\Property(property="quantity_to", description="Số lượng lớn nhất", type="integer", ),
     *			            @OA\Property(property="price", description="Giá bán", type="integer", ),
     *                  )
     *              ),
     *
     *			    @OA\Property(property="weight", description="Cân nặng (gram)", type="integer", ),
     *			    @OA\Property(property="width", description="Dài (cm)", type="integer", ),
     *			    @OA\Property(property="length", description="Rộng (cm)", type="integer", ),
     *			    @OA\Property(property="height", description="Cao (cm)", type="integer", ),
     *
     *			    @OA\Property(property="must_pre_order", description="Đặt hàng trước 0|1", type="integer", ),
     *			    @OA\Property(property="pre_order_time", description="Số ngày chờ đặt hàng", type="integer", ),
     *			    @OA\Property(property="is_used_product", description="Sản phẩm Mới/Đã sử dụng - 0|1", type="integer", ),
     *			    @OA\Property(property="sku", description="SKU sản phẩm", type="string", ),
     *
     *			    @OA\Property(property="status", description="Hiển thị, ẩn sản phẩm. 1|0", type="integer", ),
     *
     *			    @OA\Property(property="image_file_ids", description="Array file_id hình ảnh. vd: [1,2]", type="array",
     *                  @OA\Items()
     *              ),
     *			    @OA\Property(property="thumbnail_file_id", description="file_id hình đại diện", type="integer", ),
     *
     *              required={"name","description","category_id","price","inventory","weight","width","length","height","image_file_ids","thumbnail_file_id"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function add(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        Validator::extend('file_ids_exists', function ($attribute, $value, $parameters, $validator) use ($user) {
            $count = DB::table('lck_files')->whereIn('id', $value)
                ->where('user_id', $user->id)
                //->where('is_temp', Files::IS_TEMP)
                ->where('type', Files::TYPE_PRODUCT)
                ->count();
            return $count !== 0;
        }, 'The image_file_ids invalid!');

        $validate_rule = [
            'name'        => 'required|string|min:10|max:255',
            'description' => 'required|string|max:3000',
            'category_id' => "required|bail|integer|exists:lck_category,id",

            'origin'          => "nullable|bail|string|max:255",
            'factory_address' => "nullable|bail|string|max:255",

            'price'     => 'required|bail|integer|min:1',
            'inventory' => 'required|bail|integer|min:1',

            'variations'         => 'nullable|bail|array',
            'product_variations' => 'nullable|bail|array',
            'wholesales'         => 'nullable|bail|array',

            'weight' => 'required|bail|integer|min:0',
            'width'  => 'required|bail|integer|min:0',
            'length' => 'required|bail|integer|min:0',
            'height' => 'required|bail|integer|min:0',

            'must_pre_order'  => 'required|bail|in:0,1',
            'pre_order_time'  => 'nullable|bail|integer|min:0',
            'is_used_product' => 'required|bail|in:0,1',
            'sku'             => 'nullable|bail|string|max:200',
            'status'          => 'required|bail|integer|in:0,1',

            'image_file_ids'    => "required|bail|array|file_ids_exists",
            'thumbnail_file_id' => "required|bail|integer",
        ];

        $validator = Validator::make($request->all(), $validate_rule);
        if ($validator->fails())
            return $this->throwError($validator->errors()->first(), 400);

        $params = array_fill_keys(array_keys($validate_rule), null);
        $params = array_merge(
            $params, $request->only(array_keys($params))
        );

        $variations = $product_variations = $wholesales = $image_file_ids = [];
        if (!empty($params['variations'])) {
            if (!Variations::validation_variations($params['variations']))
                return $this->throwError('Khởi tạo phân loại hàng không hợp lệ!', 400);

            if (count($params['variations']) > 2)
                return $this->throwError('Khởi tạo phân loại hàng chỉ cho phép tối đa 2 phân loại!', 400);

            if (empty($params['product_variations']) || count($params['product_variations']) < 1)
                return $this->throwError('Phân loại hàng không được trống!', 400);

            if (!VariationCombine::validation_product_variations($params['product_variations'], $params['variations']))
                return $this->throwError('Phân loại hàng không hợp lệ!', 400);

            $variations = $params['variations'];
            $product_variations = $params['product_variations'];
        }

        if (!empty($params['wholesales'])) {
            if (!ProductWholesales::validation_wholesales($params['wholesales']))
                return $this->throwError('Khoảng SL - Giá sỉ không hợp lệ!', 400);

            $wholesales = $params['wholesales'];

        }
        unset($params['variations'], $params['product_variations'], $params['wholesales']);

        $image_file_ids = $params['image_file_ids'];
        unset($params['image_file_ids']);

        try {
            DB::beginTransaction();

            $params['user_id'] = $user->id;

            $product = Product::create($params);

            $product_id = $product->id;

            if ($product_id) {
                $ProductImages = [];
                foreach ($image_file_ids as $k => $v) {
                    $ProductImages[] = [
                        'product_id' => $product_id,
                        'file_id'    => $v,
                        'priority'   => $k,
                    ];
                }
                ProductImages::insert($ProductImages);

                foreach ($wholesales as $k => $v) {
                    $wholesales[$k]['product_id'] = $product_id;
                }
                if ($wholesales)
                    ProductWholesales::insert($wholesales);

                $variation_ids = $variation_value_id = $product_variation_id = [];
                foreach ($variations as $v) {
                    $variation_id = Variations::create([
                        'name'       => $v['variation_name'],
                        'user_id'    => $user->id,
                        'product_id' => $product_id,
                    ])->id;
                    $variation_ids[] = $variation_id;

                    foreach ($v['items'] as $v2) {
                        $variation_value_id[$v2['variation_value_id']] = VariationValues::create([
                            'variation_id' => $variation_id,
                            'value'        => $v2['variation_value'],
                            'product_id'   => $product_id,
                        ])->id;
                    }
                }

                foreach ($product_variations as $v) {
                    $product_variation_id[$v['product_variation_id']] = ProductVariation::create([
                        'product_id' => $product_id,
                        'name'       => isset($v['name']) ? $v['name'] : null,
                        'price'      => $v['price'],
                        'sku'        => isset($v['sku']) ? $v['sku'] : null,
                        'inventory'  => $v['inventory'],
                    ])->id;
                }

                $VariationCombine = [];
                foreach ($product_variations as $v) {
                    foreach ($v['list_variation_value_id_combine'] as $v2) {
                        $VariationCombine[] = [
                            'product_variation_id' => $product_variation_id[$v['product_variation_id']],
                            'variation_value_id'   => $variation_value_id[$v2]
                        ];
                    }
                }

                VariationCombine::insert($VariationCombine);
            }

            DB::commit();
            return $this->returnResult();
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::debug($e->getMessage());
            //return $this->throwError('Có lỗi xảy ra, vui lòng thử lại!', 500);
            return $this->throwError($e->getMessage(), 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/store/product/{product_id}",
     *     tags={"store role"},
     *     summary="Update product",
     *     description="",
     *     operationId="StoreUpdateProduct",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter(name="product_id", description="product id", required=true, in="path",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
     *			    @OA\Property(property="name", description="Tên sản phẩm", type="string", ),
     *			    @OA\Property(property="description", description="Mô tả sản phẩm", type="string", ),
     *			    @OA\Property(property="category_id", description="Danh mục", type="integer", ),
     *
     *			    @OA\Property(property="origin", description="Xuất xứ", type="string", ),
     *			    @OA\Property(property="factory_address", description="Địa chỉ sản xuất", type="string", ),
     *
     *			    @OA\Property(property="price", description="Giá", type="integer", ),
     *			    @OA\Property(property="inventory", description="SL tồn kho", type="integer", ),
     *
     *			    @OA\Property(property="variations", description="Khởi tạo phân loại hàng (cho phép tối đa 2 phân loại)", type="array",
     *                  @OA\Items(
     *			            @OA\Property(property="variation_name", description="Tên phân loại. Vd: Màu sắc", type="string", ),
     *			            @OA\Property(property="items", description="ds giá trị", type="array",
     *                          @OA\Items(
     *			                    @OA\Property(property="variation_value_id", description="variation_value_id - client tự khởi tạo", type="integer", ),
     *			                    @OA\Property(property="variation_value", description="Giá trị phân loại. Vd: Xanh", type="string", ),
     *                          )
     *                      ),
     *                  )
     *              ),
     *
     *			    @OA\Property(property="product_variations", description="Phân loại hàng", type="array",
     *                  @OA\Items(
     *			            @OA\Property(property="product_variation_id", description="product_variation_id - client tự khởi tạo", type="integer", ),
     *			            @OA\Property(property="name", description="name", type="string", ),
     *			            @OA\Property(property="price", description="Giá", type="integer", ),
     *			            @OA\Property(property="sku", description="SKU sản phẩm", type="string", ),
     *			            @OA\Property(property="inventory", description="SL tồn kho", type="integer", ),
     *        			    @OA\Property(property="list_variation_value_id_combine", description="Array variation_value_id. vd: [1,2]", type="array",
     *                          @OA\Items()
     *                      ),
     *                  )
     *              ),
     *
     *			    @OA\Property(property="wholesales", description="SL - giá bán sỉ", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="quantity_from", description="Số lượng nhỏ nhất", type="integer", ),
     *			            @OA\Property(property="quantity_to", description="Số lượng lớn nhất", type="integer", ),
     *			            @OA\Property(property="price", description="Giá bán", type="integer", ),
     *                  )
     *              ),
     *
     *			    @OA\Property(property="weight", description="Cân nặng (gram)", type="integer", ),
     *			    @OA\Property(property="width", description="Dài (cm)", type="integer", ),
     *			    @OA\Property(property="length", description="Rộng (cm)", type="integer", ),
     *			    @OA\Property(property="height", description="Cao (cm)", type="integer", ),
     *
     *			    @OA\Property(property="must_pre_order", description="Đặt hàng trước 0|1", type="integer", ),
     *			    @OA\Property(property="pre_order_time", description="Số ngày chờ đặt hàng", type="integer", ),
     *			    @OA\Property(property="is_used_product", description="Sản phẩm Mới/Đã sử dụng - 0|1", type="integer", ),
     *			    @OA\Property(property="sku", description="SKU sản phẩm", type="string", ),
     *
     *			    @OA\Property(property="status", description="Hiển thị, ẩn sản phẩm. 1|0", type="integer", ),
     *
     *			    @OA\Property(property="image_file_ids", description="Array file_id hình ảnh. vd: [1,2]", type="array",
     *                  @OA\Items()
     *              ),
     *			    @OA\Property(property="thumbnail_file_id", description="file_id hình đại diện", type="integer", ),
     *
     *              required={"name","description","category_id","price","inventory","weight","width","length","height","image_file_ids","thumbnail_file_id"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function update($product_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $product = Product::where('id', $product_id)
            ->where('user_id', $user->id)->first();

        if (empty($product))
            return $this->throwError('Sản phẩm không tồn tại', 404);

        Validator::extend('file_ids_exists', function ($attribute, $value, $parameters, $validator) use ($user) {
            $count = DB::table('lck_files')->whereIn('id', $value)
                ->where('user_id', $user->id)
                //->where('is_temp', Files::IS_TEMP)
                ->where('type', Files::TYPE_PRODUCT)
                ->count();
            return $count !== 0;
        }, 'The image_file_ids invalid!');

        $validate_rule = [
            'name'        => 'required|string|min:10|max:255',
            'description' => 'required|string|max:3000',
            'category_id' => "required|bail|integer|exists:lck_category,id",

            'origin'          => "nullable|bail|string|max:255",
            'factory_address' => "nullable|bail|string|max:255",

            'price'     => 'required|bail|integer|min:1',
            'inventory' => 'required|bail|integer|min:1',

            'variations'         => 'nullable|bail|array',
            'product_variations' => 'nullable|bail|array',
            'wholesales'         => 'nullable|bail|array',

            'weight' => 'required|bail|integer|min:0',
            'width'  => 'required|bail|integer|min:0',
            'length' => 'required|bail|integer|min:0',
            'height' => 'required|bail|integer|min:0',

            'must_pre_order'  => 'required|bail|in:0,1',
            'pre_order_time'  => 'nullable|bail|integer|min:0',
            'is_used_product' => 'required|bail|in:0,1',
            'sku'             => 'nullable|bail|string|max:200',
            'status'          => 'required|bail|integer|in:0,1',

            'image_file_ids'    => "required|bail|array|file_ids_exists",
            'thumbnail_file_id' => "required|bail|integer",
        ];

        $validator = Validator::make($request->all(), $validate_rule);
        if ($validator->fails())
            return $this->throwError($validator->errors()->first(), 400);

        $params = array_fill_keys(array_keys($validate_rule), null);
        $params = array_merge(
            $params, $request->only(array_keys($params))
        );

        $variations = $product_variations = $wholesales = $image_file_ids = [];
        if (!empty($params['variations'])) {
            if (!Variations::validation_variations($params['variations']))
                return $this->throwError('Khởi tạo phân loại hàng không hợp lệ!', 400);

            if (count($params['variations']) > 2)
                return $this->throwError('Khởi tạo phân loại hàng chỉ cho phép tối đa 2 phân loại!', 400);

            if (empty($params['product_variations']) || count($params['product_variations']) < 1)
                return $this->throwError('Phân loại hàng không được trống!', 400);

            if (!VariationCombine::validation_product_variations($params['product_variations'], $params['variations']))
                return $this->throwError('Phân loại hàng không hợp lệ!', 400);

            $variations = $params['variations'];
            $product_variations = $params['product_variations'];
        }

        if (!empty($params['wholesales'])) {
            if (!ProductWholesales::validation_wholesales($params['wholesales']))
                return $this->throwError('Khoảng SL - Giá sỉ không hợp lệ!', 400);

            $wholesales = $params['wholesales'];
        }

        unset($params['variations'], $params['product_variations'], $params['wholesales']);

        $image_file_ids = $params['image_file_ids'];
        unset($params['image_file_ids']);

        try {
            DB::beginTransaction();

            Product::where('id', $product_id)->update($params);

            ProductImages::where('product_id', $product_id)->delete();
            $ProductImages = [];
            foreach ($image_file_ids as $k => $v) {
                $ProductImages[] = [
                    'product_id' => $product_id,
                    'file_id'    => $v,
                    'priority'   => $k,
                ];
            }
            ProductImages::insert($ProductImages);

            ProductWholesales::where('product_id', $product_id)->delete();
            foreach ($wholesales as $k => $v) {
                $wholesales[$k]['product_id'] = $product_id;
            }
            if ($wholesales)
                ProductWholesales::insert($wholesales);

            Variations::where('product_id', $product_id)->delete();
            $variation_ids = $variation_value_id = $product_variation_id = [];
            foreach ($variations as $v) {
                $variation_id = Variations::create([
                    'name'       => $v['variation_name'],
                    'user_id'    => $user->id,
                    'product_id' => $product_id,
                ])->id;
                $variation_ids[] = $variation_id;

                foreach ($v['items'] as $v2) {
                    $variation_value_id[$v2['variation_value_id']] = VariationValues::create([
                        'variation_id' => $variation_id,
                        'value'        => $v2['variation_value'],
                        'product_id'   => $product_id,
                    ])->id;
                }
            }

            ProductVariation::where('product_id', $product_id)->delete();
            foreach ($product_variations as $v) {
                $product_variation_id[$v['product_variation_id']] = ProductVariation::create([
                    'product_id' => $product_id,
                    'name'       => isset($v['name']) ? $v['name'] : null,
                    'price'      => $v['price'],
                    'sku'        => isset($v['sku']) ? $v['sku'] : null,
                    'inventory'  => $v['inventory'],
                ])->id;
            }

            $VariationCombine = [];
            foreach ($product_variations as $v) {
                foreach ($v['list_variation_value_id_combine'] as $v2) {
                    $VariationCombine[] = [
                        'product_variation_id' => $product_variation_id[$v['product_variation_id']],
                        'variation_value_id'   => $variation_value_id[$v2]
                    ];
                }
            }

            VariationCombine::insert($VariationCombine);

            DB::commit();
            return $this->returnResult();
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::debug($e->getMessage());
            //return $this->throwError('Có lỗi xảy ra, vui lòng thử lại!', 500);
            return $this->throwError($e->getMessage(), 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/store/product/delete",
     *     tags={"store role"},
     *     summary="Delete product",
     *     description="",
     *     operationId="StoreDeleteProduct",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			    @OA\Property(property="product_ids", description="list product id: ex 1,2,3", type="string", ),
     *              required={"product_ids",}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function delete(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $product_ids = $request->get('product_ids');
        if (empty($product_ids))
            return $this->throwError('Mã sản phẩm không được trống!');

        $product_ids = explode(',', $product_ids);

        $products = Product::whereIn('id', $product_ids)
            ->where('user_id', $user->id)
            ->get();

        if (!count($products))
            return $this->throwError('Sản phẩm không tồn tại!', 404);

        Product::whereIn('id', $product_ids)
            ->where('user_id', $user->id)
            ->delete();

        return $this->returnResult();
    }
}