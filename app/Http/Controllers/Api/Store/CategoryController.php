<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\BaseAPIController;
use App\Models\Product;
use App\Models\StoreCategory;
use App\Utils\Filter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class CategoryController extends BaseAPIController
{
    /**
     * @OA\Get(
     *   path="/store/category",
     *   tags={"store role"},
     *   summary="Get all category of store",
     *   description="",
     *   operationId="StoreCategoryGetAll",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *   @OA\Response(response="200", description="Success operation"),
     *   @OA\Response(response="500", description="Server error")
     * )
     */
    public function getAll()
    {
        $user = $this->getAuthenticatedUser();
        $category = StoreCategory::select(['id', 'name'])->where('user_id', $user->id)->get();
        return $this->returnResult($category);
    }

    /**
     * @OA\Post(
     *     path="/store/category",
     *     tags={"store role"},
     *     summary="Add category to store",
     *     description="",
     *     operationId="StoreCategoryAdd",
     *      @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			    @OA\Property(property="name", description="category name", type="string", ),
     *			    @OA\Property(property="description", description="category description", type="string", ),
     *              required={"name",}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function add(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validate_rule = [
            'name'        => "bail|required|string|max:50",
            'description' => "bail|nullable|string|max:255",
        ];

        $validator = Validator::make($request->all(), $validate_rule);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $category_name = $request->get('name');
        $category = StoreCategory::where('name', $category_name)->first();

        if ($category)
            return $this->throwError('Danh mục đã tồn tại!', 400);

        $category = StoreCategory::create(
            [
                'name'        => $category_name,
                'slug'        => Filter::setSeoLink($category_name),
                'description' => $request->get('description'),
                'status'      => 1,
                'user_id'     => $user->id
            ]
        );

        return $this->returnResult($category);
    }

    /**
     * @OA\Post(
     *     path="/store/category/{store_category_id}",
     *     tags={"store role"},
     *     summary="Update category of store",
     *     description="",
     *     operationId="StoreCategoryUpdate",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter(name="store_category_id", description="category id of store", required=true, in="path",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			    @OA\Property(property="name", description="category name", type="string", ),
     *			    @OA\Property(property="description", description="category description", type="string", ),
     *              required={"name",}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function update($store_category_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validate_rule = [
            'name'        => "bail|required|string|max:50",
            'description' => "bail|nullable|string|max:255",
            'status'      => "bail|nullable|integer|in:0,1",
        ];

        $validator = Validator::make($request->all(), $validate_rule);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $category = StoreCategory::where('id', $store_category_id)
            ->where('user_id', $user->id)
            ->first();

        if (empty($category))
            return $this->throwError('Danh mục không tồn tại!', 404);

        $category_name = $request->get('name');
        $description = $request->get('description', null);
        $status = $request->get('status', $category->status);

        $category->update(
            [
                'name'        => $category_name,
                'slug'        => Filter::setSeoLink($category_name),
                'description' => $description,
                'status'      => $status,
            ]
        );

        return $this->returnResult($category);
    }

    /**
     * @OA\Delete(
     *     path="/store/category/{store_category_id}",
     *     tags={"store role"},
     *     summary="Delete category of store",
     *     description="",
     *     operationId="StoreCategoryDelete",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter(name="store_category_id", description="category id of store", required=true, in="path",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function delete($store_category_id)
    {
        $user = $this->getAuthenticatedUser();

        $category = StoreCategory::where('id', $store_category_id)
            ->where('user_id', $user->id)
            ->first();

        if (empty($category))
            return $this->throwError('Danh mục không tồn tại!', 404);

        $category->delete();

        return $this->returnResult();
    }

    /**
     * @OA\Post(
     *     path="/store/category/add-product",
     *     tags={"store role"},
     *     summary="Add product to store category",
     *     description="",
     *     operationId="StoreCategoryAddProduct",
     *      @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			    @OA\Property(property="store_category_id", description="category id of store", type="integer", ),
     *			    @OA\Property(property="product_ids", description="list product id: ex 1,2,3", type="string", ),
     *              required={"store_category_id","product_ids"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function addProduct(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validate_rule = [
            'store_category_id' => "bail|required|integer",
            'product_ids'       => "bail|required|string",
        ];

        $validator = Validator::make($request->all(), $validate_rule);

        if ($validator->fails())
            return $this->throwError($validator->errors()->first(), 400);

        $store_category_id = $request->get('store_category_id');
        $product_ids = explode(',', $request->get('product_ids'));

        $category = StoreCategory::where('id', $store_category_id)
            ->where('user_id', $user->id)
            ->first();

        if (empty($category))
            return $this->throwError('Danh mục không tồn tại!', 400);

        Product::whereIn('id', $product_ids)
            ->where('user_id', $user->id)
            ->update([
                'store_category_id' => $store_category_id
            ]);

        return $this->returnResult();
    }

    /**
     * @OA\Post(
     *     path="/store/category/remove-product",
     *     tags={"store role"},
     *     summary="Remove product from store category",
     *     description="",
     *     operationId="StoreCategoryRemoveProduct",
     *      @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			    @OA\Property(property="store_category_id", description="category id of store", type="integer", ),
     *			    @OA\Property(property="product_ids", description="list product id: ex 1,2,3", type="string", ),
     *              required={"store_category_id","product_ids"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function removeProduct(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validate_rule = [
            'store_category_id' => "bail|required|integer",
            'product_ids'       => "bail|required|string",
        ];

        $validator = Validator::make($request->all(), $validate_rule);

        if ($validator->fails())
            return $this->throwError($validator->errors()->first(), 400);

        $store_category_id = $request->get('store_category_id');
        $product_ids = explode(',', $request->get('product_ids'));

        $category = StoreCategory::where('id', $store_category_id)
            ->where('user_id', $user->id)
            ->first();

        if (empty($category))
            return $this->throwError('Danh mục không tồn tại!', 404);

        Product::whereIn('id', $product_ids)
            ->where('user_id', $user->id)
            ->update([
                'store_category_id' => null
            ]);

        return $this->returnResult();
    }
}