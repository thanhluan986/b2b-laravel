<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Convenience;
use App\Models\CoreUsers;
use App\Models\Exterior;
use App\Models\FrontageType;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\Category;
use App\Models\Purpose;
use App\Models\ShippingMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConfigController extends BaseAPIController
{
    /**
     * @OA\Get(
     *   path="/config",
     *   tags={"config"},
     *   summary="Get config app",
     *   description="",
     *   operationId="Config",
     *   @OA\Response(response="200", description="Success operation"),
     *   @OA\Response(response="500", description="Server error")
     * )
     */
    public function index()
    {
        $return = [
            'status' => Product::STATUS,
        ];

        $return['list_sort'] = [
            [
                'id'    => 1,
                'value' => 'recently_changed',
                'name'  => 'Mới cập nhật'
            ],
            [
                'id'    => 2,
                'value' => 'newest',
                'name'  => 'Mới nhất'
            ],
            [
                'id'    => 3,
                'value' => 'oldest',
                'name'  => 'Cũ nhất'
            ],
            [
                'id'    => 4,
                'value' => 'price_low_high',
                'name'  => 'Giá tăng dần'
            ],
            [
                'id'    => 5,
                'value' => 'price_high_low',
                'name'  => 'Giá giảm dần'
            ],
            [
                'id'    => 6,
                'value' => 'status',
                'name'  => 'Trạng thái'
            ],
        ];

        $return['payment_methods'] = PaymentMethod::select(['code', 'name', 'icon',])
            ->where('status', PaymentMethod::STATUS_ACTIVE)
            ->get();

        $return['shipping_methods'] = ShippingMethod::select(['code', 'name', 'icon',])
            ->where('status', ShippingMethod::STATUS_ACTIVE)
            ->get();

        return $this->returnResult($return);
    }
}