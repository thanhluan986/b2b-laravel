<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Banners;
use Illuminate\Http\Request;

class BannerController extends BaseAPIController
{
    /**
     * @OA\Get(
     *   path="/banner",
     *   tags={"user role"},
     *   summary="Get banner",
     *   description="",
     *   operationId="BannerGetAll",
     *     @OA\Parameter( name="category_id", description="id danh mục", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Parameter( name="type", description="store: banner màn hình store", required=false, in="query",
     *         @OA\Schema( enum={"store","home"}, )
     *     ),
     *   @OA\Response(response="200", description="Success operation"),
     *   @OA\Response(response="500", description="Server error")
     * )
     */
    public function getAll(Request $request)
    {
        $category_id = $request->get('category_id', null);
        $type = $request->get('type', 'home');

        $return = Banners::with(['image_app'])->where('status', Banners::STATUS_SHOW);

        if ($category_id > 0)
            $return->where('category_id', $category_id);
        else
            $return->where('type', $type);

        $return = $return->limit(5)->get();
        return $this->returnResult($return);
    }
}