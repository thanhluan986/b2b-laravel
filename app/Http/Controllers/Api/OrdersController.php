<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Address;
use App\Models\Basket;
use App\Models\Convenience;
use App\Models\CoreUsers;
use App\Models\Exterior;
use App\Models\FrontageType;
use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Product;
use App\Models\Category;
use App\Models\Purpose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class OrdersController extends BaseAPIController
{
    /**
     * @OA\Get(
     *   path="/orders",
     *   tags={"user role"},
     *   summary="Get all orders",
     *   description="",
     *   operationId="OrdersGetAll",
     *     @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter( name="limit", description="limit", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Parameter( name="page", description="page", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function getAll(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'page'  => 'nullable|integer|min:1',
            'limit' => 'nullable|integer|min:1',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $params = $request->all();

        $params = array_filter($params);
        $params['token'] = $request->get('token');
        $params['pagin_path'] = route('frontend.user.orders') . '?' . http_build_query($params);

        $params['user_id'] = $user->id;
        $return = Orders::get_by_where($params);

        return $this->returnResult($return);
    }

    /**
     * @OA\Post(
     *   path="/orders",
     *   tags={"user role"},
     *   summary="add orders",
     *   description="",
     *   operationId="OrdersAdd",
     *     @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
     *               @OA\Property(property="basket_item_ids", description="basket_item_ids", type="array", @OA\Items()),
     *			     @OA\Property(property="address_id", description="address_id", type="integer", ),
     *			     @OA\Property(property="payment_method_id", description="payment_method_id", type="integer", ),
     *			     @OA\Property(property="discount_code", description="discount_code", type="string", ),
     *			     @OA\Property(property="notes", description="notes", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="store_id", description="store_id", type="integer", ),
     *			            @OA\Property(property="message", description="message", type="string", )
     *                  )
     *               ),
     *			     @OA\Property(property="shipping_methods", description="notes", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="store_id", description="store_id", type="integer", ),
     *			            @OA\Property(property="shipping_method_id", description="shipping_method_id", type="integer", )
     *                  )
     *               ),
     *              required={"basket_item_ids","address_id","payment_method","shipping_method"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function add(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'basket_item_ids'   => 'required|array',
            'address_id'        => 'required|integer|exists:lck_address,id,user_id,' . $user->id,
            'payment_method_id' => 'required|integer|in:0,1',
            'discount_code'     => 'nullable|string|max:255',
            'notes'             => 'nullable|array',
            'shipping_methods'  => 'required|array',
        ]);

        if ($validator->fails())
            return $this->throwError($validator->errors()->first(), 400);

        $params['user_id'] = $user->id;
        $params['item_ids'] = $request->get('basket_item_ids');
        $params['pagin'] = false;
        $baskets = Basket::get_all_product($params);

        if (count($baskets) < 1)
            return $this->throwError('Sản phẩm không tồn tại trong giỏ hàng!', 400);

        $payment_method_id = $request->get('payment_method_id');
        $address_id = $request->get('address_id');

        $shipping_address = Address::where('user_id', $user->id)
            ->where('id', $address_id)
            ->first();

        $notes = [];
        $_notes = $request->get('notes', []);

        foreach ($_notes as $v) {
            if (isset($v['store_id']) && !empty($v['store_id']) && isset($v['message']) && !empty($v['message'])) {
                $notes[$v['store_id']] = $v['message'];
            }
        }

        $shipping_methods = [];
        $_shipping_methods = $request->get('shipping_methods', []);

        foreach ($_shipping_methods as $v) {
            if (isset($v['store_id']) && !empty($v['store_id']) && isset($v['shipping_method_id']) && !empty($v['shipping_method_id'])) {
                $shipping_methods[$v['store_id']] = $v['shipping_method_id'];
            }
        }

        DB::beginTransaction();

        try {
            foreach ($baskets as $store_id => $products) {
                $total_product_price = $total_price = $total_discount_amount = $total_weight = $total_shipping_fee = 0;
                $order_detail = [];

                foreach ($products as $product) {
                    $price = $product->product_variation_price ? $product->product_variation_price : $product->price;
                    $price_old = $product->product_variation_price ? $product->product_variation_price : $product->price_old;
                    $discount_amount = $price < $price_old ? $price_old - $price : 0;

                    $total_product_price += $price * $product->quantity;
                    $total_discount_amount += $discount_amount;
                    $total_weight += $product->weight * $product->quantity;

                    $order_detail[] = [
                        'order_id'            => null,
                        'product_id'          => $product->product_id,
                        'product_name'        => $product->name,
                        'thumbnail_file_path' => $product->thumbnail->file_path,
                        'category_name'       => null,
                        'variation_name'      => $product->product_variation_name,
                        'price'               => $price,
                        'price_old'           => $price_old,
                        'discount_amount'     => $discount_amount,
                        'quantity'            => $product->quantity,
                        'sku'                 => $product->product_variation_sku ? $product->product_variation_sku : $product->sku,
                        'weight'              => $product->weight,
                    ];
                }

                if ($order_detail) {
                    $order = Orders::create([
                        'order_code'            => md5(microtime()),
                        'total_product_price'   => $total_product_price,
                        'discount_amount'       => $total_discount_amount,
                        'discount_percent'      => 0,
                        'discount_code'         => 0,
                        'shipping_fee'          => $total_shipping_fee,
                        'free_shipping'         => 0,
                        'payment_method'        => $payment_method_id,
                        'status_payment'        => 0,
                        'shipping_method'       => isset($shipping_methods[$store_id]) ? $shipping_methods[$store_id] : null,
                        'shipping_code'         => '',
                        'total_weight'          => $total_weight,
                        'shipping_name'         => $shipping_address->name,
                        'shipping_phone'        => $shipping_address->phone,
                        'shipping_province_id'  => $shipping_address->province_id,
                        'shipping_district_id'  => $shipping_address->district_id,
                        'shipping_ward_id'      => $shipping_address->ward_id,
                        'shipping_street_name'  => $shipping_address->street_name,
                        'shipping_address'      => $shipping_address->full_address,
                        'warehouse_name'        => null,
                        'warehouse_phone'       => null,
                        'warehouse_province_id' => null,
                        'warehouse_district_id' => null,
                        'warehouse_ward_id'     => null,
                        'warehouse_street_name' => null,
                        'warehouse_address'     => null,
                        'status'                => 1,
                        'total_price'           => $total_product_price + $total_shipping_fee,
                        'note'                  => isset($notes[$store_id]) ? $notes[$store_id] : null,
                        'user_id'               => $user->id,
                        'store_id'              => $store_id,
                    ]);

                    if ($order) {
                        foreach ($order_detail as $k => $v) {
                            $order_detail[$k]['order_id'] = $order->id;
                        }
                        OrdersDetail::insert($order_detail);
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            DB::rollBack();
        }

        return $this->returnResult();
    }

    /**
     * @OA\Get(
     *   path="/orders/{order_id}",
     *   tags={"user role"},
     *   summary="get a orders detail",
     *   description="",
     *   operationId="OrdersGetDetail",
     *     @OA\Parameter( name="token", description="token from api login", required=false, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter( name="order_id", description="order_id", required=true, in="path",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function detail($order_id)
    {
        $user = $this->getAuthenticatedUser();

        return $this->returnResult();
    }

    /**
     * @OA\Post(
     *   path="/orders/cancel/{order_id}",
     *   tags={"user role"},
     *   summary="Cancel a orders",
     *   description="",
     *   operationId="OrdersCancel",
     *     @OA\Parameter( name="token", description="token from api login", required=false, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter( name="order_id", description="order_id", required=true, in="path",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			     @OA\Property(property="cancel_reason", description="cancel_reason", type="string", ),
     *              required={"cancel_reason"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function cancel($order_id, Request $request)
    {
        $user = $this->getAuthenticatedUser();

        return $this->returnResult();
    }
}