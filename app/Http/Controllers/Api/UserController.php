<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Classes\ActivationService;
use App\Mail\OTPEmail;
use App\Models\CoreUsers;
use App\Http\Controllers\BaseAPIController;
use App\Models\CoreUsersActivation;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use Illuminate\Validation\Rule;
use App\Utils\File;
use App\Utils\FbAccountKit;
use Illuminate\Support\Facades\Hash;
use App\Models\Files;
use App\Utils\Avatar;
use Mail;

class UserController extends BaseAPIController
{
    use RegistersUsers;

    public function __construct(ActivationService $activationService)
    {
        $this->activationService = $activationService;
    }

    /**
     * @OA\Post(
     *     path="/user/login",
     *     tags={"user role"},
     *     summary="User login",
     *     description="",
     *     operationId="UserLogin",
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			     @OA\Property(property="email_or_username", description="email or username", type="string", ),
     *			     @OA\Property(property="password", description="password", type="string", ),
     *			     @OA\Property(property="fcm_token", description="", type="string", ),
     *			     @OA\Property(property="device_id", description="", type="string", ),
     *			     @OA\Property(property="device_os", description="", type="string", ),
     *              required={"email_or_username","password"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email_or_username' => 'required',
            'password'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }
        $credentials = $request->only(['email_or_username', 'password']);

        $user = CoreUsers::where(['email' => $credentials['email_or_username']])
            ->orWhere(['username' => $credentials['email_or_username']])
            ->first();

        if (!$user)
            return $this->throwError('Tài khoản không tồn tại!', 404);

        if (!Hash::check($credentials['password'], $user->password))
            return $this->throwError('Mật khẩu không chính xác!', 400);

        if ($user->status == CoreUsers::$status_inactive)
            return $this->throwError('Tài khoản chưa xác thực email!', 405);

        if ($user->status == CoreUsers::$status_banned)
            return $this->throwError('Tài khoản đã bị khóa!', 403);

        if (!$token = auth('api')->login($user)) {
            return $this->throwError('Đăng nhập thất bại!', 400);
        }

        $fcm_token = $request->get('fcm_token');
        $device_id = $request->get('device_id');
        $device_os = $request->get('device_os');
        $user->fcm_token = !empty($fcm_token) ? $fcm_token : $user->fcm_token;
        $user->device_id = !empty($device_id) ? $device_id : $user->device_id;
        $user->device_os = !empty($device_os) ? $device_os : $user->device_os;
        $user->save();

        $user->token = $token;
        $user->token_type = 'bearer';
        $user->token_expires_in = auth('api')->factory()->getTTL() * 60;

        return $this->returnResult($user);
    }

    /**
     * @OA\Post(
     *     path="/user/register",
     *     tags={"user role"},
     *     summary="User register",
     *     description="",
     *     operationId="UserRegister",
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			    @OA\Property(property="username", description="username", type="string", ),
     *			    @OA\Property(property="email", description="email", type="string", ),
     *     			@OA\Property(property="password", description="password, min lenght 6", type="string", ),
     *     			@OA\Property(property="password_confirmation", description="password confirmation", type="string", ),
     *			    @OA\Property(property="fullname", description="fullname", type="string", ),
     *			    @OA\Property(property="phone", description="phone", type="numeric", ),
     *     			@OA\Property(property="fcm_token", description="", type="string", ),
     *     			@OA\Property(property="device_id", description="", type="string", ),
     *     			@OA\Property(property="device_os", description="", type="string",),
     *              required={"username","email", "password","password_confirmation"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function register(Request $request)
    {
        $validate_rule = [
            'username' => "bail|required|alpha_dash|max:255|unique:lck_core_users,username",
            'email'    => "bail|required|email|unique:lck_core_users,email",
            'password' => 'bail|required|string|min:6|confirmed',
            'fullname' => 'bail|nullable|string|max:255',
            'phone'    => 'bail|nullable|numeric|unique:lck_core_users,phone',
        ];
        $validator = Validator::make($request->all(), $validate_rule);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        try {
            DB::beginTransaction();
            $user = new CoreUsers();

            $user->password = Hash::make($request->get('password'));
            $user->status = CoreUsers::$status_inactive;

            $username = $request->get('username', null);
            $phone = $request->get('phone', null);
            $email = $request->get('email', null);
            $fullname = $request->get('fullname', null);
            $fcm_token = $request->get('fcm_token', null);
            $device_id = $request->get('device_id', null);
            $device_os = $request->get('device_os', null);

            $user->username = $username;
            $user->phone = $phone;
            $user->email = $email;
            $user->fullname = $fullname;
            $user->fcm_token = $fcm_token;
            $user->device_id = $device_id;
            $user->device_os = $device_os;

            $user->save();

            $this->activationService->sendActivationMail($user);

            DB::commit();

            $user->token = $request->get('token');
            $user->token_type = 'bearer';
            $user->token_expires_in = auth('api')->factory()->getTTL() * 60;

            return $this->returnResult([], 'Vui lòng kiểm tra hộp thư email và kích hoạt tài khoản!');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->throwError('Có lỗi xảy ra, vui lòng thử lại!' . $e->getMessage(), 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/user/update",
     *     tags={"user role"},
     *     summary="User update info",
     *     description="",
     *     operationId="UserUpdate",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *     			@OA\Property(property="fullname", description="Fullname of user", type="string", ),
     *     			@OA\Property(property="email", description="Email of user", type="string", ),
     *     			@OA\Property(property="phone", description="phone", type="numeric", ),
     *     			@OA\Property(property="gender", description="Gender of user. 1=>male | 2=>female ",type="string",),
     *     			@OA\Property(property="birthday", description="Birthday of user: Y-m-d, ex: 2019-01-01", type="string", ),
     *     			@OA\Property(property="description", description="description", type="string", ),
     *     			@OA\Property(property="avatar_file_path", description="file_path form api upload", type="string", ),
     *     			@OA\Property(property="cover_file_path", description="file_path form api upload", type="string", ),
     *     			@OA\Property(property="password_old", description="old password, require when update password", type="string", ),
     *     			@OA\Property(property="password", description="new password, min lenght 6, require when update password", type="string", ),
     *     			@OA\Property(property="password_confirmation", description="password confirmation, require when update password", type="string", ),
     *     			@OA\Property(property="fcm_token", description="fcm_token", type="string", ),
     *     			@OA\Property(property="device_id", description="device_id", type="string", ),
     *     			@OA\Property(property="device_os", description="device_os", type="string", ),
     *              required={}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function update(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        Validator::extend('avatarExits', function ($attribute, $value, $parameters, $validator) use ($user) {
            if ($user->avatar_file_path && $user->avatar_file_path == $value)
                return true;

            $count = DB::table('lck_files')->where('file_path', $value)
                ->where('user_id', $user->id)
                //->where('is_temp', Files::IS_TEMP)
                ->where('type', Files::TYPE_AVATAR)
                ->count();
            return $count !== 0;
        }, 'The avatar_file_path invalid!');

        Validator::extend('coverExits', function ($attribute, $value, $parameters, $validator) use ($user) {
            if ($user->cover_file_path && $user->cover_file_path == $value)
                return true;

            $count = DB::table('lck_files')->where('file_path', $value)
                ->where('user_id', $user->id)
                //->where('is_temp', Files::IS_TEMP)
                ->where('type', Files::TYPE_COVER)
                ->count();
            return $count !== 0;
        }, 'The cover_file_path invalid!');

        $validate_rule = [
            'fullname'              => 'nullable|string|max:255',
            'email'                 => "nullable|bail|string|unique:lck_core_users,email,{$user->id}",
            'phone'                 => "nullable|bail|numeric",
            'gender'                => ['nullable', 'bail', Rule::in([CoreUsers::GENDER_MALE, CoreUsers::GENDER_FEMALE])],
            'birthday'              => 'nullable|bail|date|date_format:Y-m-d',
            'cardid'                => 'nullable|bail|string',
            'avatar_file_path'      => "nullable|bail|string|avatarExits",
            'cover_file_path'       => "nullable|bail|string|coverExits",
            'password_old'          => 'nullable|bail|string',
            'password'              => 'nullable|bail|string|min:6',
            'password_confirmation' => 'nullable|bail|string|min:6',
            'description'           => 'nullable|string|max:500',
        ];

        $validator = Validator::make($request->all(), $validate_rule);
        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        try {
            DB::beginTransaction();

            $user = CoreUsers::findOrFail($user->id);

            $password_old = $request->get('password_old', null);
            $password = $request->get('password', null);
            $password_confirmation = $request->get('password_confirmation', null);

            if ($password) {
                if (empty($password_old))
                    return $this->throwError('Vui lòng nhập mật khẩu cũ!', 400);
                if (!Hash::check($password_old, $user->password))
                    return $this->throwError('Mật khẩu cũ không hợp lệ!', 400);
                if ($password != $password_confirmation)
                    return $this->throwError('Xác nhận mật khẩu mới không khớp!', 400);

                $user->password = Hash::make($password);
            }

            $new_avatar = $request->get('avatar_file_path', null);
            $new_avatar = $new_avatar ? $new_avatar : $user->avatar_file_path;
            if ($user->avatar_file_path != $new_avatar) {
                if ($user->avatar_file_path)
                    File::delete($user->avatar_file_path);

                $file = Files::where(['file_path' => $new_avatar])->first();
                $file->is_temp = null;
                $file->save();
                $user->avatar_file_path = $new_avatar;
            }

            $new_cover = $request->get('cover_file_path', null);
            $new_cover = $new_cover ? $new_cover : $user->cover_file_path;
            if ($user->cover_file_path != $new_cover) {
                if ($user->cover_file_path)
                    File::delete($user->cover_file_path);

                $file = Files::where(['file_path' => $new_cover])->first();
                $file->is_temp = null;
                $file->save();
                $user->cover_file_path = $new_cover;
            }

            $user->phone = $request->get('phone', $user->phone);
            $user->email = $request->get('email', $user->email);
            $user->fullname = $request->get('fullname', $user->fullname);
            $user->cardid = $request->get('cardid', $user->cardid);
            $user->gender = $request->get('gender', $user->gender);
            $user->birthday = $request->get('birthday', $user->birthday);
            $user->description = $request->get('description', $user->description);

            $user->fcm_token = $request->get('fcm_token', $user->fcm_token);
            $user->device_id = $request->get('device_id', $user->device_id);
            $user->device_os = $request->get('device_os', $user->device_os);
            $user->save();

            DB::commit();

            $user->token = $request->get('token');
            $user->token_type = 'bearer';
            $user->token_expires_in = auth('api')->factory()->getTTL() * 60;

            return $this->returnResult($user);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->throwError('Có lỗi xảy ra, vui lòng thử lại!', 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/user/forgot-password",
     *     tags={"user role"},
     *     summary="Request reset Password",
     *     description="",
     *     operationId="UserForgotPassword",
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			    @OA\Property(property="email", description="email", type="string", ),
     *              required={"email"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $email = $request->get('email');
        $user = CoreUsers::where(['email' => $email])
            ->first();

        if (!$user)
            return $this->throwError('Tài khoản không tồn tại!', 404);

        if ($user->status == CoreUsers::$status_inactive)
            return $this->throwError('Tài khoản chưa xác thực email!', 405);

        if ($user->status == CoreUsers::$status_banned)
            return $this->throwError('Tài khoản đã bị khóa!', 403);

        $active = new CoreUsersActivation();
        $otp_code = $active->createOTPActivation($user);
        $user->otp_code = $otp_code;
        $user->email_title = 'Mã reset mật khẩu của bạn là:';
        $mailable = new OTPEmail($user);
        Mail::to($user->email)->send($mailable);

        return $this->returnResult([], 'Vui lòng kiểm tra hộp thư email để lấy mã reset password!');
    }

    /**
     * @OA\Post(
     *     path="/user/reset-password",
     *     tags={"user role"},
     *     summary="User Reset Password",
     *     description="",
     *     operationId="UserResetPassword",
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			    @OA\Property(property="reset_password_code", description="reset_password_code from email", type="integer", ),
     *     			@OA\Property(property="password", description="password, min lenght 6", type="string", ),
     *     			@OA\Property(property="password_confirmation", description="password confirmation", type="string", ),
     *              required={"reset_password_code","password","password_confirmation"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reset_password_code' => 'required',
            'password'            => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $reset_password_code = $request->get('reset_password_code');

        $active = new CoreUsersActivation();

        $activation = $active->getActivationByOTP($reset_password_code);

        if (empty($activation))
            return $this->throwError('Mã reset mật khẩu không hợp lệ', 400);

        $user = CoreUsers::find($activation->user_id);

        if (!$user) {
            return $this->throwError('Tài khoản không tồn tại', 500);
        }

        if ($user->status == CoreUsers::$status_banned)
            return $this->throwError('Tài khoản đã bị khóa!', 403);

        $password = Hash::make($request->get('password'));
        $user->password = $password;
        $user->save();

        $active->deleteOTPActivation($reset_password_code);

        if (!$token = auth('api')->login($user)) {
            return $this->throwError('Login failed!', 401);
        }

        $user->token = $token;
        $user->token_type = 'bearer';
        $user->token_expires_in = auth('api')->factory()->getTTL() * 60;

        return $this->returnResult($user);
    }

    /**
     * @OA\Get(
     *     path="/user/me",
     *     tags={"user role"},
     *     summary="Get user information",
     *     description="",
     *     operationId="UserGetInfo",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function me(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        $user->token = $request->get('token');
        $user->token_type = 'bearer';
        $user->token_expires_in = auth('api')->factory()->getTTL() * 60;
        return $this->returnResult($user);
    }

    /**
     * @OA\Post(
     *     path="/user/logout",
     *     tags={"user role"},
     *     summary="User logout",
     *     description="",
     *     operationId="UserLogout",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function logout()
    {
        $this->getAuthenticatedUser();
        auth('api')->logout();
        return $this->returnResult();
    }

    public function loginFacebook(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fb_access_token' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $user_fb = [
            'id'     => 123,
            'name'   => 'Cui Bap',
            'email'  => '',
            'avatar' => '',
        ];

        try {
            $user_fb = Socialite::driver('facebook')->userFromToken($request->get('fb_access_token'));//object
        } catch (\Exception $e) {
            return $this->throwError('Đăng nhập bằng facebook thất bại! ' . $e->getMessage(), 401);
        }

        $user = CoreUsers::where(['facebook_id' => $user_fb->id])->first();

        if (!$user) {
            $email = null;
            if ($user_fb->email) {
                if (!CoreUsers::where('email', $user_fb->email)->first())
                    $email = $user_fb->email;
            }
            $user = CoreUsers::create([
                'facebook_id' => $user_fb->id,
                'fullname'    => $user_fb->name,
                'avatar'      => $user_fb->avatar,
                'email'       => $email,
                'status'      => CoreUsers::STATUS_UNREGISTERED
            ]);

            if (!$user)
                return $this->throwError('Cannot insert DB', 500);

            $user = CoreUsers::findOrFail($user->id);
        }

        if (!$token = auth('api')->login($user)) {
            return $this->throwError('Login failed!', 401);
        }
        $fcm_token = $request->get('fcm_token');
        $device_id = $request->get('device_id');
        $device_os = $request->get('device_os');
        $user->fcm_token = !empty($fcm_token) ? $fcm_token : $user->fcm_token;
        $user->device_id = !empty($device_id) ? $device_id : $user->device_id;
        $user->device_os = !empty($device_os) ? $device_os : $user->device_os;
        $user->save();

        $user->token = $token;
        $user->token_type = 'bearer';
        $user->token_expires_in = auth('api')->factory()->getTTL() * 60;

        return $this->returnResult($user);
    }

    public function loginGoogle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gg_access_token' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $user_google = [
            'id'     => 123,
            'name'   => 'Cui Bap',
            'email'  => '',
            'avatar' => '',
        ];

        try {
            $user_google = Socialite::driver('google')->userFromToken($request->get('gg_access_token'));//object
        } catch (\Exception $e) {
            return $this->throwError('Đăng nhập bằng google thất bại!' . $e->getMessage(), 401);
        }

        $user = CoreUsers::where(['google_id' => $user_google->id])->first();

        if (!$user) {
            $email = null;
            if ($user_google->email) {
                if (!CoreUsers::where('email', $user_google->email)->first())
                    $email = $user_google->email;
            }
            $user = CoreUsers::create([
                'google_id' => $user_google->id,
                'fullname'  => $user_google->name,
                'avatar'    => $user_google->avatar,
                'email'     => $email,
                'status'    => CoreUsers::STATUS_UNREGISTERED
            ]);

            if (!$user)
                return $this->throwError('Cannot insert DB', 500);

            $user = CoreUsers::findOrFail($user->id);
        }

        if (!$token = auth('api')->login($user)) {
            return $this->throwError('Login failed!', 401);
        }

        $fcm_token = $request->get('fcm_token');
        $device_id = $request->get('device_id');
        $device_os = $request->get('device_os');
        $user->fcm_token = !empty($fcm_token) ? $fcm_token : $user->fcm_token;
        $user->device_id = !empty($device_id) ? $device_id : $user->device_id;
        $user->device_os = !empty($device_os) ? $device_os : $user->device_os;
        $user->save();

        $user->token = $token;
        $user->token_type = 'bearer';
        $user->token_expires_in = auth('api')->factory()->getTTL() * 60;

        return $this->returnResult($user);
    }

    public function refresh()
    {
        $this->getAuthenticatedUser();
        return $this->returnResult([
            'token'            => auth('api')->refresh(),
            'token_type'       => 'bearer',
            'token_expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}