<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Classes\ActivationService;
use App\Mail\OTPEmail;
use App\Models\Category;
use App\Models\CoreUsers;
use App\Http\Controllers\BaseAPIController;
use App\Models\CoreUsersActivation;
use App\Models\Product;
use App\Models\StoreCategory;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use Illuminate\Validation\Rule;
use App\Utils\File;
use App\Utils\FbAccountKit;
use Illuminate\Support\Facades\Hash;
use App\Models\Files;
use App\Utils\Avatar;
use Mail;

class StoreController extends BaseAPIController
{
    public function getList(Request $request)
    {
        exit;
        if ($request->get('token', null))
            $user = $this->getAuthenticatedUser();

        $stores = CoreUsers::select([
            'id',
            'username',
            'fullname',
            'email',
            'phone',
            'avatar_file_path',
        ])
            ->limit(10)->get();

        return $this->returnResult($stores);
    }

    /**
     * @OA\Get(
     *     path="/store-page/{username}",
     *     tags={"user role"},
     *     summary="Get store information",
     *     description="",
     *     operationId="StorePageInfo",
     *     @OA\Parameter(name="username", description="store username", required=true, in="path",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Parameter(name="token", description="token from api login", required=false, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function info($username, Request $request)
    {
        if ($request->get('token', null))
            $user = $this->getAuthenticatedUser();

        $store = CoreUsers::select([
            'id',
            'username',
            'fullname',
            'avatar_file_path',
            'cover_file_path',
            'description',
            'rating',
            'chat_response_rate',
            'preparing_time',
            'cancel_order_rate',
            'total_products',
            'last_login',
            'following_count',
            'follower_count',
            'is_verified',
            'created_at',
        ])
            ->where('username', $username)
            ->first();

        return $this->returnResult($store);
    }

    /**
     * @OA\Get(
     *     path="/store-page/{username}/category",
     *     tags={"user role"},
     *     summary="Get category of store",
     *     description="",
     *     operationId="StorePageCategory",
     *     @OA\Parameter(name="username", description="store username", required=true, in="path",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Parameter(name="token", description="token from api login", required=false, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function getCategory($username, Request $request)
    {
        if ($request->get('token', null))
            $user = $this->getAuthenticatedUser();

        $store = CoreUsers::select(['id'])
            ->where('username', $username)
            ->first();

        if (empty($store))
            return $this->throwError('Shop không tồn tại!', 404);

//        $return = StoreCategory::select(['id', 'name'])->where('user_id', $store->id)->get();

        $product_category_count = Product::select(DB::raw('category_id, count(id) as total'))
            ->where('user_id', $store->id)
            ->where('status', Product::STATUS_ACTIVE)
            ->groupBy('category_id')
            ->get();

        $categories = Category::with(['thumbnail', 'icon'])
            ->where('status', Category::STATUS_ACTIVE)
            ->orderBy('priority', 'ASC')
            ->get();

        $a_categories = count($categories) ? $categories->toArray() : [];

        $_data = \App\Utils\Category::buildTree($a_categories);

        $return = $_cate_temp = [];

        foreach ($_data as $v) {
            $cate = $v;
            unset($cate['child']);
            $cate['total_product'] = 0;
            $_cate_temp[$v['id']]['cate'] = $cate;
            $_cate_temp[$v['id']]['child_ids'] = array_merge([$v['id']], \App\Utils\Category::get_all_child_categories($a_categories, $v['id']));
        }

        foreach ($product_category_count as $v) {
            foreach ($_cate_temp as $cate_id => $v2) {
                if (in_array($v->category_id, $v2['child_ids'])) {
                    $_cate_temp[$cate_id]['cate']['total_product'] += $v->total;
                }
            }
        }

        foreach ($_cate_temp as $v) {
            if ($v['cate']['total_product'] > 0) {
                $return[] = $v['cate'];
            }
        }

        return $this->returnResult($return);
    }
}