<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Models\CoreUsers;
use App\Http\Controllers\BaseAPIController;
use App\Models\Files;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class UploadController extends BaseAPIController
{
    /**
     * @OA\Post(
     *     path="/upload/image",
     *     tags={"upload"},
     *     summary="Upload image",
     *     description="",
     *     operationId="UploadImage",
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *              @OA\Property(property="token", type="string", description="token from api login", ),
     *              @OA\Property(property="file_image", type="file", ),
     *              @OA\Property(property="type", enum={"1", "2", "3"}, description="type of image, 1:avatar, 2:cover, 3:product", ),
     *              required={"token","file_image","type"}
     *          )
     *       )
     *     ),
     *
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function image(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'file_image' => 'image|required',
            'type'       => ['required', Rule::in([1, 2, 3])],
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        if (!$request->file('file_image')->isValid()) {
            $this->throwError('Unknow erorr', 500);
        }

        $sub_dir = date('Y/m/d');

        $filename = md5(rand(0, 999999) . microtime()) . '.' . $request->file_image->extension();

        $full_dir = config('constants.upload_dir.root') . '/' . $sub_dir;

        if (!is_dir($full_dir)) {
            mkdir($full_dir, 0777, true);
        }

        $image = Image::make(Input::file('file_image'));
        $size = [];

        $width = $size[0] = 800;
        $height = $size[1] = 800;

        if ($image->getWidth() > 1920)
            $image->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            });

        if ($request->get('type') == Files::TYPE_AVATAR) {
            $width = $size[0] = 400;
            $height = $size[1] = 400;
        }

        $image->width() < $image->height() ? $width = null : $height = null;

        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });

        $width = $size[0];

        $image2 = Image::canvas($width, $size[1]);

        $image2->insert($image, 'center')
            ->save($full_dir . '/' . $filename, 90);

        $file_path = $sub_dir . '/' . $filename;

        $image = Files::create([
            'user_id'   => $user->id,
            'file_path' => $file_path,
            'type'      => $request->get('type')
        ]);

        return $this->returnResult([
            'id'   => $image->id,
            'path' => $file_path,
            'url'  => config('constants.upload_dir.url') . '/' . $file_path,
        ]);

    }
}