<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Convenience;
use App\Models\CoreUsers;
use App\Models\Exterior;
use App\Models\FrontageType;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductVariation;
use App\Models\VariationValues;
use App\Models\Purpose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Basket;

class BasketController extends BaseAPIController
{
    /**
     * @OA\Get(
     *   path="/basket",
     *   tags={"user role"},
     *   summary="Get basket of user",
     *   description="",
     *   operationId="BasketGetAll",
     *     @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter( name="limit", description="limit", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Parameter( name="page", description="page", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function getAll(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'page'  => 'nullable|integer|min:1',
            'limit' => 'nullable|integer|min:1',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $params = $request->all();

        $params = array_filter($params);
        $params['token'] = $request->get('token');
        $params['pagin_path'] = route('basket.getAll') . '?' . http_build_query($params);

        $params['pagin'] = false;
        $params['user_id'] = $user->id;

        $basket = Basket::get_all_product($params);

        $basket = count($basket) ? $basket->toArray() : [];

        $store_ids = $basket ? array_keys($basket) : [];

        if (empty($store_ids))
            return $this->returnResult([]);

        $stores = CoreUsers::select(['id', 'fullname', 'username', 'avatar_file_path', 'cover_file_path',])
            ->whereIn('id', $store_ids)->get();

        foreach ($stores as $store) {
            if (isset($basket[$store->id])) {
                $store->products = $basket[$store->id];
            }
        }

        $return = $stores;

        return $this->returnResult($return);
    }

    /**
     * @OA\Get(
     *   path="/basket/counter",
     *   tags={"user role"},
     *   summary="Get basket counter",
     *   description="",
     *   operationId="BasketCounter",
     *     @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function counter()
    {
        $user = $this->getAuthenticatedUser();

        $total = Basket::select(DB::raw('sum(quantity) as total'))
            ->where('user_id', $user->id)
            ->groupBy('user_id')
            ->first();

        $return = $total;

        return $this->returnResult($return);
    }

    /**
     * @OA\Post(
     *   path="/basket",
     *   tags={"user role"},
     *   summary="add product to basket",
     *   description="",
     *   operationId="BasketAddProduct",
     *      @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			     @OA\Property(property="product_id", description="product_id", type="integer", ),
     *			     @OA\Property(property="product_variation_id", description="product_variation_id", type="integer", ),
     *			     @OA\Property(property="quantity", description="quantity", type="integer", ),
     *              required={"product_id","quantity"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function add(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'product_id'           => 'required|integer',
            'product_variation_id' => 'nullable|integer',
            'quantity'             => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }
        $product_id = $request->get('product_id');
        $product_variation_id = $request->get('product_variation_id', null);
        $quantity = $request->get('quantity');

        $product = Product::find($product_id);

        if (empty($product))
            return $this->throwError('Sản phẩm không tồn tại!', 400);

        if ($product_variation_id) {
            $product_variant = ProductVariation::where('id', $product_variation_id)->where('product_id', $product_id)->first();
            if (empty($product_variant))
                return $this->throwError('Biến thể của sản phẩm không tồn tại!', 400);
        }

        $basket = Basket::where('user_id', $user->id)->where('product_id', $product_id);

        if ($product_variation_id)
            $basket = $basket->where('product_variation_id', $product_variation_id);

        $basket = $basket->first();

        if (empty($basket)) {
            Basket::create([
                'user_id'              => $user->id,
                'product_id'           => $product_id,
                'product_variation_id' => $product_variation_id,
                'quantity'             => $quantity,
            ]);
        } else {
            $basket->quantity = $basket->quantity + $quantity;
            $basket->save();
        }

        return $this->returnResult();
    }

    /**
     * @OA\Post(
     *   path="/basket/update",
     *   tags={"user role"},
     *   summary="update basket",
     *   description="",
     *   operationId="BasketUpdate",
     *      @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
     *			     @OA\Property(property="basket_data", description="Object basket data", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="basket_item_id", description="basket_item_id", type="integer", ),
     *			            @OA\Property(property="quantity", description="số lượng", type="integer", ),
     *                  )
     *              ),
     *              required={"basket_data",}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function update(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'basket_data' => 'required|array',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $basket_data = $request->get('basket_data');

        if (empty($basket_data))
            return $this->throwError('Basket_data empty or invalid', 400);

        foreach ($basket_data as $item) {
            $basket = Basket::where('id', $item['basket_item_id'])->where('user_id', $user->id)->first();
            if ($basket) {
                if ($item['quantity'] < 1 || empty($basket->product)) {
                    $basket->delete();
                } else {
                    $basket->quantity = $item['quantity'];
                    $basket->save();
                }
            }
        }

        return $this->returnResult();
    }

    /**
     * @OA\Post(
     *   path="/basket/remove-item",
     *   tags={"user role"},
     *   summary="Remove items from basket",
     *   description="",
     *   operationId="BasketRemoveItems",
     *      @OA\Parameter( name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *			     @OA\Property(property="basket_item_ids", description="list item id in basket", type="string", ),
     *              required={"basket_item_ids",}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     *     @OA\Response(response="404", description="Account not exits"),
     *     @OA\Response(response="405", description="Account has not been activated yet"),
     * )
     */
    public function removeItems(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'basket_item_ids' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $basket_item_ids = explode(',', $request->get('basket_item_ids'));

        Basket::where('user_id', $user->id)->whereIn('id', $basket_item_ids)->delete();

        return $this->returnResult();
    }
}