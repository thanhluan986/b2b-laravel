<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Address;
use App\Models\Favorite;
use App\Models\Files;
use App\Models\HistoryView;
use App\Models\Product;
use App\Models\ProductImages;
use App\Utils\File;
use App\Utils\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Validation\Rule;

class AddressController extends BaseAPIController
{
    /**
     * @OA\Get(
     *     path="/address",
     *     tags={"user role"},
     *     summary="Get all address",
     *     description="",
     *     operationId="AddressGetAll",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Parameter(name="limit", description="limit, default 10", required=false, in="query",
     *         @OA\Schema(type="integer",)
     *     ),
     *     @OA\Parameter(name="page", description="page", required=false, in="query",
     *         @OA\Schema(type="integer",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function getAll(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $aInit = [
            'limit' => null,
        ];

        $params = array_merge(
            $aInit, $request->only(array_keys($aInit))
        );

        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer|min:1',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }
        $params = array_filter($params);
        $params['token'] = $request->get('token');
        $params['pagin_path'] = route('address.getAll') . '?' . http_build_query($params);
        $params['limit'] = $request->get('limit', 10);

        $params['user_id'] = $user->id;
        $data = Address::get_by_where($params);

        return $this->returnResult($data);
    }

    /**
     * @OA\Get(
     *     path="/address/{id}",
     *     tags={"user role"},
     *     summary="get detail address",
     *     description="",
     *     operationId="AddressDetail",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Parameter(name="id", description="address id", required=true, in="path",
     *         @OA\Schema(type="integer",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function detail($id)
    {
        $user = $this->getAuthenticatedUser();

        $data = Address::where('id', $id)
            ->where('user_id', $user->id)
            ->with(['province', 'district', 'ward'])
            ->first();

        return $this->returnResult($data);
    }

    /**
     * @OA\Post(
     *     path="/address",
     *     tags={"user role"},
     *     summary="Add address",
     *     description="",
     *     operationId="AddressAdd",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *     			@OA\Property(property="name", description="name", type="string", ),
     *     			@OA\Property(property="phone", description="phone", type="numeric", ),
     *     			@OA\Property(property="province_id", description="province_id ",type="integer",),
     *     			@OA\Property(property="district_id", description="district_id", type="integer", ),
     *     			@OA\Property(property="ward_id", description="ward_id", type="integer", ),
     *     			@OA\Property(property="street_name", description="street_name", type="string", ),
     *     			@OA\Property(property="is_default_recipient", description="is_default_recipient", enum={0,1}, ),
     *     			@OA\Property(property="is_warehouse", description="is_warehouse", enum={0,1}, ),
     *     			@OA\Property(property="is_return", description="is_return", enum={0,1}, ),
     *              required={"name","phone","province_id","district_id","ward_id","street_name",}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function add(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validate_rule = [
            'name'                 => 'required|string|max:255',
            'phone'                => "required|bail|numeric",
            'province_id'          => "required|bail|required|exists:lck_location_province,id",
            'district_id'          => "required|bail|required|exists:lck_location_district,id",
            'ward_id'              => "required|bail|required|exists:lck_location_ward,id",
            'street_name'          => 'required|bail|string|max:200',
            'is_default_recipient' => "nullable|bail|in:0,1",
            'is_warehouse'         => "nullable|bail|in:0,1",
            'is_return'            => "nullable|bail|in:0,1",
        ];

        $validator = Validator::make($request->all(), $validate_rule);
        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        try {
            DB::beginTransaction();

            $params = array_fill_keys(array_keys($validate_rule), null);
            $params = array_merge(
                $params, $request->only(array_keys($params))
            );

            $params['user_id'] = $user->id;
            $params['full_address'] = Address::get_full_address($params);
            $data = Address::create($params);

            if ($params['is_default_recipient'] == 1) {
                Address::where('is_default_recipient', '=', 1)
                    ->where('id', '<>', $data->id)
                    ->update(['is_default_recipient' => 0]);
            }
            if ($params['is_warehouse'] == 1) {
                Address::where('is_warehouse', '=', 1)
                    ->where('id', '<>', $data->id)
                    ->update(['is_warehouse' => 0]);
            }
            if ($params['is_return'] == 1) {
                Address::where('is_return', '=', 1)
                    ->where('id', '<>', $data->id)
                    ->update(['is_return' => 0]);
            }

            DB::commit();

            return $this->returnResult($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->throwError('Có lỗi xảy ra, vui lòng thử lại!', 500);
        }
    }

    /**
     * @OA\Post(
     *     path="/address/update/{id}",
     *     tags={"user role"},
     *     summary="Update address",
     *     description="",
     *     operationId="AddressUpdate",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Parameter(name="id", description="address_id", required=true, in="path",
     *         @OA\Schema(type="integer",)
     *     ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *     			@OA\Property(property="name", description="name", type="string", ),
     *     			@OA\Property(property="phone", description="phone", type="numeric", ),
     *     			@OA\Property(property="province_id", description="province_id ",type="integer",),
     *     			@OA\Property(property="district_id", description="district_id", type="integer", ),
     *     			@OA\Property(property="ward_id", description="ward_id", type="integer", ),
     *     			@OA\Property(property="street_name", description="street_name", type="string", ),
     *     			@OA\Property(property="is_default_recipient", description="is_default_recipient", enum={0,1}, ),
     *     			@OA\Property(property="is_warehouse", description="is_warehouse", enum={0,1}, ),
     *              required={"name","phone","province_id","district_id","ward_id","street_name",}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function update($id, Request $request)
    {
        $user = $this->getAuthenticatedUser();
        $data = Address::where('id', $id)->where('user_id', $user->id)->first();

        if (empty($data))
            return $this->throwError('Địa chỉ không tồn tại!', 404);

        $validate_rule = [
            'name'                 => 'required|string|max:255',
            'phone'                => "required|bail|numeric",
            'province_id'          => "required|bail|required|exists:lck_location_province,id",
            'district_id'          => "required|bail|required|exists:lck_location_district,id",
            'ward_id'              => "required|bail|required|exists:lck_location_ward,id",
            'street_name'          => 'required|bail|string|max:200',
            'is_default_recipient' => "nullable|bail|in:0,1",
            'is_warehouse'         => "nullable|bail|in:0,1",
            'is_return'            => "nullable|bail|in:0,1",
        ];

        $validator = Validator::make($request->all(), $validate_rule);
        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        try {
            DB::beginTransaction();

            $params = array_fill_keys(array_keys($validate_rule), null);
            $params = array_merge(
                $params, $request->only(array_keys($params))
            );

            $params['user_id'] = $user->id;
            $params['full_address'] = Address::get_full_address($params);

            Address::where('id', $id)->where('user_id', $user->id)->update($params);

            if ($params['is_default_recipient'] == 1) {
                Address::where('is_default_recipient', '=', 1)
                    ->where('id', '<>', $id)
                    ->update(['is_default_recipient' => 0]);
            }
            if ($params['is_warehouse'] == 1) {
                Address::where('is_warehouse', '=', 1)
                    ->where('id', '<>', $id)
                    ->update(['is_warehouse' => 0]);
            }
            if ($params['is_return'] == 1) {
                Address::where('is_return', '=', 1)
                    ->where('id', '<>', $id)
                    ->update(['is_return' => 0]);
            }
            DB::commit();

            $data = Address::where('id', $id)->where('user_id', $user->id)->first();

            return $this->returnResult($data);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->throwError('Có lỗi xảy ra, vui lòng thử lại!', 500);
        }
    }

    /**
     * @OA\Delete(
     *     path="/address/{id}",
     *     tags={"user role"},
     *     summary="Delete address",
     *     description="",
     *     operationId="AddressDelete",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Parameter(name="id", description="address id", required=true, in="path",
     *         @OA\Schema(type="integer",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function delete($id)
    {
        $user = $this->getAuthenticatedUser();

        Address::where('id', $id)->where('user_id', $user->id)->delete();

        return $this->returnResult();
    }
}