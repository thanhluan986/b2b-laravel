<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Trademark;
use Illuminate\Http\Request;

class TrademarkController extends BaseAPIController
{
    /**
     * @OA\Get(
     *   path="/trademark/top",
     *   tags={"user role"},
     *   summary="Get top trademark",
     *   description="",
     *   operationId="TrademarkTop",
     *   @OA\Response(response="200", description="Success operation"),
     *   @OA\Response(response="500", description="Server error")
     * )
     */
    public function top(Request $request)
    {
        $return = Trademark::with(['thumbnail']);

        $return = $return->get();

        return $this->returnResult($return);
    }
}