<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Favorite;
use App\Models\Files;
use App\Models\Product;
use App\Models\ProductImages;
use App\Utils\File;
use App\Utils\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Validation\Rule;

class FavoriteController extends BaseAPIController
{
    /**
     * @OA\Get(
     *     path="/favorite",
     *     tags={"user role"},
     *     summary="Get all my favorite product",
     *     description="",
     *     operationId="ProductFavoriteGetAll",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string", )
     *     ),
     *     @OA\Parameter(name="limit", description="limit, default 10", required=false, in="query",
     *         @OA\Schema(type="integer", )
     *     ),
     *     @OA\Parameter(name="page", description="page", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function getAll(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $aInit = [
            'limit' => null,
        ];

        $params = array_merge(
            $aInit, $request->only(array_keys($aInit))
        );

        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer|min:1',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }
        $params = array_filter($params);
        $params['token'] = $request->get('token');
        $params['pagin_path'] = route('favorite.getAll') . '?' . http_build_query($params);
        $params['limit'] = $request->get('limit', 10);

        $params['get_favorite'] = true;
        $params['user_id_favorite'] = $user->id;
        $products = Product::get_by_where($params);

        return $this->returnResult($products);
    }

    /**
     * @OA\Post(
     *     path="/favorite",
     *     tags={"user role"},
     *     summary="Add product to my favorite",
     *     description="",
     *     operationId="ProductFavoriteAdd",
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *              @OA\Property(property="token", type="string", description="token from api login", ),
     *              @OA\Property(property="product_id", type="integer", ),
     *              required={"token","product_id"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function add(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'product_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }
        $product_id = $request->get('product_id');

        $exist = Favorite::where('user_id', $user->id)
            ->where('product_id', $product_id)
            ->count();

        if ($exist)
            return $this->throwError('Bạn thêm sản phẩm này vào yêu thích!', 400);

        $product_exist = Product::where('id', $product_id)
            ->count();

        if (!$product_exist)
            return $this->throwError('Sản phẩm không tồn tại!', 400);

        Favorite::create(
            [
                'user_id'    => $user->id,
                'product_id' => $product_id,
            ]
        );

        return $this->returnResult();
    }

    /**
     * @OA\Delete(
     *     path="/favorite",
     *     tags={"user role"},
     *     summary="Delete products in my favorite",
     *     description="",
     *     operationId="ProductFavoriteDelete",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string", )
     *     ),
     *     @OA\Parameter(name="product_ids", description="list product id", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function delete(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'product_ids' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $product_ids = explode(',', $request->get('product_ids'));

        $count = Favorite::where('user_id', $user->id)
            ->whereIn('product_id', $product_ids)
            ->count();

        if ($count < 1)
            return $this->throwError('Sản phẩm không tồn tại trong danh sách yêu thích!', 400);

        Favorite::where('user_id', $user->id)
            ->whereIn('product_id', $product_ids)->delete();

        return $this->returnResult();
    }
}