<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Favorite;
use App\Models\Files;
use App\Models\HistoryView;
use App\Models\Product;
use App\Models\ProductImages;
use App\Utils\File;
use App\Utils\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Validation\Rule;

class HistoryController extends BaseAPIController
{
    /**
     * @OA\Get(
     *     path="/history/viewed-product",
     *     tags={"user role"},
     *     summary="Get all my history viewed product",
     *     description="",
     *     operationId="ProductGetHistory",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Parameter(name="limit", description="limit, default 10", required=false, in="query",
     *         @OA\Schema(type="integer",)
     *     ),
     *     @OA\Parameter(name="page", description="page", required=false, in="query",
     *         @OA\Schema(type="integer",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function getViewedProduct(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $aInit = [
            'limit' => null,
        ];

        $params = array_merge(
            $aInit, $request->only(array_keys($aInit))
        );

        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer|min:1',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }
        $params = array_filter($params);
        $params['token'] = $request->get('token');
        $params['pagin_path'] = route('history.getViewedProduct') . '?' . http_build_query($params);
        $params['limit'] = $request->get('limit', 10);

        $params['get_history'] = true;
        $params['user_id_history'] = $user->id;
        $products = Product::get_by_where($params);

        return $this->returnResult($products);
    }

    /**
     * @OA\Delete(
     *     path="/history/viewed-product",
     *     tags={"user role"},
     *     summary="Delete products in my history",
     *     description="",
     *     operationId="ProductDeleteMyHistory",
     *     @OA\Parameter(name="token", description="token from api login", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Parameter(name="product_ids", description="list product id", required=true, in="query",
     *         @OA\Schema(type="string",)
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function deleteViewedProduct(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'product_ids' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $product_ids = explode(',', $request->get('product_ids'));

        $count = HistoryView::where('user_id', $user->id)
            ->whereIn('product_id', $product_ids)
            ->count();

        if ($count < 1)
            return $this->throwError('Sản phẩm không tồn tại trong lịch sử!', 400);

        HistoryView::where('user_id', $user->id)
            ->whereIn('product_id', $product_ids)->delete();

        return $this->returnResult();
    }
}