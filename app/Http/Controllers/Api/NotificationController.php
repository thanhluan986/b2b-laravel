<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Favorite;
use App\Models\Files;
use App\Models\Notification;
use App\Models\NotificationRead;
use App\Models\Product;
use App\Models\ProductImages;
use App\Utils\File;
use App\Utils\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Validation\Rule;

class NotificationController extends BaseAPIController
{
    /**
     * @OA\Get(
     *     path="/notification",
     *     tags={"notification"},
     *     summary="Get all my notification",
     *     description="",
     *     operationId="notificationGetAll",
     *     @OA\Parameter(
     *         name="token",
     *         description="token from api login",
     *         required=true,
     *         in="query",
     *         @OA\Schema(
     *              type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="limit",
     *         description="limit, default 10",
     *         required=false,
     *         in="query",
     *         @OA\Schema(
     *              type="integer",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         description="page",
     *         required=false,
     *         in="query",
     *         @OA\Schema(
     *              type="integer",
     *         )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function getAll(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $aInit = [
            'limit' => null,
        ];

        $params = array_merge(
            $aInit, $request->only(array_keys($aInit))
        );

        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer|min:1',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }
        $params = array_filter($params);
        $params['token'] = $request->get('token');
        $params['pagin_path'] = route('notification.getAll') . '?' . http_build_query($params);
        $params['limit'] = $request->get('limit', 10);

        $params['user_id'] = $user->id;

//        \DB::enableQueryLog();
        $products = Notification::get_by_where($params);
//        \Log::info(\DB::getQueryLog());

        return $this->returnResult($products);
    }

    /**
     * @OA\Post(
     *     path="/notification/mark-read",
     *     tags={"notification"},
     *     summary="Mark read notification",
     *     description="",
     *     operationId="notificationMarkRead",
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *              @OA\Property( property="token", type="string", description="token from api login", ),
     *              @OA\Property( property="notification_id", type="integer", description="notification id, notification_id=0: mark all read", ),
     *              required={"token","notification_id"}
     *          )
     *       )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function postMarkRead(Request $request)
    {
        $user = $this->getAuthenticatedUser();

        $validator = Validator::make($request->all(), [
            'notification_id' => 'required|integer|min:0',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }
        $notification_id = $request->get('notification_id');

        if ($notification_id != 0) {
            $notification = Notification::find($notification_id);

            if (!$notification)
                return $this->throwError('Thông báo không tồn tại!', 400);

            if (empty($notification->notification_read->toArray())) {
                NotificationRead::create([
                    'notification_id' => $notification_id,
                    'user_id'         => $user->id,
                ]);
            }

        } else {
            $notifications_id = DB::table('lck_notification')
                ->whereRaw("((to_user_id = {$user->id} and chanel=2) or chanel=1)")
                ->whereRaw("lck_notification.id not in(select notification_id from lck_notification_read where lck_notification_read.user_id={$user->id})")
                ->select('lck_notification.id')->get();

            foreach ($notifications_id as $notification) {
                $sql = "insert into lck_notification_read set user_id={$user->id}, notification_id={$notification->id};";
                DB::insert($sql);
            }
        }

        return $this->returnResult();
    }

    /**
     * @OA\Get(
     *     path="/notification/counter",
     *     tags={"notification"},
     *     summary="Get counter new notification not yet read",
     *     description="",
     *     operationId="notificationGetCounter",
     *     @OA\Parameter(
     *         name="token",
     *         description="token from api login",
     *         required=true,
     *         in="query",
     *         @OA\Schema(
     *              type="string",
     *         )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function getCounter()
    {
        $user = $this->getAuthenticatedUser();

        $notifications_count = DB::table('lck_notification')
            ->whereRaw("((to_user_id = {$user->id} and chanel=2) or chanel=1)")
            ->whereRaw("lck_notification.id not in(select notification_id from lck_notification_read where lck_notification_read.user_id={$user->id})")
            ->select('lck_notification.id')->count();

        $return = [
            'total' => $notifications_count
        ];
        return $this->returnResult($return);
    }
}