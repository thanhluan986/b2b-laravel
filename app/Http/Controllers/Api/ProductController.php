<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Jobs\InsertViewedProduct;
use App\Models\Convenience;
use App\Models\Files;
use App\Models\Exterior;
use App\Models\FrontageType;
use App\Models\HistoryView;
use App\Models\Product;
use App\Models\ProductContactInfo;
use App\Models\ProductConvenience;
use App\Models\ProductFrontageType;
use App\Models\ProductImages;
use App\Models\ProductImagesExtra;
use App\Models\ProductExterior;
use App\Models\ProductNote;
use App\Models\ProductPurpose;
use App\Models\ProductVariation;
use App\Models\ProductWholesales;
use App\Models\VariationCombine;
use App\Models\VariationValues;
use App\Models\Purpose;
use App\Utils\File;
use App\Utils\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Validation\Rule;
use App\Models\Location\District;
use App\Models\Location\Province;
use App\Models\Location\Street;
use App\Models\Location\Ward;

class ProductController extends BaseAPIController
{
    /**
     * @OA\Get(
     *     path="/product",
     *     tags={"user role"},
     *     summary="Get all products",
     *     description="",
     *     operationId="ProductGetAll",
     *     @OA\Parameter( name="token", description="token from api login", required=false, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter(name="category_ids[]", description="Danh sách id danh mục, ex: [1,2,3]", required=false, in="query",
     *         @OA\Schema( type="array", @OA\Items())
     *     ),
     *     @OA\Parameter(name="store_id", description="id cửa hàng", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Parameter(name="store_category_ids[]", description="Danh sách id danh mục của cửa hàng, ex: [1,2,3]", required=false, in="query",
     *         @OA\Schema( type="array", @OA\Items())
     *     ),
     *     @OA\Parameter(name="trademark_ids[]", description="Danh sách id thương hiệu, ex: [1,2,3]", required=false, in="query",
     *         @OA\Schema( type="array", @OA\Items())
     *     ),
     *     @OA\Parameter(name="keywords", description="Từ khóa tìm kiếm", required=false, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter(name="keywords", description="Từ khóa tìm kiếm", required=false, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter( name="type", description="Lấy sp tìm kiếm hàng đầu, gợi ý gần nhất...", required=false, in="query",
     *         @OA\Schema( enum={"trend","recommend","top-rate-store","top-popular-store"}, )
     *     ),
     *     @OA\Parameter( name="price_to", description="Giá cao nhất", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Parameter( name="skip_product_ids[]", description="Danh sách id sản phẩm bỏ qua, ex: [1,2,3]", required=false, in="query",
     *         @OA\Schema( type="array", @OA\Items())
     *     ),
     *     @OA\Parameter( name="limit", description="limit", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Parameter( name="page", description="page", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Parameter( name="sort", description="sắp xếp sản phẩm", required=false, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function getAll(Request $request)
    {
        if ($request->get('token', null))
            $user = $this->getAuthenticatedUser();

        $aInit = [
            'category_ids'       => null,
            'store_id'           => null,
            'store_category_ids' => null,
            'trademark_ids'      => null,
            'keywords'           => null,
            'price_from'         => null,
            'price_to'           => null,
            'skip_product_ids'   => null,
            'limit'              => null,
            'sort'               => null,
            'type'               => null,
        ];

        $params = array_merge(
            $aInit, $request->only(array_keys($aInit))
        );

        $validator = Validator::make($request->all(), [
            'category_ids'       => 'nullable|array',
            'store_id'           => 'nullable|integer',
            'store_category_ids' => 'nullable|array',
            'trademark_ids'      => 'nullable|array',
            'keywords'           => 'nullable|string',
            'skip_product_ids'   => 'nullable|array',
            'price_from'         => 'nullable|integer|min:0',
            'price_to'           => 'nullable|integer|min:0',
            'limit'              => 'nullable|integer|min:1',
            'sort'               => 'nullable|string',
            'type'               => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return $this->throwError($validator->errors()->first(), 400);
        }

        $params = array_filter($params);
        $params['token'] = $request->get('token');
        $params['pagin_path'] = route('product.getAll') . '?' . http_build_query($params);
        $params['sort'] = $request->get('sort', 'newest');
        $products = Product::get_by_where($params);

        return $this->returnResult($products);
    }

    public function search(Request $request)
    {
        if ($request->get('token', null))
            $user = $this->getAuthenticatedUser();

        $_keywords = $request->get('keywords', null);
        if (empty($_keywords))
            return $this->throwError('Từ khóa không được để trống!', 400);

        $_keywords = str_replace('  ', ' ', $_keywords);
        $_keywords = str_replace(',', '', $_keywords);
        $_keywords = str_replace('.', '', $_keywords);

        $_keywords = explode(' ', $_keywords);
        $keywords = '';

        $_total = count($_keywords);
        foreach ($_keywords as $k => $v) {
            $keywords .= '+' . $v;
            if ($k < $_total - 1)
                $keywords .= ' ';
        }

        $params['limit'] = 20;
        $params['pagin_path'] = route('product.search') . '?' . http_build_query($request->all());

        $products = Product::select(DB::raw("id, title, description, price,
        MATCH (title,description) AGAINST ('{$keywords}') as score"))
            ->whereRaw("MATCH (title,description) AGAINST('{$keywords}') >0")
            ->where('status', Product::STATUS_ACTIVE)
            ->orderByRaw('score desc')
            ->paginate($params['limit'])
            ->withPath($params['pagin_path']);

        return $this->returnResult($products);
    }

    /**
     * @OA\Get(
     *     path="/product/{id}",
     *     tags={"user role"},
     *     summary="Get product by id",
     *     description="",
     *     operationId="ProductGetByID",
     *     @OA\Parameter( name="token", description="token from api login", required=false, in="query",
     *         @OA\Schema( type="string", )
     *     ),
     *     @OA\Parameter( name="id", description="id", required=true, in="path",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Response(response="200", description="Success"),
     *     @OA\Response(response="400", description="Missing/Invalid params"),
     *     @OA\Response(response="500", description="Server error"),
     *     @OA\Response(response="401", description="Unauthorized"),
     *     @OA\Response(response="403", description="Account is banned"),
     * )
     */
    public function detail($id, Request $request)
    {
        if ($request->get('token', null))
            $user = $this->getAuthenticatedUser();

        $product = Product::with(['category', 'store_category', 'store', 'thumbnail', 'images', 'images_extra',])
            ->find($id);

        if ($product && isset($user)) {
            $this->dispatch(new InsertViewedProduct(['user_id' => $user->id, 'product_id' => $id, 'time' => time()]));
        }

        if ($product) {
            $variations = VariationValues::get_group_variations($id);

            $product_variations = [];
            $variation_combine = [];

            if (count($variations))
                $product_variations = ProductVariation::get_product_variations_with_combine($id);

            $variations = collect(['variations' => $variations]);
            $product_variations = collect(['product_variations' => $product_variations]);

            $product_wholesales = ProductWholesales::select(['id', 'quantity_from', 'quantity_to', 'price'])->where('product_id', $id)->get();
            $product_wholesales = collect(['wholesales' => $product_wholesales]);

            $product = collect($product);

            $product = $product->merge($variations)
                ->merge($product_variations)
                ->merge($variation_combine)
                ->merge($product_wholesales);
        }

        return $this->returnResult($product);
    }
}