<?php
/**
 * User: LuanNT
 * Date: 29/05/2018
 * Time: 2:43 CH
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseAPIController;
use App\Models\Convenience;
use App\Models\CoreUsers;
use App\Models\Exterior;
use App\Models\FrontageType;
use App\Models\Product;
use App\Models\Category;
use App\Models\Purpose;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends BaseAPIController
{
    /**
     * @OA\Get(
     *   path="/category",
     *   tags={"user role"},
     *   summary="Get all global category",
     *   description="",
     *   operationId="CategoryGetAll",
     *     @OA\Parameter( name="only_root", description="flag get only_root, default 1", required=false, in="query",
     *         @OA\Schema( enum={0,1}, )
     *     ),
     *     @OA\Parameter( name="parent_id", description="parent_id", required=false, in="query",
     *         @OA\Schema( type="integer", )
     *     ),
     *     @OA\Parameter( name="type", description="top: lấy danh mục bán chạy", required=false, in="query",
     *         @OA\Schema( enum={"top"}, )
     *     ),
     *   @OA\Response(response="200", description="Success operation"),
     *   @OA\Response(response="500", description="Server error")
     * )
     */
    public function getAll(Request $request)
    {
        $parent_id = $request->get('parent_id', null);
        $only_root = $request->get('only_root', 1);
        $type = $request->get('type', null);

        $return = Category::with(['thumbnail', 'icon']);

        if ($only_root || $type == 'top') {
            $return->whereNull('parent_id')->orWhere('parent_id', 0);
        } else {
            if ($parent_id > 0)
                $return->where('parent_id', $parent_id);
        }

        $return = $return->get();

        return $this->returnResult($return);
    }
}