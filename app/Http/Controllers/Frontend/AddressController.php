<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseFrontendController;
use App\Models\Address;
use App\Models\CoreUsers;
use App\Models\Location\District;
use App\Models\Location\Province;
use App\Models\Location\Ward;
use App\Utils\Filter;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AddressController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $this->_data['title'] = 'Địa chỉ của tôi';
        $this->_data['menu_active'] = 'address';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Địa chỉ'
            ]
        ];

        $user = $this->getUser();

        $address = Address::where('user_id', $user->id)->get();
        $this->_data['address'] = $address;

        return view('frontend.user.address.index', $this->_data);
    }

    public function add(Request $request)
    {
        $this->_data['title'] = 'Thêm địa chỉ';
        $this->_data['menu_active'] = 'address';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Địa chỉ'
            ],
            [
                'link' => 'javascript:;',
                'name' => 'Thêm địa chỉ'
            ]
        ];

        $user = $this->getUser();

        $validate_rule = [
            'name'                 => 'required|string|max:255',
            'phone'                => "required|bail|numeric",
            'province_id'          => "required|bail|required|exists:lck_location_province,id",
            'district_id'          => "required|bail|required|exists:lck_location_district,id",
            'ward_id'              => "required|bail|required|exists:lck_location_ward,id",
            'street_name'          => 'required|bail|string|max:200',
            'is_default_recipient' => "nullable|bail|in:0,1",
            'is_warehouse'         => "nullable|bail|in:0,1",
            'is_return'            => "nullable|bail|in:0,1",
        ];

        if ($request->isMethod('POST')) {

            Validator::make($request->all(), $validate_rule)->validate();

            try {
                DB::beginTransaction();

                $params = array_fill_keys(array_keys($validate_rule), null);
                $params = array_merge(
                    $params, $request->only(array_keys($params))
                );

                $params['user_id'] = $user->id;
                $params['full_address'] = Address::get_full_address($params);
                $data = Address::create($params);

                if ($params['is_default_recipient'] == 1) {
                    Address::where('is_default_recipient', '=', 1)
                        ->where('id', '<>', $data->id)
                        ->update(['is_default_recipient' => 0]);
                }
                if ($params['is_warehouse'] == 1) {
                    Address::where('is_warehouse', '=', 1)
                        ->where('id', '<>', $data->id)
                        ->update(['is_warehouse' => 0]);
                }
                if ($params['is_return'] == 1) {
                    Address::where('is_return', '=', 1)
                        ->where('id', '<>', $data->id)
                        ->update(['is_return' => 0]);
                }

                DB::commit();

                return redirect(route('frontend.address.index'));
            } catch (\Exception $e) {
                DB::rollBack();
                $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!']);
                return redirect()->back();
            }
        }

        $this->_data['provinces'] = Province::orderBy('name', 'ASC')->get();
        $this->_data['districts'] = old('province_id') ? District::where('province_id', old('province_id'))->orderBy('position', 'ASC')->get() : [];
        $this->_data['wards'] = old('district_id') ? Ward::where('district_id', old('district_id'))->get() : [];

        $this->_data['address'] = (object)array_fill_keys(array_keys($validate_rule), null);

        return view('frontend.user.address.form', $this->_data);
    }

    public function edit(Request $request)
    {
        $this->_data['title'] = 'Sửa địa chỉ';
        $this->_data['menu_active'] = 'address';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Địa chỉ'
            ],
            [
                'link' => 'javascript:;',
                'name' => 'Sửa địa chỉ'
            ]
        ];

        $id = $request->get('id');

        if (!$id) return abort(404);

        $user = $this->getUser();

        $address = Address::where('user_id', $user->id)->where('id', $id)->first();

        if (!$address) return abort(404);

        if ($request->isMethod('POST')) {
            $validate_rule = [
                'name'                 => 'required|string|max:255',
                'phone'                => "required|bail|numeric",
                'province_id'          => "required|bail|required|exists:lck_location_province,id",
                'district_id'          => "required|bail|required|exists:lck_location_district,id",
                'ward_id'              => "required|bail|required|exists:lck_location_ward,id",
                'street_name'          => 'required|bail|string|max:200',
                'is_default_recipient' => "nullable|bail|in:0,1",
                'is_warehouse'         => "nullable|bail|in:0,1",
                'is_return'            => "nullable|bail|in:0,1",
            ];

            Validator::make($request->all(), $validate_rule)->validate();

            try {
                DB::beginTransaction();

                $params = array_fill_keys(array_keys($validate_rule), null);
                $params = array_merge(
                    $params, $request->only(array_keys($params))
                );

                $params['full_address'] = Address::get_full_address($params);

                Address::where('id', $id)->where('user_id', $user->id)->update($params);

                if ($params['is_default_recipient'] == 1) {
                    Address::where('is_default_recipient', '=', 1)
                        ->where('id', '<>', $id)
                        ->update(['is_default_recipient' => 0]);
                }
                if ($params['is_warehouse'] == 1) {
                    Address::where('is_warehouse', '=', 1)
                        ->where('id', '<>', $id)
                        ->update(['is_warehouse' => 0]);
                }
                if ($params['is_return'] == 1) {
                    Address::where('is_return', '=', 1)
                        ->where('id', '<>', $id)
                        ->update(['is_return' => 0]);
                }

                DB::commit();

                return redirect(route('frontend.address.index'));
            } catch (\Exception $e) {
                DB::rollBack();
                $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!']);
                return redirect()->back();
            }
        }

        $this->_data['provinces'] = Province::orderBy('name', 'ASC')->get();
        $this->_data['districts'] = District::where('province_id', $address->province_id)->orderBy('position', 'ASC')->get();
        $this->_data['wards'] = Ward::where('district_id', $address->district_id)->get();
        $this->_data['address'] = $address;

        return view('frontend.user.address.form', $this->_data);
    }

    public function delete(Request $request)
    {
        $user = $this->getUser();
        $id = $request->get('id');
        if ($id) {
            Address::where('user_id', $user->id)->where('id', $id)->delete();
        }
        return redirect()->back();
    }
}