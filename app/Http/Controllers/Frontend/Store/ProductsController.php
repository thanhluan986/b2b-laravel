<?php

namespace App\Http\Controllers\Frontend\Store;

use App\Http\Controllers\BaseFrontendController;
use App\Models\Category;
use App\Models\CoreUsers;
use App\Models\Files;
use App\Models\Product;
use App\Models\ProductImages;
use App\Models\Trademark;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductsController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $this->_data['title'] = 'Sản phẩm';
        $this->_data['menu_active'] = 'products';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Bán hàng'
            ],
            [
                'link' => 'javascript:;',
                'name' => 'Sản phẩm'
            ],
        ];

        $user = $this->getUser();

        $params['sort'] = $request->get('sort', 'newest');
        $params['limit'] = config('constant.item_perpage');
        $params['pagin_path'] = route('frontend.store.products.index') . '?' . http_build_query($params);

        $params['user_id'] = $user->id;
        $params['status'] = null;
        $products = Product::get_by_where($params);

        $this->_data['products'] = $products;

        return view('frontend.store.products.index', $this->_data);
    }

    public function create(Request $request)
    {
        $user = $this->getUser();

        if ($request->isMethod('POST')) {
            Validator::extend('file_ids_exists', function ($attribute, $value, $parameters, $validator) use ($user) {
                $count = DB::table('lck_files')->whereIn('id', $value)
                    ->where('user_id', $user->id)
                    //->where('is_temp', Files::IS_TEMP)
                    ->where('type', Files::TYPE_PRODUCT)
                    ->count();
                return $count !== 0;
            }, 'The image_file_ids invalid!');

            $validate_rule = [
                'name'               => 'required|string|min:10|max:255',
                'description'        => 'required|string|max:3000',
                'parent_category_id' => "nullable|bail|integer",
                'category_id'        => "required|bail|integer",
                'trademark_id'       => "required|bail|integer",

                'origin'          => "required|bail|string|max:255",
                'factory_address' => "nullable|bail|string|max:255",

                'price'     => 'required|bail|integer|min:1',
                'inventory' => 'required|bail|integer|min:1',

                'variations'         => 'nullable|bail|array',
                'product_variations' => 'nullable|bail|array',
                'wholesales'         => 'nullable|bail|array',

                'weight' => 'required|bail|integer|min:0',
                'width'  => 'nullable|bail|integer|min:0',
                'length' => 'nullable|bail|integer|min:0',
                'height' => 'nullable|bail|integer|min:0',

                'must_pre_order'  => 'nullable|bail|in:0,1',
                'pre_order_time'  => 'nullable|bail|integer|min:0',
                'is_used_product' => 'required|bail|in:0,1',
                'sku'             => 'nullable|bail|string|max:200',
                'status'          => 'required|bail|integer|in:0,1',

                'image_file_ids'    => "required|bail|array|file_ids_exists",
                'thumbnail_file_id' => "nullable|bail|integer",
            ];

            Validator::make($request->all(), $validate_rule)->validate();

            $params = array_fill_keys(array_keys($validate_rule), null);
            $params = array_merge(
                $params, $request->only(array_keys($params))
            );

            if (empty($params['parent_category_id']) || !isset($this->_data['array_categories'][$params['parent_category_id']])) {
                return redirect()->back()->withErrors(['category_id' => 'Danh mục không hợp lệ']);
            }
            if ($params['category_id'] && !isset($this->_data['array_categories'][$params['category_id']])) {
                return redirect()->back()->withErrors(['category_id' => 'Danh mục không hợp lệ']);
            }

            if (empty($params['category_id'])) {
                if (!empty($this->_data['array_tree_categories'][$params['parent_category_id']]['child'])) {
                    return redirect()->back()->withErrors(['category_id' => 'Vui lòng chọn danh mục con']);
                } else {
                    $params['category_id'] = $params['parent_category_id'];
                }
            }

            $image_file_ids = $params['image_file_ids'];
            unset($params['image_file_ids']);
            unset($params['parent_category_id']);

            try {
                DB::beginTransaction();

                $params['user_id'] = $user->id;
                $params['thumbnail_file_id'] = $image_file_ids[0];

                $product = Product::create($params);

                $product_id = $product->id;

                if ($product_id) {
                    $ProductImages = [];
                    foreach ($image_file_ids as $k => $v) {
                        $ProductImages[] = [
                            'product_id' => $product_id,
                            'file_id'    => $v,
                            'priority'   => $k,
                        ];
                    }
                    ProductImages::insert($ProductImages);
                }

                DB::commit();
                return redirect(route('frontend.store.products.index'));
            } catch (\Exception $e) {
                DB::rollBack();
                \Log::debug($e->getMessage());
                return redirect()->back()->withErrors($e->getMessage());
            }
        }

        $this->_data['title'] = 'Thêm phẩm mới';
        $this->_data['menu_active'] = 'products';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Bán hàng'
            ],
            [
                'link' => 'javascript:;',
                'name' => 'Sản phẩm'
            ],
            [
                'link' => 'javascript:;',
                'name' => 'Thêm sản phẩm mới'
            ]
        ];

        $categories_html = \App\Utils\Category::build_select_tree($this->_data['array_categories'], 0, '', [old('category_id')]);

        $this->_data['categories_html'] = $categories_html;
        $this->_data['trademarks'] = Trademark::all();
        $this->_data['file_images'] = old('image_file_ids') ? Files::whereIn('id', old('image_file_ids'))->get() : [];

        $this->_data['child_categories'] = old('parent_category_id') ? Category::where('parent_id', old('parent_category_id'))->get() : [];

        return view('frontend.store.products.create', $this->_data);
    }

    public function update(Request $request)
    {
        $user = $this->getUser();

        $product_id = $request->get('id');

        if (!$product_id)
            abort(404);

        $product = Product::where('id', $product_id)
            ->where('user_id', $user->id)->first();

        if (empty($product))
            abort(404);

        if ($request->isMethod('POST')) {
            Validator::extend('file_ids_exists', function ($attribute, $value, $parameters, $validator) use ($user) {
                $count = DB::table('lck_files')->whereIn('id', $value)
                    ->where('user_id', $user->id)
                    //->where('is_temp', Files::IS_TEMP)
                    ->where('type', Files::TYPE_PRODUCT)
                    ->count();
                return $count !== 0;
            }, 'The image_file_ids invalid!');

            $validate_rule = [
                'name'               => 'required|string|min:10|max:255',
                'description'        => 'required|string|max:3000',
                'parent_category_id' => "nullable|bail|integer",
                'category_id'        => "nullable|bail|integer",
                'trademark_id'       => "required|bail|integer",

                'origin'          => "required|bail|string|max:255",
                'factory_address' => "nullable|bail|string|max:255",

                'price'     => 'required|bail|integer|min:1',
                'inventory' => 'required|bail|integer|min:1',

                'variations'         => 'nullable|bail|array',
                'product_variations' => 'nullable|bail|array',
                'wholesales'         => 'nullable|bail|array',

                'weight' => 'required|bail|integer|min:0',
                'width'  => 'nullable|bail|integer|min:0',
                'length' => 'nullable|bail|integer|min:0',
                'height' => 'nullable|bail|integer|min:0',

                'must_pre_order'  => 'nullable|bail|in:0,1',
                'pre_order_time'  => 'nullable|bail|integer|min:0',
                'is_used_product' => 'required|bail|in:0,1',
                'sku'             => 'nullable|bail|string|max:200',
                'status'          => 'required|bail|integer|in:0,1',

                'image_file_ids'    => "required|bail|array|file_ids_exists",
                'thumbnail_file_id' => "nullable|bail|integer",
            ];

            Validator::make($request->all(), $validate_rule)->validate();

            $params = array_fill_keys(array_keys($validate_rule), null);
            $params = array_merge(
                $params, $request->only(array_keys($params))
            );

            if (empty($params['parent_category_id']) || !isset($this->_data['array_categories'][$params['parent_category_id']])) {
                return redirect()->back()->withErrors(['category_id' => 'Danh mục không hợp lệ']);
            }
            if ($params['category_id'] && !isset($this->_data['array_categories'][$params['category_id']])) {
                return redirect()->back()->withErrors(['category_id' => 'Danh mục không hợp lệ']);
            }

            if (empty($params['category_id'])) {
                if (!empty($this->_data['array_tree_categories'][$params['parent_category_id']]['child'])) {
                    return redirect()->back()->withErrors(['category_id' => 'Vui lòng chọn danh mục con']);
                } else {
                    $params['category_id'] = $params['parent_category_id'];
                }
            }

            $image_file_ids = $params['image_file_ids'];
            unset($params['image_file_ids']);
            unset($params['parent_category_id']);
            unset($params['variations'], $params['product_variations'], $params['wholesales']);

            try {
                DB::beginTransaction();

                $params['thumbnail_file_id'] = $image_file_ids[0];

                Product::where('id', $product_id)->update($params);

                ProductImages::where('product_id', $product_id)->delete();

                $product_id = $product->id;

                if ($product_id) {
                    $ProductImages = [];
                    foreach ($image_file_ids as $k => $v) {
                        $ProductImages[] = [
                            'product_id' => $product_id,
                            'file_id'    => $v,
                            'priority'   => $k,
                        ];
                    }
                    ProductImages::insert($ProductImages);
                }

                DB::commit();
                return redirect(route('frontend.store.products.index'));
            } catch (\Exception $e) {
                DB::rollBack();
                \Log::debug($e->getMessage());
                return redirect()->back()->withErrors($e->getMessage());
            }
        }

        $this->_data['title'] = 'Cập nhật sản phẩm';
        $this->_data['menu_active'] = 'products';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Bán hàng'
            ],
            [
                'link' => 'javascript:;',
                'name' => 'Sản phẩm'
            ],
            [
                'link' => 'javascript:;',
                'name' => 'Cập nhật sản phẩm'
            ]
        ];

        $categories_html = \App\Utils\Category::build_select_tree($this->_data['array_categories'], 0, '', [old('category_id')]);

        $this->_data['categories_html'] = $categories_html;
        $this->_data['trademarks'] = Trademark::all();

        $file_images_id = ProductImages::where('product_id', $product_id)
            ->orderBy('priority', 'ASC')
            ->get()
            ->pluck('file_id')->toArray();

        $file_images_id = old('image_file_ids', $file_images_id);

        $s_file_images_id = $file_images_id ? implode(',', $file_images_id) : null;

        $this->_data['file_images'] = $s_file_images_id ? Files::whereIn('id', $file_images_id)->orderByRaw("FIND_IN_SET(id, '{$s_file_images_id}')")->get() : [];

        $category = isset($this->_data['array_categories'][$product->category_id]) ? $this->_data['array_categories'][$product->category_id] : null;
        $parent_category_id = !empty($category) && !empty($category['parent_id']) ? $category['parent_id'] : $product->category_id;

        $this->_data['child_categories'] = old('parent_category_id', $parent_category_id) ? Category::where('parent_id', old('parent_category_id', $parent_category_id))->get() : [];

        $this->_data['parent_category_id'] = $parent_category_id;
        $this->_data['product'] = $product;

        return view('frontend.store.products.update', $this->_data);
    }

}