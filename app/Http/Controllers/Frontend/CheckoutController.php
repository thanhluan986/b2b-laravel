<?php

namespace App\Http\Controllers\Frontend;

use App\Classes\ActivationService;
use App\Http\Controllers\BaseFrontendController;
use App\Models\Address;
use App\Models\Basket;
use App\Models\CoreUsers;
use App\Models\Orders;
use App\Models\OrdersDetail;
use App\Models\Product;
use App\Utils\Filter;
use Illuminate\Foundation\Auth\RegistersUsers;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CheckoutController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $this->_data['title'] = 'Đặt hàng';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Đặt hàng'
            ],
            [
                'link' => 'javascript:;',
                'name' => 'Thanh toán'
            ]
        ];

        $state = $request->get('state');
        $basket_data = [];

        $address_id = null;

        try {
            $state = base64_decode($state);
            $basket_data = json_decode($state, true);
        } catch (\Exception $e) {

        }

        if (empty($basket_data['basket_item_ids']))
            return redirect(route('frontend.basket.index'));

        $address_id = isset($basket_data['address_id']) ? $basket_data['address_id'] : null;

        $user = $this->getUser();

        if ($address_id) {
            $address = Address::where('id', $address_id)
                ->where('user_id', $user->id)
                ->first();
            if (!$address) {
                $basket_data = [
                    'basket_item_ids' => $basket_data['basket_item_ids']
                ];
                $state = base64_encode(json_encode($basket_data));
                return redirect(route('frontend.checkout.index') . '?state=' . $state);
            }
        }

        $params['item_ids'] = $basket_data['basket_item_ids'];
        $params['user_id'] = $user->id;
        $params['pagin'] = false;

        $baskets = Basket::get_all_product($params);

        if (empty($baskets))
            return redirect(route('frontend.basket.index'));

        if ($request->isMethod('POST')) {
            $submit_type = $request->get('submit_type', 'checkout');

            if ($submit_type == 'change_address') {
                $address_id = $request->get('address_id');
                $basket_data = [
                    'basket_item_ids' => $basket_data['basket_item_ids'],
                    'address_id'      => $address_id,
                ];
                $state = base64_encode(json_encode($basket_data));
                return redirect(route('frontend.checkout.index') . '?state=' . $state);
            } else {

                $payment_method_id = $request->get('payment_method_id');
                $address_id = $request->get('address_id');

                $notes = $request->get('notes', []);
                $shipping_methods = $request->get('shipping_methods', []);

                $shipping_address = Address::where('user_id', $user->id)
                    ->where('id', $address_id)
                    ->first();

                DB::beginTransaction();

                try {
                    foreach ($baskets as $store_id => $products) {

                        $total_product_price = $total_price = $total_discount_amount = $total_weight = $total_shipping_fee = 0;
                        $order_detail = [];

                        foreach ($products as $product) {
                            $price = isset($product->product_variation_price) ? $product->product_variation_price : $product->price;
                            $price_old = isset($product->product_variation_price) ? $product->product_variation_price : $product->price_old;
                            $discount_amount = $price < $price_old ? $price_old - $price : 0;

                            $total_product_price += $price * $product->quantity;
                            $total_discount_amount += $discount_amount;
                            $total_weight += $product->weight * $product->quantity;

                            $order_detail[] = [
                                'order_id'            => null,
                                'product_id'          => $product->product_id,
                                'product_name'        => $product->name,
                                'thumbnail_file_path' => $product->thumbnail->file_path,
                                'category_name'       => null,
                                'variation_name'      => isset($product->product_variation_name),
                                'price'               => $price,
                                'price_old'           => $price_old,
                                'discount_amount'     => $discount_amount,
                                'quantity'            => $product->quantity,
                                'sku'                 => isset($product->product_variation_sku) ? $product->product_variation_sku : $product->sku,
                                'weight'              => $product->weight,
                            ];
                        }

                        if ($order_detail) {
                            $order = Orders::create([
                                'order_code'            => md5(microtime()),
                                'total_product_price'   => $total_product_price,
                                'discount_amount'       => $total_discount_amount,
                                'discount_percent'      => 0,
                                'discount_code'         => 0,
                                'shipping_fee'          => $total_shipping_fee,
                                'free_shipping'         => 0,
                                'payment_method'        => $payment_method_id,
                                'status_payment'        => 0,
                                'shipping_method'       => isset($shipping_methods[$store_id]) ? $shipping_methods[$store_id] : null,
                                'shipping_code'         => '',
                                'total_weight'          => $total_weight,
                                'shipping_name'         => $shipping_address->name,
                                'shipping_phone'        => $shipping_address->phone,
                                'shipping_province_id'  => $shipping_address->province_id,
                                'shipping_district_id'  => $shipping_address->district_id,
                                'shipping_ward_id'      => $shipping_address->ward_id,
                                'shipping_street_name'  => $shipping_address->street_name,
                                'shipping_address'      => $shipping_address->full_address,
                                'warehouse_name'        => null,
                                'warehouse_phone'       => null,
                                'warehouse_province_id' => null,
                                'warehouse_district_id' => null,
                                'warehouse_ward_id'     => null,
                                'warehouse_street_name' => null,
                                'warehouse_address'     => null,
                                'status'                => 1,
                                'total_price'           => $total_product_price + $total_shipping_fee,
                                'note'                  => isset($notes[$store_id]) ? $notes[$store_id] : null,
                                'user_id'               => $user->id,
                                'store_id'              => $store_id,
                            ]);

                            if ($order) {
                                foreach ($order_detail as $k => $v) {
                                    $order_detail[$k]['order_id'] = $order->id;
                                }
                                OrdersDetail::insert($order_detail);
                            }
                        }
                    }

                    Basket::whereIn('id', $basket_data['basket_item_ids'])->delete();

                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollBack();
                    return redirect()->back();
                }
                return redirect(route('frontend.user.orders'));
            }
        }

        $baskets = count($baskets) ? $baskets->toArray() : [];

        $store_ids = $baskets ? array_keys($baskets) : [];

        if (empty($store_ids))
            return redirect(route('frontend.basket.index'));

        $stores = CoreUsers::select(['id', 'fullname', 'username', 'avatar_file_path', 'cover_file_path',])
            ->whereIn('id', $store_ids)->get();

        foreach ($stores as $store) {
            if (isset($baskets[$store->id])) {
                $store->products = $baskets[$store->id];
            }
        }

        $this->_data['baskets'] = $stores;
        $this->_data['address_id'] = $address_id;
        $this->_data['address'] = Address::where('user_id', $user->id)->get();

        return view('frontend.checkout.index', $this->_data);
    }
}