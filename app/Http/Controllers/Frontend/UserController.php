<?php

namespace App\Http\Controllers\Frontend;

use App\Classes\ActivationService;
use App\Http\Controllers\BaseFrontendController;
use App\Models\CoreUsers;
use App\Utils\Filter;
use Illuminate\Foundation\Auth\RegistersUsers;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends BaseFrontendController
{
    use RegistersUsers;

    public function __construct(ActivationService $activationService)
    {
        parent::__construct();
        $this->activationService = $activationService;
    }

    public function profile(Request $request)
    {
        $this->_data['title'] = 'Tài khoản';
        $this->_data['menu_active'] = 'profile';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Tài khoản'
            ]
        ];

        $user = $this->getUser();

        if ($request->isMethod('POST')) {
            $validate_rule = [
                'fullname'              => 'nullable|string|max:255',
                'email'                 => "nullable|bail|string|unique:lck_core_users,email,{$user->id}",
                'phone'                 => "nullable|bail|numeric",
                'gender'                => ['nullable', 'bail', Rule::in([CoreUsers::GENDER_MALE, CoreUsers::GENDER_FEMALE])],
                'birthday'              => 'nullable|bail|date|date_format:Y-m-d',
                'cardid'                => 'nullable|bail|string',
                'avatar_file_path'      => "nullable|bail|string|avatarExits",
                'cover_file_path'       => "nullable|bail|string|coverExits",
                'password_old'          => 'nullable|bail|string',
                'password'              => 'nullable|bail|string|min:6',
                'password_confirmation' => 'nullable|bail|string|min:6',
                'description'           => 'nullable|string|max:500',
            ];

            Validator::make($request->all(), $validate_rule)->validate();

            try {
                DB::beginTransaction();

                $user = CoreUsers::findOrFail($user->id);

                $password_old = $request->get('password_old', null);
                $password = $request->get('password', null);
                $password_confirmation = $request->get('password_confirmation', null);

                if ($password) {
                    if (empty($password_old))
                        return redirect()->back()->withErrors(['password_old' => 'Vui lòng nhập mật khẩu cũ'])->withInput($request->all());
                    if (!Hash::check($password_old, $user->password))
                        return redirect()->back()->withErrors(['password_old' => 'Mật khẩu cũ không hợp lệ!'])->withInput($request->all());
                    if ($password != $password_confirmation)
                        return redirect()->back()->withErrors(['password_confirmation' => 'Xác nhận mật khẩu mới không khớp!'])->withInput($request->all());

                    $user->password = Hash::make($password);
                }

                $new_avatar = $request->get('avatar_file_path', null);
                $new_avatar = $new_avatar ? $new_avatar : $user->avatar_file_path;
                if ($user->avatar_file_path != $new_avatar) {
                    if ($user->avatar_file_path)
                        File::delete($user->avatar_file_path);

                    $file = Files::where(['file_path' => $new_avatar])->first();
                    $file->is_temp = null;
                    $file->save();
                    $user->avatar_file_path = $new_avatar;
                }

                $new_cover = $request->get('cover_file_path', null);
                $new_cover = $new_cover ? $new_cover : $user->cover_file_path;
                if ($user->cover_file_path != $new_cover) {
                    if ($user->cover_file_path)
                        File::delete($user->cover_file_path);

                    $file = Files::where(['file_path' => $new_cover])->first();
                    $file->is_temp = null;
                    $file->save();
                    $user->cover_file_path = $new_cover;
                }

                $user->phone = $request->get('phone', $user->phone);
                $user->email = $request->get('email', $user->email);
                $user->fullname = $request->get('fullname', $user->fullname);
                $user->cardid = $request->get('cardid', $user->cardid);
                $user->gender = $request->get('gender', $user->gender);
                $user->birthday = $request->get('birthday', $user->birthday);
                $user->description = $request->get('description', $user->description);
                $user->save();

                DB::commit();

                $request->session()->flash('msg', ['info', 'Cập nhật thành công!']);
                return redirect()->back();
            } catch (\Exception $e) {
                DB::rollBack();
                $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!']);
                return redirect()->back();
            }
        }

        return view('frontend.user.profile', $this->_data);
    }

    public function index()
    {
        $data = array(
            'title' => 'Đăng nhập / Đăng ký',
        );

        $this->_data['menu_active'] = 'login';

        return view('frontend.user.login', $this->_data);
    }

    public function login()
    {
        $data = array(
            'title' => 'Đăng nhập / Đăng ký',
        );

        $this->_data['menu_active'] = 'login';

        return view('frontend.user.login', $this->_data);
    }

    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
            die('Xác thực tài khoản thành công!');
        } else {
            die('Xác thực tài khoản thất bại!');
        }
    }

    public function logout(Request $request)
    {
        $ref = $request->get('_ref', config('app.url'));
        Auth::shouldUse('frontend');
        if (Auth()->guard('frontend')->user()->id) {
            Auth()->guard('frontend')->logout();
        }
        return redirect($ref);
    }
}