<?php

namespace App\Http\Controllers\Frontend\Ajax;

use App\Http\Controllers\BaseFrontendController;
use App\Classes\ActivationService;
use App\Mail\OTPEmail;
use App\Models\CoreUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseFrontendController
{
    public function __construct(ActivationService $activationService)
    {
        parent::__construct();
        $this->activationService = $activationService;
    }

    public function login(Request $request)
    {
        if (!$request->isXmlHttpRequest())
            exit;

        if ($request->isMethod('POST')) {

            $validator = Validator::make($request->all(), [
                'credential' => 'required',
                'password'   => 'required',
            ]);

            if ($validator->fails()) {
                return $this->throwError($validator->errors()->first(), 1);
            }

            $credentials = $request->only(['credential', 'password']);

            try {
                $user = CoreUsers::where(['email' => $credentials['credential']])
                    ->orWhere(['username' => $credentials['credential']])
                    ->first();

                if (!$user || !Hash::check($credentials['password'], $user->password))
                    return $this->throwError(['credential' => 'Thông tin đăng nhập không hợp lệ!'], 1);

                if ($user->status == CoreUsers::$status_banned)
                    return $this->throwError(['system' => 'Tài khoản đã bị khóa!'], 1);

                Auth::shouldUse('frontend');
                Auth::guard('frontend')->login($user, true);

                return $this->returnResult();
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return $this->throwError(['system' => 'Đăng nhập thất bại!'], 1);
            }
        }
        return view('frontend.ajax.login', $this->_data);
    }

    public function register(Request $request)
    {
        if (!$request->isXmlHttpRequest())
            exit;

        if ($request->isMethod('POST')) {

            $validator = Validator::make($request->all(), [
                'username' => "bail|required|alpha_dash|max:255|unique:lck_core_users,username",
                'phone'    => 'bail|nullable|numeric|unique:lck_core_users,phone',
                'email'    => "bail|required|email|unique:lck_core_users,email",
                'password' => 'bail|required|string|min:6|confirmed',
                'fullname' => 'bail|nullable|string|max:255',
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();
                $error_return = [];

                if ($errors->first('username'))
                    $error_return['username'] = $errors->first('username');
                if ($errors->first('phone'))
                    $error_return['phone'] = $errors->first('phone');
                if ($errors->first('email'))
                    $error_return['email'] = $errors->first('email');
                if ($errors->first('password'))
                    $error_return['password'] = $errors->first('password');
                if ($errors->first('password_confirmed'))
                    $error_return['password_confirmed'] = $errors->first('password_confirmed');

                return $this->throwError($error_return, 1);
            }

            try {
                \DB::beginTransaction();
                $user = new CoreUsers();

                $user->password = Hash::make($request->get('password'));
                $user->status = CoreUsers::$status_inactive;

                $username = $request->get('username', null);
                $phone = $request->get('phone', null);
                $email = $request->get('email', null);
                $fullname = $request->get('fullname', null);

                $user->username = $username;
                $user->phone = $phone;
                $user->email = $email;
                $user->fullname = $fullname;

                $user->save();

                $this->activationService->sendActivationMail($user);

                \DB::commit();

                return $this->returnResult([], 'Đăng ký tài khoản thành công! Vui lòng kiểm tra hộp thư email và kích hoạt tài khoản!');
            } catch (\Exception $e) {
                \DB::rollBack();
                \Log::error($e->getMessage());
                return $this->throwError(['system' => 'Có lỗi xảy ra, vui lòng thử lại!'], 1);
            }
        }

        return view('frontend.ajax.register', $this->_data);
    }
}