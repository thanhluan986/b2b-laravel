<?php

namespace App\Http\Controllers\Frontend\Ajax;

use App\Http\Controllers\BaseFrontendController;
use Illuminate\Http\Request;
use Validator;
use App\Utils\Filter;
use App\Models\Files;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;


class ImageController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function upload(Request $request)
    {
        $user = $this->getUser();

        $photos = $request->file('file');
        if (empty($photos))
            return \Response::json(['e' => 1, 'r' => 'Vui lòng chọn hình upload!']);

        if (!is_array($photos)) {
            $photos = [$photos];
        }

        $sub_dir = date('Y/m/d');
        $full_dir = config('constants.upload_dir.root') . '/' . $sub_dir;

        if (!is_dir($full_dir)) {
            mkdir($full_dir, 0777, true);
        }

        $items = [];
        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];

            $filename = md5(rand(0, 999999) . microtime()) . '.' . $photo->getClientOriginalExtension();

            $image = Image::make($photo);

            if ($image->getWidth() > 1920)
                $image->resize(1920, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

            $size = [];

            $width = $size[0] = 800;
            $height = $size[1] = 800;

            $image->width() < $image->height() ? $width = null : $height = null;

            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });

            $width = $size[0];

            $image2 = Image::canvas($width, $size[1]);

            $image2->insert($image, 'center')
                ->save($full_dir . '/' . $filename, 90);

            $file_path = $sub_dir . '/' . $filename;

            $type = $request->get('type', Files::TYPE_PRODUCT);

            $image = Files::create([
                'user_id'   => $user->id,
                'file_path' => $file_path,
                'type'      => $type
            ]);
            $items[] = [
                'id'   => $image->id,
                'path' => $file_path,
                'url'  => config('constants.upload_dir.url') . '/' . $file_path,
            ];
        }

        return \Response::json(['e' => 0, 'r' => $items]);
    }

    public function remove(Request $request)
    {
        return \Response::json([]);
    }
}