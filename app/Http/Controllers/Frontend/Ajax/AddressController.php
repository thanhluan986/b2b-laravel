<?php

namespace App\Http\Controllers\Frontend\Ajax;

use App\Http\Controllers\BaseFrontendController;
use App\Models\Address;
use App\Models\Location\District;
use App\Models\Location\Province;
use App\Models\Location\Ward;
use Illuminate\Http\Request;
use Validator;
use App\Utils\Filter;
use App\Models\Files;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;


class AddressController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add(Request $request)
    {
        $user = $this->getUser();

        if (empty($user))
            return $this->throwError('Vui lòng đăng nhập để tiếp tục!', 401);

        $validate_rule = [
            'name'                 => 'required|string|max:255',
            'phone'                => "required|bail|numeric",
            'province_id'          => "required|bail|required|exists:lck_location_province,id",
            'district_id'          => "required|bail|required|exists:lck_location_district,id",
            'ward_id'              => "required|bail|required|exists:lck_location_ward,id",
            'street_name'          => 'required|bail|string|max:200',
            'is_default_recipient' => "nullable|bail|in:0,1",
            'is_warehouse'         => "nullable|bail|in:0,1",
            'is_return'            => "nullable|bail|in:0,1",
        ];

        if ($request->isMethod('POST')) {

            $validator = Validator::make($request->all(), $validate_rule);

            if ($validator->fails()) {
                $errors = $validator->errors();
                $error_return = [];

                foreach (array_keys($validate_rule) as $v) {
                    if ($errors->first($v))
                        $error_return[$v] = $errors->first($v);
                }

                return $this->throwError($error_return, 1);
            }

            try {
                DB::beginTransaction();

                $params = array_fill_keys(array_keys($validate_rule), null);
                $params = array_merge(
                    $params, $request->only(array_keys($params))
                );

                $params['user_id'] = $user->id;
                $params['full_address'] = Address::get_full_address($params);
                $data = Address::create($params);

                if ($params['is_default_recipient'] == 1) {
                    Address::where('is_default_recipient', '=', 1)
                        ->where('id', '<>', $data->id)
                        ->update(['is_default_recipient' => 0]);
                }
                if ($params['is_warehouse'] == 1) {
                    Address::where('is_warehouse', '=', 1)
                        ->where('id', '<>', $data->id)
                        ->update(['is_warehouse' => 0]);
                }
                if ($params['is_return'] == 1) {
                    Address::where('is_return', '=', 1)
                        ->where('id', '<>', $data->id)
                        ->update(['is_return' => 0]);
                }

                DB::commit();

                $this->returnResult();
            } catch (\Exception $e) {
                DB::rollBack();
                return $this->throwError('Có lỗi xảy ra, vui lòng thử lại!', 500);
            }
        }

        $this->_data['provinces'] = Province::orderBy('name', 'ASC')->get();
        $this->_data['districts'] = old('province_id') ? District::where('province_id', old('province_id'))->orderBy('position', 'ASC')->get() : [];
        $this->_data['wards'] = old('district_id') ? Ward::where('district_id', old('district_id'))->get() : [];

        $this->_data['address'] = (object)array_fill_keys(array_keys($validate_rule), null);

        $html = view('frontend.ajax.address.add', $this->_data)->render();

        return $this->returnResult(['html' => $html]);
    }
}