<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseFrontendController;
use App\Models\Banners;
use App\Models\CoreUsers;
use App\Models\Location\Province;
use App\Models\Product;
use App\Models\ProductImages;
use App\Models\ProductVariation;
use App\Utils\Category;
use App\Utils\Filter;
use Illuminate\Http\Request;
use View;
use Illuminate\Support\Facades\Auth;

class IndexController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $this->_data['menu_active'] = 'home';

        $this->_data['banners'] = Banners::with(['image'])
            ->where('status', Banners::STATUS_SHOW)
            ->where('type', 'home')
            ->limit(5)->get();

        $trending_products = Product::get_by_where([
            'pagin' => false,
            'limit' => 8,
            'sort'  => 'newest',
        ]);

        $this->_data['trending_products'] = $trending_products;

        $top_sale_products = Product::get_by_where([
            'pagin' => false,
            'limit' => 8,
            'sort'  => 'newest',
        ]);

        $this->_data['top_sale_products'] = $top_sale_products;

        $suggested_products = Product::get_by_where([
            'pagin' => false,
            'limit' => 8,
            'sort'  => 'newest',
        ]);

        $this->_data['suggested_products'] = $suggested_products;

//        $category = \App\Models\Category::all();

//        foreach ($category as $cate) {
//            //$cate->name = ucfirst(mb_strtolower($cate->name));
//            $cate->slug = Filter::setSeoLink($cate->name);
//            $cate->save();
//        }

        return view('frontend.index.index', $this->_data);
    }
}