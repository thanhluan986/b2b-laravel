<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseFrontendController;
use App\Utils\Filter;
use View;

class NewsController extends BaseFrontendController
{
    protected $_data = [];

    public function index()
    {
        $data = array(
            'title' => 'Tin tức',
        );

        $this->_data['menu_active'] = 'news';

        return view('frontend.news.index', $this->_data);
    }

    public function detail()
    {
        $data = array(
            'title' => 'Chi tiết tin tức',
        );

        $this->_data['menu_active'] = 'news';

        return view('frontend.news.detail', $this->_data);
    }
}