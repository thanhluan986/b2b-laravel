<?php

namespace App\Http\Controllers\Frontend;

use App\Classes\ActivationService;
use App\Http\Controllers\BaseFrontendController;
use App\Models\Basket;
use App\Models\CoreUsers;
use App\Models\Product;
use App\Utils\Filter;
use Illuminate\Foundation\Auth\RegistersUsers;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BasketController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ajax(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $user = $this->getUser();

            $params['user_id'] = $user->id;
            $params['pagin'] = false;
            $params['group'] = false;
            $basket = Basket::get_all_product($params);

            $total = count($basket);

            $this->_data['basket'] = $basket;

            $html = view('frontend.basket.ajax', $this->_data)->render();

            return $this->returnResult(['html' => $html, 't' => $total]);
        }
    }

    public function index(Request $request)
    {
        $this->_data['title'] = 'Giỏ hàng';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Giỏ hàng'
            ]
        ];

        $user = $this->getUser();

        if ($request->isMethod('POST')) {
            $basket_item_ids = $request->get('basket_item_ids');

            if (!empty($basket_item_ids) && is_array($basket_item_ids)) {
                $basket_data = [
                    'basket_item_ids' => $basket_item_ids
                ];
                $state = base64_encode(json_encode($basket_data));
                return redirect(route('frontend.checkout.index') . '?state=' . $state);
            }
        }

        $params['user_id'] = $user->id;
        $params['pagin'] = false;

        $basket = Basket::get_all_product($params);
        $stores = [];

        if (count($basket)) {

            $basket = count($basket) ? $basket->toArray() : [];

            $store_ids = $basket ? array_keys($basket) : [];

            $stores = CoreUsers::select(['id', 'fullname', 'username', 'avatar_file_path', 'cover_file_path',])
                ->whereIn('id', $store_ids)->get();

            foreach ($stores as $store) {
                if (isset($basket[$store->id])) {
                    $store->products = $basket[$store->id];
                }
            }
        }

        $this->_data['baskets'] = $stores;

        return view('frontend.basket.index', $this->_data);
    }

    public function add(Request $request)
    {
        $user = $this->getUser();

        if (empty($user))
            return $this->throwError('Vui lòng đăng nhập để tiếp tục!', -1);

        if ($request->isMethod('POST')) {
            $product_id = $request->get('product_id');
            $product_variation_id = $request->get('product_variation_id', null);
            $quantity = $request->get('quantity');

            if (!$product_id || !$quantity || $quantity < 1)
                return $this->throwError('Dữ liệu không hợp lệ!', 400);

            $product = Product::where('id', $product_id)
                ->where('status', Product::STATUS_ACTIVE)
                ->first();

            $error = null;

            if (empty($product))
                $error = 'Sản phẩm không tồn tại!';

            if ($product->user_id == $user->id)
                return $this->throwError('Dữ liệu không hợp lệ!', 400);

            if ($product->inventory < $quantity)
                $error = 'Kho hàng của sản phẩm không có đủ số lượng bạn chọn!';

            if (empty($error)) {
                $basket = Basket::where('user_id', $user->id)->where('product_id', $product_id);

                $basket = $basket->first();

                if (empty($basket)) {
                    Basket::create([
                        'user_id'              => $user->id,
                        'product_id'           => $product_id,
                        'product_variation_id' => $product_variation_id,
                        'quantity'             => $quantity,
                    ]);
                } else {
                    $new_quantity = $basket->quantity + $quantity;
                    if ($product->inventory < $new_quantity) {
                        $error = 'Kho hàng của sản phẩm không có đủ số lượng bạn chọn!';
                    } else {
                        $basket->quantity = $new_quantity;
                        $basket->save();
                    }
                }
            }

            $message = 'Thêm sản phẩm vào giỏ hàng thành công!';

            $this->_data['message'] = $message;
            $this->_data['error'] = $error;

            $html = view('frontend.basket.add', $this->_data)->render();;

            return $this->returnResult(['html' => $html]);
        }

        exit;
    }

    public function update(Request $request)
    {
        if ($request->isMethod('POST')) {
            $basket_item_id = $request->get('basket_item_id');
            $quantity = $request->get('quantity');

            if (!$basket_item_id || $quantity < 0)
                return $this->throwError('Dữ liệu không hợp lệ!', 400);

            $user = $this->getUser();

            $basket = Basket::where('user_id', $user->id)->where('id', $basket_item_id);

            $basket = $basket->first();

            if (empty($basket))
                return $this->throwError('Dữ liệu không hợp lệ!', 400);

            $product = Product::where('id', $basket->product_id)
                ->where('status', Product::STATUS_ACTIVE)
                ->first();

            if (!$product)
                return $this->throwError('Dữ liệu không hợp lệ!', 400);

            if ($product->user_id == $user->id)
                return $this->throwError('Dữ liệu không hợp lệ!', 400);

            if ($product->inventory < $quantity)
                return $this->throwError('Kho hàng của sản phẩm không có đủ số lượng bạn chọn!', 400);

            if ($quantity < 1) {
                $basket->delete();
            } else {
                $basket->quantity = $quantity;
                $basket->save();
            }

            return $this->returnResult();
        }

        exit;
    }

    public function remove(Request $request)
    {
        $id = $request->get('id');
        $user = $this->getUser();
        $basket = Basket::where('user_id', $user->id)->where('id', $id)->delete();
        return redirect(route('frontend.basket.index'));

    }

}