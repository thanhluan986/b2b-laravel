<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseFrontendController;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrdersController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $this->_data['title'] = 'Đơn mua';
        $this->_data['menu_active'] = 'my_orders';
        $this->_data['breadcrumbs'] = [
            [
                'link' => 'javascript:;',
                'name' => 'Đơn mua'
            ]
        ];

        $user = $this->getUser();

        $params['pagin_path'] = route('orders.getAll') . '?' . http_build_query($request->all());

        $params['user_id'] = $user->id;
        $orders = Orders::get_by_where($params);

        $this->_data['orders'] = $orders;

        return view('frontend.user.orders.index', $this->_data);
    }
}