<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseFrontendController;
use App\Http\Controllers\BaseFrontendNewsController;
use App\Models\Post;
use App\Models\PostCategory;
use App\Utils\Category;
use App\Utils\Common as Utils;
use Illuminate\Http\Request;

class BlogController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();

        $categories = PostCategory::get_all();

        $this->_data['all_categories'] = $categories;
        $this->_data['category_id'] = null;

        $tree_categories = Category::buildTree($categories);

        $this->_data['tree_categories'] = $tree_categories;

        $this->_data['top_post'] = Post::get_by_where([
            'pagin'  => false,
            'limit'  => 5,
            'sort'   => 'top-view',
            'status' => Post::STATUS_SHOW,
        ]);
    }

    public function index(Request $request, $cate_slug = null, $cate_id = null)
    {
        $filter = array_merge(array(
            'page' => null,
            'q'    => null,
        ), $request->all());

        $category_ids = [];
        $category = [];

        if ($cate_slug) {
            $category = PostCategory::where('slug', $cate_slug)
                ->where('status', PostCategory::STATUS_SHOW)
                ->first();

            if (empty($category))
                return abort(404);

            $category_ids[] = $category->id;

            $category_ids = array_merge($category_ids, Category::get_all_child_categories($this->_data['all_categories'], $category->id));

            $this->_data['category_id'] = $category->id;
        }

        $params = [
            'pagin_path'   => Utils::get_pagin_path($filter),
            'keyword'      => $filter['q'],
            'status'       => Post::STATUS_SHOW,
            'category_ids' => array_unique($category_ids),
        ];

        $this->_data['items'] = Post::get_by_where($params);

        $this->_data['title'] = 'Blog';
        $this->_data['category'] = $category;

        if ($category)
            $this->_data['title'] = $category->name . ' | Blog';

        return view('frontend.blog.index', $this->_data);
    }

    public function detail($slug, $id)
    {
        $post = Post::where('id', $id)->where('status', Post::STATUS_SHOW)
            ->with(['image', 'image_fb', 'category'])
            ->first();

        if (empty($post))
            return abort(404);

        $this->_data['title'] = $post->name;
        $this->_data['post'] = $post;

        $post->views = (int)$post->views + 1;
        $post->save();

        $relative_post = [];

        if ($post->category_id) {
            $relative_post = Post::get_by_where([
                'pagin'        => false,
                'limit'        => 2,
                'category_ids' => [$post->category_id],
                'skip_ids'     => [$id],
                'status'       => Post::STATUS_SHOW,
            ]);
        }

        $this->_data['relative_post'] = $relative_post;
        $this->_data['category_id'] = $post->id;

        return view('frontend.blog.detail', $this->_data);
    }
}