<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseFrontendController;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\ProductWholesales;
use App\Models\VariationValues;
use App\Utils\Category;
use Illuminate\Http\Request;

class ProductsController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($slug = null, $id = null, Request $request)
    {
        $aInit = [
            'category_ids'       => null,
            'store_id'           => null,
            'store_category_ids' => null,
            'trademark_ids'      => null,
            'q'                  => null,
            'price_from'         => null,
            'price_to'           => null,
            'skip_product_ids'   => null,
            'sort'               => null,
            'type'               => null,
        ];

        $params = array_merge(
            $aInit, $request->only(array_keys($aInit))
        );

        $category_ids = [];
        $category = [];
        $root_category_id = null;

        if ($id) {
            $category = isset($this->_data['array_categories'][$id]) ? $this->_data['array_categories'][$id] : null;

            if (empty($category))
                return abort(404);

            $category_ids[] = $id;

            $category_ids = array_merge($category_ids, Category::get_all_child_categories($this->_data['array_categories'], $id));

            $root_category_id = Category::find_root_parent($this->_data['array_categories'], $id);
        }

        $keywords = !empty($params['q']) ? strip_tags($params['q']) : null;

        $this->_data['keywords'] = $keywords;
        $this->_data['category_id'] = $id;
        $this->_data['root_category_id'] = $root_category_id;

        $params = array_filter($params);
        $params['pagin_path'] = url()->current() . '?' . http_build_query($params);
        $params['keywords'] = $keywords;

        $params['category_ids'] = $category_ids;
        $params['sort'] = $request->get('sort', 'newest');
        $params['limit'] = 16;
        $products = Product::get_by_where($params);

        $this->_data['products'] = $products;
        $this->_data['title'] = $keywords ? "Kết quả tìm kiếm cho từ khóa '{$keywords}'" : ($category ? $category['name'] : '');

        return view('frontend.products.index', $this->_data);
    }

    public function detail($slug, $id, Request $request)
    {
        $id = explode('-', $id);
        $id = end($id);

        $product = Product::with(['category', 'store_category', 'store', 'thumbnail', 'images', 'images_extra',])
            ->find($id);

        if (!$product) {
            return abort(404);
        }

        $variations = VariationValues::get_group_variations($id);

        $product_variations = [];
        $variation_combine = [];

        if (count($variations))
            $product_variations = ProductVariation::get_product_variations_with_combine($id);

        $variations = collect(['variations' => $variations]);

        $product_wholesales = ProductWholesales::select(['id', 'quantity_from', 'quantity_to', 'price'])->where('product_id', $id)->get();


        $this->_data['product'] = $product;
        $this->_data['variations'] = $variations;
        $this->_data['product_variations'] = $product_variations;
        $this->_data['variation_combine'] = $variation_combine;
        $this->_data['product_wholesales'] = $product_wholesales;

        $this->_data['title'] = $product->name;

        $products_shop = Product::get_by_where([
            'pagin'    => false,
            'store_id' => $product->store->id,
            'limit'    => 8,
            'sort'     => 'newest',
        ]);
        $this->_data['products_shop'] = $products_shop;

        $products_same_category = Product::get_by_where([
            'pagin'        => false,
            'category_ids' => $product->category_id,
            'limit'        => 8,
            'sort'         => 'newest',
        ]);
        $this->_data['products_same_category'] = $products_same_category;

        $top_sale_products = Product::get_by_where([
            'pagin'        => false,
            'category_ids' => $product->category_id,
            'limit'        => 4,
            'sort'         => 'newest',
        ]);
        $this->_data['top_sale_products'] = $top_sale_products;

        return view('frontend.products.detail', $this->_data);
    }
}