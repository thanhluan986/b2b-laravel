<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseFrontendController;
use App\Models\CoreUsers;
use App\Models\Product;
use App\Utils\Filter;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class StoreController extends BaseFrontendController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request, $username)
    {
        $store_data = CoreUsers::where('username', $username)->first();

        if (!$store_data) return abort(404);

        $this->_data['title'] = 'Shop ' . $store_data->fullname;
        $this->_data['store_data'] = $store_data;

        $new_products = Product::get_by_where([
            'pagin'    => false,
            'limit'    => 12,
            'store_id' => $store_data->id,
            'sort'     => 'newest',
        ]);

        $this->_data['new_products'] = $new_products;

        $all_products = Product::get_by_where([
            'pagin'    => false,
            'store_id' => $store_data->id,
            'limit'    => 12,
            'sort'     => 'newest',
        ]);

        $this->_data['all_products'] = $all_products;

        return view('frontend.store.index', $this->_data);
    }
}