<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseBackendController;
use App\Models\Banners;
use App\Models\Files;
use App\Utils\Category;
use App\Utils\Filter;
use Illuminate\Http\Request;
use App\Utils\Common as Utils;
use Illuminate\Support\Facades\Validator;

class BannerController extends BaseBackendController
{
    protected $_data = array(
        'title'    => 'Quản lý banner',
        'subtitle' => 'Quản lý banner',
    );

    public function __construct()
    {
        $this->_data['status'] = Banners::STATUS;
        $this->_data['type'] = Banners::TYPE;
        parent::__construct();
    }

    public function index(Request $request)
    {
        $filter = $params = array_merge(array(
            'name'        => null,
            'category_id' => null,
            'status'      => null,
            'type'        => null,
        ), $request->all());

        $params['pagin_path'] = Utils::get_pagin_path($filter);

        $data = Banners::orderBy('created_at', 'DESC')->get();

        $this->_data['list_data'] = $data;
        $this->_data['filter'] = $filter;
        $this->_data['start'] = 0;

        return view('backend.banners.index', $this->_data);
    }

    public function add(Request $request)
    {
        $validator_rule = Banners::get_validation_admin();

        if ($request->getMethod() == 'POST') {

            Validator::make($request->all(), $validator_rule)->validate();

            $params = array_fill_keys(array_keys($validator_rule), null);
            $params = array_merge(
                $params, $request->only(array_keys($validator_rule))
            );

            if ($params['type'] == 'category') {
                if (empty($params['category_id'])) {
                    $request->session()->flash('msg', ['danger', 'Vui lòng chọn danh mục!']);
                    return redirect()->back();
                }
            } else {
                $params['category_id'] = null;
            }

            try {
                $params['user_id'] = Auth()->guard('backend')->user()->id;

                Banners::create($params);

                $request->session()->flash('msg', ['info', 'Thêm thành công!']);
            } catch (\Exception $e) {
                $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!' . $e->getMessage()]);
            }
            return redirect()->back();
        }

        $categories = \App\Models\Category::select('id', 'name', 'parent_id')->whereRaw('(parent_id is null or parent_id = 0)')->get();

        $categories_html = Category::build_select_tree($categories->toArray(), 0, '', [old('category_id')]);

        $this->_data['categories'] = $categories;
        $this->_data['categories_html'] = $categories_html;
        $this->_data['subtitle'] = 'Thêm mới';

        $this->_data['image'] = old('image_file_id') ? Files::find(old('image_file_id')) : [];
        $this->_data['app_image'] = old('app_image_file_id') ? Files::find(old('app_image_file_id')) : [];

        return view('backend.banners.add', $this->_data);
    }

    public function edit(Request $request, $id)
    {
        $data = Banners::findOrFail($id);

        $validator_rule = Banners::get_validation_admin();

        if ($request->getMethod() == 'POST') {
            Validator::make($request->all(), $validator_rule)->validate();

            $params = array_fill_keys(array_keys($validator_rule), null);
            $params = array_merge(
                $params, $request->only(array_keys($validator_rule))
            );

            if ($params['type'] == 'category') {
                if (empty($params['category_id'])) {
                    $request->session()->flash('msg', ['danger', 'Vui lòng chọn danh mục!']);
                    return redirect()->back();
                }
            } else {
                $params['category_id'] = null;
            }

            try {

                $data->update($params);

                $request->session()->flash('msg', ['info', 'Cập nhật thành công!']);
            } catch (\Exception $e) {
                $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!' . $e->getMessage()]);
            }
            return redirect($this->_ref ? $this->_ref : route('backend.banners.index'));
        }

        $categories = \App\Models\Category::select('id', 'name', 'parent_id')->whereRaw('(parent_id is null or parent_id = 0)')->get();
        $categories_html = Category::build_select_tree($categories->toArray(), 0, '', [old('category_id', $data->category_id)]);

        $this->_data['categories'] = $categories;
        $this->_data['categories_html'] = $categories_html;
        $this->_data['data'] = $data;
        $this->_data['subtitle'] = 'Chỉnh sửa';

        $this->_data['image'] = old('image_file_id', $data->image_file_id) ? Files::find(old('image_file_id', $data->image_file_id)) : [];
        $this->_data['app_image'] = old('app_image_file_id', $data->app_image_file_id) ? Files::find(old('app_image_file_id', $data->app_image_file_id)) : [];

        return view('backend.banners.edit', $this->_data);
    }

    public function delete(Request $request, $id)
    {
        try {
            $data = Banners::findOrFail($id);
            $data->delete();
            $request->session()->flash('msg', ['info', 'Đã xóa thành công!']);

        } catch (\Exception $e) {
            $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!']);
        }
        return redirect()->back();
    }
}