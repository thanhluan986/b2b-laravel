<?php

namespace App\Http\Controllers\Backend\Location;

use App\Events\UpdateStreet;
use App\Http\Controllers\BaseBackendController;
use App\Models\Location\District;
use App\Models\Location\Province;
use App\Models\Location\Street;
use App\Models\Location\Ward;
use App\Utils\Filter;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\CoreUsers;
use App\Utils\Common as Utils;
use App\Utils\Avatar;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Psy\Util\Str;

class StreetController extends BaseBackendController
{
    protected $_data = array(
        'title'    => 'Quản lý tên đường',
        'subtitle' => 'Quản lý tên đường',
    );

    public function index(Request $request)
    {
        $filter = $params = array_merge(array(
            'province_id' => null,
            'district_id' => null,
            'ward_id'     => null,
            'name'        => null,
            'limit'       => config('constants.item_per_page_admin'),
        ), $request->all());

        $params['pagin_path'] = Utils::get_pagin_path($filter);
        $params['limit'] = (int)$params['limit'] > 0 ? (int)$params['limit'] : config('constants.item_per_page_admin');

        $streets = Street::select('*');

        if ($filter['name'])
            $streets->where('name', 'like', "%{$filter['name']}%");

        if ($filter['district_id'])
            $streets->where('district_id', $filter['district_id']);

        $streets = $streets->paginate($params['limit'])->withPath($params['pagin_path']);

        $this->_data['streets'] = $streets;
        $this->_data['filter'] = $filter;
        $this->_data['start'] = ($streets->currentPage() - 1) * config('constants.item_perpage');

        $province_id = $filter['province_id'];
        $district_id = $filter['district_id'];
        $ward_id = $filter['ward_id'];

        $this->_data['provinces'] = Province::all();
        $this->_data['districts'] = $province_id ? District::where('province_id', $province_id)->get() : [];
        $this->_data['wards'] = $district_id ? Ward::where('district_id', $district_id)->get() : [];
        $this->_data['_limits'] = config('constants.limit_records');

        return view('backend.location.street.index', $this->_data);
    }

    public function add(Request $request)
    {
        $validator_rule = [
            'name_origin' => ['required', 'string'],
            'district_id' => ['required', 'integer'],
        ];

        $params = array_merge(
            array(
                'name_origin' => null,
                'district_id' => null,
            ), array_map('trim', $request->all())
        );

        if ($request->getMethod() == 'POST') {

            Validator::make($request->all(), $validator_rule)->validate();

            $check = Street::whereRaw("BINARY name = ?", $params['name_origin'])->count();

            if ($check)
                return redirect()->back()->withInput()->withErrors(['messagge' => 'Tên đường đã tồn tại. Vui lòng kiểm tra lại!']);

            try {

                Street::create(
                    [
                        'name'            => $params['name_origin'],
                        'name_ascii'      => Filter::vnToAscii($params['name_origin']),
                        'district_id'     => $params['district_id'],
                        'user_id_created' => Auth()->guard('backend')->user()->id,
                    ]
                );

                $request->session()->flash('msg', ['info', 'Thêm thành công!']);
            } catch (\Exception $e) {
                $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!' . $e->getMessage()]);
            }
            return redirect(Route('backend.location.street.add'));
        }

        $this->_data['provinces'] = Province::all();
        $this->_data['districts'] = old('district_id') ? District::where('province_id', old('province_id'))->get() : [];
        $this->_data['wards'] = old('district_id') ? Ward::where('district_id', old('district_id'))->get() : [];

        $this->_data['subtitle'] = 'Thêm mới';
        return view('backend.location.street.add', $this->_data);
    }

    public function edit(Request $request, $id)
    {
        $street = Street::findOrFail($id);

        $validator_rule = [
            'name_origin' => ['required', 'string'],
            'district_id' => ['required', 'integer'],
        ];

        $params = array_merge(
            array(
                'name_origin' => null,
                'district_id' => null,
            ), array_map('trim', $request->all())
        );

        if ($request->getMethod() == 'POST') {

            Validator::make($request->all(), $validator_rule)->validate();

            try {

                $street->update(
                    [
                        'name'        => $params['name_origin'],
                        'name_ascii'  => Filter::vnToAscii($params['name_origin']),
                        'district_id' => $params['district_id'],
                    ]
                );

                $request->session()->flash('msg', ['info', 'Sửa thành công!']);
            } catch (\Exception $e) {
                $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!' . $e->getMessage()]);
            }
            return redirect(Route('backend.location.street.index'));
        }

        $this->_data['provinces'] = Province::all();
        $this->_data['districts'] = old('province_id', $street->district->province_id) ? District::where('province_id', old('province_id', $street->district->province_id))->get() : [];
//        $this->_data['wards'] = old('district_id', $street->ward->district_id) ? Ward::where('district_id', old('district_id', $street->ward->district_id))->get() : [];
        $this->_data['street'] = $street;

        $this->_data['subtitle'] = 'Chỉnh sửa';
        return view('backend.location.street.edit', $this->_data);
    }

    public function delete(Request $request, $id)
    {
        try {
            $data = Street::findOrFail($id);
            $data->delete();
            $request->session()->flash('msg', ['info', 'Đã xóa thành công!']);

        } catch (\Exception $e) {
            $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!']);
        }
        return redirect($this->_ref ? $this->_ref : Route('backend.location.street.index'));
    }

    public function ajaxEdit(Request $request)
    {
        $id = $request->get('id', null);
        $name = $request->get('name', null);

        if (!$id || !$name)
            return \Response::json([
                'e' => 1,
                'r' => 'Dữ liệu không hợp lệ!'
            ]);

        $street = Street::find($id);
        if (!$street)
            return \Response::json([
                'e' => 1,
                'r' => 'Dữ liệu không hợp lệ!'
            ]);

        $old_street = $street->toArray();

        $street->name = $name;
        $street->name_ascii = Filter::vnToAscii($name);
        $street->save();

        //event(new UpdateStreet($old_street, $street->toArray()));

        return \Response::json([
            'e'      => 0,
            'r'      => '',
            '_token' => csrf_token()
        ]);
    }
}