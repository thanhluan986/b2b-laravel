<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseBackendController;
use App\Jobs\PushNotification;
use App\Models\Location\Province;
use App\Models\Product;
use App\Models\ProductContactInfo;
use App\Models\ProductNote;
use App\Models\ThienMinh\District;
use App\Models\ThienMinh\Lands;
use App\Models\ThienMinh\Notes;
use App\Models\ThienMinh\Ward;
use App\ReportsHourly;
use App\Utils\Firebase;
use App\Utils\GoogleMaps;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class DashboardController extends BaseBackendController
{
    protected $_data = [
        'title' => 'Dashboard',
    ];

    public function merge()
    {
        exit;
    }

    public function mergeNote()
    {
        exit;
    }

    private function _getAddress($params)
    {
        if (empty($params['province_id'])
            || empty($params['district_id'])
            || empty($params['ward_id'])
            || empty($params['street_id'])
            || empty($params['home_number']))
            return null;

        $province = \App\Models\Location\Province::findOrFail($params['province_id']);
        $district = \App\Models\Location\District::findOrFail($params['district_id']);
        $ward = \App\Models\Location\Ward::findOrFail($params['ward_id']);
        $street = \App\Models\Location\Street::findOrFail($params['street_id']);
        $address = $params['home_number'] . ' ' . $street->name . ', ' . $ward->name . ', ' . $district->name . ', ' . $province->name;
        return $address;
    }

    public function test()
    {
        $d1 = District::all()->toArray();

        foreach ($d1 as $v) {
            $d2 = \App\Models\Location\District::find($v['district_id'])->toArray();
//            echo $v['district_id'];
//            echo ' - ';
//            echo $v['name'];
//            echo ' - ';
//            echo isset($d2['name']) ? $d2['name'] : '<strong>Thiếu</strong>';
//            echo '<br>';

            $ward1 = Ward::where('district_id', $v['district_id'])->get()->toArray();
            $_ward2 = \App\Models\Location\Ward::where('district_id', $v['district_id'])->get()->toArray();

            $ward2 = [];
            foreach ($_ward2 as $w2) {
                $ward2[$w2['id']] = $w2;
            }
            echo '-------------------------------------------<br>';

            foreach ($ward1 as $w1) {
                echo $w1['ward_id'];
                echo ' - ';
                echo $w1['name'];
                echo ' - ';
                echo isset($ward2[$w1['ward_id']]['name']) ? $ward2[$w1['ward_id']]['name'] : '<strong style="color:red;">Thiếu</strong>';
                echo '<br>';
            }
        }
    }

    public function index()
    {

//        $p = \App\Models\Admin\Product::get_by_where_join();
//        echo "<pre>";
//        print_r($p->toArray());
//        exit;

        $user = Auth()->guard('backend')->user();

        /*Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'products.index', 'group' => 'bất động sản']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Thêm mới', 'name' => 'products.add', 'group' => 'bất động sản']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'products.edit', 'group' => 'bất động sản']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'products.delete', 'group' => 'bất động sản']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa (cá nhân)', 'name' => 'products.owned.edit', 'group' => 'bất động sản']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa (cá nhân)', 'name' => 'products.owned.delete', 'group' => 'bất động sản']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'users.index', 'group' => 'tài khoản']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Thêm mới', 'name' => 'users.add', 'group' => 'tài khoản']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'users.edit', 'group' => 'tài khoản']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'users.delete', 'group' => 'tài khoản']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'staff.index', 'group' => 'nhân viên']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Thêm mới', 'name' => 'staff.add', 'group' => 'nhân viên']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'staff.edit', 'group' => 'nhân viên']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'staff.delete', 'group' => 'nhân viên']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'notification.index', 'group' => 'thông báo']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Push', 'name' => 'notification.add', 'group' => 'thông báo']);
//        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'staff.edit', 'group' => 'thông báo']);
//        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'staff.delete', 'group' => 'thông báo']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'province.index', 'group' => 'tỉnh/tp']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Thêm mới', 'name' => 'province.add', 'group' => 'tỉnh/tp']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'province.edit', 'group' => 'tỉnh/tp']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'province.del', 'group' => 'tỉnh/tp']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'district.index', 'group' => 'quận/huyện']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Thêm mới', 'name' => 'district.add', 'group' => 'quận/huyện']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'district.edit', 'group' => 'quận/huyện']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'district.del', 'group' => 'quận/huyện']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'ward.index', 'group' => 'phường/xã']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Thêm mới', 'name' => 'ward.add', 'group' => 'phường/xã']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'ward.edit', 'group' => 'phường/xã']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'ward.del', 'group' => 'phường/xã']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'street.index', 'group' => 'tên đường']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Thêm mới', 'name' => 'street.add', 'group' => 'tên đường']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'street.edit', 'group' => 'tên đường']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'street.del', 'group' => 'tên đường']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'convenience.index', 'group' => 'tiện nghi trong nhà']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Thêm mới', 'name' => 'convenience.add', 'group' => 'tiện nghi trong nhà']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'convenience.edit', 'group' => 'tiện nghi trong nhà']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'convenience.del', 'group' => 'tiện nghi trong nhà']);

        Permission::create(['guard_name' => 'backend', 'title' => 'Danh sách', 'name' => 'exterior.index', 'group' => 'tiện nghi xung quanh']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Thêm mới', 'name' => 'exterior.add', 'group' => 'tiện nghi xung quanh']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Sửa', 'name' => 'exterior.edit', 'group' => 'tiện nghi xung quanh']);
        Permission::create(['guard_name' => 'backend', 'title' => 'Xóa', 'name' => 'exterior.del', 'group' => 'tiện nghi xung quanh']);

        $user->givePermissionTo(Permission::all());*/
        return view('backend.index', $this->_data);
    }
}