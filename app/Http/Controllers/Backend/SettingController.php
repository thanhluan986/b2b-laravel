<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseBackendController;
use App\Models\Settings;
use Illuminate\Http\Request;

class SettingController extends BaseBackendController
{
    protected $_data = [
        'title' => 'Cài đặt chung',
    ];

    public function index(Request $request)
    {
        $settings = Settings::get_all();

        if ($request->getMethod() == 'POST') {

            $validator_rule = [
                'EMAIL'              => NULL,
                'ADDRESS'            => NULL,
                'PHONE'              => NULL,
                'HOTLINE'            => NULL,
                'FAX'                => NULL,
                'META_TITLE'         => NULL,
                'META_DESCRIPTIONS'  => NULL,
                'META_KEYWORDS'      => NULL,
                'SITE_DESCRIPTION'   => NULL,
                'META_AUTHOR'        => NULL,
                'COMPANY_NAME'       => NULL,
                'COMPANY_INTRO'      => NULL,
                'COMPANY_DETAIL'     => NULL,
                'PRODUCTS_PERPAGE'   => NULL,
                'FACEBOOK'           => NULL,
                'POSTS_PERPAGE'      => NULL,
                'TWITTER'            => NULL,
                'LINKEDIN'           => NULL,
                'PINTEREST'          => NULL,
                'INSTAGRAM'          => NULL,
                'YOUTUBE'            => NULL,
                'GOOGLE_PLUS'        => NULL,
                'PHONE_SUPPORT'      => NULL,
                'IS_PUBLISHED'       => NULL,
                'GA_CODE'            => NULL,
                'OPTION_CODE_FOOTER' => NULL,
                'ADDRESS_FOOTER'     => NULL,
                'ADDRESS_CONTACT'    => NULL,
                'SITE_SLOGAN'        => NULL,
                'SERVICE_INTRO'      => NULL,
                'FOOTER_HTML'        => NULL,
            ];

            $params = array_fill_keys(array_keys($validator_rule), null);
            $params = array_merge(
                $params, $request->only(array_keys($validator_rule))
            );

            $errors = array();

            foreach ($settings as $k => $v) {
                if ($v['require'] && empty($params[$k])) {
                    $errors[$k] = $v['setting_desc'] . ' không được để trống!';
                }
            }

            if (empty($errors)) {
                try {

                    $params['OPTION_CODE_FOOTER'] = htmlspecialchars($params['OPTION_CODE_FOOTER']);
                    $params['GA_CODE'] = htmlspecialchars($params['GA_CODE']);

                    $params['COMPANY_INTRO'] = htmlspecialchars($params['COMPANY_INTRO']);
                    $params['COMPANY_DETAIL'] = htmlspecialchars($params['COMPANY_DETAIL']);
                    $params['ADDRESS_FOOTER'] = htmlspecialchars($params['ADDRESS_FOOTER']);
                    $params['ADDRESS_CONTACT'] = htmlspecialchars($params['ADDRESS_CONTACT']);
                    $params['SITE_SLOGAN'] = htmlspecialchars($params['SITE_SLOGAN']);
                    $params['SERVICE_INTRO'] = htmlspecialchars($params['SERVICE_INTRO']);
                    $params['FOOTER_HTML'] = htmlspecialchars($params['FOOTER_HTML']);

                    Settings::update_by_key($params);
                    $request->session()->flash('msg', ['info', 'Cập nhật thành công!']);
                } catch (\Exception $e) {
                    $request->session()->flash('msg', ['danger', 'Có lỗi xảy ra, vui lòng thử lại!' . $e->getMessage()]);
                }
            } else {
                $request->session()->flash('my_errors', $errors);
            }

            return redirect()->back();
        }

        $this->_data['subtitle'] = 'Cài đặt chung';
        $this->_data['settings'] = $settings;
        return view('backend.setting.index', $this->_data);
    }
}