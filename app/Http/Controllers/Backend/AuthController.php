<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseBackendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseBackendController
{
    public function login(Request $request)
    {
        Auth::shouldUse('backend');
        if (Auth()->guard('backend')->user()) {
            return redirect(Route('backend.dashboard'));
        }

        $data = array(
            'title' => 'Login'
        );

        if ($request->getMethod() == 'POST') {

            if (!Auth()->guard('backend')->attempt(['phone' => $request->phone, 'password' => $request->password], true)) {
                $request->session()->flash('msg', 'Đăng nhập thất bại');
            } else {
                return redirect(Route('backend.dashboard'));
            }

        }
        return view('backend.login', $data);
    }

    public function logout()
    {
        Auth::shouldUse('backend');
        if (Auth()->guard('backend')->user()->id) {
            Auth()->guard('backend')->logout();
        }
        return redirect(Route('backend.login'));
    }
}