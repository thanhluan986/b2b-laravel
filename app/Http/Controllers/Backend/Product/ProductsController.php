<?php

namespace App\Http\Controllers\Backend\Product;

use App\Events\UpdateProduct;
use App\Http\Controllers\BaseBackendController;
use App\Models\CallRequest;
use App\Models\Location\District;
use App\Models\Location\Province;
use App\Models\Location\Street;
use App\Models\Location\Ward;
use App\Models\ProductFrontageType;
use App\Models\ProductNote;
use App\Models\Project;
use App\Utils\Category as UtilsCategory;
use App\Utils\File;
use App\Utils\Filter;
use App\Utils\GoogleMaps;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\CoreUsers;
use App\Models\Product;
use App\Utils\Common as Utils;
use App\Utils\Avatar;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use App\Models\Convenience;
use App\Models\Files;
use App\Models\Exterior;
use App\Models\FrontageType;
use App\Models\HistoryView;
use App\Models\ProductContactInfo;
use App\Models\ProductConvenience;
use App\Models\ProductImages;
use App\Models\ProductImagesExtra;
use App\Models\ProductExterior;
use App\Models\ProductPurpose;
use App\Models\Purpose;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProductsController extends BaseBackendController
{
    protected $_data = array(
        'title'    => 'Sản phẩm',
        'subtitle' => '',
        '_type'    => 'all',
    );
    protected $_limits = [
        10, 30, 50, 100, 500, 1000, 5000, 10000
    ];

    public function __construct()
    {
        $this->_data['status'] = Product::STATUS;

        parent::__construct();
    }

    public function index(Request $request)
    {
        $filter = $params = array_merge(array(
            'id'          => null,
            'category_id' => null,
            'date_from'   => null,
            'date_to'     => null,
            'fullname'    => null,
            'user_id'     => null,
            'keywords'    => null,
            'sort'        => null,
            'status'      => null,
            'limit'       => config('constants.item_per_page_admin'),
        ), $request->all());

        $objProduct = new Product();

        $params['limit'] = (int)$params['limit'] > 0 ? (int)$params['limit'] : config('constants.item_per_page_admin');
        $params['pagin_path'] = Utils::get_pagin_path($filter);
        $params['order_by'] = 'created_at';
        $params['order_by_direction'] = 'DESC';

        $product_category = Category::get_by_where(['assign_key' => true,], ['id', 'name', 'parent_id']);
        $all_child = [];
        $product_type_id = null;

        if ($params['category_id']) {
            $product_type_id = $params['category_id'];
            $all_child = \App\Utils\Category::get_all_child_categories($product_category, $product_type_id);
            $all_child = array_merge($all_child, [$params['category_id']]);
        }

        $params['category_ids'] = $all_child;

        $products = $objProduct->get_by_where($params);

        $start = ($products->currentPage() - 1) * $params['limit'];

        $this->_data['products'] = $products;
        $this->_data['start'] = $start;
        $this->_data['filter'] = $filter;
        $this->_data['sort'] = $params['sort'];
        $this->_data['_limits'] = $this->_limits;

        $product_category_html = \App\Utils\Category::build_select_tree($product_category, 0, '', [$product_type_id]);

        $this->_data['product_category_html'] = $product_category_html;

        if ($request->ajax()) {
            $view = \View::make('backend.products.ajaxList', $this->_data);

            $return = [
                'e' => 0,
                'r' => $view->render()
            ];
            return \Response::json($return);
        }


        return view('backend.products.index', $this->_data);
    }

    public function approved(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_ids' => 'required|array',
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'e' => 1,
                'r' => $validator->errors()->first()
            ]);
        }
        $product_ids = $request->get('product_ids');

        Product::whereIn('id', $product_ids)->update(['status' => Product::STATUS_ACTIVE]);

        $return = [
            'e' => 0,
            'r' => ''
        ];
        return \Response::json($return);
    }

    public function un_approved(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_ids' => 'required|array',
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'e' => 1,
                'r' => $validator->errors()->first()
            ]);
        }
        $product_ids = $request->get('product_ids');

        Product::whereIn('id', $product_ids)->update(['status' => Product::STATUS_IN_ACTIVE]);

        $return = [
            'e' => 0,
            'r' => ''
        ];
        return \Response::json($return);
    }

    public function ajaxdelete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_ids' => 'required|array',
        ]);

        if ($validator->fails()) {
            return \Response::json([
                'e' => 1,
                'r' => $validator->errors()->first()
            ]);
        }
        $product_ids = $request->get('product_ids');

        Product::whereIn('id', $product_ids)->delete();
        $return = [
            'e' => 0,
            'r' => ''
        ];
        return \Response::json($return);
    }
}