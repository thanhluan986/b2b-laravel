<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseBackendController;
use App\Jobs\PushNotification;
use App\Models\CoreUsers;
use App\Models\Location\Street;
use App\Utils\Filter;
use App\Utils\Firebase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Files;
use Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class AjaxController extends BaseBackendController
{
    public function searchUser(Request $request)
    {
        $term = trim($request->q);
        $t = trim($request->get('t', null));

        if (empty($term)) {
            return \Response::json([]);
        }

        $users = CoreUsers::select(['id', 'phone'])
            ->where('phone', 'like', "%$term%");

        if ($t == 'callback')
            $users = $users->whereIn('account_position', [2, 3]);

        $users = $users->limit(100)->get();

        $formatted_users = [];

        foreach ($users as $user) {
            $formatted_users[] = ['id' => $user->id, 'text' => $user->phone];
        }

        return \Response::json($formatted_users);
    }

    public function addStreet(Request $request)
    {
        $district_id = trim($request->district_id);
        $street_name = trim($request->street_name);

        if (empty($district_id) || empty($street_name)) {
            return \Response::json(['e' => 1, 'r' => 'Dữ liệu không hợp lệ, vui lòng thử lại!']);
        }
        $street_name = ucwords($street_name);
        $street_name = preg_replace('!\s+!', ' ', $street_name);
        $street_name_ascii = Filter::vnToAscii($street_name);

        \DB::enableQueryLog();

        $count = Street::whereRaw("`name` = '{$street_name}' and name_ascii = '{$street_name_ascii}' and district_id = '{$district_id}' ")
            ->count();

//        \Log::info(\DB::getQueryLog());

        if ($count > 0)
            return \Response::json(['e' => -1, 'r' => 'Tên đường đã tồn tại!']);

        Street::create(
            [
                'name'            => $street_name,
                'name_ascii'      => $street_name_ascii,
                'district_id'     => $district_id,
                'user_id_created' => Auth()->guard('backend')->user()->id
            ]
        );

        return \Response::json(['e' => 0]);
    }

    public function uploadImage(Request $request)
    {
        $photos = $request->file('file');
        if (empty($photos))
            return \Response::json(['e' => 1, 'r' => 'Vui lòng chọn hình upload!']);

        if (!is_array($photos)) {
            $photos = [$photos];
        }

        $sub_dir = date('Y/m/d');
        $full_dir = config('constants.upload_dir.root') . '/' . $sub_dir;

        if (!is_dir($full_dir)) {
            mkdir($full_dir, 0777, true);
        }

        $type = $request->get('type', Files::TYPE_PRODUCT);

        $items = [];
        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];

            $filename = md5(rand(0, 999999) . microtime()) . '.' . $photo->getClientOriginalExtension();

            Image::make($photo)->resize(2048, null, function ($constraint) {
                $constraint->aspectRatio();
            })->orientate()->save($full_dir . '/' . $filename);

            $file_path = $sub_dir . '/' . $filename;

            $image = Files::create([
                'user_id'   => Auth()->guard('backend')->user()->id,
                'file_path' => $file_path,
                'type'      => $type
            ]);
            $items[] = [
                'id'   => $image->id,
                'path' => $file_path,
                'url'  => config('constants.upload_dir.url') . '/' . $file_path,
            ];
        }

        return \Response::json(['e' => 0, 'r' => $items]);
    }

    public function removeImage(Request $request)
    {
        return \Response::json([]);
    }
}