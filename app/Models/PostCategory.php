<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_post_category';

    const STATUS_HIDE = 0;
    const STATUS_SHOW = 1;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'parent_id',
        'status',
        'priority',
        'seo_title',
        'seo_descriptions',
        'seo_keywords',
        'user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    const STATUS = [
        ['id' => 0, 'name' => 'Ẩn'],
        ['id' => 1, 'name' => 'Hiện'],
    ];

    public static function get_all()
    {
        $categories = self::orderBy('priority', 'ASC')->get();
        $return = [];
        foreach ($categories as $category) {
            $return[$category->id] = $category->toArray();
        }

        return $return;
    }

    public static function get_validation_admin()
    {
        return [
            'name'             => 'required|string',
            'slug'             => 'nullable|string',
            'description'      => 'nullable|string',
            'parent_id'        => 'nullable|integer',
            'status'           => 'nullable|in:0,1',
            'priority'         => 'nullable|integer',
            'seo_title'        => 'nullable|string',
            'seo_descriptions' => 'nullable|string',
            'seo_keywords'     => 'nullable|string',
            'user_id'          => 'nullable|integer',
        ];
    }
}
