<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_category';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'parent_id',
        'status',
        'priority',
        'thumbnail_file_id',
        'icon_file_id',
        'type',
        'seo_title',
        'seo_descriptions',
        'seo_keywords',
        'user_id',
        'total_product',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    const STATUS_ACTIVE = 1;
    const STATUS_IN_ACTIVE = 0;

    const STATUS = [
        ['id' => 0, 'name' => 'Ẩn'],
        ['id' => 1, 'name' => 'Hiện'],
    ];

    public function thumbnail()
    {
        return $this->belongsTo(Files::class, 'thumbnail_file_id', 'id')->select(['id', 'file_path']);
    }

    public function icon()
    {
        return $this->belongsTo(Files::class, 'icon_file_id', 'id')->select(['id', 'file_path']);
    }

    public static function get_by_where($params = [], $select = '*')
    {
        $params = array_merge([
            'status'     => null,
            'assign_key' => false,
        ], $params);

        $data = self::select($select)
            ->orderBy('priority', 'ASC');

        if ($params['status'])
            $data->where('status', $params['status']);

        $data = $data->get();

        $return = $data;
        if ($params['assign_key'] && count($data)) {
            $return = [];
            $data = $data->toArray();
            foreach ($data as $v) {
                $return[$v['id']] = $v;
            }
        }
        return $return;
    }

    public static function get_validation_admin()
    {
        return [
            'name'              => 'required|string',
            'slug'              => 'nullable|string',
            'description'       => 'nullable|string',
            'parent_id'         => 'nullable|integer',
            'status'            => 'nullable|in:0,1',
            'priority'          => 'nullable|integer',
            'seo_title'         => 'nullable|string',
            'seo_descriptions'  => 'nullable|string',
            'seo_keywords'      => 'nullable|string',
            'user_id'           => 'nullable|integer',
            'thumbnail_file_id' => 'nullable|integer|exists:lck_files,id',
            'icon_file_id'      => 'nullable|integer|exists:lck_files,id',
        ];
    }

    public static function get_all()
    {
        $categories = self::with(['thumbnail', 'icon'])->orderBy('priority', 'ASC')->get();
        $return = [];
        foreach ($categories as $category) {
            $return[$category->id] = $category->toArray();
        }

        return $return;
    }
}
