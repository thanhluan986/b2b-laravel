<?php

namespace App\Models;

class Variations extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_variations';
    public $timestamps = false;
    protected $fillable = [
        'name',
        'user_id',
        'product_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public static function validation_variations($params = [])
    {
        foreach ($params as $v) {
            if (!isset($v['variation_name']) || !isset($v['items']) || empty($v['variation_name']) || count($v['items']) < 1)
                return false;

            foreach ($v['items'] as $v2)
                if (!isset($v2['variation_value_id']) || !isset($v2['variation_value']) || empty($v2['variation_value_id']) || empty($v2['variation_value']))
                    return false;
        }

        return true;
    }
}
