<?php

namespace App\Models;

class Basket extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_basket';
    //public $timestamps = false;
    protected $fillable = [
        'user_id',
        'product_id',
        'product_variation_id',
        'quantity',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public static function get_by_where($params)
    {
        $params = array_merge([
            'user_id'    => null,
            'item_ids'   => null,
            'pagin'      => true,
            'pagin_path' => null,
            'limit'      => config('constants.item_perpage')
        ], $params);

        $data = self::select(\DB::raw("*"))
            ->with(['products', 'product_variations',]);

        $data->orderBy('created_at', 'DESC');

        if ($params['item_ids'])
            $data->whereIn('id', $params['item_ids']);

        if ($params['user_id'])
            $data->where('user_id', $params['user_id']);

        if ($params['pagin']) {
            $data = $data->paginate($params['limit'])
                ->withPath($params['pagin_path']);
        } else {
            $data = $data->get();
        }

        return $data;
    }

    public static function get_all_product($params)
    {
        $params = array_merge([
            'user_id'    => null,
            'item_ids'   => null,
            'group'      => true,
            'pagin'      => true,
            'pagin_path' => null,
            'limit'      => config('constants.item_perpage')
        ], $params);

        $data = self::select(\DB::raw('b.id as basket_item_id, b.quantity,
        p.id as product_id, 
        p.name, 
        p.price, 
        p.price_old, 
        p.sku, 
        p.inventory, 
        p.weight, 
        p.thumbnail_file_id, 
        pv.id as product_variation_id, 
        pv.name as product_variation_name, 
        pv.price as product_variation_price, 
        pv.sku as product_variation_sku, 
        pv.inventory as product_variation_inventory,
        p.user_id as store_id,
        b.created_at,
        b.updated_at '
        ))
            ->from(parent::$tbl_lck_basket . ' as b')
            ->leftJoin(parent::$tbl_lck_product . ' as p', 'p.id', 'b.product_id')
            ->leftJoin(parent::$tbl_lck_product_variation . ' as pv', 'pv.id', 'b.product_variation_id');
        //->leftJoin(parent::$tbl_lck_core_users . ' as u', 'u.id', 'p.user_id');

        $data->with(['thumbnail']);

        $data->orderBy('b.created_at', 'DESC');

        if ($params['user_id'])
            $data->where('b.user_id', $params['user_id']);

        if ($params['item_ids'])
            $data->whereIn('b.id', $params['item_ids']);

        if ($params['pagin']) {
            $data = $data->paginate($params['limit'])
                ->withPath($params['pagin_path']);
        } else {

//            $data = $data->get()->groupBy('store_id');

            $data = $data->get();

//            $product_ids = $data->pluck('product_id');
//            $whole_sales = ProductWholesales::whereIn('product_id', $product_ids)->get();

            foreach ($data as $v) {

                $whole_sales = ProductWholesales::where('product_id', $v->product_id)
                    ->where('quantity_from', '<=', $v->quantity)
                    ->whereRaw(\DB::raw("(quantity_to >= {$v->quantity} OR quantity_to IS NULL)"))
                    ->first();

                if ($whole_sales) {
                    $v->price_old = $v->price;
                    $v->price = $whole_sales->price;
                } else {
                    $v->price_old = 0;
                }
            }

            if ($params['group'])
                $data = $data->groupBy('store_id');
        }

        return $data;
    }

    public function thumbnail()
    {
        return $this->belongsTo(Files::class, 'thumbnail_file_id', 'id')->select(['id', 'file_path']);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id')
            ->where('status', Product::STATUS_ACTIVE)
            ->select([
                'id',
                'name',
                'price',
                'sku',
                'status',
                'thumbnail_file_id',
                'user_id',
            ])
            ->with(['thumbnail', 'store', 'wholesale']);
    }

    public function product_variations()
    {
        return $this->hasOne(ProductVariation::class, 'id', 'product_variation_id')
            ->select([
                'id',
                'name',
                'price',
                'sku',
                'inventory',
            ]);
    }
}