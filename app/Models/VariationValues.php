<?php

namespace App\Models;

class VariationValues extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_variation_values';
    public $timestamps = false;
    protected $fillable = [
        'variation_id',
        'value',
        'product_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public static function get_variants($product_id)
    {
        $data = self::select(\DB::raw('v.id as variation_id, pv.id as variation_value_id, v.name as variation_name, pv.value as variation_value'))
            ->from(parent::$tbl_lck_variations . ' as v')
            ->join(parent::$tbl_lck_variation_values . ' as pv', 'pv.variation_id', '=', 'v.id');

        $data->where('pv.product_id', $product_id);
        $data = $data->get();

        return $data;
    }

    public static function get_group_variations($product_id)
    {
        $data = self::get_variants($product_id);

        $variants = [];

        if (count($data)) {
            $data = $data->toArray();

            foreach ($data as $item) {
                if (!isset($variants[$item['variation_id']])) {
                    $variants[$item['variation_id']] = [
                        'variation_id'   => $item['variation_id'],
                        'variation_name' => $item['variation_name'],
                    ];
                }
                $variants[$item['variation_id']]['items'][] = [
                    'variation_value_id' => $item['variation_value_id'],
                    'variation_value'    => $item['variation_value'],
                ];
            }
            $variants = array_values($variants);
        }

        return $variants;
    }
}
