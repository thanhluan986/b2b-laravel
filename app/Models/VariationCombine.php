<?php

namespace App\Models;

class VariationCombine extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_variation_combine';
    public $timestamps = false;
    protected $fillable = [
        'product_variation_id',
        'variation_value_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public static function get_variation_combine(array $product_variation_ids)
    {
        $data = self::select(["*"])
            ->whereIn('product_variation_id', $product_variation_ids)
            ->get();
        //->groupBy('product_variation_id');

        return $data;
    }

    public static function group_variation_combine(array $product_variation_ids)
    {
        $data = self::get_variation_combine($product_variation_ids);

        $return = [];
        if (count($data)) {
            $data = $data->toArray();
            foreach ($data as $item) {
                if (!isset($return[$item['product_variation_id']])) {
                    $return[$item['product_variation_id']] = [
                        'product_variation_id' => $item['product_variation_id'],
                    ];
                }
                $return[$item['product_variation_id']]['list_variation_value_id'][] = $item['variation_value_id'];
            }
            $return = array_values($return);
        }

        return $return;
    }

    public static function validation_product_variations($params = [], $variations = [])
    {
        $variation_value_ids = [];
        foreach ($variations as $v) {
            foreach ($v['items'] as $v2)
                $variation_value_ids[] = $v2['variation_value_id'];
        }

        foreach ($params as $v) {
            if (!isset($v['product_variation_id']) || !isset($v['price'])
                || !isset($v['inventory']) || !isset($v['list_variation_value_id_combine'])
                || count($v['list_variation_value_id_combine']) <= 0)
                return false;

            if (count(array_diff($v['list_variation_value_id_combine'], $variation_value_ids)) != 0)
                return false;
        }

        return true;
    }
}
