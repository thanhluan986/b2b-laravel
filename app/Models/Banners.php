<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\Links;

class Banners extends Model
{
    const STATUS_HIDE = 0;
    const STATUS_SHOW = 1;

    const STATUS = [
        ['id' => 0, 'name' => 'Ẩn'],
        ['id' => 1, 'name' => 'Hiển thị'],
    ];

    const TYPE = [
        ['id' => 'home', 'name' => 'Trang chủ'],
        ['id' => 'store', 'name' => 'Cửa hàng'],
        ['id' => 'category', 'name' => 'Danh mục'],
        ['id' => 'promo', 'name' => 'CTKM'],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_banners';

    protected $fillable = [
        'name',
        'descripttion',
        'link',
        'image_file_id',
        'app_image_file_id',
        'app_destination',
        'app_params',
        'start_datetime',
        'end_datetime',
        'category_id',
        'user_id',
        'type',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id')->select(['id', 'name']);
    }

    public function image()
    {
        return $this->belongsTo(Files::class, 'image_file_id', 'id')->select(['id', 'file_path']);
    }

    public function image_app()
    {
        return $this->belongsTo(Files::class, 'app_image_file_id', 'id')->select(['id', 'file_path']);
    }

    public static function get_validation_admin()
    {
        return [
            'name'              => 'required|string',
            'link'              => 'nullable|string',
            'image_file_id'     => 'nullable|string',
            'app_image_file_id' => 'nullable|string',
            'category_id'       => 'nullable|integer',
            'status'            => 'nullable|in:0,1',
            'start_datetime'    => 'nullable|string',
            'end_datetime'      => 'nullable|string',
            'type'              => 'nullable|string',
            'user_id'           => 'nullable|integer',
        ];
    }
}
