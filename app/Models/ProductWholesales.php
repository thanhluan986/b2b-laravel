<?php

namespace App\Models;

class ProductWholesales extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_product_wholesales';
    public $timestamps = false;
    protected $fillable = [
        'product_id',
        'quantity_from',
        'quantity_to',
        'price',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public static function validation_wholesales($params)
    {
        $count = count($params);
        foreach ($params as $k => $v) {
            if (!isset($v['quantity_from']) || !isset($v['quantity_to']) || !isset($v['price']) || $v['quantity_from'] <= 0 || $v['quantity_to'] <= 0 || $v['price'] <= 0)
                return false;

            if ($v['quantity_from'] > $v['quantity_to'])
                return false;

            if ($count > 1 && $k <= $count - 1 && $k > 0) {
                if ($v['quantity_from'] != $params[$k - 1]['quantity_to'] + 1)
                    return false;
            }
        }

        return true;
    }
}
