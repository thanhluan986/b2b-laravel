<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trademark extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_trademark';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'status',
        'priority',
        'thumbnail_file_id',
        'category_id',
        'seo_title',
        'seo_descriptions',
        'seo_keywords',
        'user_id',
        'total_product',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    const STATUS = [
        ['id' => 0, 'name' => 'Ẩn'],
        ['id' => 1, 'name' => 'Hiện'],
    ];

    public function thumbnail()
    {
        return $this->belongsTo(Files::class, 'thumbnail_file_id', 'id')->select(['id', 'file_path']);
    }

    public static function get_validation_admin()
    {
        return [
            'name'              => 'required|string',
            'slug'              => 'nullable|string',
            'description'       => 'nullable|string',
            'status'            => 'nullable|in:0,1',
            'priority'          => 'nullable|integer',
            'seo_title'         => 'nullable|string',
            'seo_descriptions'  => 'nullable|string',
            'seo_keywords'      => 'nullable|string',
            'user_id'           => 'nullable|integer',
            'thumbnail_file_id' => 'nullable|integer|exists:lck_files,id',
            'category_id'       => 'nullable|integer|exists:lck_category,id',
        ];
    }
}
