<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(@OA\Xml(name="Street"))
 * @OA\Property(property="id",type="integer",description="id"),
 * @OA\Property(property="name",type="string",description="Tên đường phố"),
 **/
class Street extends Model
{
    use SoftDeletes;
    protected $table = 'lck_location_street';

    protected $fillable = [
        'name',
        'name_ascii',
        'location',
        'ward_id',
        'district_id',
        'user_id_created',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function ward()
    {
        return $this->belongsTo(Ward::class)->select(['id', 'name', 'district_id']);
    }

    public function district()
    {
        return $this->belongsTo(District::class)->select(['id', 'name', 'province_id']);
    }
}
