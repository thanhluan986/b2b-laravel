<?php

namespace App\Models;

use App\Utils\Links;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class CoreUsers extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasRoles;

    protected $guard_name = 'backend';

    protected $primaryKey = 'id';

    public static $status_inactive = 0;
    public static $status_active = 1;
    public static $status_banned = 2;

    const STATUS_UNREGISTERED = 0;
    const STATUS_REGISTERED = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_BANNED = 3;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const ACCOUNT_POSITION_ADMIN = 1;

    const ACCOUNT_POSITION = [
        ['id' => self::ACCOUNT_POSITION_ADMIN, 'name' => 'Admin'],
    ];

    const ACCOUNT_TYPE_GOLD = 1,
        ACCOUNT_TYPE_SLIVER = 2;

    const ACCOUNT_TYPE = [
        ['id' => self::ACCOUNT_TYPE_GOLD, 'name' => 'Gold'],
        ['id' => self::ACCOUNT_TYPE_SLIVER, 'name' => 'Sliver'],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_core_users';
    protected $fillable = [
        'username',
        'fullname',
        'email',
        'phone',
        'password',
        'status',
        'fcm_token',
        'device_id',
        'device_os',
        'recommender',
        'gender',
        'birthday',
        'cardid',
        'avatar_file_path',
        'cover_file_path',
        'rating',
        'balance',
        'point',
        'address',
        'facebook_id',
        'google_id',
        'account_position',
        'account_type',
        'description',
        'chat_response_rate',
        'preparing_time',
        'cancel_order_rate',
        'total_products',
        'last_login',
        'following_count',
        'follower_count',
        'is_verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = array('avatar_src', 'cover_src');

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAvatarSrcAttribute()
    {
        $file_path = $this->avatar_file_path ? $this->avatar_file_path : 'common/avatar.png';
        return Links::ImageLink($file_path);
    }

    public function avatar_src()
    {
        $file_path = $this->avatar_file_path ? $this->avatar_file_path : 'common/avatar.png';
        return Links::ImageLink($file_path);
    }

    public function getCoverSrcAttribute()
    {
        $file_path = $this->cover_file_path ? $this->cover_file_path : 'common/cover.png';
        return Links::ImageLink($file_path);
    }

    public function cover_src()
    {
        $file_path = $this->cover_file_path ? $this->cover_file_path : 'common/cover.png';
        return Links::ImageLink($file_path);
    }

    public function get_by_where($params)
    {
        $params = array_merge(array(
            'fullname'         => null,
            'email'            => null,
            'phone'            => null,
            'status'           => null,
            'account_position' => null,
            'is_staff'         => false,
            'pagin_path'       => null,
        ), $params);

        \DB::enableQueryLog();

        $data = $this->orderBy('created_at', 'DESC');

        if (!empty($params['fullname'])) {
            $data->where('fullname', 'like', "%{$params['fullname']}%");
        }
        if (!empty($params['email'])) {
            $data->where('email', 'like', "%{$params['email']}%");
        }
        if (!empty($params['status'])) {
            $data->where('status', $params['status']);
        } else {
            $data->where('status', '<>', self::STATUS_UNREGISTERED);
        }
        if ($params['account_position']) {
            $data->where('account_position', $params['account_position']);
        }
        if ($params['phone']) {
            $data->where('phone', 'like', "%{$params['phone']}%");
        }
        if ($params['is_staff']) {
            $data->whereNotNull('account_position');
        } else {
            $data->whereNull('account_position');
        }

        $data = $data->paginate(config('constants.item_perpage'))->withPath($params['pagin_path']);

        return $data;
    }
}
