<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_store_category';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'parent_id',
        'status',
        'priority',
        'thumbnail_id',
        'type',
        'seo_title',
        'seo_descriptions',
        'seo_keywords',
        'user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    const STATUS = [
        ['id' => 0, 'name' => 'Ẩn'],
        ['id' => 1, 'name' => 'Hiện'],
    ];

    public static function get_validation_admin()
    {
        return [
            'name'             => 'required|string',
            'slug'             => 'nullable|string',
            'description'      => 'nullable|string',
            'parent_id'        => 'nullable|integer',
            'status'           => 'nullable|in:0,1',
            'priority'         => 'nullable|integer',
            'seo_title'        => 'nullable|string',
            'seo_descriptions' => 'nullable|string',
            'seo_keywords'     => 'nullable|string',
            'user_id'          => 'nullable|integer',
        ];
    }
}
