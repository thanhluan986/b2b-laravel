<?php

namespace App\Models;

use App\Models\Location\District;
use App\Models\Location\Province;
use App\Models\Location\Street;
use App\Models\Location\Ward;
use App\Utils\File;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Product extends BaseModel implements Jsonable
{
    use SoftDeletes;

    protected $table = 'lck_product';
    protected $fillable = [
        'name',
        'description',
        'detail',
        'price',
        'status',
        'thumbnail_file_id',
        'user_id',
        'category_id',
        'store_category_id',
        'trademark_id',
        'sku',
        'inventory',
        'length',
        'width',
        'height',
        'weight',
        'shipping_cost',
        'origin',
        'factory_address',
        'rating',
        'is_used_product',
        'must_pre_order',
        'pre_order_time',
        'created_at',
        'updated_at',
    ];

    protected $hidden = ['pivot', 'deleted_at'];

    const STATUS_ACTIVE = 1;
    const STATUS_IN_ACTIVE = 0;

    const STATUS = [
        ['id' => 1, 'name' => 'Hiển thị'],
        ['id' => 0, 'name' => 'Ẩn'],
    ];

    public static function get_by_where($params = [])
    {
        $params = array_merge(
            [
                'id'                 => null,
                'user_id'            => null,
                'store_id'           => null,
                'category_ids'       => null,
                'store_category_ids' => null,
                'trademark_ids'      => null,
                'price_from'         => null,
                'price_to'           => null,
                'date_from'          => null,
                'date_to'            => null,
                'skip_product_ids'   => null,
                'keywords'           => null,
                'fullname'           => null,
                'status'             => self::STATUS_ACTIVE,
                'multi_status'       => null,

                'user_id_created'  => null,
                'get_favorite'     => false,
                'user_id_favorite' => null,

                'get_history'     => false,
                'user_id_history' => null,

                'pagin'      => true,
                'pagin_path' => null,
                'limit'      => 500,

                'order_by'           => null,
                'order_by_direction' => 'ASC',
                'sort'               => null,
                'type'               => null,
            ],
            $params
        );

        \DB::enableQueryLog();

        $products = self::select([
            'id',
            'name',
            'description',
            'price',
            'price_old',
            'status',
            'thumbnail_file_id',
            'user_id',
            'category_id',
            'sku',
            'inventory',
            'rating',
            'created_at',
        ])
            ->with(['thumbnail']);

        if (in_array($params['type'], ['top-rate-store', 'top-popular-store'])) {
            $products->with(['store']);
        }

        //->with(['thumbnail', 'category', 'store_category', 'images']);

        if ($params['id'])
            $products->where('id', $params['id']);

        if ($params['category_ids']) {
            $cate_ids = !is_array($params['category_ids']) ? explode(',', $params['category_ids']) : $params['category_ids'];
            $products->whereIn('category_id', $cate_ids);
//            $products->whereHas('categories', function ($query) use ($cate_ids) {
//                $query->whereIn('lck_product_category.id', $cate_ids);
//            })->get();
        }

        if ($params['store_category_ids']) {
            $cate_ids = !is_array($params['store_category_ids']) ? explode(',', $params['store_category_ids']) : $params['store_category_ids'];
            $products->whereIn('store_category_id', $cate_ids);
//            $products->whereHas('store_categories', function ($query) use ($cate_ids) {
//                $query->whereIn('lck_store_product_category.id', $cate_ids);
//            })->get();
        }
        if ($params['trademark_ids']) {
            $ids = !is_array($params['trademark_ids']) ? explode(',', $params['trademark_ids']) : $params['trademark_ids'];
            $products->whereIn('trademark_id', $ids);
        }

        if ($params['store_id'])
            $products->where('user_id', $params['store_id']);

        if ($params['user_id'])
            $products->where('user_id', $params['user_id']);

        if ($params['date_from'])
            $products->whereRaw('DATE(created_at) >="' . date('Y-m-d', strtotime($params['date_from'])) . '"');

        if ($params['date_to'])
            $products->whereRaw('DATE(created_at) <="' . date('Y-m-d', strtotime($params['date_to'])) . '"');

        if ($params['keywords'])
            $products->where('name', 'like', "%{$params['keywords']}%");

        if ($params['status'])
            $products->where('status', $params['status']);

        if ($params['multi_status'])
            $products->whereIn('status', $params['multi_status']);

        if ($params['price_from'])
            $products->where('price', '>=', $params['price_from']);

        if ($params['price_to'])
            $products->where('price', '<=', $params['price_to']);

        if ($params['skip_product_ids']) {
            $skip_product_ids = !is_array($params['skip_product_ids']) ? explode(',', $params['skip_product_ids']) : $params['skip_product_ids'];
            $products->whereNotIn('id', $skip_product_ids);
        }

        if ($params['user_id'])
            $products->where('user_id', $params['user_id']);

        if ($params['get_favorite'] && $params['user_id_favorite']) {
            $products->whereRaw('id IN (select product_id from lck_favorite where user_id = ? )', $params['user_id_favorite']);
        }

        if ($params['get_history'] && $params['user_id_history']) {
            $products->whereRaw('id IN (select product_id from lck_history_view where user_id = ? )', $params['user_id_history']);
        }

        if ($params['fullname']) {
            $products->whereRaw("user_id IN (select id from lck_core_users where (fullname LIKE '%{$params['fullname']}%' OR phone LIKE '%{$params['fullname']}%'))");
        }

        $aSort = [
            'recently_changed' => [
                'column'    => 'updated_at',
                'direction' => 'DESC'
            ],
            'newest'           => [
                'column'    => 'created_at',
                'direction' => 'DESC'
            ],
            'oldest'           => [
                'column'    => 'created_at',
                'direction' => 'ASC'
            ],
            'price_low_high'   => [
                'column'    => 'price',
                'direction' => 'ASC'
            ],
            'price_high_low'   => [
                'column'    => 'price',
                'direction' => 'DESC'
            ],
            'area_low_high'    => [
                'column'    => 'area',
                'direction' => 'ASC'
            ],
            'area_high_low'    => [
                'column'    => 'area',
                'direction' => 'DESC'
            ],
            'status'           => [
                'column'    => 'status',
                'direction' => 'ASC'
            ],
            'id_low_high'      => [
                'column'    => 'id',
                'direction' => 'ASC'
            ],
            'id_high_low'      => [
                'column'    => 'id',
                'direction' => 'DESC'
            ],
        ];

        if (isset($aSort[$params['sort']])) {
            $products->orderBy($aSort[$params['sort']]['column'], $aSort[$params['sort']]['direction']);
        } else if ($params['order_by']) {
            $products->orderBy($params['order_by'], $params['order_by_direction']);
        }

        if ($params['pagin']) {
            $products = $products->paginate($params['limit'])
                ->withPath($params['pagin_path']);
        } else {
            if ($params['limit'])
                $products = $products->limit($params['limit']);

            $products = $products->get();
        }

        return $products;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id')->select(['id', 'name']);
    }

    public function store_category()
    {
        return $this->belongsTo(StoreCategory::class, 'store_category_id', 'id')->select(['id', 'name']);
    }

    public function owner()
    {
        return $this->belongsTo(CoreUsers::class, 'user_id', 'id')->select(['id', 'fullname', 'username', 'email', 'phone']);
    }

    public function store()
    {
        return $this->belongsTo(CoreUsers::class, 'user_id', 'id')->select(['id', 'fullname', 'username', 'email', 'phone', 'avatar_file_path']);
    }

    public function thumbnail()
    {
        return $this->belongsTo(Files::class, 'thumbnail_file_id', 'id')->select(['id', 'file_path']);
    }

    public function images()
    {
        return $this->belongsToMany(Files::class, 'lck_product_images', 'product_id', 'file_id')
            ->select(['lck_files.id', 'lck_files.file_path'])->orderBy('priority');
    }

    public function images_extra()
    {
        return $this->belongsToMany(Files::class, 'lck_product_images_extra', 'product_id', 'file_id')
            ->select(['lck_files.id', 'lck_files.file_path'])->orderBy('priority');
    }

    public function wholesale()
    {
        return $this->hasMany(ProductWholesales::class, 'product_id', 'id')
            ->select([
                'product_id',
                'quantity_from',
                'quantity_to',
                'price',
            ]);
    }

    function status_name()
    {
        foreach (self::STATUS as $v)
            if ($this->status == $v['id'])
                return $v['name'];
    }

    public static function get_validation_admin()
    {
        return [
            'name'                    => 'nullable|string',
            'description'             => 'nullable|string',
            'province_id'             => 'required|bail|integer|exists:lck_location_province,id',
            'district_id'             => 'required|bail|integer|exists:lck_location_district,id',
            'ward_id'                 => 'required|bail|integer|exists:lck_location_ward,id',
            'street_id'               => 'required|bail|integer|exists:lck_location_street,id',
            'project_id'              => 'nullable|bail|integer|exists:lck_project,id',
            'home_number'             => 'required|string',
            'address'                 => 'nullable|string',
            'location_lat'            => 'nullable|string',
            'location_long'           => 'nullable|string',
            'horizontal'              => 'required|numeric|min:0',
            'vertical'                => 'required|numeric|min:0',
            'area'                    => 'nullable|numeric|min:0',
            'land_area'               => 'nullable|numeric|min:0',
            'floor_area'              => 'nullable|numeric|min:0',
            'floor_number'            => 'nullable|numeric|min:0',
            'home_structure'          => 'nullable|integer',
            'home_mold'               => 'nullable|integer',
            'home_quality'            => 'nullable|integer|min:0|max:100',
            'price'                   => 'required|numeric|min:0',
            'product_type_id'         => 'required|integer',
            'status'                  => 'nullable|in:1,2,3,4,5,6,7,8',
            'frontage_type'           => 'nullable|integer',
            'frontage_area'           => 'nullable|integer',
            'legal_status'            => 'nullable|integer',
            'home_direction'          => 'nullable|integer',
            'basement_number'         => 'nullable|integer|min:0',
            'mezzanine_number'        => 'nullable|integer|min:0',
            'purpose_ids'             => 'nullable|array',
            'year_built_start'        => 'nullable|integer|max:2050',
            'year_built_finish'       => 'nullable|integer|max:2050',
            'price_per_square_meters' => 'nullable|numeric|min:0',
            'monthly_price'           => 'nullable|string',
            'bedroom_number'          => 'nullable|integer|min:0',
            'room_number'             => 'nullable|integer|min:0',
            'bathroom_number'         => 'nullable|integer|min:0',
            'kitchen_number'          => 'nullable|integer|min:0',
            'balcony_number'          => 'nullable|integer|min:0',
            'elevator'                => 'nullable|integer|in:0,1',
            'terrace'                 => 'nullable|integer|in:0,1',
            'convenient_ids'          => 'nullable|array',
            'exterior_ids'            => 'nullable|array',
            'contact_type'            => 'nullable|integer',
            'contact_name'            => 'nullable|string|max:200',
            'contact_phone'           => 'nullable|string|max:30',
            'contact_phone1'          => 'nullable|string|max:30',
            'contact_phone2'          => 'nullable|string|max:30',
            'contact_address'         => 'nullable|string|max:255',
            'image_ids'               => 'nullable|array',
            'image_extra_ids'         => 'nullable|array',
            'note'                    => 'nullable|string|max:255',
            'price_type'              => 'required|in:1,2',
            'status_local'            => 'nullable|in:1,2,3,4,5,6,7,8',
            'status_censorship'       => 'required|in:1,2,3',
            'frontage_type_ids'       => 'nullable|array',
            'home_facade'             => 'nullable|integer',
            'road_width'              => 'nullable|numeric|min:0',
            'callback_at'             => 'nullable|date_format:Y-m-d H:i:s',
        ];
    }
}
