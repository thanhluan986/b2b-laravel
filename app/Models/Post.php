<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_post';

    const STATUS_HIDE = 0;
    const STATUS_SHOW = 1;

    protected $fillable = [
        'name',
        'slug',
        'excerpt',
        'detail',
        'image_file_path',
        'image_file_id',
        'image_extra_file_path',
        'image_extra_file_id',
        'image_fb_file_path',
        'image_fb_file_id',
        'user_id',
        'views',
        'status',
        'video_url',
        'custom_link',
        'tags',
        'sticky',
        'seo_title',
        'seo_descriptions',
        'seo_keywords',
        'category_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    const STATUS = [
        ['id' => 0, 'name' => 'Ẩn'],
        ['id' => 1, 'name' => 'Hiện'],
    ];

    public function category()
    {
        return $this->belongsTo(PostCategory::class, 'category_id', 'id')
            ->select(['id', 'name']);
    }

    public function image()
    {
        return $this->belongsTo(Files::class, 'image_file_id', 'id')->select(['id', 'file_path']);
    }

    public function image_extra()
    {
        return $this->belongsTo(Files::class, 'image_extra_file_id', 'id')->select(['id', 'file_path']);
    }

    public function image_fb()
    {
        return $this->belongsTo(Files::class, 'image_fb_file_id', 'id')->select(['id', 'file_path']);
    }

    public static function get_by_where($params = [])
    {
        $params = array_merge(
            [
                'id'           => null,
                'skip_ids'     => null,
                'keyword'      => null,
                'category_ids' => null,
                'status'       => null,
                'multi_status' => null,
                'pagin_path'   => null,
                'limit'        => 10,
                'pagin'        => true,

                'order_by'           => null,
                'order_by_direction' => 'ASC',
                'sort'               => null,
            ],
            $params
        );

        $data = self::select([
            'id',
            'name',
            'slug',
            'excerpt',
            'image_file_id',
            'views',
            'category_id',
            'created_at',
        ])
            ->with(['image', 'category']);

        if ($params['id'])
            $data->where('id', $params['id']);

        if ($params['skip_ids'])
            $data->whereNotIn('id', $params['skip_ids']);

        if ($params['keyword'])
            $data->where('name', 'like', "%{$params['keyword']}%");

        if ($params['category_ids'])
            $data->whereIn('category_id', $params['category_ids']);

        if ($params['status'])
            $data->where('status', $params['status']);

        $aSort = [
            'recently_changed' => [
                'column'    => 'updated_at',
                'direction' => 'DESC'
            ],
            'newest'           => [
                'column'    => 'created_at',
                'direction' => 'DESC'
            ],
            'oldest'           => [
                'column'    => 'created_at',
                'direction' => 'ASC'
            ],
            'top-view'         => [
                'column'    => 'views',
                'direction' => 'desc'
            ],
        ];

        if (isset($aSort[$params['sort']])) {
            $data->orderBy($aSort[$params['sort']]['column'], $aSort[$params['sort']]['direction']);
        } else if ($params['order_by']) {
            $data->orderBy($params['order_by'], $params['order_by_direction']);
        } else {
            $data->orderBy('created_at', 'DESC');
        }

        if ($params['pagin']) {
            $data = $data->paginate($params['limit'])
                ->withPath($params['pagin_path']);
        } else {
            if ($params['limit'])
                $data = $data->limit($params['limit']);

            $data = $data->get();
        }

        return $data;
    }

    public static function get_validation_admin()
    {
        return [
            'name'                => 'required|string',
            'slug'                => 'nullable|string',
            'excerpt'             => 'nullable|string',
            'detail'              => 'nullable|string',
            'category_id'         => 'nullable|integer',
            'status'              => 'nullable|in:0,1',
            'seo_title'           => 'nullable|string',
            'seo_descriptions'    => 'nullable|string',
            'seo_keywords'        => 'nullable|string',
            'user_id'             => 'nullable|integer',
            'image_file_id'       => 'nullable|integer',
            'image_extra_file_id' => 'nullable|integer',
            'image_fb_file_id'    => 'nullable|integer',
        ];
    }
}
