<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    protected static $tbl_lck_basket = 'lck_basket';
    protected static $tbl_lck_category = 'lck_category';
    protected static $tbl_lck_core_users = 'lck_core_users';
    protected static $tbl_lck_favorite = 'lck_favorite';
    protected static $tbl_lck_files = 'lck_files';
    protected static $tbl_lck_history_view = 'lck_history_view';

    protected static $tbl_lck_location_district = 'lck_location_district';
    protected static $tbl_lck_location_province = 'lck_location_province';
    protected static $tbl_lck_location_street = 'lck_location_street';
    protected static $tbl_lck_location_ward = 'lck_location_ward';

    protected static $tbl_lck_notification = 'lck_notification';
    protected static $tbl_lck_notification_read = 'lck_notification_read';

    protected static $tbl_lck_post = 'lck_post';
    protected static $tbl_lck_post_category = 'lck_post_category';

    protected static $tbl_lck_product = 'lck_product';
    protected static $tbl_lck_product_images = 'lck_product_images';
    protected static $tbl_lck_product_images_extra = 'lck_product_images_extra';

    protected static $tbl_lck_variations = 'lck_variations';
    protected static $tbl_lck_variation_values = 'lck_variation_values';
    protected static $tbl_lck_product_variation = 'lck_product_variation';
    protected static $tbl_lck_variation_combine = 'lck_variation_combine';

    protected static $tbl_lck_store_category = 'lck_store_category';


}