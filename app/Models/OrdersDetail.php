<?php

namespace App\Models;

use App\Utils\Links;

class OrdersDetail extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_orders_detail';
    public $timestamps = false;
    protected $fillable = [
        'order_id',
        'product_id',
        'product_name',
        'thumbnail_file_path',
        'category_name',
        'variation_name',
        'price_old',
        'price',
        'discount_amount',
        'quantity',
        'sku',
        'weight',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $appends = array('thumbnail_src');

    public static function get_by_where($params)
    {
        $params = array_merge([
            'order_id'   => null,
            'pagin_path' => null,
            'limit'      => config('constants.item_perpage')
        ], $params);

        $data = self::select(\DB::raw("*"));

        $data->orderBy('created_at', 'DESC');

        if ($params['order_id'])
            $data->where('order_id', $params['order_id']);

        $data = $data->paginate($params['limit'])->withPath($params['pagin_path']);

        return $data;
    }

    public function getThumbnailSrcAttribute()
    {
        return Links::ImageLink($this->thumbnail_file_path);
    }
}