<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const STATUS = [
        ['id' => 0, 'name' => 'Không hoạt động'],
        ['id' => 1, 'name' => 'Hoạt động'],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_payment_method';

    protected $fillable = [
        'code',
        'name',
        'icon',
        'api_key',
        'api_url',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
