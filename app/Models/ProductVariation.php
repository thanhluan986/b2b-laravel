<?php

namespace App\Models;

class ProductVariation extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_product_variation';
    protected $fillable = [
        'product_id',
        'name',
        'price',
        'sku',
        'inventory',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public static function get_product_variations($product_id)
    {
        $data = self::select(\DB::raw('id as product_variation_id, price, sku, inventory'))
            ->where('product_id', $product_id)
            ->get();

        return $data;
    }

    public static function get_product_variations_with_combine($product_id)
    {
        $data = self::select(\DB::raw('id as product_variation_id, name, price, sku, inventory, vc.variation_value_id'))
            ->from(self::$tbl_lck_product_variation . ' as pv')
            ->join(self::$tbl_lck_variation_combine . ' as vc', 'pv.id', '=', 'vc.product_variation_id')
            ->where('product_id', $product_id)
            ->get();

        $return = [];

        if (count($data)) {
            foreach ($data as $item) {
                if (!isset($return[$item['product_variation_id']])) {
                    $return[$item['product_variation_id']] = [
                        'product_variation_id' => $item['product_variation_id'],
                        'name'                 => $item['name'],
                        'price'                => $item['price'],
                        'sku'                  => $item['sku'],
                        'inventory'            => $item['inventory'],
                    ];
                }
                $return[$item['product_variation_id']]['list_variation_value_id_combine'][] = $item['variation_value_id'];
            }
            $return = array_values($return);
        }

        return $return;
    }
}
