<?php

namespace App\Models;

class Orders extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lck_orders';
    protected $fillable = [
        'order_code',
        'total_product_price',
        'discount_amount',
        'discount_percent',
        'discount_code',
        'shipping_fee',
        'free_shipping',
        'payment_method',
        'status_payment',
        'shipping_method',
        'shipping_code',
        'total_weight',
        'shipping_name',
        'shipping_phone',
        'shipping_province_id',
        'shipping_district_id',
        'shipping_ward_id',
        'shipping_street_name',
        'shipping_address',
        'warehouse_name',
        'warehouse_phone',
        'warehouse_province_id',
        'warehouse_district_id',
        'warehouse_ward_id',
        'warehouse_street_name',
        'warehouse_address',
        'status',
        'total_price',
        'cancel_reason',
        'note',
        'user_id',
        'store_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public static function get_by_where($params)
    {
        $params = array_merge([
            'user_id'    => null,
            'store_id'   => null,
            'pagin_path' => null,
            'limit'      => config('constants.item_perpage')
        ], $params);

        $data = self::select(\DB::raw("*"))->with(['store', 'orders_detail']);

        $data->orderBy('created_at', 'DESC');

        if ($params['user_id'])
            $data->where('user_id', $params['user_id']);

        if ($params['store_id'])
            $data->where('store_id', $params['store_id']);

        $data = $data->paginate($params['limit'])->withPath($params['pagin_path']);

        return $data;
    }

    public function store()
    {
        return $this->belongsTo(CoreUsers::class, 'store_id', 'id')->select(['id', 'fullname', 'username',]);
    }

    public function orders_detail()
    {
        return $this->hasMany(OrdersDetail::class, 'order_id', 'id')
            ->select([
                'order_id',
                'product_id',
                'product_name',
                'thumbnail_file_path',
                'category_name',
                'variation_name',
                'price_old',
                'price',
                'discount_amount',
                'quantity',
            ]);
    }
}